// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './views/theme/base/base.component';
import { ErrorPageComponent } from './views/theme/content/error-page/error-page.component';
// Auth
import { AuthGuard } from './core/auth';
import {SpbCreateComponent} from './views/pages/spb/spb-create/spb-create.component';
import {SpbHourlyListComponent} from './views/pages/spb-hourly/spb-hourly-list/spb-hourly-list.component';
import {ManifestSenderComponent} from './views/pages/manifest/manifest-sender/manifest-sender.component';
import {SpbListComponent} from './views/pages/spb/spb-list/spb-list.component';
import {SpbListReceiverComponent} from './views/pages/spb/spb-list-receiver/spb-list-receiver.component';
import {ManifestListReceiverComponent} from './views/pages/manifest/manifest-list-receiver/manifest-list-receiver.component';
import {GoodsMonitoringComponent} from './views/pages/goods-monitoring/goods-monitoring.component';
import {ManifestCarrierComponent} from './views/pages/manifest-carrier/manifest-carrier.component';
import {SpbStatisticInputComponent} from './views/pages/statistik/spb-statistic-input/spb-statistic-input.component';
import {ReadManifestCarrierComponent} from './views/pages/manifest-carrier/read-manifest-carrier/read-manifest-carrier.component';
import {SpbHourlyReceiverListComponent} from './views/pages/spb-hourly-receiver/spb-hourly-receiver-list/spb-hourly-receiver-list.component';
import {GoodsMonitoringReceiverListComponent} from './views/pages/goods-monitoring-receiver/goods-monitoring-receiver-list/goods-monitoring-receiver-list.component';
import {PrivilegeMasterListComponent} from './views/pages/privilege-master/privilege-master-list/privilege-master-list.component';
import {UpdatePasswordComponent} from './views/pages/privilege-user/update-password/update-password.component';
import {RatesComponent} from './views/pages/rates/rates.component';
import {MasterDataCustomerComponent} from './views/pages/master-data-customer/master-data-customer/master-data-customer.component';
import { MasterDataCustomerDetailComponent } from './views/pages/master-data-customer/master-data-customer/master-data-customer-detail/master-data-customer-detail/master-data-customer-detail.component';
import { MasterDataCustomerSenderComponent } from './views/pages/master-data-customer/master-data-customer/master-data-customer-sender/master-data-customer-sender.component';
import { MasterDataCustomerReceiverComponent } from './views/pages/master-data-customer/master-data-customer/master-data-customer-receiver/master-data-customer-receiver.component';
import { PrivilegeUserListComponent } from './views/pages/configuration/privilege-user/privilege-user-list/privilege-user-list.component';
import { PrivilegeListComponent } from './views/pages/configuration/privilege-user/privilege-list/privilege-list.component';
import { UserListComponent } from './views/pages/configuration/privilege-user/user-list/user-list.component';
import { PrivilegeSettingComponent } from './views/pages/configuration/privilege-user/privilege-list/privilege-setting/privilege-setting.component';
import { DetailPrivilegeSettingComponent } from './views/pages/configuration/privilege-user/privilege-list/detail-privilege-setting/detail-privilege-setting.component';
import { RoleUserListComponent } from './views/pages/configuration/privilege-user/privilege-list/role-user-list/role-user-list.component';
import { DetailRoleListComponent } from './views/pages/configuration/privilege-user/privilege-list/detail-role-list/detail-role-list.component';
import { RoleUserSettingComponent } from './views/pages/configuration/privilege-user/privilege-list/role-user-setting/role-user-setting.component';
import { AddRoleCityComponent } from './views/pages/configuration/privilege-user/privilege-list/add-role-city/add-role-city.component';
import { DeleteRoleCityComponent } from './views/pages/configuration/privilege-user/privilege-list/delete-role-city/delete-role-city.component';
import { PrivilegeUpdateSettingComponent } from './views/pages/configuration/privilege-user/privilege-list/privilege-update-setting/privilege-update-setting.component';
import { AddUserComponent } from './views/pages/configuration/privilege-user/user-list/add-user/add-user.component';
import { UpdateUnitUserComponent } from './views/pages/configuration/privilege-user/user-list/update-unit-user/update-unit-user.component';
import { UpdateRolePrivilegeComponent } from './views/pages/configuration/privilege-user/user-list/update-role-privilege/update-role-privilege.component';
import { PrivilegeCreateComponent } from './views/pages/configuration/privilege-user/privilege-list/privilege-create/privilege-create.component';
import { RoleCreateComponent } from './views/pages/configuration/privilege-user/role-list/role-create/role-create.component';
import { UnitPlaceCreateComponent } from './views/pages/configuration/privilege-user/user-list/unit-place-create/unit-place-create.component';
import { UnitSubbranchPlaceCreateComponent } from './views/pages/configuration/privilege-user/user-list/unit-subbranch-place-create/unit-subbranch-place-create.component';
import { UpdateUnitSubbranchUserComponent } from './views/pages/configuration/privilege-user/user-list/update-unit-subbranch-user/update-unit-subbranch-user.component';
import { UpdateUnitCompanyComponent } from './views/pages/configuration/privilege-user/user-list/update-unit-company/update-unit-company.component';
import { UpdateUnitBranchComponent } from './views/pages/configuration/privilege-user/user-list/update-unit-branch/update-unit-branch.component';
import { UpdateUnitSubbranchComponent } from './views/pages/configuration/privilege-user/user-list/update-unit-subbranch/update-unit-subbranch.component';
import { UserRoleListComponent } from './views/pages/configuration/privilege-user/role-list/user-role-list/user-role-list.component';
import { UserDetailComponent } from './views/pages/configuration/privilege-user/user-list/user-detail/user-detail.component';
import { UserSettingComponent } from './views/pages/configuration/privilege-user/user-list/user-setting/user-setting.component';
import {TransitListComponent} from './views/pages/manifest-carrier-master/master-transit/transit-list/transit-list.component';
import { TransitUpdateComponent } from './views/pages/manifest-carrier-master/master-transit/transit-update/transit-update.component';
import { DetailManifestCarrierComponent } from './views/pages/manifest-carrier/detail-manifest-carrier/detail-manifest-carrier.component';
import { RevisiManifestCarrierComponent } from './views/pages/manifest-carrier/revisi-manifest-carrier/revisi-manifest-carrier.component';
import { ListRevisiManifestCarrierComponent } from './views/pages/manifest-carrier/list-revisi-manifest-carrier/list-revisi-manifest-carrier.component';
import { DetailCostManifestCarrierComponent } from './views/pages/manifest-carrier/detail-cost-manifest-carrier/detail-cost-manifest-carrier.component';
import {HistoryAksiComponent} from './views/pages/HistoryAksi/history-aksi/history-aksi.component';
import {HistoryPrintComponent} from './views/pages/history-print/history-print/history-print.component';
import {HistoryLoginComponent} from './views/pages/history-login/history-login/history-login.component';
import { SuperVisionComponent } from './views/pages/super-vision/super-vision.component';
import { SuperVisionListComponent } from './views/pages/super-vision/super-vision-list/super-vision-list.component';
import { SuperVisionHistoryComponent } from './views/pages/super-vision/super-vision-history/super-vision-history.component';
import { SuperVisionCreateComponent } from './views/pages/super-vision/super-vision-create/super-vision-create.component';
import { SuperVisionApprovalReprintComponent } from './views/pages/super-vision/super-vision-approval/super-vision-approval-reprint/super-vision-approval-reprint.component';
import { SuperVisionApprovalSpbComponent } from './views/pages/super-vision/super-vision-approval/super-vision-approval-spb/super-vision-approval-spb.component';
import { SuperVisionApprovalVoidComponent } from './views/pages/super-vision/super-vision-approval/super-vision-approval-void/super-vision-approval-void.component';
import { SuperVisionApprovalFocComponent } from './views/pages/super-vision/super-vision-approval/super-vision-approval-foc/super-vision-approval-foc.component';
import { SuperVisionApprovalKiloComponent } from './views/pages/super-vision/super-vision-approval/super-vision-approval-kilo/super-vision-approval-kilo.component';
import { SuperVisionCreateReprintComponent } from './views/pages/super-vision/super-vision-create/super-vision-create-reprint/super-vision-create-reprint.component';
import { SuperVisionCreateSpbComponent } from './views/pages/super-vision/super-vision-create/super-vision-create-spb/super-vision-create-spb.component';
import { SuperVisionCreateVoidComponent } from './views/pages/super-vision/super-vision-create/super-vision-create-void/super-vision-create-void.component';
import { SuperVisionCreateFocComponent } from './views/pages/super-vision/super-vision-create/super-vision-create-foc/super-vision-create-foc.component';
import { SuperVisionCreateKiloComponent } from './views/pages/super-vision/super-vision-create/super-vision-create-kilo/super-vision-create-kilo.component';
import { MasterAgreementComponent } from './views/pages/master-agreement/master-agreement.component';
import { MasterAgreementDetailComponent } from './views/pages/master-agreement/master-agreement-detail/master-agreement-detail.component';
import { ManifestCarrierCostCarrierComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-carrier/manifest-carrier-cost-carrier.component';
import { ManifestCarrierCostDestinationComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-destination/manifest-carrier-cost-destination.component';
import { ManifestCarrierCostGeraiComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-gerai/manifest-carrier-cost-gerai.component';
import { ManifestCarrierCostInlandOriginComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-inland-origin/manifest-carrier-cost-inland-origin.component';
import { ManifestCarrierCostOriginComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-origin/manifest-carrier-cost-origin.component';
import { ManifestCarrierCostPenerusComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-penerus/manifest-carrier-cost-penerus.component';
import { ManifestCarrierCostTransitComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-transit/manifest-carrier-cost-transit.component';
import { ManifestCarrierCostCarrierCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-carrier/manifest-carrier-cost-carrier-create/manifest-carrier-cost-carrier-create.component';
import { ManifestCarrierCostCarrierUpdateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-carrier/manifest-carrier-cost-carrier-update/manifest-carrier-cost-carrier-update.component';
import { ManifestCarrierCostCarrierDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-carrier/manifest-carrier-cost-carrier-delete/manifest-carrier-cost-carrier-delete.component';
import { ManifestCarrierCostDestinationCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-destination/manifest-carrier-cost-destination-create/manifest-carrier-cost-destination-create.component';
import { ManifestCarrierCostGeraiCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-gerai/manifest-carrier-cost-gerai-create/manifest-carrier-cost-gerai-create.component';
import { ManifestCarrierCostInlandOriginCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-inland-origin/manifest-carrier-cost-inland-origin-create/manifest-carrier-cost-inland-origin-create.component';
import { ManifestCarrierCostOriginCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-origin/manifest-carrier-cost-origin-create/manifest-carrier-cost-origin-create.component';
import { ManifestCarrierCostPenerusCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-penerus/manifest-carrier-cost-penerus-create/manifest-carrier-cost-penerus-create.component';
import { ManifestCarrierCostPenerusUpdateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-penerus/manifest-carrier-cost-penerus-update/manifest-carrier-cost-penerus-update.component';
import { ManifestCarrierCostPenerusDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-penerus/manifest-carrier-cost-penerus-delete/manifest-carrier-cost-penerus-delete.component';
import { ManifestCarrierCostTransitCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-transit/manifest-carrier-cost-transit-create/manifest-carrier-cost-transit-create.component';
import { ManifestCarrierCostTransitUpdateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-transit/manifest-carrier-cost-transit-update/manifest-carrier-cost-transit-update.component';
import { ManifestCarrierCostTransitDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-transit/manifest-carrier-cost-transit-delete/manifest-carrier-cost-transit-delete.component';
import { ManifestCarrierCostDestinationUpdateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-destination/manifest-carrier-cost-destination-update/manifest-carrier-cost-destination-update.component';
import { ManifestCarrierCostDestinationDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-destination/manifest-carrier-cost-destination-delete/manifest-carrier-cost-destination-delete.component';
import { ManifestCarrierCostGeraiUpdateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-gerai/manifest-carrier-cost-gerai-update/manifest-carrier-cost-gerai-update.component';
import { ManifestCarrierCostGeraiDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-gerai/manifest-carrier-cost-gerai-delete/manifest-carrier-cost-gerai-delete.component';
import { ManifestCarrierCostInlandOriginUpdateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-inland-origin/manifest-carrier-cost-inland-origin-update/manifest-carrier-cost-inland-origin-update.component';
import { ManifestCarrierCostInlandOriginDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-inland-origin/manifest-carrier-cost-inland-origin-delete/manifest-carrier-cost-inland-origin-delete.component';
import { ManifestCarrierCostOriginUpdateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-origin/manifest-carrier-cost-origin-update/manifest-carrier-cost-origin-update.component';
import { ManifestCarrierCostOriginDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-cost-origin/manifest-carrier-cost-origin-delete/manifest-carrier-cost-origin-delete.component';
import { MasterMethodPaymentComponent } from './views/pages/master-method-payment/master-method-payment.component';
import { MasterMethodPaymentCreateComponent } from './views/pages/master-method-payment/master-method-payment-create/master-method-payment-create.component';
import { MasterMethodPaymentDeleteComponent } from './views/pages/master-method-payment/master-method-payment-delete/master-method-payment-delete.component';
import { MasterModaCityComponent } from './views/pages/master-moda-city/master-moda-city.component';
import { MasterModaCityCreateComponent } from './views/pages/master-moda-city/master-moda-city-create/master-moda-city-create.component';
import { MasterModaCityDeleteComponent } from './views/pages/master-moda-city/master-moda-city-delete/master-moda-city-delete.component';
import { LastManifestComponent } from './views/pages/manifest-carrier/last-manifest/last-manifest.component';
import { MasterCityComponent } from './views/pages/master-city/master-city/master-city.component';
import { MasterCityCreateComponent } from './views/pages/master-city/master-city-create/master-city-create.component';
import { MasterCityDeleteComponent } from './views/pages/master-city/master-city-delete/master-city-delete.component';
import { FlightRouteComponent } from './views/pages/manifest-carrier-master/flight-route/flight-route.component';
import { MappingAreaNoExistsComponent } from './views/pages/mapping-area-no-exists/mapping-area-no-exists.component';
import { MappingAreaNoExistsCreateComponent } from './views/pages/mapping-area-no-exists/mapping-area-no-exists-create/mapping-area-no-exists-create.component';
import { MappingAreaNoExistsDeleteComponent } from './views/pages/mapping-area-no-exists/mapping-area-no-exists-delete/mapping-area-no-exists-delete.component';
import { MasterStatistikComponent } from './views/pages/statistik/master-statistik/master-statistik.component';
import { MonitoringCapacityComponent } from './views/pages/monitoring-capacity/monitoring-capacity.component';
import { ManifestCarrierMasterVendorComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor/manifest-carrier-master-vendor.component';
import { ManifestCarrierMasterVendorTransitComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-transit/manifest-carrier-master-vendor-transit.component';
import { ManifestCarrierMasterVendorCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor/manifest-carrier-master-vendor-create/manifest-carrier-master-vendor-create.component';
import { ManifestCarrierMasterVendorDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor/manifest-carrier-master-vendor-delete/manifest-carrier-master-vendor-delete.component';
import { MCMasterVendorTransitCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-transit/mc-master-vendor-transit-create/mc-master-vendor-transit-create.component';
import { MCMasterVendorTransitDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-transit/mc-master-vendor-transit-delete/mc-master-vendor-transit-delete.component';
import { ManifestCarrierMasterVendorOriginComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-origin/manifest-carrier-master-vendor-origin.component';
import { ManifestCarrierMasterVendorOriginCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-origin/manifest-carrier-master-vendor-origin-create/manifest-carrier-master-vendor-origin-create.component';
import { ManifestCarrierMasterVendorOriginDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-origin/manifest-carrier-master-vendor-origin-delete/manifest-carrier-master-vendor-origin-delete.component';
import { ManifestCarrierMasterVendorCarrierComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-carrier/manifest-carrier-master-vendor-carrier.component';
import { MCMasterVendorCarrierCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-carrier/mc-master-vendor-carrier-create/mc-master-vendor-carrier-create.component';
import { MCMasterVendorCarrierDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-carrier/mc-master-vendor-carrier-delete/mc-master-vendor-carrier-delete.component';
import { ManifestCarrierMasterCarrierComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-carrier/manifest-carrier-master-carrier.component';
import { ManifestCarrierMasterCarrierCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-carrier/manifest-carrier-master-carrier-create/manifest-carrier-master-carrier-create.component';
import { ManifestCarrierMasterCarrierDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-carrier/manifest-carrier-master-carrier-delete/manifest-carrier-master-carrier-delete.component';
import { ManifestCarrierMasterFlightComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-flight/manifest-carrier-master-flight.component';
import { ManifestCarrierMasterFlightCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-flight/manifest-carrier-master-flight-create/manifest-carrier-master-flight-create.component';
import { ManifestCarrierMasterFlightDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-flight/manifest-carrier-master-flight-delete/manifest-carrier-master-flight-delete.component';
import { FlightRouteCreateComponent } from './views/pages/manifest-carrier-master/flight-route/flight-route-create/flight-route-create.component';
import { FlightRouteUpdateComponent } from './views/pages/manifest-carrier-master/flight-route/flight-route-update/flight-route-update.component';
import { FlightRouteDeleteComponent } from './views/pages/manifest-carrier-master/flight-route/flight-route-delete/flight-route-delete.component';
import { TransitCreateComponent } from './views/pages/manifest-carrier-master/master-transit/transit-create/transit-create.component';
import { TransitDeleteComponent } from './views/pages/manifest-carrier-master/master-transit/transit-delete/transit-delete.component';
import { HistoryNotPrintComponent } from './views/pages/history-not-print/history-not-print.component';
import { MonitoringShipmentComponent } from './views/pages/monitoring-shipment/monitoring-shipment.component';
import { MonitoringShipmentCreateComponent } from './views/pages/monitoring-shipment/monitoring-shipment-create/monitoring-shipment-create.component';
import { MonitoringShipmentUpdateComponent } from './views/pages/monitoring-shipment/monitoring-shipment-update/monitoring-shipment-update.component';
import { MonitoringShipmentDeleteComponent } from './views/pages/monitoring-shipment/monitoring-shipment-delete/monitoring-shipment-delete.component';
import { ManifestCarrierMasterStationComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-station/manifest-carrier-master-station.component';
import { ManifestCarrierMasterStationCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-station/manifest-carrier-master-station-create/manifest-carrier-master-station-create.component';
import { ManifestCarrierMasterStationDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-station/manifest-carrier-master-station-delete/manifest-carrier-master-station-delete.component';
import { ManifestCarrierMasterStationCityComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-station-city/manifest-carrier-master-station-city.component';
import { ManifestCarrierMasterStationCityCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-station-city/manifest-carrier-master-station-city-create/manifest-carrier-master-station-city-create.component';
import { ManifestCarrierMasterStationCityDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-station-city/manifest-carrier-master-station-city-delete/manifest-carrier-master-station-city-delete.component';
import { ManifestCarrierMasterVendorDestComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-dest/manifest-carrier-master-vendor-dest.component';
import { ManifestCarrierMasterVendorDestCreateComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-dest/manifest-carrier-master-vendor-dest-create/manifest-carrier-master-vendor-dest-create.component';
import { ManifestCarrierMasterVendorDestDeleteComponent } from './views/pages/manifest-carrier-master/manifest-carrier-master-vendor-dest/manifest-carrier-master-vendor-dest-delete/manifest-carrier-master-vendor-dest-delete.component';
import { ResetPasswordComponent } from './views/pages/privilege-user/reset-password/reset-password.component';
import { MasterCustomerCodeComponent } from './views/pages/master-customer-code/master-customer-code.component';
import { MasterCustomerCodeCreateComponent } from './views/pages/master-customer-code/master-customer-code-create/master-customer-code-create.component';
import { MasterCustomerCodeDeleteComponent } from './views/pages/master-customer-code/master-customer-code-delete/master-customer-code-delete.component';
import { MasterPorterCodeComponent } from './views/pages/master-porter-code/master-porter-code.component';
import { MasterPorterCodeDeleteComponent } from './views/pages/master-porter-code/master-porter-code-delete/master-porter-code-delete.component';
import { MasterPorterCodeCreateComponent } from './views/pages/master-porter-code/master-porter-code-create/master-porter-code-create.component';
import { SpbScansComponent } from './views/pages/spb-scans/spb-scans.component';
import { SpbUpdateComponent } from './views/pages/spb-update/spb-update.component';
import { MasterRoundComponent } from './views/pages/master-round/master-round.component';
import { MasterRoundCreateComponent } from './views/pages/master-round/master-round-create/master-round-create.component';
import { MasterRoundDeleteComponent } from './views/pages/master-round/master-round-delete/master-round-delete.component';
import { MasterDividedComponent } from './views/pages/master-divided/master-divided.component';
import { MasterDividedCreateComponent } from './views/pages/master-divided/master-divided-create/master-divided-create.component';
import { MasterDividedDeleteComponent } from './views/pages/master-divided/master-divided-delete/master-divided-delete.component';
import { CoaCodeComponent } from './views/pages/finance/coa-code/coa-code/coa-code.component';
import { CoaCodeCreateComponent } from './views/pages/finance/coa-code/coa-code-create/coa-code-create.component';
import { CoaArManagementCreateComponent } from './views/pages/finance/coa-code/coa-ar-management-create/coa-ar-management-create.component';
import { CoaAccuredExpensesManagementCreateComponent } from './views/pages/finance/coa-code/coa-accured-expenses-management-create/coa-accured-expenses-management-create.component';
import { CoaAccuredExpensesManagementComponent } from './views/pages/finance/coa-code/coa-accured-expenses-management/coa-accured-expenses-management.component';
import { CoaArManagementComponent } from './views/pages/finance/coa-code/coa-ar-management/coa-ar-management.component';
import { ProfitLostStatementMatTabComponent } from './views/pages/finance/profit-loss-statement/profit-lost-statement-mat-tab/profit-lost-statement-mat-tab.component';
import { SalesReportComponent } from './views/pages/finance/sales-report/sales-report/sales-report.component';
import { BukuKasComponent } from './views/pages/finance/buku-kas/buku-kas/buku-kas.component';
import { AccountReceivableComponent } from './views/pages/finance/account-receivable/account-receivable/account-receivable.component';
import { AccountPayableComponent } from './views/pages/finance/account-payable/account-payable/account-payable.component';
import { CostComponent } from './views/pages/finance/cost/cost/cost.component';
import { AccountReceivableCreateComponent } from './views/pages/finance/account-receivable/account-receivable-create/account-receivable-create.component';
import { BukuKasCreateComponent } from './views/pages/finance/buku-kas/buku-kas-create/buku-kas-create.component';
import { NeracaComponent } from './views/pages/finance/neraca/neraca.component';
import { GeneralLedgerComponent } from './views/pages/finance/general-ledger/general-ledger.component';
import { ExpensesFixComponent } from './views/pages/finance/expenses/expenses-fix/expenses-fix.component';
import { ExpensesFixCreateComponent } from './views/pages/finance/expenses/expenses-fix-create/expenses-fix-create.component';

const routes: Routes = [
	{path: 'auth', loadChildren: () => import('app/views/pages/auth/auth.module').then(m => m.AuthModule)},

	{
		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: 'spb-create', // <= Page URL
				component: SpbCreateComponent // <= Page component registration
			},

			{
				path: 'spb-list-sender',
				component: SpbListComponent
			},


			{
				path: 'spb-statistic-input',
				component: SpbStatisticInputComponent
			},
			{
				path: 'goods-monitoring-sender',
				component: GoodsMonitoringComponent
			},
			{
				path: 'spb-hourly/list',
				component: SpbHourlyListComponent
			},
			{
				path: 'spb-list-receiver',
				component: SpbListReceiverComponent
			},
			{
				path: 'manifest-list-receiver',
				component: ManifestListReceiverComponent
			},
			{
				path: 'spb-hourly-receiver',
				component: SpbHourlyReceiverListComponent
			},
			{
				path: 'goods-monitoring-receiver',
				component: GoodsMonitoringReceiverListComponent
			},
			{
				path: 'manifest-sender',
				component: ManifestSenderComponent
			},
			{
				path: 'manifest-carrier',
				component: ManifestCarrierComponent
			},
			{
				path: 'detail-manifest-carrier',
				component: DetailManifestCarrierComponent
			},
			{
				path: 'detail-cost-manifest-carrier',
				component: DetailCostManifestCarrierComponent
			},
			{
				path: 'revisi-manifest-carrier',
				component: RevisiManifestCarrierComponent
			},
			{
				path: 'list-revisi-manifest-carrier',
				component: ListRevisiManifestCarrierComponent
			},
			{
				path: 'manifest-carrier-cost-carrier',
				component: ManifestCarrierCostCarrierComponent
			},
			{
				path: 'manifest-carrier-cost-carrier-create',
				component: ManifestCarrierCostCarrierCreateComponent
			},
			{
				path: 'manifest-carrier-cost-carrier-update',
				component: ManifestCarrierCostCarrierUpdateComponent
			},
			{
				path: 'manifest-carrier-cost-carrier-delete',
				component: ManifestCarrierCostCarrierDeleteComponent
			},
			{
				path: 'manifest-carrier-cost-destination',
				component: ManifestCarrierCostDestinationComponent
			},
			{
				path: 'manifest-carrier-cost-destination-create',
				component: ManifestCarrierCostDestinationCreateComponent
			},
			{
				path: 'manifest-carrier-cost-destination-update',
				component: ManifestCarrierCostDestinationUpdateComponent
			},
			{
				path: 'manifest-carrier-cost-destination-delete',
				component: ManifestCarrierCostDestinationDeleteComponent
			},
			{
				path: 'manifest-carrier-cost-gerai',
				component: ManifestCarrierCostGeraiComponent
			},
			{
				path: 'manifest-carrier-cost-gerai-create',
				component: ManifestCarrierCostGeraiCreateComponent
			},
			{
				path: 'manifest-carrier-cost-gerai-update',
				component: ManifestCarrierCostGeraiUpdateComponent
			},
			{
				path: 'manifest-carrier-cost-gerai-delete',
				component: ManifestCarrierCostGeraiDeleteComponent
			},
			{
				path: 'manifest-carrier-cost-inland-origin',
				component: ManifestCarrierCostInlandOriginComponent
			},
			{
				path: 'manifest-carrier-cost-inland-origin-create',
				component: ManifestCarrierCostInlandOriginCreateComponent
			},
			{
				path: 'manifest-carrier-cost-inland-origin-update',
				component: ManifestCarrierCostInlandOriginUpdateComponent
			},
			{
				path: 'manifest-carrier-cost-inland-origin-delete',
				component: ManifestCarrierCostInlandOriginDeleteComponent
			},
			{
				path: 'manifest-carrier-cost-origin',
				component: ManifestCarrierCostOriginComponent
			},
			{
				path: 'manifest-carrier-cost-origin-create',
				component: ManifestCarrierCostOriginCreateComponent
			},
			{
				path: 'manifest-carrier-cost-origin-update',
				component: ManifestCarrierCostOriginUpdateComponent
			},
			{
				path: 'manifest-carrier-cost-origin-delete',
				component: ManifestCarrierCostOriginDeleteComponent
			},
			{
				path: 'manifest-carrier-cost-penerus',
				component: ManifestCarrierCostPenerusComponent
			},
			{
				path: 'manifest-carrier-cost-penerus-create',
				component: ManifestCarrierCostPenerusCreateComponent
			},
			{
				path: 'manifest-carrier-cost-penerus-update',
				component: ManifestCarrierCostPenerusUpdateComponent
			},
			{
				path: 'manifest-carrier-cost-penerus-delete',
				component: ManifestCarrierCostPenerusDeleteComponent
			},
			{
				path: 'manifest-carrier-cost-transit',
				component: ManifestCarrierCostTransitComponent
			},
			{
				path: 'manifest-carrier-cost-transit-create',
				component: ManifestCarrierCostTransitCreateComponent
			},
			{
				path: 'manifest-carrier-cost-transit-update',
				component: ManifestCarrierCostTransitUpdateComponent
			},
			{
				path: 'manifest-carrier-cost-transit-delete',
				component: ManifestCarrierCostTransitDeleteComponent
			},
			{
				path: 'manifest-carrier-master-vendor',
				component: ManifestCarrierMasterVendorComponent
			},
			{
				path: 'manifest-carrier-master-vendor-create',
				component: ManifestCarrierMasterVendorCreateComponent
			},
			{
				path: 'manifest-carrier-master-vendor-delete',
				component: ManifestCarrierMasterVendorDeleteComponent
			},
			{
				path: 'manifest-carrier-master-vendor-origin',
				component: ManifestCarrierMasterVendorOriginComponent
			},
			{
				path: 'manifest-carrier-master-vendor-origin-create',
				component: ManifestCarrierMasterVendorOriginCreateComponent
			},
			{
				path: 'manifest-carrier-master-vendor-origin-delete',
				component: ManifestCarrierMasterVendorOriginDeleteComponent
			},
			{
				path: 'manifest-carrier-master-vendor-transit',
				component: ManifestCarrierMasterVendorTransitComponent
			},
			{
				path: 'mc-master-vendor-transit-create',
				component: MCMasterVendorTransitCreateComponent
			},
			{
				path: 'mc-master-vendor-transit-delete',
				component: MCMasterVendorTransitDeleteComponent
			},
			{
				path: 'manifest-carrier-master-vendor-dest',
				component: ManifestCarrierMasterVendorDestComponent
			},
			{
				path: 'manifest-carrier-master-vendor-dest-create',
				component: ManifestCarrierMasterVendorDestCreateComponent
			},
			{
				path: 'manifest-carrier-master-vendor-dest-delete',
				component: ManifestCarrierMasterVendorDestDeleteComponent
			},
			{
				path: 'manifest-carrier-master-vendor-carrier',
				component: ManifestCarrierMasterVendorCarrierComponent
			},
			{
				path: 'mc-master-vendor-carrier-create',
				component: MCMasterVendorCarrierCreateComponent
			},
			{
				path: 'mc-master-vendor-carrier-delete',
				component: MCMasterVendorCarrierDeleteComponent
			},
			{
				path: 'manifest-carrier-master-carrier',
				component: ManifestCarrierMasterCarrierComponent
			},
			{
				path: 'manifest-carrier-master-carrier-create',
				component: ManifestCarrierMasterCarrierCreateComponent
			},
			{
				path: 'manifest-carrier-master-carrier-delete',
				component: ManifestCarrierMasterCarrierDeleteComponent
			},
			{
				path: 'manifest-carrier-master-flight',
				component: ManifestCarrierMasterFlightComponent
			},
			{
				path: 'manifest-carrier-master-flight-create',
				component: ManifestCarrierMasterFlightCreateComponent
			},
			{
				path: 'manifest-carrier-master-flight-delete',
				component: ManifestCarrierMasterFlightDeleteComponent
			},
			{
				path: 'last-manifest',
				component: LastManifestComponent
			},
			{
				path: 'privilege-master-list',
				component: PrivilegeMasterListComponent
			},

			{
				path: 'privilege-user-list',
				component: PrivilegeUserListComponent
			},

			{
				path: 'privilege-list',
				component: PrivilegeListComponent
			},

			{
				path: 'user-list',
				component: UserListComponent
			},

			{
				path: 'privilege-setting',
				component: PrivilegeSettingComponent
			},

			{
				path: 'role-user-setting',
				component: RoleUserSettingComponent
			},

			{
				path: 'detail-privilege-setting',
				component: DetailPrivilegeSettingComponent
			},

			{
				path: 'role-user-list',
				component: RoleUserListComponent
			},
			{
				path: 'detail-role-list',
				component: DetailRoleListComponent
			},

			{
				path: 'add-role-city',
				component: AddRoleCityComponent
			},

			{
				path: 'delete-role-city',
				component: DeleteRoleCityComponent
			},

			{
				path: 'privilege-update-setting',
				component: PrivilegeUpdateSettingComponent
			},

			{
				path: 'update-password',
				component: UpdatePasswordComponent
			},
			{
				path: 'rates-list',
				component: RatesComponent
			},
			{
				path: 'master-data-customer',
				component: MasterDataCustomerComponent
			},
			{
				path: 'master-data-customer-detail',
				component: MasterDataCustomerDetailComponent
			},
			{
				path: 'master-data-customer-sender',
				component: MasterDataCustomerSenderComponent
			},
			{
				path: 'master-data-customer-receiver',
				component: MasterDataCustomerReceiverComponent
			},
			{
				path: 'add-user',
				component: AddUserComponent
			},

			{
				path: 'user-setting',
				component: UserSettingComponent
			},

			{
				path: 'update-unit-user',
				component: UpdateUnitUserComponent
			},

			{
				path: 'update-unit-company',
				component: UpdateUnitCompanyComponent
			},

			{
				path: 'update-unit-branch',
				component: UpdateUnitBranchComponent
			},

			{
				path: 'update-unit-subbranch',
				component: UpdateUnitSubbranchComponent
			},

			{
				path: 'update-role-privilege',
				component: UpdateRolePrivilegeComponent
			},

			{
				path: 'privilege-create',
				component: PrivilegeCreateComponent
			},

			{
				path: 'role-create',
				component: RoleCreateComponent
			},

			{
				path: 'unit-place-create',
				component: UnitPlaceCreateComponent
			},
			{
				path: 'unit-subbranch-place-create',
				component: UnitSubbranchPlaceCreateComponent
			},

			{
				path: 'update-unit-subbranch-user',
				component: UpdateUnitSubbranchUserComponent
			},

			{
				path: 'unit-subbranch-place-create',
				component: UnitSubbranchPlaceCreateComponent
			},

			{
				path: 'user-detail',
				component: UserDetailComponent
			},

			{
				path: 'user-role-list',
				component: UserRoleListComponent
			},
			
			{
				path: 'transit-list',
				component: TransitListComponent
			},

			{
				path: 'transit-create',
				component: TransitCreateComponent
			},

			{
				path: 'transit-delete',
				component: TransitDeleteComponent
			},

			{
				path: 'transit-update',
				component: TransitUpdateComponent
			},
			{
				path: 'history-print',
				component: HistoryPrintComponent
			},
			{
				path: 'history-not-print',
				component: HistoryNotPrintComponent
			},
			{
				path: 'history-login',
				component: HistoryLoginComponent
			},

			{
				path: 'history-aksi',
				component: HistoryAksiComponent
			},
			
			{
				path: 'super-vision',
				component: SuperVisionComponent
			},

			{
				path: 'super-vision-list',
				component: SuperVisionListComponent
			},

			{
				path: 'super-vision-history',
				component: SuperVisionHistoryComponent
			},

			{
				path: 'super-vision-create',
				component: SuperVisionCreateComponent
			},

			{
				path: 'super-vision-approval-reprint',
				component: SuperVisionApprovalReprintComponent
			},

			{
				path: 'super-vision-approval-spb',
				component: SuperVisionApprovalSpbComponent
			},
			
			{
				path: 'super-vision-approval-void',
				component: SuperVisionApprovalVoidComponent
			},

			{
				path: 'super-vision-approval-foc',
				component: SuperVisionApprovalFocComponent
			},

			{
				path: 'super-vision-approval-kilo',
				component: SuperVisionApprovalKiloComponent
			},

			{
				path: 'super-vision-create-void',
				component: SuperVisionCreateVoidComponent
			},

			{
				path: 'super-vision-create-spb',
				component: SuperVisionCreateSpbComponent
			},

			{
				path: 'super-vision-create-reprint',
				component: SuperVisionCreateReprintComponent
			},
			{
				path: 'super-vision-create-foc',
				component: SuperVisionCreateFocComponent
			},

			{
				path: 'super-vision-create-kilo',
				component: SuperVisionCreateKiloComponent
			},
			{
				path: 'master-agreement',
				component: MasterAgreementComponent	
			},
			{
				path: 'master-agreement-detail',
				component: MasterAgreementDetailComponent	
			},
			{
				path: 'master-method-payment',
				component: MasterMethodPaymentComponent	
			},
			{
				path: 'master-method-payment-create',
				component: MasterMethodPaymentCreateComponent	
			},
			{
				path: 'master-method-payment-delete',
				component: MasterMethodPaymentDeleteComponent	
			},
			{
				path: 'master-moda-city',
				component: MasterModaCityComponent	
			},
			{
				path: 'master-moda-city-create',
				component: MasterModaCityCreateComponent	
			},
			{
				path: 'master-moda-city-delete',
				component: MasterModaCityDeleteComponent	
			},
			{
				path: 'master-city',
				component: MasterCityComponent
			},
			{
				path: 'master-city-create',
				component: MasterCityCreateComponent
			},			
			{
				path: 'master-city-delete',
				component: MasterCityDeleteComponent
			},
			{
				path: 'flight-route',
				component: FlightRouteComponent
			},
			{
				path: 'flight-route-create',
				component: FlightRouteCreateComponent
			},
			{
				path: 'flight-route-update',
				component: FlightRouteUpdateComponent
			},
			{
				path: 'flight-route-delete',
				component: FlightRouteDeleteComponent
			},
			{
				path: 'mapping-area-no-exists',
				component: MappingAreaNoExistsComponent
			},
			{
				path: 'mapping-area-no-exists-create',
				component: MappingAreaNoExistsCreateComponent
			},
			{
				path: 'mapping-area-no-exists-delete',
				component: MappingAreaNoExistsDeleteComponent
			},
			{
				path: 'master-statistik',
				component: MasterStatistikComponent
			},
			{
				path: 'monitoring-capacity',
				component: MonitoringCapacityComponent
			},
			{
				path: 'monitoring-shipment',
				component: MonitoringShipmentComponent	
			},
			{
				path: 'monitoring-shipment-create',
				component: MonitoringShipmentCreateComponent	
			},
			{
				path: 'monitoring-shipment-update',
				component: MonitoringShipmentUpdateComponent	
			},
			{
				path: 'monitoring-shipment-delete',
				component: MonitoringShipmentDeleteComponent	
			},
			//---
			{
				path: 'manifest-carrier-master-station',
				component: ManifestCarrierMasterStationComponent
			},
			{
				path: 'manifest-carrier-master-station-create',
				component: ManifestCarrierMasterStationCreateComponent
			},
			{
				path: 'manifest-carrier-master-station-delete',
				component: ManifestCarrierMasterStationDeleteComponent	
			},
			{
				path: 'manifest-carrier-master-station-city',
				component: ManifestCarrierMasterStationCityComponent	
			},
			{
				path: 'manifest-carrier-master-station-city-create',
				component: ManifestCarrierMasterStationCityCreateComponent	
			},
			{
				path: 'manifest-carrier-master-station-city-delete',
				component: ManifestCarrierMasterStationCityDeleteComponent
			},
			{
				path: 'master-customer-code',
				component: MasterCustomerCodeComponent	
			},
			{
				path: 'master-customer-code-create',
				component: MasterCustomerCodeCreateComponent	
			},
			{
				path: 'master-customer-code-delete',
				component: MasterCustomerCodeDeleteComponent
			},
			{
				path: 'master-porter-code',
				component: MasterPorterCodeComponent	
			},
			{
				path: 'master-porter-code-create',
				component: MasterPorterCodeCreateComponent	
			},
			{
				path: 'master-porter-code-delete',
				component: MasterPorterCodeDeleteComponent
			},
			{
				path: 'reset-password',
				component: ResetPasswordComponent
			},
			{
				path: 'spb-scans',
				component: SpbScansComponent
			},
			{
				path: 'spb-update',
				component: SpbUpdateComponent
			},
			{
				path: 'master-round',
				component: MasterRoundComponent
			},
			{
				path: 'master-round-create',
				component: MasterRoundCreateComponent
			},
			{
				path: 'master-round-delete',
				component: MasterRoundDeleteComponent
			},
			{
				path: 'master-divided',
				component: MasterDividedComponent
			},
			{
				path: 'master-divided-create',
				component: MasterDividedCreateComponent
			},
			{
				path: 'master-divided-delete',
				component: MasterDividedDeleteComponent
			},
			{
				path: 'coa-code-list',
				component: CoaCodeComponent
			},
			{
				path: 'coa-code-create',
				component: CoaCodeCreateComponent
			},
			{
				path: 'coa-ar-management',
				component: CoaArManagementComponent
			},
			{
				path: 'coa-ar-management-create',
				component: CoaArManagementCreateComponent
			},
			{
				path: 'coa-accured-expenses-management',
				component: CoaAccuredExpensesManagementComponent
			},
			{
				path: 'coa-accured-expenses-management-create',
				component: CoaAccuredExpensesManagementCreateComponent
			},
			{
				path: 'profit-loss-statement',
				component: ProfitLostStatementMatTabComponent
			},
			{
				path: 'sales-report',
				component: SalesReportComponent
			},
			{
				path: 'buku-bank',
				component: BukuKasComponent
			},
			{
				path: 'account-receivable',
				component: AccountReceivableComponent
			},
			{
				path: 'account-receivable-create',
				component: AccountReceivableCreateComponent
			},
			{
				path: 'buku-kas-create',
				component: BukuKasCreateComponent
			}, 
			{
				path: 'account-payable',
				component: AccountPayableComponent
			},
			{
				path: 'cost',
				component: CostComponent
			},
			{
				path: 'general-ledger',
				component: GeneralLedgerComponent
			},
			{
				path: 'fix-expenses',
				component: ExpensesFixComponent
			},
			{
				path: 'fix-expenses-create',
				component: ExpensesFixCreateComponent
			},
			{
				path: 'neraca',
				component: NeracaComponent
			},
			{
				path: 'dashboard',
				loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule)
			},
			{
				path: 'mail',
				loadChildren: () => import('app/views/pages/apps/mail/mail.module').then(m => m.MailModule)
			},
			{
				path: 'ecommerce',
				loadChildren: () => import('app/views/pages/apps/e-commerce/e-commerce.module').then(m => m.ECommerceModule),
			},
			{
				path: 'ngbootstrap',
				loadChildren: () => import('app/views/pages/ngbootstrap/ngbootstrap.module').then(m => m.NgbootstrapModule)
			},
			{
				path: 'material',
				loadChildren: () => import('app/views/pages/material/material.module').then(m => m.MaterialModule)
			},
			{
				path: 'user-management',
				loadChildren: () => import('app/views/pages/user-management/user-management.module').then(m => m.UserManagementModule)
			},
			{
				path: 'wizard',
				loadChildren: () => import('app/views/pages/wizard/wizard.module').then(m => m.WizardModule)
			},
			{
				path: 'builder',
				loadChildren: () => import('app/views/theme/content/builder/builder.module').then(m => m.BuilderModule)
			},
			{
				path: 'error/403',
				component: ErrorPageComponent,
				data: {
					type: 'error-v6',
					code: 403,
					title: '403... Access forbidden',
					desc: 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator'
				}
			},
			{path: 'error/:type', component: ErrorPageComponent},
			{path: '', redirectTo: 'dashboard', pathMatch: 'full'},
			{path: '**', redirectTo: 'dashboard', pathMatch: 'full'}
		]
	},

	{path: '**', redirectTo: 'error/403', pathMatch: 'full'},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
