export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
				// {
				// 	title: 'Custom',
				// 	root: true,
				// 	alignment: 'left',
				// 	toggle: 'click',
				// 	submenu: [
				// 		{
				// 			title: 'Error Pages',
				// 			bullet: 'dot',
				// 			icon: 'flaticon2-list-2',
				// 			submenu: [
				// 				{
				// 					title: 'Error 1',
				// 					page: '/error/error-v1'
				// 				},
				// 				{
				// 					title: 'Error 2',
				// 					page: '/error/error-v2'
				// 				},
				// 				{
				// 					title: 'Error 3',
				// 					page: '/error/error-v3'
				// 				},
				// 				{
				// 					title: 'Error 4',
				// 					page: '/error/error-v4'
				// 				},
				// 				{
				// 					title: 'Error 5',
				// 					page: '/error/error-v5'
				// 				},
				// 				{
				// 					title: 'Error 6',
				// 					page: '/error/error-v6'
				// 				},
				// 			]
				// 		},
				// 		{
				// 			title: 'Wizard',
				// 			bullet: 'dot',
				// 			icon: 'flaticon2-mail-1',
				// 			submenu: [
				// 				{
				// 					title: 'Wizard 1',
				// 					page: '/wizard/wizard-1'
				// 				},
				// 				{
				// 					title: 'Wizard 2',
				// 					page: '/wizard/wizard-2'
				// 				},
				// 				{
				// 					title: 'Wizard 3',
				// 					page: '/wizard/wizard-3'
				// 				},
				// 				{
				// 					title: 'Wizard 4',
				// 					page: '/wizard/wizard-4'
				// 				},
				// 			]
				// 		},
				// 	]
				// },
				{
					title: "Kirim Barang",
					root: true,
					alignment: "left",
					toggle: "click",
					submenu: [
						{
							title: "Input Barang",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/spb-create",
						},
						{
							title: "SPB",
							bullet: "dot",
							icon: "flaticon-business",
							submenu: [
								{
									title: "SPB Edit",
									page: "/spb-scans",
								},
								{
									title: "SPB List",
									page: "/spb-list-sender",
								},
							],
						},
						{
							title: "Manifest List",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/manifest-sender",
						},
						{
							title: "Monitoring SPB",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/goods-monitoring-sender",
						},

						{
							title: "Monitoring Barang",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/spb-hourly/list",
						},

						{
							title: "Monitoring Kapasitas",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/monitoring-capacity",
						},
					],
				},

				{
					title: "Terima Barang",
					root: true,
					alignment: "left",
					toggle: "click",
					submenu: [
						{
							title: "SPB List",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/spb-list-receiver",
						},
						{
							title: "Manifest List",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/manifest-list-receiver",
						},
						{
							title: "Monitoring SPB",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/goods-monitoring-receiver",
						},
						{
							title: "Monitoring Barang",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/spb-hourly-receiver",
						},
					],
				},

				{
					title: "Statistik",
					root: true,
					alignment: "left",
					toggle: "click",
					submenu: [
						{
							title: "Statistik Input",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/spb-statistic-input",
						},
						{
							title: "Statistik Omset",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-statistik",
						},
					],
				},
				{
					title: "Tracking",
					root: true,
					alignment: "left",
					toggle: "click",
					submenu: [
						{
							title: "LP - Monitoring",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/monitoring-shipment",
						},
					],
				},
				{
					title: "Manifest Carrier",
					root: true,
					alignment: "left",
					toggle: "click",
					submenu: [
						{
							title: "Manifest Carrier Create",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/manifest-carrier",
						},
						{
							title: "Manifest Carrier Master",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							toggle: "click",
							submenu: [
								{
									title: "Flight Route",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/flight-route",
								},
								{
									title: "Flight Transit Route",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/transit-list",
								},
								{
									title: "AirPort",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-master-station",
								},
								{
									title: "AirPort City",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-master-station-city",
								},
								{
									title: "Carrier",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-master-carrier",
								},
								{
									title: "Flight",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-master-flight",
								},
								{
									title: "Vendor",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-master-vendor",
								},
								{
									title: "Vendor Origin",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-master-vendor-origin",
								},
								{
									title: "Vendor Transit",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-master-vendor-transit",
								},
								{
									title: "Vendor Tujuan",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-master-vendor-dest",
								},
								{
									title: "Vendor Carrier",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-master-vendor-carrier",
								},
							],
						},
						{
							title: "Manifest Carrier Cost",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							toggle: "click",
							submenu: [
								{
									title: "Cost Carrier",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-cost-carrier",
								},
								{
									title: "Cost Origin",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-cost-origin",
								},
								{
									title: "Cost Destination",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-cost-destination",
								},
								{
									title: "Cost Gerai",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-cost-gerai",
								},
								{
									title: "Cost Penerus",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-cost-penerus",
								},
								{
									title: "Cost Transit",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/manifest-carrier-cost-transit",
								},
							],
						},
					],
				},
				{
					title: "Pengajuan",
					root: true,
					alignment: "left",
					toggle: "click",
					submenu: [
						{
							title: "List Pengajuan",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/super-vision-list",
						},
						{
							title: "Buat Pengajuan",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/super-vision-create",
						},
						{
							title: "Riwayat Pengajuan",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/super-vision-history",
						},
					],
				},
				{
					title: "Finance",
					root: true,
					alignment: "left",
					toggle: "click",
					submenu: [
						{
							title: "COA Code",
							bullet: "dot",
							icon: "flaticon-business",
							submenu: [
								{
									title: "Coa Code List",
									page: "/coa-code-list",
								},
								{
									title: "Coa AR Manage",
									page: "/coa-ar-management",
								},
								{
									title: "Coa Accured Expenses Manage",
									page: "/coa-accured-expenses-management",
								},
							],
						},
						{
							title: "Kas",
							bullet: "dot",
							icon: "flaticon-business",
							submenu: [
								{
									title: "Buku Bank",
									page: "/buku-bank",
								},
								{
									title: "Buku Kas",
									page: "/dashboard",
								},
							],
						},
						{
							title: "Account Receivable",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/account-receivable",
						},
						{
							title: "Account Payable",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/account-payable",
						},
						{
							title: "Sales Report",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/sales-report",
						},
						{
							title: "Cost",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/cost",
						},
						{
							title: "Expenses",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/fix-expenses",
						},
						{
							title: "General Ledger",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/general-ledger",
						},
						// {
						// 	title: "AR/AP",
						// 	bullet: "dot",
						// 	icon: "flaticon-business",
						// 	submenu: [
						// 		{
						// 			title: "Account Receivable",
						// 			page: "/account-receivable",
						// 		},
						// 		{
						// 			title: "Account Payable",
						// 			page: "/account-payable",
						// 		}
						// 	],
						// },
						{
							title: "Profit and Loss Statement",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/profit-loss-statement",
						},
						{
							title: "Balance Sheet",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/neraca",
						},
					],
				},
				{
					title: "Master",
					root: true,
					alignment: "left",
					toggle: "click",
					submenu: [
						{
							title: "Data Pelanggan",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-data-customer",
						},
						{
							title: "Kode Pelanggan",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-customer-code",
						},
						{
							title: "Kode Pengantar / Porter",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-porter-code",
						},
						{
							title: "As Agreed / Kesepakatan",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-agreement",
						},
						{
							title: "Kota",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-city",
						},
						{
							title: "Metode Pembayaran",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-method-payment",
						},
						{
							title: "Moda",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-divided",
						},
						{
							title: "Moda Kota",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-moda-city",
						},
						{
							title: "Mapping Area No Exists",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/mapping-area-no-exists",
						},
						{
							title: "Tarif",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/rates-list",
						},
						{
							title: "Pembulatan",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/master-round",
						},
					],
				},
				{
					title: "Admin",
					root: true,
					alignment: "left",
					toggle: "click",
					submenu: [
						{
							title: "Reset Password",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/reset-password",
						},
						{
							title: "Konfigurasi",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							page: "/privilege-user-list",
						},
						{
							title: "History",
							bullet: "dot",
							icon: "flaticon-business",
							alignment: "left",
							toggle: "click",
							submenu: [
								{
									title: "History Print",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/history-print",
								},
								{
									title: "History Not Print",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/history-not-print",
								},
								{
									title: "History Login",
									bullet: "dot",
									icon: "flaticon-business",
									alignment: "left",
									page: "/history-login",
								},
								// {
								// 	title: 'History Action',
								// 	bullet: 'dot',
								// 	icon: 'flaticon-business',
								// 	alignment: 'left',
								// 	page: '/history-aksi',
								// }
							],
						},
					],
				},
			],
		},
		aside: {
			self: {},
			items: [
				{
					title: "Dashboard",
					root: true,
					icon: "flaticon2-architecture-and-city",
					page: "/dashboard",
					translate: "MENU.DASHBOARD",
					bullet: "dot",
				},
				{
					title: "Layout Builder",
					root: true,
					icon: "flaticon2-expand",
					page: "/builder",
				},
				{ section: "Components" },
				{
					title: "Google Material",
					root: true,
					bullet: "dot",
					icon: "flaticon2-browser-2",
					submenu: [
						{
							title: "Form Controls",
							bullet: "dot",
							submenu: [
								{
									title: "Auto Complete",
									page: "/material/form-controls/autocomplete",
									permission: "accessToECommerceModule",
								},
								{
									title: "Checkbox",
									page: "/material/form-controls/checkbox",
								},
								{
									title: "Radio Button",
									page: "/material/form-controls/radiobutton",
								},
								{
									title: "Datepicker",
									page: "/material/form-controls/datepicker",
								},
								{
									title: "Form Field",
									page: "/material/form-controls/formfield",
								},
								{
									title: "Input",
									page: "/material/form-controls/input",
								},
								{
									title: "Select",
									page: "/material/form-controls/select",
								},
								{
									title: "Slider",
									page: "/material/form-controls/slider",
								},
								{
									title: "Slider Toggle",
									page: "/material/form-controls/slidertoggle",
								},
							],
						},
						{
							title: "Navigation",
							bullet: "dot",
							submenu: [
								{
									title: "Menu",
									page: "/material/navigation/menu",
								},
								{
									title: "Sidenav",
									page: "/material/navigation/sidenav",
								},
								{
									title: "Toolbar",
									page: "/material/navigation/toolbar",
								},
							],
						},
						{
							title: "Layout",
							bullet: "dot",
							submenu: [
								{
									title: "Card",
									page: "/material/layout/card",
								},
								{
									title: "Divider",
									page: "/material/layout/divider",
								},
								{
									title: "Expansion panel",
									page: "/material/layout/expansion-panel",
								},
								{
									title: "Grid list",
									page: "/material/layout/grid-list",
								},
								{
									title: "List",
									page: "/material/layout/list",
								},
								{
									title: "Tabs",
									page: "/material/layout/tabs",
								},
								{
									title: "Stepper",
									page: "/material/layout/stepper",
								},
								{
									title: "Default Forms",
									page: "/material/layout/default-forms",
								},
								{
									title: "Tree",
									page: "/material/layout/tree",
								},
							],
						},
						{
							title: "Buttons & Indicators",
							bullet: "dot",
							submenu: [
								{
									title: "Button",
									page: "/material/buttons-and-indicators/button",
								},
								{
									title: "Button toggle",
									page: "/material/buttons-and-indicators/button-toggle",
								},
								{
									title: "Chips",
									page: "/material/buttons-and-indicators/chips",
								},
								{
									title: "Icon",
									page: "/material/buttons-and-indicators/icon",
								},
								{
									title: "Progress bar",
									page: "/material/buttons-and-indicators/progress-bar",
								},
								{
									title: "Progress spinner",
									page: "/material/buttons-and-indicators/progress-spinner",
								},
								{
									title: "Ripples",
									page: "/material/buttons-and-indicators/ripples",
								},
							],
						},
						{
							title: "Popups & Modals",
							bullet: "dot",
							submenu: [
								{
									title: "Bottom sheet",
									page: "/material/popups-and-modals/bottom-sheet",
								},
								{
									title: "Dialog",
									page: "/material/popups-and-modals/dialog",
								},
								{
									title: "Snackbar",
									page: "/material/popups-and-modals/snackbar",
								},
								{
									title: "Tooltip",
									page: "/material/popups-and-modals/tooltip",
								},
							],
						},
						{
							title: "Data table",
							bullet: "dot",
							submenu: [
								{
									title: "Paginator",
									page: "/material/data-table/paginator",
								},
								{
									title: "Sort header",
									page: "/material/data-table/sort-header",
								},
								{
									title: "Table",
									page: "/material/data-table/table",
								},
							],
						},
					],
				},
				{
					title: "Ng-Bootstrap",
					root: true,
					bullet: "dot",
					icon: "flaticon2-digital-marketing",
					submenu: [
						{
							title: "Accordion",
							page: "/ngbootstrap/accordion",
						},
						{
							title: "Alert",
							page: "/ngbootstrap/alert",
						},
						{
							title: "Buttons",
							page: "/ngbootstrap/buttons",
						},
						{
							title: "Carousel",
							page: "/ngbootstrap/carousel",
						},
						{
							title: "Collapse",
							page: "/ngbootstrap/collapse",
						},
						{
							title: "Datepicker",
							page: "/ngbootstrap/datepicker",
						},
						{
							title: "Dropdown",
							page: "/ngbootstrap/dropdown",
						},
						{
							title: "Modal",
							page: "/ngbootstrap/modal",
						},
						{
							title: "Pagination",
							page: "/ngbootstrap/pagination",
						},
						{
							title: "Popover",
							page: "/ngbootstrap/popover",
						},
						{
							title: "Progressbar",
							page: "/ngbootstrap/progressbar",
						},
						{
							title: "Rating",
							page: "/ngbootstrap/rating",
						},
						{
							title: "Tabs",
							page: "/ngbootstrap/tabs",
						},
						{
							title: "Timepicker",
							page: "/ngbootstrap/timepicker",
						},
						{
							title: "Tooltips",
							page: "/ngbootstrap/tooltip",
						},
						{
							title: "Typehead",
							page: "/ngbootstrap/typehead",
						},
					],
				},
				{ section: "Applications" },
				{
					title: "eCommerce",
					bullet: "dot",
					icon: "flaticon2-list-2",
					root: true,
					permission: "accessToECommerceModule",
					submenu: [
						{
							title: "Customers",
							page: "/ecommerce/customers",
						},
						{
							title: "Products",
							page: "/ecommerce/products",
						},
					],
				},
				{
					title: "User Management",
					root: true,
					bullet: "dot",
					icon: "flaticon2-user-outline-symbol",
					submenu: [
						{
							title: "Users",
							page: "/user-management/users",
						},
						{
							title: "Roles",
							page: "/user-management/roles",
						},
					],
				},
				{ section: "Custom" },
				{
					title: "Error Pages",
					root: true,
					bullet: "dot",
					icon: "flaticon2-list-2",
					submenu: [
						{
							title: "Error 1",
							page: "/error/error-v1",
						},
						{
							title: "Error 2",
							page: "/error/error-v2",
						},
						{
							title: "Error 3",
							page: "/error/error-v3",
						},
						{
							title: "Error 4",
							page: "/error/error-v4",
						},
						{
							title: "Error 5",
							page: "/error/error-v5",
						},
						{
							title: "Error 6",
							page: "/error/error-v6",
						},
					],
				},
				{
					title: "Wizard",
					root: true,
					bullet: "dot",
					icon: "flaticon2-mail-1",
					submenu: [
						{
							title: "Wizard 1",
							page: "/wizard/wizard-1",
						},
						{
							title: "Wizard 2",
							page: "/wizard/wizard-2",
						},
						{
							title: "Wizard 3",
							page: "/wizard/wizard-3",
						},
						{
							title: "Wizard 4",
							page: "/wizard/wizard-4",
						},
					],
				},
			],
		},
	};

	public get configs(): any {
		// console.log(this.defaults.header.items[3].title);
		// this.defaults.enableItems(this.defaults.header.items[3].title, false, false);
		return this.defaults;
	}
}
