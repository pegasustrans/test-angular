
let baseUrl = '';
let baseReportUrl = '';
let isDebug = false;
if (window.location.href.includes('localhost')) {
	baseUrl = 'http://localhost:5000/';
	baseReportUrl = 'http://127.0.0.1:8000/';
	isDebug = true;
}

if (window.location.href.includes('ps.pegatara')) {
	baseUrl = 'https://api.ps.pegatara.com/';
	baseReportUrl = 'https://report.ps.pegatara.com/';
}

if (window.location.href.includes('demo.pegatara')) {
	baseUrl = 'https://api.demo.pegatara.com/';
	baseReportUrl = 'https://report.demo.pegatara.com/';
}

	// baseUrl = 'http://localhost:5000/';
	// baseReportUrl = 'http://127.0.0.1:8000/';

export const AppGlobalVar = ({
	// BASE_API_URL: 'http://localhost:5000/',
	// BASE_API_URL: 'https://psapi.pegatara.com/',
	BASE_API_URL: baseUrl,
	BASE_REPORT_URL: baseReportUrl,


	// JWT
	ACCESS_TOKEN : null,

	// APP
	IS_DEBUG : isDebug,

	//status Entry IDPel, Notelp dan No HP di SPB 
	ENTRY_IDPEL_SENDER : 0, ENTRY_IDPEL_RECEIVER : 0,
	ENTRY_NOTELP_SENDER : 0, ENTRY_NOTELP_RECEIVER : 0,
	ENTRY_NOHP_SENDER : 0, ENTRY_NOHP_RECEIVER : 0,
	ENTRY_NAMA_SENDER : 0, ENTRY_NAMA_RECEIVER : 0

});
