export const UserGlobalVar = ({

	USER_ID : '',
	USERNAME : '',
	EMAIL : '',

	COMPANY_ID : '',
	COMPANY_NAME : '',
	COMPANY_ADDRESS  : '',

	WORK_TYPE : '',
	WORK_ID : '',
	WORK_NAME : '',
	WORK_ADDRESS : '',
	WORK_TIMEZONE : '',
	WORK_CITY_ID : '',
	WORK_AREA_ID : '',
	WORK_CITY_NAME : '',
	WORK_AREA_NAME : '',
	WORK_TIME_ZONE_HOUR: '',
	WORK_TIME_ZONE_MINUTE: '',

	MENU_KEY_LOGIN: [],
	MENU_ACTION_ID_LOGIN: [],
	MENU_ACTION_LOGIN: [],

	MENU_KEY_LOGIN_NAME: [],
	MENU_ACTION_LOGIN_NAME: []

});
