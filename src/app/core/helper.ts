import * as $ from 'jquery';

export function HighlightProcess(text, objectId) {
	/**
	 * used for highlight search keyword at select box
	 *
	 */
	const search = $('#' + objectId).find('.dx-texteditor-input').val().toString(); // .value

	if (search.length <= 0) { return text; }
	return text.toUpperCase().split(search.toUpperCase())
		.join(['<span class=\'kt-font-boldest kt-font-danger\'>', search.toUpperCase(), '</span>'].join(''));
}

