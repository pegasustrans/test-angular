import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryAksiComponent } from './history-aksi.component';

describe('HistoryAksiComponent', () => {
  let component: HistoryAksiComponent;
  let fixture: ComponentFixture<HistoryAksiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryAksiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryAksiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
