//import { Component, OnInit } from '@angular/core';
import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
//import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
//import {HifilterHistoryAksiPrintComponent} from '../HifilterHistoryAksi-print/HifilterHistoryAksi-print.component';
//import DataSource from 'devextreme/data/data_source';
//import {HifilterHistoryAksiPrintV2Component} from '../HifilterHistoryAksi-print-v2/HifilterHistoryAksi-print-v2.component';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
//import {FormControl, FormGroup} from '@angular/forms';
//import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';
import { UserManagementModule } from '../../user-management/user-management.module';

@Component({
  selector: 'kt-history-aksi',
  templateUrl: './history-aksi.component.html',
  styleUrls: ['./history-aksi.component.scss']
})
export class HistoryAksiComponent implements OnInit {

	//@ViewChild('targetDataGrid',{static: true}) dataGrid: DxDataGridComponent;
	ds: any = {};
	searchForm = {
		date: new Date(),
		dateString: null
		//name: String()
	};
  
	menuPriv = false;
	menuForb = true;

	HistoryForm = {
		menu: "History Aksi",
	};

  constructor(private http: HttpClient) { }
  
  ngOnInit() {
	this.checkuserGlobalVar();
	this.GetList();
	this.historyAksi();
}

historyAksi(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	  .post(AppGlobalVar.BASE_API_URL + 'history-aksi/create', JSON.stringify(this.HistoryForm), config)
	  .subscribe((data: any) => {

		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	);
}

private async checkuserGlobalVar() {
	if(UserGlobalVar.USER_ID === '') {
		setTimeout(() => {
			this.checkuserGlobalVar();
		}, 1000);
	}

	else{		
		var checkPriv = 0;
		await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "gmr-spb"){
				checkPriv = 1;
			}
		});

		if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
		
		}
	}
}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	GetList(){
		const params = {
			CreatedAt : this.searchForm.date.getFullYear() + '-' + (this.searchForm.date.getMonth() + 1) + '-' + this.searchForm.date.getDate(),
			//Username :  this.searchForm.name.toString()
		};
		this.ds = new CustomStore({
			key: 'username',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'history-aksi/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						if(data != null){
							return {
								data: data.data
							};
						}
					})
					.catch(error => { console.log(error); });
			},
		});
	}

	onFormSubmit(e) {
		this.GetList();
	}
}