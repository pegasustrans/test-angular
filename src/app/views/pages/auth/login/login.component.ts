// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// RxJS
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil, tap } from 'rxjs/operators';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Store
import { Store } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';
// Auth
import { AuthNoticeService, AuthService, Login } from '../../../../core/auth';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {HttpClient} from '@angular/common/http';

// APP

import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';
import {environment} from '../../../../../environments/environment';
import notify from 'devextreme/ui/notify';


/**
 * ! Just example => Should be removed in development
 */
const DEMO_PARAMS = {
	EMAIL: '',
	PASSWORD: ''
};

@Component({
	selector: 'kt-login',
	templateUrl: './login.component.html',
	encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy {
	// Public params
	loginForm: FormGroup;
	loading = false;
	isLoggedIn$: Observable<boolean>;
	errors: any = [];

	private unsubscribe: Subject<any>;

	private returnUrl: any;

	// Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param router: Router
	 * @param auth: AuthService
	 * @param authNoticeService: AuthNoticeService
	 * @param translate: TranslateService
	 * @param store: Store<AppState>
	 * @param fb: FormBuilder
	 * @param cdr
	 * @param route
	 * @param http
	 */
	constructor(
		private router: Router,
		private auth: AuthService,
		private authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private store: Store<AppState>,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute,
		private http: HttpClient
	) {
		this.unsubscribe = new Subject();

	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.initLoginForm();

		// redirect back to the returnUrl before login
		this.route.queryParams.subscribe(params => {
			this.returnUrl = params.returnUrl || '/';
		});
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.authNoticeService.setNotice(null);
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	/**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		// demo message to show
		if (!this.authNoticeService.onNoticeChanged$.getValue()) {
			const initialNotice = `Use account
			<strong>${DEMO_PARAMS.EMAIL}</strong> and password
			<strong>${DEMO_PARAMS.PASSWORD}</strong> to continue.`;
			this.authNoticeService.setNotice(initialNotice, 'info');
		}

		this.loginForm = this.fb.group({
			email: [DEMO_PARAMS.EMAIL, Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(320) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
			])
			],
			password: [DEMO_PARAMS.PASSWORD, Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(100)
			])
			]
		});
	}


	// csw
	submitPs() {
		const controls = this.loginForm.controls;
		this.loading = true;

		const formData: any = new FormData();
		formData.append('username', controls.email.value);
		formData.append('password', controls.password.value);


		// @ts-ignore
		return this.http
			.post(AppGlobalVar.BASE_API_URL + 'auth/login', formData)
			.subscribe((user: any) => {

				if(user == null){
					this.loading = false;
					this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');

					notify({
						message: 'Sign In Gagal',
						type: 'error',
						displayTime: 5000,
						width: 400,
					  });
				}else{
					if (user) {
						UserGlobalVar.USER_ID = user.userProfile.userId;
						UserGlobalVar.USERNAME = user.userProfile.username;
						UserGlobalVar.EMAIL = user.userProfile.email;
						UserGlobalVar.COMPANY_ID = user.userProfile.companyId;
						UserGlobalVar.COMPANY_NAME = user.userProfile.companyName;
						UserGlobalVar.COMPANY_ADDRESS = user.userProfile.companyAddress;
						UserGlobalVar.WORK_TYPE = user.userProfile.workType;
						UserGlobalVar.WORK_ID = user.userProfile.workId;
						UserGlobalVar.WORK_NAME = user.userProfile.workName;
						UserGlobalVar.WORK_ADDRESS = user.userProfile.workAddress;
						UserGlobalVar.WORK_TIMEZONE = user.userProfile.workTimeZone;
						UserGlobalVar.WORK_CITY_ID = user.userProfile.workCityId;
						UserGlobalVar.WORK_AREA_ID = user.userProfile.workAreaId;
						UserGlobalVar.WORK_CITY_NAME = user.userProfile.workCityName;
						UserGlobalVar.WORK_AREA_NAME = user.userProfile.workAreaName;

						UserGlobalVar.MENU_ACTION_ID_LOGIN = user.userProfile.menuActionIdLogin;
							
						UserGlobalVar.MENU_KEY_LOGIN = null;
						UserGlobalVar.MENU_KEY_LOGIN_NAME = null;
						UserGlobalVar.MENU_ACTION_LOGIN = null;
						UserGlobalVar.MENU_ACTION_LOGIN_NAME = null;


						if(user.userProfile.menuKeyLogin != null){
							UserGlobalVar.MENU_KEY_LOGIN = user.userProfile.menuKeyLogin;
							UserGlobalVar.MENU_KEY_LOGIN_NAME = [];

							var temp = [];
							UserGlobalVar.MENU_KEY_LOGIN.forEach(function(value){
								temp.push(value.split("~"));
							});

							temp.forEach(function(value){
								UserGlobalVar.MENU_KEY_LOGIN_NAME.push(value[1]);
							});
						}

						if(user.userProfile.menuActionLogin != null){
							
							UserGlobalVar.MENU_ACTION_LOGIN = user.userProfile.menuActionLogin;
							UserGlobalVar.MENU_ACTION_LOGIN_NAME = [];

							var temp2 = [];
							UserGlobalVar.MENU_ACTION_LOGIN.forEach(function(value){
								temp2.push(value.split("~"));
							})
							temp2.forEach(function(value){
								UserGlobalVar.MENU_ACTION_LOGIN_NAME.push(value[1]);
							});
						}

						UserGlobalVar.WORK_TIME_ZONE_HOUR = user.userProfile.workTimeZoneHour;
						UserGlobalVar.WORK_TIME_ZONE_MINUTE = user.userProfile.workTimeZoneMinute;

						AppGlobalVar.ACCESS_TOKEN = user.accessToken;

						localStorage.setItem('refreshToken', user.refreshToken);
						localStorage.setItem('accessToken', user.accessToken);

						this.store.dispatch(new Login({authToken: user.accessToken}));
						this.router.navigateByUrl(this.returnUrl); // Main page

						this.cdr.markForCheck();
					} else {
						this.loading = false;
						this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
					}
				}
				this.loading = false;
			}, (e) => { this.loading = false;  }
			, () => {	this.loading = false;  }
			);

	}

	/**
	 * Form Submit
	 */
	submit() {
		const controls = this.loginForm.controls;
		/** check form */
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;

		const authData = {
			email: controls.email.value,
			password: controls.password.value
		};
		this.auth
			.login(authData.email, authData.password)
			.pipe(
				tap(user => {
					console.log(user);
					if (user) {
						// this.store.dispatch(new Login({authToken: user.accessToken}));
						this.store.dispatch(new Login({authToken: 'asdasdasdasdasdasd'}));
						this.router.navigateByUrl(this.returnUrl); // Main page
					} else {
						this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
					}
				}),
				takeUntil(this.unsubscribe),
				finalize(() => {
					this.loading = false;
					this.cdr.markForCheck();
				})
			)
			.subscribe();
	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}


	// csw
	/**
	 *
	 * every open login page, wajib check the refreshtoken
	 */
	// RefreshToken() {
	// 	// get refresh token
	// 	const refreshToken = localStorage.getItem('refreshToken');
	//
	// 	// return if refreshToken is null
	// 	if (refreshToken === null) { console.log('RefreshToken kosong'); return;}
	//
	// 	// get new token
	// 	// @ts-ignore
	// 	this.http
	// 		.post(AppGlobalVar.BASE_API_URL + 'auth/refresh-token/' + refreshToken)
	// 		.subscribe((user: any) => {
	// 				if (user) {
	// 					UserGlobalVar.USERNAME = user.userProfile.username;
	// 					AppGlobalVar.ACCESS_TOKEN = user.accessToken;
	// 					localStorage.setItem('refreshToken', user.refreshToken);
	// 					this.store.dispatch(new Login({authToken: user.accessToken}));
	// 					this.router.navigateByUrl(this.returnUrl); // Main page
	//
	// 					this.cdr.markForCheck();
	// 				} else {
	// 					this.loading = false;
	// 					this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
	// 				}
	// 				this.loading = false;
	// 			}, (e) => { this.loading = false;  }
	// 			, () => {	this.loading = false;  }
	// 		);
	//
	//
	// 	// if refresh token is invalid then remove refresh token from local strage
	// }
}
