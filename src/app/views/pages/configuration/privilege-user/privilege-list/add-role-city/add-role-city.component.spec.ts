import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRoleCityComponent } from './add-role-city.component';

describe('AddRoleCityComponent', () => {
  let component: AddRoleCityComponent;
  let fixture: ComponentFixture<AddRoleCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRoleCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRoleCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
