import { Component, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';
import DataSource from 'devextreme/data/data_source';

@Component({
  selector: 'kt-add-role-city',
  templateUrl: './add-role-city.component.html',
  styleUrls: ['./add-role-city.component.scss']
})
export class AddRoleCityComponent implements OnInit {
  appGlobalVar  = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  // gridColumns: any = ['masterCityId', 'masterCityCode', 'masterCityType', 'masterCityName', 'masterCityCapital'];
  isGridBoxOpened: boolean;

  privilegeId   : any;
  privilegeName : any;
  type          : any;
  roleId        : any;
  rolesName     : any;
  desc          : any;
  cityId        : any;

  cityDs        : any;
  gridBoxValue  : any;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
		})
  };

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.type           = params.p1;         
        this.privilegeId    = params.p2         
        this.privilegeName  = params.p3;         
        this.roleId         = params.p4;         
        this.rolesName      = params.p5;         
        this.desc           = params.p6;         
      }
    );

    this.listCity();
  }

  listCity() {
		const params = {
		};
    this.cityDs = new DataSource({
			store: new CustomStore({
				key: 'masterCityId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'roles/list-city', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  onSelectionChanged (e){
    this.isGridBoxOpened = false;
  }

  onValueChanged(e) {
    // console.log();
    e.value.forEach((item) => {
      this.cityId = item;
      
    });

  }

  onSave(){
    const params = { 
      cityId    : this.cityId 
      , mode    : 'add'
      , type    : this.type
      , roleId  : parseInt(this.roleId)
    };

    console.log(params);
    
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'roles/save-city', params, config)
      .subscribe((data: any) => {
        
        notify({
          message: 'Data telah di Tambahkan',
          type: 'success',
          displayTime: 5000,
          width: 400,
        });

        setTimeout (() => {
          this.onBack();
       }, 2000);
        

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  gridBox_displayExpr(item){
    return item && item.masterCityId + " <" + item.masterCityCode + ">";
  }

  onBack(){
      this.router.navigate(['/detail-role-list'], 
          { 
              queryParams: { 
                p1: this.privilegeId,
                p2: this.roleId,
                p3: this.rolesName,
                p4: this.desc,
                p5: this.privilegeName
              } 
          });
  }

}
