import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteRoleCityComponent } from './delete-role-city.component';

describe('DeleteRoleCityComponent', () => {
  let component: DeleteRoleCityComponent;
  let fixture: ComponentFixture<DeleteRoleCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteRoleCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteRoleCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
