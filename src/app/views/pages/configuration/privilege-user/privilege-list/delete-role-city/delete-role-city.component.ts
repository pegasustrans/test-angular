import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-delete-role-city',
  templateUrl: './delete-role-city.component.html',
  styleUrls: ['./delete-role-city.component.scss']
})
export class DeleteRoleCityComponent implements OnInit {
  appGlobalVar  = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  privilegeId   : any;
  privilegeName : any;
  cityId        : any;
  cityName      : any;
  type          : any;
  roleId        : any;
  rolesName     : any;
  desc          : any;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.cityId         = params.p1;         
        this.cityName       = params.p2;         
        this.type           = params.p3;         
        this.privilegeId    = params.p4;         
        this.privilegeName  = params.p5;         
        this.roleId         = params.p6;         
        this.rolesName      = params.p7;         
        this.desc           = params.p8;         
      }
    );
  }

  onDelete(){
    const params = { 
      cityId    : this.cityId 
      , mode    : 'remove'
      , type    : this.type
      , roleId  : parseInt(this.roleId)
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'roles/save-city', params, config)
      .subscribe((data: any) => {
        
        notify({
          message: 'Data telah Hapus',
          type: 'success',
          displayTime: 5000,
          width: 400,
        });

        setTimeout (() => {
          this.onBack();
       }, 2000);
        

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  onBack(){
    this.router.navigate(['/detail-role-list'], 
        { 
            queryParams: { 
               p1: this.privilegeId,
               p2: this.roleId,
               p3: this.rolesName,
               p4: this.desc,
               p5: this.privilegeName
            } 
        });
  }

}
