import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPrivilegeSettingComponent } from './detail-privilege-setting.component';

describe('DetailPrivilegeSettingComponent', () => {
  let component: DetailPrivilegeSettingComponent;
  let fixture: ComponentFixture<DetailPrivilegeSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPrivilegeSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPrivilegeSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
