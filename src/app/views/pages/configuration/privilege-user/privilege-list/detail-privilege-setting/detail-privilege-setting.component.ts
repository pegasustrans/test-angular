import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-detail-privilege-setting',
  templateUrl: './detail-privilege-setting.component.html',
  styleUrls: ['./detail-privilege-setting.component.scss']
})
export class DetailPrivilegeSettingComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  menuActionsData = [];

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  privilegeId : any;
  privilegeName : any;
  menuId : any;
  menuName : any;

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        console.log(params);        
        this.privilegeId    = params.p1;         
        this.privilegeName  = params.p2;         
        this.menuId         = params.p3;         
        this.menuName       = params.p4;         
      }
    );

    this.getMenuDetails();
  }

  getMenuDetails() {
    this.menuActionsData = [];
    const params = { 
      privilegeId : parseInt(this.privilegeId),
      menuId : parseInt(this.menuId)
     };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/list-menu-detail', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.menuActionsData.push(item);
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }


  checkBox_valueChanged (e, id, val) {
    // console.log(e.value, id, val);
    var action = "remove";
    if(e.value == true){
      action = "create";
    }
    
    this.SetDataValue(action, id, val);
  }

  SetDataValue(action, id, location){
    const params = { 
          privilegeId : parseInt(this.privilegeId) 
          , action    : action
          , id        : id
          , location  : location
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/manage-data', params, config)
      .subscribe((data: any) => {
        
        console.log(data.data);
        console.log(data.data[0].result);
        notify({
          message: 'Data telah Update',
          type: 'success',
          displayTime: 5000,
          width: 400,
        });
        

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  onBack(){
    this.router.navigate(['/privilege-setting'], 
        { 
            queryParams: { 
               p1: this.privilegeId,
               p2: this.privilegeName
            } 
        });
  }

}
