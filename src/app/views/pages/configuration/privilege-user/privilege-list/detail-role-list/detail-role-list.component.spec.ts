import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailRoleListComponent } from './detail-role-list.component';

describe('DetailRoleListComponent', () => {
  let component: DetailRoleListComponent;
  let fixture: ComponentFixture<DetailRoleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailRoleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailRoleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
