import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-detail-role-list',
  templateUrl: './detail-role-list.component.html',
  styleUrls: ['./detail-role-list.component.scss']
})
export class DetailRoleListComponent implements OnInit {
  appGlobalVar  = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  dsRoleUserList: any = {};

  privilegeId   : any;
  privilegeName : any;
  rolesId       : any;
  rolesName     : any;
  desc          : any;

  kotaTujuanData = [];
  kotaBlokirData = [];

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.privilegeId    = params.p1;         
        this.rolesId        = params.p2;         
        this.rolesName      = params.p3;         
        this.desc           = params.p4;         
        this.privilegeName  = params.p5;         
      }
    );

    this.getTujuan();
    this.getBlokir();
  }

  getTujuan() {
    this.kotaTujuanData = [];
    const params = { rolesId : parseInt(this.rolesId) };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/list-kota-tujuan', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.kotaTujuanData.push(item);
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  getBlokir() {
    this.kotaBlokirData = [];
    const params = { rolesId : parseInt(this.rolesId) };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/list-kota-blokir', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.kotaBlokirData.push(item);
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }
  
  onBack(){
    if(this.privilegeId == 0){
      this.router.navigate(['privilege-user-list']);
    }else{
      this.router.navigate(['/role-user-list'], 
        { 
            queryParams: { 
               p1: this.privilegeId,
               p2: this.privilegeName
            } 
        });
    }
  }

  onDeleteCity(val, nm, type){
    this.router.navigate(['/delete-role-city'], 
        { 
            queryParams: { 
              p1 : val,
              p2 : nm,
              p3 : type,
              p4 : this.privilegeId,
              p5 : this.privilegeName,
              p6 : this.rolesId,
              p7 : this.rolesName,
              p8 : this.desc
            } 
        });
        
  }

  onAddCity(type){
    this.router.navigate(['/add-role-city'], 
        { 
            queryParams: { 
              p1 : type,
              p2 : this.privilegeId,
              p3 : this.privilegeName,
              p4 : this.rolesId,
              p5 : this.rolesName,
              p6 : this.desc
            } 
        });
  }
  
}
