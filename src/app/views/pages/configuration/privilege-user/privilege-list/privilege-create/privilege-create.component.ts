import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-privilege-create',
  templateUrl: './privilege-create.component.html',
  styleUrls: ['./privilege-create.component.scss']
})
export class PrivilegeCreateComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  formCreate = {
		privilegeName : null,
	};

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
  }

  onSave(){
   
    if(this.formCreate.privilegeName == '' || this.formCreate.privilegeName == null){ 
      notify({
        message: 'Privilege Name tidak boleh kosong',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    }else{
      const params = {
        privilegeName : this.formCreate.privilegeName
      };

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
        .post(AppGlobalVar.BASE_API_URL + 'privilege/create-privilege', params, config)
        .subscribe((data: any) => {

              if(data.data[0].result == 'false'){
                notify({
                  message: data.data[0].message,
                  type: 'error',
                  displayTime: 5000,
                  width: 400,
                });
              }else{
                this.router.navigate(['/privilege-setting'], 
                  { 
                      queryParams: { 
                        p1: parseInt(data.data[0].message),
                        p3: this.formCreate.privilegeName, 
                      } 
                  });
              }
          },
          (error) => {
            alert(error.statusText + '. ' + error.message);
          }
        );
    }
    
  }

  onBack(){
    this.router.navigate(['/privilege-user-list']);
  }

}
