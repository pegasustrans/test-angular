import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { Router } from '@angular/router';

@Component({
  selector: 'kt-privilege-list',
  templateUrl: './privilege-list.component.html',
  styleUrls: ['./privilege-list.component.scss']
})
export class PrivilegeListComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;

  dsPrivilegeList: any = {};

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.GetList();
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	GetList() {
		this.dsPrivilegeList = new CustomStore({
			key: 'privilegeId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege/list')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  detailDataPrivilege = (e) => {
	//   console.log(e.row.data.privilegeId);
	  
		this.router.navigate(['/privilege-setting'], 
        { 
            queryParams: { 
               p1: e.row.data.privilegeId,
               p2: e.row.data.privilegeName
            } 
        });
	}

  detailDataUser = (e) => {
		this.router.navigate(['/role-user-list'], 
        { 
            queryParams: { 
				p1: e.row.data.privilegeId,
				p2: e.row.data.privilegeName
            } 
        });
	}

addPrivilegeData(){
	this.router.navigate(['/privilege-create'], 
	{ 
		queryParams: { 
		   p1: 'privilege'
		} 
	});
}

}
