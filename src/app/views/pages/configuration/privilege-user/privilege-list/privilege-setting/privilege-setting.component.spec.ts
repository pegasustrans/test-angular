import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegeSettingComponent } from './privilege-setting.component';

describe('PrivilegeSettingComponent', () => {
  let component: PrivilegeSettingComponent;
  let fixture: ComponentFixture<PrivilegeSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegeSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegeSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
