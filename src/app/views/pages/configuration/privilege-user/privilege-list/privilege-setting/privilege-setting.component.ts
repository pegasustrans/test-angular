import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import { L } from '@angular/cdk/keycodes';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-privilege-setting',
  templateUrl: './privilege-setting.component.html',
  styleUrls: ['./privilege-setting.component.scss']
})

export class PrivilegeSettingComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  menusData = [];
  printsData = [];
  
  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  privilegeId   : any;
  privilegeName : any;
  type          : any;

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        // console.log(params);        
        this.privilegeId    = params.p1;         
        this.privilegeName  = params.p2;         
      }
    );

    this.getMenus();
    this.getPrints();
    
  }

  getMenus() {
    this.menusData = [];
    const params = { privilegeId : parseInt(this.privilegeId) };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/list-menu', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.menusData.push(item);
        });
        // console.log('ini data menu');
        
        // console.log(this.menusData);

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  getPrints(){
    this.printsData = [];
    const params = { privilegeId : parseInt(this.privilegeId) };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/list-print-spb', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.printsData.push(item);
        });

        // console.log('ini data print');
        
        // console.log(this.printsData);

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  checkBox_valueChanged (e, id, val) {
    var action = "remove";
    if(e.value == true){
      action = "create";
    }

    // console.log(action, e.value, id, val);
    this.SetDataValue(action, id, val);
  }

  SetDataValue(action, id, location){
    const params = { 
          privilegeId : parseInt(this.privilegeId) 
          , action    : action
          , id        : id
          , location  : location
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/manage-data', params, config)
      .subscribe((data: any) => {
        
        console.log(data.data[0].result);
        notify({
          message: 'Data telah Update',
          type: 'success',
          displayTime: 5000,
          width: 400,
        });
        

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  onBack(){
    this.router.navigate(['privilege-user-list']);
  }

  onDetailMenu(val, nm){
    this.router.navigate(['/detail-privilege-setting'], 
        { 
            queryParams: { 
              p1 : this.privilegeId,
              p2 : this.privilegeName,
              p3 : val,
              p4 : nm
            } 
        });
  }
}
