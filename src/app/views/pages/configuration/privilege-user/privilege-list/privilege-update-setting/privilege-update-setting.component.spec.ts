import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegeUpdateSettingComponent } from './privilege-update-setting.component';

describe('PrivilegeUpdateSettingComponent', () => {
  let component: PrivilegeUpdateSettingComponent;
  let fixture: ComponentFixture<PrivilegeUpdateSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegeUpdateSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegeUpdateSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
