import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-privilege-update-setting',
  templateUrl: './privilege-update-setting.component.html',
  styleUrls: ['./privilege-update-setting.component.scss']
})
export class PrivilegeUpdateSettingComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  dataItems = [];
  selectionPriorityId: number;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  privilegeId : any;
  privilegeName : any;
  rolesId : any;
  userId : any;
  userName : any;
  fullName : any;
  halaman : any;

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.privilegeId    = params.p1;         
        this.rolesId        = params.p2;         
        this.userId         = params.p3;         
        this.userName       = params.p4;         
        this.fullName       = params.p5;         
        this.privilegeName  = params.p6;   
        this.halaman        = params.p7;
      }
    );

    this.getPrivilege(this.privilegeId);
  }

  getPrivilege(privilegeId) {
    this.dataItems = [];
    const params = {};

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/list-item', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.dataItems.push(item);
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
    
    this.selectionPriorityId = parseInt(this.privilegeId);
  }

  onValueChanged($event){
    const params = { 
      privilegeId : $event.value
      , userId    : parseInt(this.userId) 
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/update-data', params, config)
      .subscribe((data: any) => {
        
        console.log(data.data);
        notify({
          message: 'Data telah Update',
          type: 'success',
          displayTime: 5000,
          width: 400,
        });

        setTimeout (() => {
          this.onBack();
       }, 2000);
        

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    ); 
  }

  onBack(){
    if(this.halaman == 'detail'){
      const params = { 
        userId    : parseInt(this.userId) 
      };
  
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
        .post(AppGlobalVar.BASE_API_URL + 'users/check-detail-user', params, config)
        .subscribe((data: any) => {
          console.log(data.data[0]);
          this.router.navigate(['/user-detail'], 
            { 
                queryParams: { 
                  p1: data.data[0].userId,
                  p2: data.data[0].username,
                  p3: data.data[0].workPlace,
                  p4: data.data[0].workUnit,
                  p5: data.data[0].email,
                  p6: data.data[0].fullName,
                  p7: data.data[0].isActive,
                  p8: data.data[0].privilegeId,
                  p9: data.data[0].privilegeName,
                  p10: data.data[0].rolesId,
                  p11: data.data[0].rolesName,
                  p12: data.data[0].description, 
                  p13: data.data[0].status
                } 
            });
        },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
      ); 
    }else{
      this.router.navigate(['/role-user-list'], 
      { 
          queryParams: { 
             p1: this.privilegeId,
             p2: this.privilegeName
          } 
      });
    }
   
  }

}
