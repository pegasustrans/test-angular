import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-role-user-list',
  templateUrl: './role-user-list.component.html',
  styleUrls: ['./role-user-list.component.scss']
})
export class RoleUserListComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  dsRoleUserList: any = {};

  privilegeId : any;
  privilegeName : any;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        console.log(params);        
        this.privilegeId    = params.p1;         
        this.privilegeName  = params.p2;         
      }
    );

    this.GetList();
  }

  GetList() {
    const params = {
			privilegeId: this.privilegeId
		}; 

		this.dsRoleUserList = new CustomStore({
			key: 'privilegeId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege/list-role-user', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  detailDataRoles = (e) => {
    //   console.log(e.row.data.privilegeId);
      
    console.log(e.row.data);
    
      this.router.navigate(['/detail-role-list'], 
          { 
              queryParams: { 
                 p1: e.row.data.privilegeId,
                 p2: e.row.data.rolesId,
                 p3: e.row.data.rolesName,
                 p4: e.row.data.desc,
                 p5: this.privilegeName
              } 
          });
    }

    updateRoles = (e) => {
      //   console.log(e.row.data.privilegeId);
        
      console.log(e.row.data);
      
        this.router.navigate(['/role-user-setting'], 
            { 
                queryParams: { 
                   p1: e.row.data.privilegeId,
                   p2: e.row.data.rolesId,
                   p3: e.row.data.userId,
                   p4: e.row.data.username,
                   p5: e.row.data.fullName,
                   p6: this.privilegeName,
                   p7: 'role-user'
                } 
            });
      }

      updatePrivilege = (e) => {
        //   console.log(e.row.data.privilegeId);
          
        console.log(e.row.data);
        
          this.router.navigate(['/privilege-update-setting'], 
              { 
                  queryParams: { 
                     p1: e.row.data.privilegeId,
                     p2: e.row.data.rolesId,
                     p3: e.row.data.userId,
                     p4: e.row.data.username,
                     p5: e.row.data.fullName,
                     p6: this.privilegeName,
                     p7: 'role-user'
                  } 
              });
        }  

    onBack(){
      this.router.navigate(['/privilege-user-list']);
    }

}
