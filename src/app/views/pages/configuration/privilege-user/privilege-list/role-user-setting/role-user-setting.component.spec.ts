import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleUserSettingComponent } from './role-user-setting.component';

describe('RoleUserSettingComponent', () => {
  let component: RoleUserSettingComponent;
  let fixture: ComponentFixture<RoleUserSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleUserSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleUserSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
