import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegeUserListComponent } from './privilege-user-list.component';

describe('PrivilegeUserListComponent', () => {
  let component: PrivilegeUserListComponent;
  let fixture: ComponentFixture<PrivilegeUserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegeUserListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegeUserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
