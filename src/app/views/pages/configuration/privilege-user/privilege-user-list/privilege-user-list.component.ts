import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-privilege-user-list',
  templateUrl: './privilege-user-list.component.html',
  styleUrls: ['./privilege-user-list.component.scss']
})
export class PrivilegeUserListComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
  
  constructor(private http: HttpClient) { }

  HistoryForm = {
	menu			: "Konfigurasi",
	historyloginId 	: 0
};


  	menuPriv = false;
	menuForb = true;


	ngOnInit() {
		this.checkuserGlobalVar();
		this.historyLoginStart();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	
	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "privilege-master"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;
			
			}
		}
	}

}
