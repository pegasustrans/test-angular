import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.scss']
})
export class RoleCreateComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  formCreate = {
		rolesName   : null,
		description : null,
	};

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
  }

  onSave(){
   
    if(this.formCreate.rolesName == '' || this.formCreate.rolesName == null){ 
      notify({
        message: 'Roles tidak boleh kosong',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    } else if(this.formCreate.description == '' || this.formCreate.description == null){
      notify({
        message: 'Keterangan tidak boleh kosong',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    }else{
      const params = {
        rolesName   : this.formCreate.rolesName,
        description : this.formCreate.description
      };

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
        .post(AppGlobalVar.BASE_API_URL + 'roles/create-roles', params, config)
        .subscribe((data: any) => {

          if(data.data[0].result == 'false'){
            notify({
              message: data.data[0].message,
              type: 'error',
              displayTime: 5000,
              width: 400,
            });
          }else{
            notify({
              message: data.data[0].message,
              type: 'success',
              displayTime: 5000,
              width: 400,
            });
          }

        },
          (error) => {
            alert(error.statusText + '. ' + error.message);
          }
        );
    }
    
  }

  onBack(){
    this.router.navigate(['/privilege-user-list']);
  }

}
