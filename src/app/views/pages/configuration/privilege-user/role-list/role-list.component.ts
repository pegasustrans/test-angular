import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { Router } from '@angular/router'

@Component({
  selector: 'kt-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss']
})
export class RoleListComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;

  dsRoleList: any = {};

  constructor(private http: HttpClient, private router: Router) { }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.GetList();
  }

  GetList() {
		this.dsRoleList = new CustomStore({
			key: 'rolesId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'roles/list-data')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  detailRole = (e) => {    
    this.router.navigate(['/detail-role-list'], 
        { 
            queryParams: { 
                p1: 0,                
                p2: e.row.data.rolesId,
                p3: e.row.data.rolesName,
                p4: e.row.data.description,
                p5: 0,
            } 
        });
  }

  detailUser = (e) => {
    this.router.navigate(['/user-role-list'], 
        { 
            queryParams: { 
              p1: e.row.data.rolesId,
              p2: e.row.data.rolesName
            } 
        });
  }

  addRoleData(){
		this.router.navigate(['/role-create']);
	}

}
