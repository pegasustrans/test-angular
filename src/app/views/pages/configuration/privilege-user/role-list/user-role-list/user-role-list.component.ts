import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-user-role-list',
  templateUrl: './user-role-list.component.html',
  styleUrls: ['./user-role-list.component.scss']
})
export class UserRoleListComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  dsRoleUserList: any = {};

  rolesId   : any;
  rolesName : any;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.rolesId    = params.p1;         
        this.rolesName  = params.p2;         
      }
    );

    this.GetList();
  }

  GetList() {
    const params = {
			rolesId: this.rolesId
		}; 

		this.dsRoleUserList = new CustomStore({
			key: 'rolesId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'roles/role-user-list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

    // updateRoles = (e) => {
    //   //   console.log(e.row.data.privilegeId);
        
    //   console.log(e.row.data);
      
    //     this.router.navigate(['/role-user-setting'], 
    //         { 
    //             queryParams: { 
    //                p1: e.row.data.privilegeId,
    //                p2: e.row.data.rolesId,
    //                p3: e.row.data.userId,
    //                p4: e.row.data.username,
    //                p5: e.row.data.fullName,
    //                p6: this.privilegeName
    //             } 
    //         });
    //   }

    //   updatePrivilege = (e) => {
    //     //   console.log(e.row.data.privilegeId);
          
    //     console.log(e.row.data);
        
    //       this.router.navigate(['/privilege-update-setting'], 
    //           { 
    //               queryParams: { 
    //                  p1: e.row.data.privilegeId,
    //                  p2: e.row.data.rolesId,
    //                  p3: e.row.data.userId,
    //                  p4: e.row.data.username,
    //                  p5: e.row.data.fullName,
    //                  p6: this.privilegeName
    //               } 
    //           });
    //     }  

    onBack(){
      this.router.navigate(['/privilege-user-list']);
    }

}