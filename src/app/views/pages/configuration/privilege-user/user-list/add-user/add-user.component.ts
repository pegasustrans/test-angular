import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  unitDs: any;

  formCreate = {
		username : null,
		fullname : null,
		email    : null,
		unitId   : null,
	};

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
    this.readDataUnit();
  }

  readDataUnit() {
    const params = { type : 'WorkUnit' };

		this.unitDs = new DataSource({
			store: new CustomStore({
				key: 'id',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'users/list-unit', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  onSave(){
   
    if(this.formCreate.username == '' || this.formCreate.username == null){ 
      notify({
        message: 'Username tidak boleh kosong',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    } else if(this.formCreate.fullname == '' || this.formCreate.fullname == null){
      notify({
        message: 'Nama Lengkap tidak boleh kosong',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    }else if(this.formCreate.email == '' || this.formCreate.email == null){
      notify({
        message: 'Email tidak boleh kosong',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    }else if(this.formCreate.unitId == '' || this.formCreate.unitId == null){
      notify({
        message: 'Unit Kerja tidak boleh kosong',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    }else{
      const params = {
        username : this.formCreate.username,
        fullname : this.formCreate.fullname,
        email    : this.formCreate.email,
        unitId   : this.formCreate.unitId
      };

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
        .post(AppGlobalVar.BASE_API_URL + 'users/create-user', params, config)
        .subscribe((data: any) => {

          if(data.data[0].result == 'false'){
            notify({
              message: data.data[0].message,
              type: 'error',
              displayTime: 5000,
              width: 400,
            });
          }else{
            this.UnitValue(this.formCreate.unitId, data.data[0].message);
          }

        },
          (error) => {
            alert(error.statusText + '. ' + error.message);
          }
        );
    }
    
  }

  UnitValue(unitId, userId){
    const params = {
      username : this.formCreate.username,
      fullname : this.formCreate.fullname,
      email    : this.formCreate.email,
      unitId   : unitId
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/unit-value', params, config)
      .subscribe((data: any) => {
          if(data.data[0].value == "Company"){
              this.router.navigate(['/update-unit-company'], 
              { 
                  queryParams: { 
                    p1: userId,
                    p2: this.formCreate.username,
                    p3: this.formCreate.fullname, 
                    p4: unitId,
                  } 
              });
          }else if(data.data[0].value == "Branch"){
            this.router.navigate(['/update-unit-branch'], 
              { 
                  queryParams: { 
                    p1: userId,
                    p2: this.formCreate.username,
                    p3: this.formCreate.fullname, 
                    p4: unitId,
                    p5: data.data[0].value,
                    p6: 0
                  } 
              });
          }else if(data.data[0].value == "SubBranch"){
            this.router.navigate(['/update-unit-branch'], 
              { 
                  queryParams: { 
                    p1: userId,
                    p2: this.formCreate.username,
                    p3: this.formCreate.fullname, 
                    p4: unitId,
                    p5: data.data[0].value,
                    p6: 0
                  } 
              });
          }

      },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
      );
  }

  onBack(){
    this.router.navigate(['/privilege-user-list']);
  }

}
