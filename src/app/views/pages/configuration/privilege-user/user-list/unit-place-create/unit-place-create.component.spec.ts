import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitPlaceCreateComponent } from './unit-place-create.component';

describe('UnitPlaceCreateComponent', () => {
  let component: UnitPlaceCreateComponent;
  let fixture: ComponentFixture<UnitPlaceCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitPlaceCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitPlaceCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
