import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitSubbranchPlaceCreateComponent } from './unit-subbranch-place-create.component';

describe('UnitSubbranchPlaceCreateComponent', () => {
  let component: UnitSubbranchPlaceCreateComponent;
  let fixture: ComponentFixture<UnitSubbranchPlaceCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitSubbranchPlaceCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitSubbranchPlaceCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
