import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-unit-subbranch-place-create',
  templateUrl: './unit-subbranch-place-create.component.html',
  styleUrls: ['./unit-subbranch-place-create.component.scss']
})
export class UnitSubbranchPlaceCreateComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  cityDs: any;
  subDistrictDs: any;

  formCreate = {
		cityId          : null,
		subDistrictId   : null,
		unitPlaceCode   : null,
		unitPlaceName   : null,
		unitPlaceAddress: null,
    unitBranchValue : null,
		timeZone        : null,
	};

  userId              : any;
  username            : any;
  fullname            : any;
  unitId              : any;
  unitName            : any;
  unitPlaceId         : any;
  unitBranchId        : any;
  unitBranchName      : any;
  unitSubBranchId     : any;
  unitBranchPlaceName : any;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
    this.formCreate.timeZone = '07:00';

    this.route.queryParams
      .subscribe(params => {
        this.userId               = params.p1;         
        this.username             = params.p2;         
        this.fullname             = params.p3;         
        this.unitId               = params.p4;         
        this.unitName             = params.p5;         
        this.unitPlaceId          = params.p6;       
        this.unitBranchId         = params.p7;       
        this.unitBranchName       = params.p8;       
        this.unitBranchPlaceName  = params.p9;       
      }
    );
    this.getDataCity();

    this.formCreate.unitBranchValue = this.unitBranchPlaceName;
  }

  getDataCity(){
    const params = { type : 'City' };

		this.cityDs = new DataSource({
			store: new CustomStore({
				key: 'id',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'users/list-city', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onTriggerChange(e){
    this.getDataSubDistrict(e.value);
  }

  getDataSubDistrict(cityId){
    const params = { type : 'SubDistrict', cityId : cityId };

		this.subDistrictDs = new DataSource({
			store: new CustomStore({
				key: 'id',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'users/list-subdistrict', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onSave(){
    const params = {
      cityId            : this.formCreate.cityId,
      subDistrictId     : this.formCreate.subDistrictId,
      unitBranchId      : parseInt(this.unitBranchId),
      unitPlaceCode     : this.formCreate.unitPlaceCode,
      unitPlaceName     : this.formCreate.unitPlaceName,
      unitPlaceAddress  : this.formCreate.unitPlaceAddress,
      timeZone          : this.formCreate.timeZone,
      unitId            : parseInt(this.unitId)       
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/create-unit-subbranch-place', params, config)
      .subscribe((data: any) => {
          
        if(data.data[0].result == 'false'){
          notify({
            message: data.data[0].message,
            type: 'error',
            displayTime: 5000,
            width: 400,
          });
        }else{
          this.router.navigate(['/update-unit-subbranch'], 
          { 
              queryParams: { 
                p1: this.userId,
                p2: this.username,
                p3: this.fullname, 
                p4: this.unitId,
                p5: this.unitBranchName,
                p6: 'SubBranch',
                p7: this.unitBranchId,
                p8: data.data[0].message
              } 
          });
        }

      },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
      );
  }

  onBack(){
    this.router.navigate(['/privilege-user-list']);
  }

}
