import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRolePrivilegeComponent } from './update-role-privilege.component';

describe('UpdateRolePrivilegeComponent', () => {
  let component: UpdateRolePrivilegeComponent;
  let fixture: ComponentFixture<UpdateRolePrivilegeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRolePrivilegeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRolePrivilegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
