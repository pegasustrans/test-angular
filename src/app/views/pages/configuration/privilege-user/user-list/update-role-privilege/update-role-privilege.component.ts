import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-update-role-privilege',
  templateUrl: './update-role-privilege.component.html',
  styleUrls: ['./update-role-privilege.component.scss']
})
export class UpdateRolePrivilegeComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  userId         : any;
  username       : any;
  fullname       : any;
  unitId         : any;
  unitName       : any;
  privilegeId    : any;
  rolesId        : any;
  unitPlaceId    : any;

  privilegeItems = [];
  rolesItems = [];
  selectionPrivilegeId: number;
  selectionRolesId: number;

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.userId       = params.p1;         
        this.username     = params.p2;         
        this.fullname     = params.p3;         
        this.unitId       = params.p4;         
        this.unitName     = params.p5;         
        this.privilegeId  = params.p6;         
        this.rolesId      = params.p7;         
        this.unitPlaceId  = params.p8;         
      }
    );

    this.getPrivilege(this.privilegeId);
    this.getRoles(this.rolesId);
  }

  getPrivilege(privilegeId) {
    this.privilegeItems = [];
    const params = {};

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege/list-item', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.privilegeItems.push(item);
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
    
    this.selectionPrivilegeId = parseInt(privilegeId);
  }

  getRoles(rolesId) {
    this.rolesItems = [];
    const params = {};

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'roles/list', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.rolesItems.push(item);
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
    
    this.selectionRolesId = parseInt(rolesId);
  }

  onValuePrivilegeChanged($event){
    // console.log($event)

    this.privilegeId = $event.value;
    
    const params = { 
      unitId        : parseInt(this.unitId) 
      , userId      : parseInt(this.userId) 
      , privilegeId : parseInt(this.privilegeId) 
      , rolesId     : parseInt(this.rolesId) 
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/create-privilege-role', params, config)
      .subscribe((data: any) => {
        
       if(data.data[0].result == 'true'){
        
        notify({
          message: data.data[0].message,
          type: 'success',
          displayTime: 5000,
          width: 400,
        });

       }else{
        notify({
          message: data.data[0].message,
          type: 'danger',
          displayTime: 5000,
          width: 400,
        });
       }
        

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );      
  }

  onValueRolesChanged($event){
    // console.log($event);
    this.rolesId = $event.value;
    
    const params = { 
      unitId        : parseInt(this.unitId) 
      , userId      : parseInt(this.userId) 
      , privilegeId : parseInt(this.privilegeId) 
      , rolesId     : parseInt(this.rolesId) 
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/create-privilege-role', params, config)
      .subscribe((data: any) => {
        
       if(data.data[0].result == 'true'){
        
        notify({
          message: data.data[0].message,
          type: 'success',
          displayTime: 5000,
          width: 400,
        });

       }else{
        notify({
          message: data.data[0].message,
          type: 'danger',
          displayTime: 5000,
          width: 400,
        });
       }
        

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );      
  }

  onFinish(){
    console.log('Privilege id', this.privilegeId);
    console.log('Roles id', this.rolesId);
    
    if(this.privilegeId == 0){
        notify({
          message: 'Privilege Belum di Pilih',
          type: 'error',
          displayTime: 5000,
          width: 400,
        });
    }else if(this.rolesId == 0){
        notify({
          message: 'Roles Belum di Pilih',
          type: 'error',
          displayTime: 5000,
          width: 400,
        });
    }else{
      this.router.navigate(['/privilege-user-list']);
    }
  }

  onBack(){
    this.router.navigate(['/update-unit-user'], 
      { 
          queryParams: { 
            p1: this.userId,
            p2: this.username,
            p3: this.fullname, 
            p4: this.unitId,
            p5: this.unitPlaceId
          } 
      });
  }

}
