import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUnitBranchComponent } from './update-unit-branch.component';

describe('UpdateUnitBranchComponent', () => {
  let component: UpdateUnitBranchComponent;
  let fixture: ComponentFixture<UpdateUnitBranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUnitBranchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUnitBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
