import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUnitCompanyComponent } from './update-unit-company.component';

describe('UpdateUnitCompanyComponent', () => {
  let component: UpdateUnitCompanyComponent;
  let fixture: ComponentFixture<UpdateUnitCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUnitCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUnitCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
