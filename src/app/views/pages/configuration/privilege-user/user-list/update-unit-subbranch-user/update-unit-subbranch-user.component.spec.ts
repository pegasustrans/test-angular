import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUnitSubbranchUserComponent } from './update-unit-subbranch-user.component';

describe('UpdateUnitSubbranchUserComponent', () => {
  let component: UpdateUnitSubbranchUserComponent;
  let fixture: ComponentFixture<UpdateUnitSubbranchUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUnitSubbranchUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUnitSubbranchUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
