import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUnitSubbranchComponent } from './update-unit-subbranch.component';

describe('UpdateUnitSubbranchComponent', () => {
  let component: UpdateUnitSubbranchComponent;
  let fixture: ComponentFixture<UpdateUnitSubbranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUnitSubbranchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUnitSubbranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
