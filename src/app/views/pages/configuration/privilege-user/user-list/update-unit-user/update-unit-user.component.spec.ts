import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUnitUserComponent } from './update-unit-user.component';

describe('UpdateUnitUserComponent', () => {
  let component: UpdateUnitUserComponent;
  let fixture: ComponentFixture<UpdateUnitUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUnitUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUnitUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
