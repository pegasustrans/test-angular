import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';
import { param } from 'jquery';


@Component({
  selector: 'kt-update-unit-user',
  templateUrl: './update-unit-user.component.html',
  styleUrls: ['./update-unit-user.component.scss']
})
export class UpdateUnitUserComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  userId          : any;
  username        : any;
  fullname        : any;
  unitId          : any;
  unitPlaceId     : any;
  unitName        : any;
  privilegeId     : any;
  unitSubbranchId : any;

  isButtonVisible = true;

  dataItems = [];
  selectionUnitId: number;

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.userId           = params.p1;         
        this.username         = params.p2;         
        this.fullname         = params.p3;         
        this.unitPlaceId      = params.p5;        
        this.unitSubbranchId  = params.p6; 
        
        this.getBranchId(params.p4);         
      }
    );

  }

  getBranchId(subBranchId){
    const params = {
      subBranchId  : parseInt(subBranchId) 
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/branch-id', params, config)
      .subscribe((data: any) => {

          this.unitId = data.data[0].id 
          this.getData(data.data[0].id);

          if(data.data[0].id == 1){
            this.isButtonVisible = false;
          }
          
        },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
      );
  }

  getData(unitId){
    const params = {
      unitId  : parseInt(unitId), 
      type    : 'WorkUnit', 
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/unit-name', params, config)
      .subscribe((data: any) => {
          this.unitName = data.data[0].name;
          this.getUnit(data.data[0].code, parseInt(unitId));
        },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
      );
  }

  getUnit(unitCode, unitId) {
    this.dataItems = [];
    const params = {type : unitCode, unitId : unitId };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/data-unit', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.dataItems.push(item);
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
    
    this.selectionUnitId = parseInt(this.unitPlaceId);
  }

  onValueChanged($event){
    // console.log($event);
    this.unitPlaceId = $event.value;
    
    const params = { 
      unitId          : parseInt(this.unitId) 
      , userId        : parseInt(this.userId) 
      , unitPlaceId   : parseInt(this.unitPlaceId) 
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/manage-data', params, config)
      .subscribe((data: any) => {
        
       if(data.data[0].result == 'true'){
        
        notify({
          message: data.data[0].message,
          type: 'success',
          displayTime: 5000,
          width: 400,
        });

       }else{
        notify({
          message: data.data[0].message,
          type: 'error',
          displayTime: 5000,
          width: 400,
        });
       }
        

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );      
  }

  onAddUnitPlace(){
    this.router.navigate(['/unit-place-create'], 
        { 
          queryParams: { 
            p1: this.userId,
            p2: this.username,
            p3: this.fullname,  
            p4: this.unitId,
            p5: this.unitName,
            p6: 0,
            p7: this.unitSubbranchId
          } 
        });
  }

  onNext(){
    if(this.unitPlaceId == 0){
      notify({
        message: 'Tempat Unit Kerja Belum di Pilih',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    }else{
      if(this.unitId == this.unitSubbranchId){
          this.router.navigate(['/update-role-privilege'], 
          { 
            queryParams: { 
              p1: this.userId,
              p2: this.username,
              p3: this.fullname, 
              p4: this.unitId,
              p5: this.unitName,
              p6: 0,
              p7: 0,
              p8: this.unitPlaceId
            } 
          });
      }else{
          this.router.navigate(['/update-unit-subbranch-user'], 
          { 
              queryParams: { 
                p1: this.userId,
                p2: this.username,
                p3: this.fullname, 
                p6: this.unitId,
                p4: this.unitSubbranchId,
                p5: this.unitPlaceId,
                p7: this.unitName  
              } 
          });
      }
      
    }
    
  }

  onBack(){
    this.router.navigate(['/privilege-user-list']);
  }


}
