import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  userId         : any;  
  username       : any;
  workPlace      : any;
  workUnit       : any;
  email          : any;
  fullName       : any;
  isActive       : any;
  privilegeId    : any;
  privilegeName  : any;
  rolesId        : any;
  rolesName      : any;
  description    : any;

  formDetail = {
    userId        : null,
    username      : null,
    fullname      : null,
    email         : null,
    privilegeId   : null,
    privilegeName : null,
    rolesId       : null,
    rolesName     : null,
    description   : null,
    status        : null,
    isActive      : null
  };

  statusDs: any;
  dataItems = [];

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}
  
  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.userId         = params.p1; 
				this.username       = params.p2; 
				this.workPlace      = params.p3; 
				this.workUnit       = params.p4; 
				this.email          = params.p5; 
				this.fullName       = params.p6; 
				this.isActive       = params.p7; 
				this.privilegeId    = params.p8; 
				this.privilegeName  = params.p9; 
				this.rolesId        = params.p10; 
				this.rolesName      = params.p11; 
        this.description    = params.p12;

        this.formDetail.username      = params.p2;
        this.formDetail.fullname      = params.p6;
        this.formDetail.email         = params.p5;
        this.formDetail.privilegeName = params.p9;
        this.formDetail.rolesName     = params.p11;
        this.formDetail.description   = params.p12;
        this.formDetail.status        = params.p13;
      }
    );
  }

  onSave(){
    // console.log(this.formDetail);
    
    const params = { 
      userId      : parseInt(this.userId), 
      username    : this.formDetail.username,    
      fullName    : this.formDetail.fullname,    
      email       : this.formDetail.email
    };


    
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
        .post(AppGlobalVar.BASE_API_URL + 'users/update-user-detail', params, config)
        .subscribe((data: any) => {

            console.log(data.data);
            if(data.data[0].result == 'true'){
              notify({
                message: data.data[0].message,
                type: 'success',
                displayTime: 5000,
                width: 400,
              });   
            }else{
              notify({
                message: data.data[0].message,
                type: 'error',
                displayTime: 5000,
                width: 400,
              }); 
            }
           
            
          },
          (error) => {
            alert(error.statusText + '. ' + error.message);
          }
		);
  }

  onDelete(){
    const params = { 
      userId      : parseInt(this.userId)
    };


    
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
        .post(AppGlobalVar.BASE_API_URL + 'users/delete-user-detail', params, config)
        .subscribe((data: any) => {
          
          console.log(data.data);
          
            if(data.data[0].result == 'true'){
              this.onBack();
              notify({
                message: data.data[0].message,
                type: 'success',
                displayTime: 5000,
                width: 400,
              });   
            }else{
              notify({
                message: data.data[0].message,
                type: 'error',
                displayTime: 5000,
                width: 400,
              }); 
            }
           
            
          },
          (error) => {
            alert(error.statusText + '. ' + error.message);
          }
		);
  }

  onBack(){
    this.router.navigate(['/privilege-user-list']);
  }

  onChangeStatus(){
    this.router.navigate(['/user-setting'], 
        { 
            queryParams: { 
              p1 : parseInt(this.userId), 
              p2 : this.formDetail.username,    
              p3 : this.formDetail.fullname,    
              p4 : this.formDetail.email,
              p5 : this.privilegeName,
              p6 : this.rolesName,
              p7 : this.description,
              p8 : this.isActive
            } 
        });
  }

  onChangePrivilege(){
    this.router.navigate(['/privilege-update-setting'], 
        { 
            queryParams: { 
              p1: this.privilegeId,
              p2: this.rolesId,
              p3: this.userId,
              p4: this.username,
              p5: this.fullName,
              p6: this.privilegeName,
              p7: 'detail'
            } 
        });
  }

  onChangeRole(){
    this.router.navigate(['/role-user-setting'], 
            { 
                queryParams: { 
                   p1: this.privilegeId,
                   p2: this.rolesId,
                   p3: this.userId,
                   p4: this.username,
                   p5: this.fullName,
                   p6: this.privilegeName,
                   p7: 'detail'
                } 
            });
  }


}
