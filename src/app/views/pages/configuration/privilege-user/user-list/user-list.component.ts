import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';
import { Router } from '@angular/router';


@Component({
  selector: 'kt-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;
  statusDs: any;

  dsUserList: any = {};

  constructor(private http: HttpClient, private router: Router) { }

  formSearch = {
	statusId : null
  };

  ngOnInit() {
    this.readDataStatus();
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	readDataStatus() {
		const params = { type : 'Status' };

		this.statusDs = new DataSource({
			store: new CustomStore({
				key: 'id',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'users/list-option', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	GetList(statusId) {
		const params = { statusId : statusId };

		this.dsUserList = new CustomStore({
			key: 'userId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'users/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	addUserData(){
		this.router.navigate(['/add-user']);
	}

	onSearch(){
		// console.log(this.formSearch);
		if(this.formSearch.statusId == null || this.formSearch.statusId == undefined){
			notify({
			  message: 'Silahkan Pilih Kriteria Search Terlebih Dahulu',
			  type: 'error',
			  displayTime: 5000,
			  width: 400,
			});
		  }else{
			this.GetList(this.formSearch.statusId);
		  }
	}

	detailDataUser = (e) => {
		this.router.navigate(['/user-detail'], 
        { 
            queryParams: { 
				p1: e.row.data.userId,
				p2: e.row.data.username,
				p3: e.row.data.workPlace,
				p4: e.row.data.workUnit,
				p5: e.row.data.email,
				p6: e.row.data.fullName,
				p7: e.row.data.isActive,
				p8: e.row.data.privilegeId,
				p9: e.row.data.privilegeName,
				p10: e.row.data.rolesId,
				p11: e.row.data.rolesName,
				p12: e.row.data.description,
				p13: e.row.data.status,
            } 
        });
	}

}
