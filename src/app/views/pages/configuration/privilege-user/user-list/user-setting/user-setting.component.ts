import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-user-setting',
  templateUrl: './user-setting.component.html',
  styleUrls: ['./user-setting.component.scss']
})
export class UserSettingComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  dataItems = [];
  selectionId: number;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  formDetail = {
    userId        : null,
    username      : null,
    fullname      : null,
    email         : null,
    privilegeName : null,
    rolesName     : null,
    description   : null,
  };


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.formDetail.userId        = params.p1;         
        this.formDetail.username      = params.p2;         
        this.formDetail.fullname      = params.p3;         
        this.formDetail.email         = params.p4;         
        this.formDetail.privilegeName = params.p5;         
        this.formDetail.rolesName     = params.p6;   
        this.formDetail.description   = params.p7;

        this.readDataStatus(params.p8);
      }
    );    
  }

  readDataStatus(isActive) {
    this.dataItems = [];
    const params = { type : 'Status' };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/list-status', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.dataItems.push(item);
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
    
    this.selectionId = parseInt(isActive);
	}

  onValueChanged($event){
    const params = { 
      isActive : $event.value
      , userId : parseInt(this.formDetail.userId) 
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/update-status', params, config)
      .subscribe((data: any) => {
        
        console.log(data.data);
        notify({
          message: 'Data telah Update',
          type: 'success',
          displayTime: 5000,
          width: 400,
        });       

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    ); 
  }

  onBack(){
    const params = { 
      userId    : parseInt(this.formDetail.userId) 
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'users/check-detail-user', params, config)
      .subscribe((data: any) => {
        console.log(data.data[0]);
        this.router.navigate(['/user-detail'], 
          { 
              queryParams: { 
                p1: data.data[0].userId,
                p2: data.data[0].username,
                p3: data.data[0].workPlace,
                p4: data.data[0].workUnit,
                p5: data.data[0].email,
                p6: data.data[0].fullName,
                p7: data.data[0].isActive,
                p8: data.data[0].privilegeId,
                p9: data.data[0].privilegeName,
                p10: data.data[0].rolesId,
                p11: data.data[0].rolesName,
                p12: data.data[0].description, 
                p13: data.data[0].status
              } 
          });
      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    ); 
  }

}
