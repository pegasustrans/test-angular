import { BrowserModule, BrowserTransferStateModule } from "@angular/platform-browser";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { DxDataGridModule, DxButtonModule } from "devextreme-angular";
import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Component, HostListener, OnInit } from "@angular/core";
import CustomStore from "devextreme/data/custom_store";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import notify from "devextreme/ui/notify";
import { HighlightProcess } from "../../../../../core/helper";

@Component({
  selector: 'kt-account-payable',
  templateUrl: './account-payable.component.html',
  styleUrls: ['./account-payable.component.scss']
})
export class AccountPayableComponent implements OnInit {
  ds: any = {};
	menuPriv = false;
	menuForb = true;
	cityBranchDs: any;

	modaDs: any[] = [
		{ modaValue: "air", modaName: "UDARA" },
		{ modaValue: "sea", modaName: "LAUT" },
		{ modaValue: "land", modaName: "DARAT" },
		{ modaValue: "land-sea", modaName: "DARAT - LAUT (INTER-MODA)" },
		{ modaValue: "land-air", modaName: "DARAT - UDARA (INTER-MODA)" },
	];

	formSearch = {
		cityId: null,
		moda: null,
	};

	HistoryForm = {
		menu: "Account Payable",
		historyloginId: 0,
	};

	constructor(private route: ActivatedRoute, private matDialog: MatDialog, private router: Router, private http: HttpClient) {}

	ngOnInit() {
		this.getCity();
		this.checkuserGlobalVar();
		this.historyLoginStart();
	}

	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	onSearch() {
		this.getList();
	}

	getCity() {
		const params = {};

		this.http.get<any>(AppGlobalVar.BASE_API_URL + "account-receivable/city-branch-list", { params }).subscribe(
			(response) => {
				this.cityBranchDs = response.data;  
			},
		  	(error) => {
				console.error('Error fetching revenue data:', error);
		  	}
		);
	}

	ngOnDestroy() {
		this.historyLoginEnd();
	}

	historyLoginStart() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/start-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	historyLoginEnd() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/end-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if (UserGlobalVar.USER_ID === "") {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		} else {
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function (value) {
				if (value == "account-payable") {
					checkPriv = 1;
				}
			});

			if (checkPriv == 1) {
				this.menuPriv = true;
				this.menuForb = false;
			}
		}
	}

	getList() {
		const params = {
			moda: this.formSearch.moda,
			cityId: this.formSearch.cityId,
		};
		this.ds = new CustomStore({
			key: "arId",
			load: (loadOptions: any) => {
				return this.http
					.get(AppGlobalVar.BASE_API_URL + "account-receivable/list", { headers: this.httpOptions.headers, params })
					.toPromise()
					.then((data: any) => {
						console.log(data.data);
						return {
							data: data.data,
						};
					})
					.catch((error) => {
						console.log(error);
					});
			},
		});
	}

	arCreate() {
		this.router.navigate(["/account-payable-create"]);
	}

	thousandSeparator(data) {
		var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		//roundUp = Math.round(roundUp);
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

}
