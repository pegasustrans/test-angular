import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';;
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { FormControl,Validators,FormGroup ,FormBuilder} from '@angular/forms';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';
@Component({
  selector: 'kt-account-receivable-create',
  templateUrl: './account-receivable-create.component.html',
  styleUrls: ['./account-receivable-create.component.scss']
})
export class AccountReceivableCreateComponent implements OnInit {
  menuPriv = true;
	menuForb = false;
  date = new Date();
  coaCodeDs: any;
  submitted = false;
  formCreate = {
    coaId  : null,
    keterangan  : null,
    debit  : null,
		credit   : null,
	};

  HistoryForm = {
		menu: "Create - AR",
		historyloginId:0
	};

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
    this.readCoaCode();
    this.historyLoginStart();
  }

  onBack(){
    this.router.navigate(['/account-receivable']);
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	}
 

  readCoaCode() {
    const params = {};

		this.coaCodeDs = new DataSource({
			store: new CustomStore({
				key: 'coaId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'coa-code/list', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							
							return result.data;
						});
				}
			})
		});

	}

  onSave(){
		
        const params = {
          coaId : parseInt(this.formCreate.coaId),
          keterangan : this.formCreate.keterangan,
          debit : parseInt(this.formCreate.debit),
          credit  : parseInt(this.formCreate.credit)
        };
  
        const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'account-receivable/insert-ar', params, config)
          .subscribe((data: any) => {
  
            this.router.navigate(['/account-receivable']);
  
            notify({
              message: 'Data telah di Tambahkan',
              type: 'success',
              displayTime: 5000,
              width: 400,
            });
  
          },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
          );
    }
  

  historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}


}
