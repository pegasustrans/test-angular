import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BukuKasPrintComponent } from './buku-kas-print.component';

describe('BukuKasPrintComponent', () => {
  let component: BukuKasPrintComponent;
  let fixture: ComponentFixture<BukuKasPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BukuKasPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BukuKasPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
