import { AfterContentChecked, ChangeDetectionStrategy, Component, EventEmitter, HostListener, Inject, OnDestroy, OnInit, Output, Pipe, PipeTransform } from "@angular/core";
import * as $ from "jquery";
import "datatables.net";

import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import printJS from "print-js";
import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import * as Enumerable from "linq";
import notify from "devextreme/ui/notify";

@Component({
	selector: "kt-buku-kas-print",
	templateUrl: "./buku-kas-print.component.html",
	styleUrls: ["./buku-kas-print.component.scss"],
})
export class BukuKasPrintComponent implements OnInit, OnDestroy {
	dtOptions: any = {};
	itemData: any = {};
	period: any;
	manifestList;
	bukuKasList;
	dtTrigger: Subject<any> = new Subject();
	tanggalPrint: any = this.data;
	// manifestData: any;
	// manifestId = 0;
	sumDebit = 0;
	sumKredit = 0;
	sumSaldo = 0;
	prinDateParams: any;

	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	constructor(public dialogRef: MatDialogRef<BukuKasPrintComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private http: HttpClient) {}

	ngOnInit() {
		// this.manifestData = this.data;
		// this.manifestId = this.data.manifestId;
		this.processData();
		this.DtOptions();
	}

	processData() {
		if (this.data.period == "CustomeDate") {
			this.getListBukuKas("buku-kas/list?paramStartDate=" + this.data.startDate + "&paramEndDate=" + this.data.endDate);
		} else if (this.data.period != "CustomeDate") {
			this.getListBukuKas("buku-kas/list?period=" + this.data.period + "&date=" + this.data.date);
		} else {
			this.getListBukuKas("buku-kas/list");
		}
	}

	getListBukuKas(urlBukuKas) {
		this.http
			.get(AppGlobalVar.BASE_API_URL + urlBukuKas)
			.toPromise()
			.then((data: any) => {
				this.bukuKasList = data.data;
				console.log(this.bukuKasList);
				this.dtTrigger.next();

				Enumerable.from(this.bukuKasList).forEach((obj: any, index: number) => {
					this.sumDebit = this.sumDebit + obj.debit;
					this.sumKredit = this.sumKredit + obj.credit;
				});

				const lastRow = this.bukuKasList[this.bukuKasList.length - 1];
				this.sumSaldo = lastRow.cumulativeBalance;
			})
			.catch((error) => {
				console.log(error);
			});
	}

	close() {
		this.dialogRef.close("Thanks for using me!");
	}

	DtOptions() {

		const today = new Date();
		const formattedDate = today.toLocaleDateString("en-GB", {
			day: "2-digit",
			month: "long",
			year: "numeric",
		});

		this.dtOptions = {
			// Declare the use of the extension in the dom parameter
			dom: "Bfrtip",
			rowGroup: [0],
			ordering: false,
			paging: false,
			// Configure the buttons
			buttons: [
				"copy",
				{
					extend: "print",
					title: "",
					messageTop: () => {
						return '<table border="0" width="100%">\n' + "<tr>\n" + '<td style="text-align: center; font-size: 20px; font-weight: bold">Buku Kas Tanggal <br/> '+ formattedDate +' </td>\n' + "</tr>\n" + "\n" + "</table>";
					},
					messageBottom: null,
					customize: (win) => {
						// $(win.document.body)
						// 	.css( 'font-size', '10pt' )
						// 	.prepend(
						// 		'<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
						// 	);

						$(win.document.body).css("font-size", "10pt").prepend();

						$(win.document.body).find("table").addClass("cell-border compact stripe").css("font-size", "inherit");
					},
				},
				{
					extend: "excelHtml5",
					title: "",
					messageTop: () => {
						return "";
					},
				},
			],
		};
	}

	printPage() {
		window.print();
	}

	ngOnDestroy(): void {}
}
