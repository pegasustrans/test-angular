import { TestBed } from '@angular/core/testing';

import { BukuKasServicesService } from './buku-kas-services.service';

describe('BukuKasServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BukuKasServicesService = TestBed.get(BukuKasServicesService);
    expect(service).toBeTruthy();
  });
});
