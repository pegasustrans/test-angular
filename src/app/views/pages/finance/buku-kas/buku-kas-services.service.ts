import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppGlobalVar } from "../../../../core/globalvar/appGlobalVar";
import notify from "devextreme/ui/notify";

@Injectable({
  providedIn: 'root'
})
export class BukuKasServicesService {
  dataDeleted = new EventEmitter<void>();

  constructor(private http: HttpClient, private router: Router) { }

  deleteBukuKas(id: any) {
    const params = {
			idBukuKas: id,
		};
    const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
    this.http.post(AppGlobalVar.BASE_API_URL + "buku-kas/delete-buku-kas", params, config).subscribe(
      (data: any) => {
        // Emit peristiwa dataDeleted
        

        this.dataDeleted.emit();
      },
      (error) => {
        // Tampilkan pesan kesalahan jika ada
        alert(error.statusText + ". " + error.message);
      }
    );
  }
}
