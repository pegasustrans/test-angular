import { BrowserModule, BrowserTransferStateModule } from "@angular/platform-browser";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { DxDataGridModule, DxButtonModule } from "devextreme-angular";
import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Component, HostListener, OnInit } from "@angular/core";
import CustomStore from "devextreme/data/custom_store";
import { BukuKasPrintComponent } from "../buku-kas-print/buku-kas-print.component";
import { BukuKasServicesService } from "../buku-kas-services.service";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import notify from "devextreme/ui/notify";
import { HighlightProcess } from "../../../../../core/helper";

@Component({
	selector: "kt-buku-kas",
	templateUrl: "./buku-kas.component.html",
	styleUrls: ["./buku-kas.component.scss"],
})
export class BukuKasComponent implements OnInit {
	highlightProcess = HighlightProcess;

	ds: any = {};
	dsCoaCode: any = {};
	menuPriv = false;
	menuForb = true;
	events: Array<string> = [];
	public innerWidth: any = window.innerWidth;
	public innerHeight: any = window.innerHeight;
	bukuKasId: any = 0;
	selectedDate: string;
	startDate: string;
	endDate: string;
	minDate: string;
	maxDate: string;
	editBukuKasPopupVisible = false;
	currentBukuKas: any;

	HistoryForm = {
		menu: "Buku Bank",
		historyloginId: 0,
	};

	isToday(date: string): boolean {
		const todayKas = new Date();
		const givenDate = new Date(date);
		return givenDate.getDate() === todayKas.getDate() && givenDate.getMonth() === todayKas.getMonth() && givenDate.getFullYear() === todayKas.getFullYear();
	}

	dateOptions = [
		{ label: " Today", value: "today" },
		// { label: " Per Date", value: "perDate" },
		// { label: " Per Bulan", value: "perMonth" },
		// { label: " Per Tahun", value: "perYear" },
		// { label: " Range Date", value: "customDate" },
	];

	selectedOption: string;

	onOptionChange() {}

	constructor(private bukuKasService: BukuKasServicesService, private route: ActivatedRoute, private matDialog: MatDialog, private router: Router, private http: HttpClient) {}

	ngOnInit() {
		this.getList();
		this.checkuserGlobalVar();
		this.historyLoginStart();
		this.getListCoaCode();
		const today = new Date();
		const year = today.getFullYear();
		const month = today.getMonth();

		// Set min date as the first day of the current month
		this.minDate = new Date(year, month, 1).toISOString().split("T")[0];

		// Set max date as the last day of the current month
		this.maxDate = new Date(year, month + 1, 0).toISOString().split("T")[0];
		// this.bukuKasService.dataDeleted.subscribe(() => {
		// 	notify({
		// 		message: "Data berhasil dihapus",
		// 		type: "success",
		// 		displayTime: 5000,
		// 		width: 400,
		// 	});
		// 	location.reload();
		// 	this.getList();
		// });
	}

	editBukuKasSubmit(e) {
		this.editBukuKas();
	}

	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	tesPrint() {
		let printParams;

		const today = new Date();
		const todayDateOnly = new Date(today.getFullYear(), today.getMonth(), today.getDate());

		const year = todayDateOnly.getFullYear();
		const month = String(todayDateOnly.getMonth() + 1).padStart(2, "0"); // Tambahkan 1 karena bulan dimulai dari 0
		const day = String(todayDateOnly.getDate()).padStart(2, "0");

		const formattedDateToday = `${year}-${month}-${day}`;

		try {
			switch (this.selectedOption) {
				case "today":
					printParams = { date: formattedDateToday, period: "Today" };

					break;
				case "perDate":
					printParams = { date: this.selectedDate, period: "PerDate" };
					break;
				case "perMonth":
					const monthDate = new Date(this.selectedDate);
					if (isNaN(monthDate.getTime())) throw new Error("Invalid date format");
					const updateMonth = String(monthDate.getMonth() + 1).padStart(2, "0");
					const monthNumber = monthDate.getMonth() + 1;
					const monthName = new Date(0, monthNumber - 1).toLocaleString("default", { month: "long" });
					printParams = { date: monthDate.getFullYear() + "-" + updateMonth + "-01", period: "PerMonth", month: monthName, year: monthDate.getFullYear() };
					break;
				case "perYear":
					const yearDate = this.selectedDate;
					printParams = { date: yearDate + "-01-01", period: "PerYear", year: yearDate };
					break;
				case "customDate":
					printParams = { startDate: this.startDate, endDate: this.endDate, period: "CustomeDate" };
					break;
				default:
					printParams = { date: formattedDateToday, period: "Today" };
			}
		} catch (error) {
			console.error("Error:", error.message);
			return;
		}

		const dialogConfig = new MatDialogConfig();
		dialogConfig.data = printParams;
		dialogConfig.minWidth = this.innerWidth + "px";
		dialogConfig.minHeight = this.innerHeight + "px";
		let dialogRef = this.matDialog.open(BukuKasPrintComponent, dialogConfig);
		dialogRef.afterClosed().subscribe((value) => {
			// console.log(`Dialog terima nehhh: ${value}`);
		});
	}

	editBukuKas() {
		const params = {
			bukuKasId: parseInt(this.currentBukuKas.bukuKasId),
			coaId: parseInt(this.currentBukuKas.coaId),
			keterangan: this.currentBukuKas.keterangan,
			noReferensi: this.currentBukuKas.noReferensi,
			debit: parseInt(this.currentBukuKas.debit),
			credit: parseInt(this.currentBukuKas.credit),
		};

		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "buku-kas/update-buku-kas", params, config).subscribe(
			(data: any) => {
				notify({
					message: "Berhasil Edit Buku Kas.",
					type: "success",
					displayTime: 5000,
					width: 400,
				});
				location.reload();
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	ngOnDestroy() {
		this.historyLoginEnd();
	}

	historyLoginStart() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/start-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	historyLoginEnd() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/end-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if (UserGlobalVar.USER_ID === "") {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		} else {
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function (value) {
				if (value == "buku-bank") {
					checkPriv = 1;
				}
			});

			console.log(UserGlobalVar.MENU_KEY_LOGIN_NAME);

			if (checkPriv == 1) {
				this.menuPriv = true;
				this.menuForb = false;
			}
		}
	}

	getList() {
		const params = {
			//createdAt: this.formSearch.dateString
		};
		this.ds = new CustomStore({
			key: "bukuKasId",
			load: (loadOptions: any) => {
				return this.http
					.get(AppGlobalVar.BASE_API_URL + "buku-kas/list", { headers: this.httpOptions.headers, params })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data,
						};
					})
					.catch((error) => {
						console.log(error);
					});
			},
		});
	}

	getListCoaCode() {
		const params = {
			//createdAt: this.formSearch.dateString
		};

		this.http
			.get(AppGlobalVar.BASE_API_URL + "coa-code/list", { headers: this.httpOptions.headers, params })
			.toPromise()
			.then((data: any) => {
				this.dsCoaCode = data.data; // Assign the fetched data to coaCodeDs
			})
			.catch((error) => {
				console.log(error);
			});
	}

	logEvent(eventName) {
		this.events.unshift(eventName);
	}

	handleRowInserted(newRowData: any) {
		console.log("Baris baru ditambahkan:", newRowData);
		// Lakukan operasi lain yang Anda butuhkan dengan data baru di sini
	}

	addBukuKasPage() {
		this.router.navigate(["/buku-kas-create"]);
	}

	clearEvents() {
		this.events = [];
		this.getListCoaCode();
	}

	customCurrencyFormatter(cellInfo) {
		return "Rp " + cellInfo.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	thousandSeparator(data) {
		var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		//roundUp = Math.round(roundUp);
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	getRowNumber(dataIndex) {
		return dataIndex + 1;
	}

	bukuKasAction = (e) => {
		if (e.itemData.key == "edit") {
			this.currentBukuKas = e.itemData.data;
			this.editBukuKasPopupVisible = true;
		}

		if (e.itemData.key == "delete") {
			this.bukuKasId = parseInt(e.itemData.data.bukuKasId);
			this.executeDeleteBukuKas(parseInt(e.itemData.data.bukuKasId));
		}
	};

	executeDeleteBukuKas(id: any) {
		const params = {
			bukuKasId: id,
		};

		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "buku-kas/delete-buku-kas", params, config).subscribe(
			(data: any) => {
				notify({
					message: "Data di Hapus",
					type: "success",
					displayTime: 5000,
					width: 400,
				});

				location.reload();
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}
}
