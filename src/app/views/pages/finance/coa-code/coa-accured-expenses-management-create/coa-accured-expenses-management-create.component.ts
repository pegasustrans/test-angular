import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Component, HostListener, OnInit } from "@angular/core";
import CustomStore from "devextreme/data/custom_store";
import { FormControl, Validators, FormGroup, FormBuilder } from "@angular/forms";
import DataSource from "devextreme/data/data_source";
import notify from "devextreme/ui/notify";

@Component({
	selector: "kt-coa-accured-expenses-management-create",
	templateUrl: "./coa-accured-expenses-management-create.component.html",
	styleUrls: ["./coa-accured-expenses-management-create.component.scss"],
})
export class CoaAccuredExpensesManagementCreateComponent implements OnInit {
	@HostListener("window:beforeunload", ["$event"]) unload(event) {
		this.historyLoginEnd();
	}
	menuPriv = true;
	menuForb = false;
	date = new Date();
	CityDs: any;
	coaCodeDs: any;

	submitted = false;
	formCreate = {
		coaCode: null,
	};

	HistoryForm = {
		menu: "Create - Coa - City - Rev - Lia",
		historyloginId: 0,
	};

	constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {}

	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	ngOnInit() {
		this.historyLoginStart();
		this.readCoaCode();
	}

	readCoaCode() {
		const coaParentId = 6;
		const params = {
			coaParentId: coaParentId.toString(),
		};

		this.coaCodeDs = new DataSource({
			store: new CustomStore({
				key: "coaId",
				loadMode: "raw",
				load: (loadOptions) => {
					return this.http
						.get(AppGlobalVar.BASE_API_URL + "coa-code/list", { headers: this.httpOptions.headers, params })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
			}),
		});
	}

	onBack() {
		this.router.navigate(["/coa-accured-expenses-management"]);
	}

	ngOnDestroy() {
		this.historyLoginEnd();
	}

	onSave() {
		const params = {
			coaId: this.formCreate.coaCode
		};
		console.log(params);

		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "coa-code/coa-accured-expenses-management-create", params, config).subscribe(
			(data: any) => {
				console.log(data.data);

				this.router.navigate(["/coa-accured-expenses-management"]);

				notify({
					message: "Data telah di Tambahkan",
					type: "success",
					displayTime: 5000,
					width: 400,
				});
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	historyLoginStart() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/start-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	historyLoginEnd() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/end-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}
}
