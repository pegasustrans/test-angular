import { ActivatedRoute, Router } from '@angular/router';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
@Component({
  selector: 'kt-coa-accured-expenses-management',
  templateUrl: './coa-accured-expenses-management.component.html',
  styleUrls: ['./coa-accured-expenses-management.component.scss']
})
export class CoaAccuredExpensesManagementComponent implements OnInit {

  ds: any = {};
  menuPriv = false;
  menuForb = true;

  editTarifPopupVisible 	= false;
  addTarifPopupVisible 		= false;
  hapusTarifPopupVisible 	= false;



  HistoryForm = {
	menu: "Coa Accured Expenses Management",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  ngOnInit() {
	this.checkuserGlobalVar();
	this.historyLoginStart();
    
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
		  setTimeout(() => {
			this.checkuserGlobalVar();
		  }, 1000);
		}
	  
		else{		
		  var checkPriv = 0;
		  await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "coa-ar-management"){
			  checkPriv = 1;
			}
		  });
	  
		  if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
			
			this.GetList();
		  }
		}
	  }

  GetList() {
		const params = {
			//createdAt: this.formSearch.dateString
		}; 
		this.ds = new CustomStore({
			key: 'accuredExpensesId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'coa-code/coa-accured-expenses-management-list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
            console.log(data.data)
						return {
            	data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	coaAccuredExpensesManagementCreate(){
		this.router.navigate(['/coa-accured-expenses-management-create']);
	}

}
