import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';;
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { FormControl,Validators,FormGroup ,FormBuilder} from '@angular/forms';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';


@Component({
  selector: 'kt-coa-ar-management-create',
  templateUrl: './coa-ar-management-create.component.html',
  styleUrls: ['./coa-ar-management-create.component.scss']
})
export class CoaArManagementCreateComponent implements OnInit {

  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }
  menuPriv = true;
	menuForb = false;
  date = new Date();
  CityDs	: any;
  coaCodeAssetsDs: any;
  coaCodeRevenueDs: any;
 
  submitted = false;
  formCreate = {
    moda  : null,
    coaIdLiability  : null,
    coaIdRevenue  : null,
    coaIdAr  : null,
		cityId   : null,
		isCorporate   : null,
	};

  HistoryForm = {
		menu: "Create - Coa - City - Rev - Lia",
		historyloginId:0
	};

  modaDs: any[] = [
    { modaValue: 'air', modaName: 'UDARA' },
    { modaValue: 'sea', modaName: 'LAUT' },
    { modaValue: 'land', modaName: 'DARAT' },
  ];

  isCorporateDs: any[] = [
    { isCorporateValue: 'Corporate', isCorporateName: 'Corporate' },
    { isCorporateValue: 'Non Corporate', isCorporateName: 'Non Corporate' },
  ]; 

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
    this.historyLoginStart();
    this.getCity();
    this.readCoaCodeAssets();
    this.readCoaCodeRevenue();
  }


  readCoaCodeAssets() {

    const coaParentId = 1;

    const params = {
      coaParentId : coaParentId.toString()
    };

		this.coaCodeAssetsDs = new DataSource({
			store: new CustomStore({
				key: 'coaId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'coa-code/list', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							
							return result.data;
						});
				}
			})
		});
	}

  readCoaCodeRevenue() {

    const coaParentId = 4;
 
    const params = {
      coaParentId : coaParentId.toString()
    };

		this.coaCodeRevenueDs = new DataSource({
			store: new CustomStore({
				key: 'coaId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'coa-code/list', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							
							return result.data;
						});
				}
			})
		});
	}

  onBack(){
    this.router.navigate(['/coa-ar-management']);
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	}

  getCity(){
    const params = {};

		this.CityDs = new DataSource({
			store: new CustomStore({
				key: 'cityId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-station-city-list-city', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onSave(){
		
        const params = {
          moda  : this.formCreate.moda,
          coaIdLiability  : this.formCreate.coaIdLiability,
          coaIdAr  : this.formCreate.coaIdAr,
          coaIdRevenue  : this.formCreate.coaIdRevenue,
          cityId   : this.formCreate.cityId,
          isCorporate   : this.formCreate.isCorporate,
        };
        // console.log(params);
  
        const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'coa-city-revenue-liability/coa-city-revenue-liability-create', params, config)
          .subscribe((data: any) => {
            console.log(data.data);
  
            this.router.navigate(['/coa-ar-management']);
  
            notify({
              message: 'Data telah di Tambahkan',
              type: 'success',
              displayTime: 5000,
              width: 400,
            });
  
          },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
          );
    }
  

  historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}


}
