
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';;
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { FormControl,Validators,FormGroup ,FormBuilder} from '@angular/forms';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-coa-code-create',
  templateUrl: './coa-code-create.component.html',
  styleUrls: ['./coa-code-create.component.scss']
})
export class CoaCodeCreateComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }
  menuPriv = true;
	menuForb = false;
  date = new Date();
  coaParentDs: any;
  submitted = false;
  formCreate = {
    companyId  : null,
    coaCode  : null,
    coaName  : null,
		branchId   : null,
		coaParentId   : null,
	};

  HistoryForm = {
		menu: "Create - Coa - Code",
		historyloginId:0
	};
  
  // digit1 = new FormControl(null,Validators.required);
  // digit2 = new FormControl(null,Validators.required);
  // digit3 = new FormControl(null,Validators.required);
  // digit4 = new FormControl(null,Validators.required);
  // digit5 = new FormControl(null,Validators.required);
  // digit6 = new FormControl(null,Validators.required);

  branchData: any[] = [
    { branchId: 1, branchName: 'Tanah Abang' },
    { branchId: 2, branchName: 'Bandung' },
  ]; 

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
    this.readDataCoaParent();
    this.historyLoginStart();
  }

  onBack(){
    this.router.navigate(['/coa-code-list']);
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	}
  generateCoaCode(){
  this.submitted = true;

  // comanyId dan branchId masih belum mengikuti database
  this.formCreate.companyId = 1;
  // this.formCreate.coaCode = this.digit1.value+'.'+this.digit2.value+'.'+this.digit3.value+'.'+this.digit4.value+'.'+this.digit5.value+'.'+this.digit6.value

  if(this.formCreate.coaCode.slice(-1) != '.'){
    this.formCreate.coaCode= this.formCreate.coaCode + '.';
  }
  
  this.onSave();
  
  }

  readDataCoaParent() {
    const params = {};

		this.coaParentDs = new DataSource({
			store: new CustomStore({
				key: 'id',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'coa-code/list-coa-parent', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							
							return result.data;
						});
				}
			})
		});

	}

  onSave(){
		
        const params = {
          companyId : parseInt(this.formCreate.companyId),
          coaCode : this.formCreate.coaCode,
          branchId : parseInt(this.formCreate.branchId),
          coaName : this.formCreate.coaName,
          coaParentId  : parseInt(this.formCreate.coaParentId)
        };
  
        const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'coa-code/create-coa-code', params, config)
          .subscribe((data: any) => {
            console.log(data.data);
  
            this.router.navigate(['/coa-code-list']);
  
            notify({
              message: 'Data telah di Tambahkan',
              type: 'success',
              displayTime: 5000,
              width: 400,
            });
  
          },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
          );
    }
  

  historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}


}