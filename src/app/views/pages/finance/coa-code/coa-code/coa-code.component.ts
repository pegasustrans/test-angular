import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Component, HostListener, OnInit } from "@angular/core";
import CustomStore from "devextreme/data/custom_store";
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';

@Component({
	selector: "kt-coa-code",
	templateUrl: "./coa-code.component.html",
	styleUrls: ["./coa-code.component.scss"],
})
export class CoaCodeComponent implements OnInit {
	ds: any = {};
	menuPriv = false;
	menuForb = true;

	editCoaCodeVisible = false;
	currentCoaCode: any;
	coaId: any = 0;
	coaParentDs: any;

	editTarifPopupVisible = false;
	addTarifPopupVisible = false;
	hapusTarifPopupVisible = false;

	HistoryForm = {
		menu: "Coa Code List",
		historyloginId: 0,
	};

	branchData: any[] = [
		{ branchId: 1, branchName: 'Tanah Abang' },
		{ branchId: 2, branchName: 'Bandung' },
	  ]; 

	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

	ngOnInit() {
		this.checkuserGlobalVar();
		this.historyLoginStart();
		this.readDataCoaParent();
	}

	readDataCoaParent() {
		const params = {};
	
			this.coaParentDs = new DataSource({
				store: new CustomStore({
					key: 'id',
					loadMode: 'raw',
					load: (loadOptions) => {
						return this.http.get(AppGlobalVar.BASE_API_URL + 'coa-code/list-coa-parent', { headers: this.httpOptions.headers, params  })
							.toPromise()
							.then((result: any) => {
								
								return result.data;
							});
					}
				})
			});
	
		}

	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	ChooseAction = (e) => {
		// if(e.itemData.key == "update"){
		//    this.editTarif(e.itemData.data);
		// }
		// if(e.itemData.key == "delete"){
		// 	this.deleteCity (e.itemData.data);
		//  }
	};

	//   deleteCity(e){
	// 	this.router.navigate(['/master-city-delete'],
	// 	{
	// 		queryParams: {
	// 		  p1: e.masterCityCapital,
	// 		  p2: e.masterCityCode,
	// 		  p3: e.masterCityId,
	// 		  p4: e.masterCityName,
	// 		  p5: e.masterCityType,
	// 		  p6: e.masterProvinceId,
	// 		  p7: e.masterProvinceName
	// 		}
	// 	});
	//  }

	ngOnDestroy() {
		this.historyLoginEnd();
	}

	historyLoginStart() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/start-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	historyLoginEnd() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/end-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if (UserGlobalVar.USER_ID === "") {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		} else {
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function (value) {
				if (value == "coa-code-list") {
					checkPriv = 1;
				}
			});

			if (checkPriv == 1) {
				this.menuPriv = true;
				this.menuForb = false;

				this.GetList();
			}
		}
	}

	GetList() {
		const params = {
			//createdAt: this.formSearch.dateString
		};
		this.ds = new CustomStore({
			key: "coaId",
			load: (loadOptions: any) => {
				return this.http
					.get(AppGlobalVar.BASE_API_URL + "coa-code/list", { headers: this.httpOptions.headers, params })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data,
						};
					})
					.catch((error) => {
						console.log(error);
					});
			},
		});
	}

	addCodeCoa() {
		this.router.navigate(["/coa-code-create"]);
	}

	coaCodeAction = (e) => {
		if (e.itemData.key == "edit") {
			this.currentCoaCode = e.itemData.data;
			this.editCoaCodeVisible = true;
			// console.log(this.currentCoaCode);
		}

		if (e.itemData.key == "delete") {
			this.coaId = parseInt(e.itemData.data.coaId);
			// console.log(this.coaId);
			// this.executeDeleteBukuKas(parseInt(e.itemData.data.bukuKasId));
		}
	};

	editCoaCodeSubmit(e) {
		const params = {
			coaId: parseInt(this.currentCoaCode.coaId),
			coaName: this.currentCoaCode.coaName,
			coaCode: this.currentCoaCode.coaCode,
			coaParentId: parseInt(this.currentCoaCode.coaParentId),
			branchId : 1, // sementera hardcore dahulu hanya untuk jakarta
			companyId : 1
		};

		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "coa-code/update-coa-code", params, config).subscribe(
			(data: any) => {
				notify({
					message: "Berhasil Edit Coa.",
					type: "success",
					displayTime: 5000,
					width: 400,
				});
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
		this.editCoaCodeVisible = false;
	}

}
