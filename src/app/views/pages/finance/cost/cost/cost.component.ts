import { Component, HostListener, OnInit } from "@angular/core";
import CustomStore from "devextreme/data/custom_store";
import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
	selector: "kt-cost",
	templateUrl: "./cost.component.html",
	styleUrls: ["./cost.component.scss"],
})
export class CostComponent implements OnInit {
	ds: any = {};
	menuPriv = false;
	menuForb = true;

	selectedDate: string;

	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
		const date = new Date();
		this.selectedDate = date.toISOString().split("T")[0];
	}

	HistoryForm = {
		menu: "Cost",
		historyloginId: 0,
	};

	ngOnInit() {
		this.checkuserGlobalVar();
		this.historyLoginStart();
	}

	onSearch() {
		this.getList();
	}

	ngOnDestroy() {
		this.historyLoginEnd();
	}

	getList() {
		const [year, month] = this.selectedDate.split("-");
		const params = {
			year: year,
			month: month,
		};

		console.log(params);
		this.ds = new CustomStore({
			key: "costId",
			load: (loadOptions: any) => {
				return this.http
					.get(AppGlobalVar.BASE_API_URL + "cost/list", { headers: this.httpOptions.headers, params })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data,
						};
					})
					.catch((error) => {
						console.log(error);
					});
			},
		});
	}

	addCost() {
		this.router.navigate(["/cost"]);
	}

	historyLoginStart() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/start-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	historyLoginEnd() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/end-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if (UserGlobalVar.USER_ID === "") {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		} else {
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function (value) {
				if (value == "cost") {
					checkPriv = 1;
				}
			});

			if (checkPriv == 1) {
				this.menuPriv = true;
				this.menuForb = false;
			}
		}
	}

	thousandSeparator(data) {
		var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		//roundUp = Math.round(roundUp);
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
}
