import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';;
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { FormControl,Validators,FormGroup ,FormBuilder} from '@angular/forms';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-expenses-fix-create',
  templateUrl: './expenses-fix-create.component.html',
  styleUrls: ['./expenses-fix-create.component.scss']
})
export class ExpensesFixCreateComponent implements OnInit {

  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }
  menuPriv = true;
	menuForb = false;
  date = new Date();
  coaCodeDs: any;

  formCreate = {
    coaId  : null,
    keterangan  : null,
    debit  : null,
		credit   : null,
	};

  HistoryForm = {
		menu: "Expenses - Create",
		historyloginId:0
	};

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
    this.readCoaCode();
    this.historyLoginStart();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	} 

  readCoaCode() {
    const params = {
      
    };

		this.coaCodeDs = new DataSource({
			store: new CustomStore({
				key: 'coaId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'coa-code/coa-accured-expenses-management-list', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							
							return result.data;
						});
				}
			})
		});

	}

  onSave(){
    const params = {
      coaId : parseInt(this.formCreate.coaId),
      keterangan : this.formCreate.keterangan,
      debit : parseInt(this.formCreate.debit),
      credit  : parseInt(this.formCreate.credit)
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'expenses/expenses-create', params, config)
      .subscribe((data: any) => {

        this.router.navigate(['/fix-expenses']);

        notify({
          message: 'Data telah di Tambahkan',
          type: 'success',
          displayTime: 5000,
          width: 400,
        });

      },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
      );
  }

  onBack(){
    this.router.navigate(['/fix-expenses']);
  }

  
  historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

}
