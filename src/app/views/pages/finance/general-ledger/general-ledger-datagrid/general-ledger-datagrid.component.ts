import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import {Component, OnInit, Input, ViewChild} from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";
import {HttpClient, HttpHeaders} from '@angular/common/http';
@Component({
  selector: 'kt-general-ledger-datagrid',
  templateUrl: './general-ledger-datagrid.component.html',
  styleUrls: ['./general-ledger-datagrid.component.scss']
})
export class GeneralLedgerDatagridComponent implements OnInit {

  @ViewChild('targetDataGrid',{static: true}) dataGrid: DxDataGridComponent;

	gmDs: any = {};
	cawCorporate : boolean = false;
	awCorporate : boolean = false;
	koliCorporate : boolean = false;
	awGerai : boolean = false;
	cawGerai : boolean = false;
	koliGerai : boolean = false;

  constructor(private http: HttpClient) { }
  
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};	

	ngOnInit() {
		// this.GetList();
	}

	GetList(dateString) {
		// this.gmDs = new CustomStore({
		// 	key: 'manifestId',
		// 	load:  (loadOptions: any) => {
		// 		return this.http.get(AppGlobalVar.BASE_API_URL + 'goodsmonitoring/list?dateLocal=' + dateString)
		// 			.toPromise()
		// 			.then((data: any) => {
		// 				return {
		// 					data: data.data
		// 				};
		// 			})
		// 			.catch(error => { console.log(error); });
		// 	},
		// });
		const params = {
			createdAt: dateString
			, type : 'sender'
		}; 
		this.gmDs = new CustomStore({
			key: 'manifestId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring/monitoring-spb', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						console.log(data.data);
						
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});

	}

	expandGrid() {
		this.dataGrid.instance.expandAll(0);
		this.dataGrid.instance.expandAll(1);
	}

	thousandSeparator(data) {
		var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		//roundUp = Math.round(roundUp);
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	}

	ColManifestNo(rowData) {
		if (rowData.corporation == ''){
			return rowData.destinationCityCode + ' ' + rowData.alphabet + ' # ' + rowData.manifestId;
		}

		if (rowData.corporation != ''){
			return rowData.destinationCityCode + ' ' + rowData.alphabet + ' # ' + rowData.manifestId  + ' - ' + rowData.corporation ;
		}
	}

	showHide(e, column) {
		if (column === 'koliGerai') { this.koliGerai = !this.koliGerai; }
		if (column === 'awGerai') { this.awGerai = !this.awGerai; }
		if (column === 'cawGerai') { this.cawGerai = !this.cawGerai; }
		if (column === 'awCorporate') { this.awCorporate = !this.awCorporate; }
		if (column === 'cawCorporate') { this.cawCorporate = !this.cawCorporate; }
		if (column === 'koliCorporate') { this.koliCorporate = !this.koliCorporate; }
	}

}
