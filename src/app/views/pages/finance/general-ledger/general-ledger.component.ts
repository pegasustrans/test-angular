import { Component, HostListener, OnInit, ViewChild } from "@angular/core";
import CustomStore from "devextreme/data/custom_store";
import { AppGlobalVar } from "../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../core/globalvar/userGlobalVar";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { GeneralLedgerDatagridComponent } from "./general-ledger-datagrid/general-ledger-datagrid.component";
import { ActivatedRoute, Router } from "@angular/router";
@Component({
	selector: "kt-general-ledger",
	templateUrl: "./general-ledger.component.html",
	styleUrls: ["./general-ledger.component.scss"],
})
export class GeneralLedgerComponent implements OnInit {
	ds: any = {};
	cityDs: any;
	// dataSource: any;

	menuPriv = false;
	menuForb = true;
	kepalaCoa: boolean = false;

	selectedDate: string;
	monthFix: string;
	endPoint: string;

	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	HistoryForm = {
		menu: "General Ledger",
		historyloginId: 0,
	};

	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
		const today = new Date();
		// Format menjadi yyyy-MM-dd (format yang digunakan dalam dateSerializationFormat)
		this.selectedDate = today.toISOString().split("T")[0];
	}

	ngOnInit() {
		this.historyLoginStart();
		this.checkuserGlobalVar();
	}

	onSearch() {
		this.getList();
	}

	getList() {
		const [year, month] = this.selectedDate.split("-");
		const params = {
			year : year,
			month : month
		};

		this.ds = new CustomStore({
			load: (loadOptions: any) => {
				return this.http
					.get(AppGlobalVar.BASE_API_URL + "general-ledger/list", { headers: this.httpOptions.headers, params })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data,
						};
					})
					.catch((error) => {
						console.log(error);
					});
			},
		});
	}

	private async checkuserGlobalVar() {
		if (UserGlobalVar.USER_ID === "") {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		} else {
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function (value) {
				if (value == "general-ledger") {
					checkPriv = 1;
				}
				// console.log(value);
			});

			if (checkPriv == 1) {
				this.menuPriv = true;
				this.menuForb = false;

				// this.GetList();
			}
		}
	}

	ngOnDestroy() {
		this.historyLoginEnd();
	}

	historyLoginStart() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/start-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	historyLoginEnd() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/end-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	thousandSeparator(data) {
		var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		//roundUp = Math.round(roundUp);
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
}
