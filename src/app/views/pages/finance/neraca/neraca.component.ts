import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kt-neraca',
  templateUrl: './neraca.component.html',
  styleUrls: ['./neraca.component.scss']
})
export class NeracaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  assets = [
    { description: 'Cash', amount: 10000 },
    { description: 'Accounts Receivable', amount: 5000 },
    { description: 'Inventory', amount: 8000 }
  ];

  liabilities = [
    { description: 'Accounts Payable', amount: 4000 },
    { description: 'Loans Payable', amount: 10000 }
  ];

  equity = [
    { description: 'Owner’s Equity', amount: 9000 }
  ];

  totalAssets() {
    return this.assets.reduce((acc, asset) => acc + asset.amount, 0);
  }

  totalLiabilities() {
    return this.liabilities.reduce((acc, liability) => acc + liability.amount, 0);
  }

  totalEquity() {
    return this.equity.reduce((acc, equityItem) => acc + equityItem.amount, 0);
  }

}
