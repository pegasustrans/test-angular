import { BrowserModule, BrowserTransferStateModule } from "@angular/platform-browser";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { DxDataGridModule, DxButtonModule } from "devextreme-angular";
import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Component, HostListener, OnInit } from "@angular/core";
import CustomStore from "devextreme/data/custom_store";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import notify from "devextreme/ui/notify";
import { HighlightProcess } from "../../../../../core/helper";


@Component({
  selector: 'kt-profit-lost-statement-list',
  templateUrl: './profit-lost-statement-list.component.html',
  styleUrls: ['./profit-lost-statement-list.component.scss']
})
export class ProfitLostStatementListComponent implements OnInit {

  ds: any = {};
  menuPriv = true;
	menuForb = false;

  httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};
  
  constructor( private route: ActivatedRoute, private matDialog: MatDialog, private router: Router, private http: HttpClient) {}

  HistoryForm = {
		menu: "Laba - Rugi - List",
		historyloginId: 0,
	};
  

  ngOnInit() {
    this.checkuserGlobalVar();
		this.historyLoginStart();
    this.getList();
  }

  ngOnDestroy() {
		this.historyLoginEnd();
	}

  getList() {
		const params = {
		};
		this.ds = new CustomStore({
      key: "tanggal",
			load: (loadOptions: any) => {
				return this.http
					.get(AppGlobalVar.BASE_API_URL + "profit-loss/list-per-day", { headers: this.httpOptions.headers, params })
					.toPromise()
					.then((data: any) => {
            console.log(data.data)
						return {
							data: data.data,
						};
					})
					.catch((error) => {
						console.log(error);
					});
			},
		});
	}

  historyLoginStart() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/start-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

  historyLoginEnd() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/end-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if (UserGlobalVar.USER_ID === "") {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		} else {
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function (value) {
				if (value == "master-city") {
					checkPriv = 1;
				}
			});

			if (checkPriv == 1) {
				this.menuPriv = true;
				this.menuForb = false;
			}
		}
	}

}
