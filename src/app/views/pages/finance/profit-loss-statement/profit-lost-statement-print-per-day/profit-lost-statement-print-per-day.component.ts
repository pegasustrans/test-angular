import { map } from "rxjs/operators";
import html2pdf from "html2pdf.js";
import { ChangeDetectorRef } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import CustomStore from "devextreme/data/custom_store";
import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";

@Component({
  selector: 'kt-profit-lost-statement-print-per-day',
  templateUrl: './profit-lost-statement-print-per-day.component.html',
  styleUrls: ['./profit-lost-statement-print-per-day.component.scss']
})
export class ProfitLostStatementPrintPerDayComponent implements OnInit {

	menuPriv = true;
	menuForb = false;
	dateString = "2024-1-9";
	currentDate: Date;

	gmDs: any;

	totalUdara: number = 0;
	totalLaut: number = 0;
	totalDarat: number = 0;
	totalRevenue: number = 0;
	revenueData: any[] = []; // Variabel untuk menyimpan data revenue
	bebanData: any[] = [];
	costData: any[] = [];
	totalBeban: number = 0;
	totalCost: number = 0;

	constructor(private http: HttpClient, private cdRef: ChangeDetectorRef) {}

	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	HistoryForm = {
		menu: "Laba Rugi",
		historyloginId: 0,
	};
	ngOnInit() {
		// this.GetList();
		this.historyLoginStart();
		this.checkuserGlobalVar();
		this.loadRevenueData();
		this.loadBebanData();
		this.loadCostData();
	}

	loadRevenueData() {
		const today = new Date();
		const tomorrow = new Date(today);
		tomorrow.setDate(today.getDate() + 1);
		const todayFormatted = today.toISOString().split("T")[0];
		const tomorrowFormatted = tomorrow.toISOString().split("T")[0];

		const apiUrl = AppGlobalVar.BASE_API_URL + "sales-report/get-total-list";
		const params = new HttpParams().set("startDate", todayFormatted).set("endDate", tomorrowFormatted);

		this.http.get<any>(apiUrl, { params }).subscribe(
			(response) => {
				this.revenueData = response.data;
				this.totalUdara = 0;
				this.totalLaut = 0;
				this.totalDarat = 0;

				// Memproses revenue data untuk menghitung total berdasarkan Carrier
				this.revenueData.forEach((revenue) => {
					if (revenue.carrier === "air") {
						this.totalUdara += revenue.totalPrice; // Jumlahkan untuk Carrier Udara
						console.log(this.totalUdara);
					} else if (revenue.carrier === "sea") {
						this.totalLaut += revenue.totalPrice; // Jumlahkan untuk Carrier Laut
						console.log(this.totalLaut);
					} else {
						this.totalDarat += revenue.totalPrice; // Jumlahkan untuk Carrier Darat jika ada
					}
				});

				this.totalRevenue = this.totalUdara + this.totalLaut + this.totalDarat;

				this.cdRef.detectChanges();
			},
			(error) => {
				console.error("Error fetching revenue data:", error);
			}
		);
	}

	loadBebanData() {
		const today = new Date();
		const coaParentId = 6;
		const todayFormatted = today.toISOString().split("T")[0];

		const apiUrl = AppGlobalVar.BASE_API_URL + "general-ledger/list";
		const params = new HttpParams().set("date", todayFormatted).set("coaParentId", coaParentId.toString());

		this.http.get<any>(apiUrl, { params }).subscribe(
			(response) => {
				this.bebanData = response.data; // Ensure this is the correct field
				this.calculateTotalBeban(); // Calculate total after fetching data
			},
			(error) => {
				console.error("Error fetching beban data:", error);
			}
		);
	}

	loadCostData() {
		const today = new Date();
		const coaParentId = 5;
		const todayFormatted = today.toISOString().split("T")[0];

		const apiUrl = AppGlobalVar.BASE_API_URL + "general-ledger/list";
		const params = new HttpParams().set("date", todayFormatted).set("coaParentId", coaParentId.toString());

		this.http.get<any>(apiUrl, { params }).subscribe(
			(response) => {
				console.log("Cost data:", response);
				this.costData = response.data; // Ensure this is the correct field
				this.calculateTotalCost(); // Calculate total after fetching data
			},
			(error) => {
				console.error("Error fetching beban data:", error);
			}
		);
	}

	calculateTotalBeban() {
		this.totalBeban = this.bebanData.reduce((sum, beban) => sum + beban.total, 0);
	}
	calculateTotalCost() {
		this.totalCost = this.costData.reduce((sum, cost) => sum + cost.total, 0);
	}

	ngOnDestroy() {
		this.historyLoginEnd();
	}
	historyLoginStart() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/start-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if (UserGlobalVar.USER_ID === "") {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		} else {
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function (value) {
				if (value == "laba-rugi") {
					checkPriv = 1;
				}
				// console.log(value);
			});

			if (checkPriv == 1) {
				this.menuPriv = true;
				this.menuForb = false;

				// this.GetList();
			}
		}
	}

	historyLoginEnd() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/end-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	downloadPDF() {
		const collapsibleElements = document.querySelectorAll(".collapse");

		collapsibleElements.forEach((element) => {
			element.classList.add("show");
		});

		const element = document.getElementById("content-to-save");

		const opt = {
			margin: 0.5,
			filename: "Laporan_Laba_Rugi.pdf",
			image: { type: "jpeg", quality: 0.98 },
			jsPDF: { unit: "in", format: "letter", orientation: "portrait" },
		};

		html2pdf()
			.from(element)
			.set(opt)
			.save()
			.then(() => {
				collapsibleElements.forEach((element) => {
					element.classList.remove("show");
				});
			});
	}

}
