import { Component, HostListener, OnInit, ViewChild } from "@angular/core";
import CustomStore from "devextreme/data/custom_store";
import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { DxDataGridComponent } from "devextreme-angular/ui/data-grid";

@Component({
	selector: "kt-sales-report",
	templateUrl: "./sales-report.component.html",
	styleUrls: ["./sales-report.component.scss"],
})
export class SalesReportComponent implements OnInit {
	@HostListener("window:beforeunload", ["$event"]) unload(event) {
		this.historyLoginEnd();
	}

	@ViewChild("targetDataGrid", { static: false }) dataGrid: DxDataGridComponent;

	constructor(private http: HttpClient) {}
	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	selectedOption: string;
	selectedDate: string;
	startDate: string;
	endDate: string;

	gmDs: any = {};
	cawCorporate: boolean = false;
	awCorporate: boolean = false;
	koliCorporate: boolean = false;
	awGerai: boolean = false;
	cawGerai: boolean = false;
	koliGerai: boolean = false;
	params: any;

	menuPriv = false;
	menuForb = true;

	dateOptions = [
		{ label: " Per Hari Ini", value: "today" },
		{ label: " Per Tanggal", value: "perDate" },
		{ label: " Per Minggu", value: "perWeek" },
		{ label: " Per Bulan", value: "perMonth" },
		{ label: " Per Tahun", value: "perYear" },
		{ label: " Range Date", value: "customDate" },
	];

	getDataMonitoring() {
		const today = new Date();
		const formattedDateToday = `${today.getFullYear()}-${String(today.getMonth() + 1).padStart(2, "0")}-${String(today.getDate()).padStart(2, "0")}`;

		const tomorrow = new Date(today);
		tomorrow.setDate(today.getDate() + 1);
		const formattedDateTomorrow = `${tomorrow.getFullYear()}-${String(tomorrow.getMonth() + 1).padStart(2, "0")}-${String(tomorrow.getDate()).padStart(2, "0")}`;

		try {
			switch (this.selectedOption) {
				case "perDate":
					const selectedDateObj = new Date(this.selectedDate);

					// Menambahkan satu hari untuk mendapatkan afterDate
					const afterDateObj = new Date(selectedDateObj);
					afterDateObj.setDate(selectedDateObj.getDate() + 1);

					// Format afterDate menjadi string dalam format 'YYYY-MM-DD'
					const yearAfter = afterDateObj.getFullYear();
					const monthAfter = String(afterDateObj.getMonth() + 1).padStart(2, "0");
					const dayAfter = String(afterDateObj.getDate()).padStart(2, "0");
					const afterDate = `${yearAfter}-${monthAfter}-${dayAfter}`;

					this.params = { startDate: this.selectedDate, endDate: afterDate };

					break;
				case "perWeek":
					const [year, week] = this.selectedDate.split("-W").map(Number); // Ekstrak tahun dan nomor minggu
					const firstDayOfYear = new Date(year, 0, 1); // 1 Januari pada tahun yang dipilih

					// Hitung jumlah hari untuk mencapai minggu yang dipilih
					const daysOffset = (week - 1) * 7;

					// Mulai minggu pada hari Senin pertama (ISO 8601), jadi kita perlu menyesuaikan ke hari Senin
					const dayOfWeek = firstDayOfYear.getDay();
					const dayOffset = dayOfWeek <= 4 ? 1 - dayOfWeek : 8 - dayOfWeek; // Pastikan minggu dimulai dari Senin
					const startOfWeek = new Date(firstDayOfYear.setDate(firstDayOfYear.getDate() + daysOffset + dayOffset));

					// Hitung akhir minggu (Minggu)
					const endOfWeek = new Date(startOfWeek);
					endOfWeek.setDate(startOfWeek.getDate() + 6);

					// Format tanggal awal dan akhir minggu
					const formattedStartOfWeek = `${startOfWeek.getFullYear()}-${String(startOfWeek.getMonth() + 1).padStart(2, "0")}-${String(startOfWeek.getDate()).padStart(2, "0")}`;
					const formattedEndOfWeek = `${endOfWeek.getFullYear()}-${String(endOfWeek.getMonth() + 1).padStart(2, "0")}-${String(endOfWeek.getDate()).padStart(2, "0")}`;

					this.params = { startDate: formattedStartOfWeek, endDate: formattedEndOfWeek };

					console.log("Start of week:", this.params.startDate);
					console.log("End of week:", this.params.endDate);

					break;

				case "perMonth":
					const monthDate = new Date(this.selectedDate);
					if (isNaN(monthDate.getTime())) throw new Error("Invalid date format");
					const updateMonth = String(monthDate.getMonth() + 1).padStart(2, "0");
					const updateMonth2 = String(monthDate.getMonth() + 2).padStart(2, "0");

					const startDate = monthDate.getFullYear() + "-" + updateMonth + "-01";
					const endDate = monthDate.getFullYear() + "-" + updateMonth2 + "-01";

					this.params = { startDate: startDate, endDate: endDate };

					break;
				case "perYear":
					const startYear = this.selectedDate;
					const endYear = parseInt(startYear) + 1;

					this.params = { startDate: startYear + "-01-01", endDate: endYear + "-01-01" };

					break;
				case "customDate":

					// Ubah string menjadi objek Date
					let date = new Date(this.endDate);

					// Tambahkan satu hari
					date.setDate(date.getDate() + 1);

					// Ubah kembali ke format string (YYYY-MM-DD)
					let endDateFinal = date.toISOString().split("T")[0];

					console.log(endDateFinal);

					this.params = { startDate: this.startDate, endDate: endDateFinal };

					break;
				default:
					this.params = { startDate: formattedDateToday, endDate: formattedDateTomorrow };
			}

			this.GetList();
		} catch (error) {
			console.error("Error:", error.message);
			return;
		}
	}
	onOptionChange() {}

	HistoryForm = {
		menu: "Sales Report",
		historyloginId: 0,
	};

	ngOnInit() {
		this.historyLoginStart();
		this.checkuserGlobalVar();
	}

	ngOnDestroy() {
		this.historyLoginEnd();
	}

	historyLoginStart() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/start-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	historyLoginEnd() {
		const config = { headers: new HttpHeaders().set("Content-Type", "application/json; charset=utf-8") };
		this.http.post(AppGlobalVar.BASE_API_URL + "history-login/end-time", JSON.stringify(this.HistoryForm), config).subscribe(
			(data: any) => {},
			(error) => {
				alert(error.statusText + ". " + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if (UserGlobalVar.USER_ID === "") {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		} else {
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function (value) {
				if (value == "sales-report") {
					checkPriv = 1;
				}
			});

			if (checkPriv == 1) {
				this.menuPriv = true;
				this.menuForb = false;
			}
		}
	}

	GetList() {
		this.gmDs = new CustomStore({
			key: "destinationCityCode",
			load: (loadOptions: any) => {
				return this.http
					.get(AppGlobalVar.BASE_API_URL + "sales-report/list", { headers: this.httpOptions.headers, params: this.params })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data,
						};
					})
					.catch((error) => {
						console.log(error);
					});
			},
		});
	}

	expandGrid() {
		this.dataGrid.instance.expandAll(0);
		this.dataGrid.instance.expandAll(1);
		this.dataGrid.instance.expandAll(2);
		this.dataGrid.instance.expandAll(3);
	}

	thousandSeparator(data) {
		var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}


}
