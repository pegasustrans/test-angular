import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsMonitoringReceiverListComponent } from './goods-monitoring-receiver-list.component';

describe('GoodsMonitoringReceiverListComponent', () => {
  let component: GoodsMonitoringReceiverListComponent;
  let fixture: ComponentFixture<GoodsMonitoringReceiverListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsMonitoringReceiverListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsMonitoringReceiverListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
