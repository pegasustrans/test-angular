import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import {DxDataGridComponent} from 'devextreme-angular/ui/data-grid';
import moment from 'moment';

@Component({
	selector: 'kt-goods-monitoring-receiver-list',
	templateUrl: './goods-monitoring-receiver-list.component.html',
	styleUrls: ['./goods-monitoring-receiver-list.component.scss']
})
export class GoodsMonitoringReceiverListComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	@ViewChild('targetDataGrid',{static: true}) dataGrid: DxDataGridComponent;
	gmDs: any = {};
	searchForm = {
		date: new Date(),
		dateString: null,
	};

	menuPriv = false;
	menuForb = true;

	HistoryForm = {
		menu: "Monitoring SPB Receiver",
		historyloginId : null
	};

	dateStart:any;

	constructor(private http: HttpClient) { }

	ngOnInit() {
		this.checkuserGlobalVar();
		// this.GetList();
		this.historyLoginStart();
		// this.dateStart = moment();
	}

	 ngOnDestroy() { 
		// let period = moment.utc(moment(this.dateStart).diff(moment())).format("HH:mm:ss");
		// console.log('period : ',period);
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "gmr-spb"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;
			
			}
		}
	}

	// checkData(){
	// 	var a = 0;
	// 	this.http.get(AppGlobalVar.BASE_API_URL + 'goods-monitoring-receiver/checked').subscribe((response: any) =>{
	// 		console.log(response);
	// 		if(response != null || response > 0){
	// 			console.log('ijin diberikan');
	// 		}else{
	// 			console.log('ijin tidak diberikan');
	// 			this.permit = true;
	// 			this.monitoring_receiver_list = false;
	// 		}
	// 		// console.log(response.status);
	// 		// this.http.get(AppGlobalVar.BASE_API_URL + 'goods-monitoring-receiver/getAllData').subscribe((response: any) =>{
	// 		// 	console.log(response);
	// 		// });
	// 		// response.data.forEach(function (value) {
	// 		// 	a++;
	// 		// });

	// 		// if(a > 0){
	// 		// 	console.log('ijin diberikan');
	// 		// 	this.GetList();
	// 		// }else{
	// 		// 	console.log('ijin tidak diberikan');
			
	// 		// }
	// 	});
	// }
	httpOptions = {
		headers: new HttpHeaders({
		  'Content-Type': 'application/json'
			})
	  };

	GetList(){
		const params = {
			CreatedAt : this.searchForm.date.getFullYear() + '-' + (this.searchForm.date.getMonth() + 1) + '-' + this.searchForm.date.getDate()
			, type : 'receiver'
		};

		this.gmDs = new CustomStore({
			key: 'manifestId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring/monitoring-spb', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						if(data != null){
							return {
								data: data.data
							};
						}
					})
					.catch(error => { console.log(error); });
			},
		});
	}

	// GetList(){
	// 	const params = {
	// 		CreatedAt : this.searchForm.date.getFullYear() + '-' + (this.searchForm.date.getMonth() + 1) + '-' + this.searchForm.date.getDate()
	// 	};

	// 	this.gmDs = new CustomStore({
	// 		key: 'manifestId',
	// 		load:  (loadOptions: any) => {
	// 			return this.http.get(AppGlobalVar.BASE_API_URL + 'goods-monitoring-receiver/list', { headers: this.httpOptions.headers, params  })
	// 				.toPromise()
	// 				.then((data: any) => {
	// 					if(data != null){
	// 						return {
	// 							data: data.data
	// 						};
	// 					}
	// 				})
	// 				.catch(error => { console.log(error); });
	// 		},
	// 	});
	// }

	onFormSubmit(e) {
		this.GetList();
	}

	expandGrid() {
		this.dataGrid.instance.expandAll(0);
		this.dataGrid.instance.expandAll(1);
	}

	thousandSeparator(data) {
		var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		//roundUp = Math.round(roundUp);
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
		// return data.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	}

	ColManifestNo(rowData) {
		return rowData.destinationCityCode + ' ' + rowData.alphabet + ' # ' + rowData.manifestId;
	}

}
