import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsMonitoringDatagridComponent } from './goods-monitoring-datagrid.component';

describe('GoodsMonitoringDatagridComponent', () => {
  let component: GoodsMonitoringDatagridComponent;
  let fixture: ComponentFixture<GoodsMonitoringDatagridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsMonitoringDatagridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsMonitoringDatagridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
