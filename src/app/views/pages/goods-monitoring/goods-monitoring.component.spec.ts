import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsMonitoringComponent } from './goods-monitoring.component';

describe('GoodsMonitoringComponent', () => {
  let component: GoodsMonitoringComponent;
  let fixture: ComponentFixture<GoodsMonitoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsMonitoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsMonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
