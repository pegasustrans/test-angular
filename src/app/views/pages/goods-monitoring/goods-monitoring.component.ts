import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../core/globalvar/appGlobalVar';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GoodsMonitoringDatagridComponent} from './goods-monitoring-datagrid/goods-monitoring-datagrid.component';
import {UserGlobalVar} from '../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-goods-monitoring',
  templateUrl: './goods-monitoring.component.html',
  styleUrls: ['./goods-monitoring.component.scss']
})
export class GoodsMonitoringComponent implements OnInit {

	// @ViewChild('viaSrg', {static: false}) viaSrg: GoodsMonitoringDatagridComponent;
	// @ViewChild('viaKalTim', {static: false}) viaKalTim: GoodsMonitoringDatagridComponent;
	// @ViewChild('viaHlm', {static: false}) viaHlm: GoodsMonitoringDatagridComponent;
	// @ViewChild('viaLpKedoya', {static: false}) viaLpKedoya: GoodsMonitoringDatagridComponent;
	// @ViewChild('viaSrgTruck', {static: false}) viaSrgTruck: GoodsMonitoringDatagridComponent;
	// @ViewChild('viaAll', {static: false}) viaAll: GoodsMonitoringDatagridComponent;
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	@ViewChild('allData', {static: false}) allData: GoodsMonitoringDatagridComponent;

	searchForm = {
		date: new Date(),
		dateString: null,
	};

	menuPriv = false;
	menuForb = true;

	
	HistoryForm = {
		menu: "Monitoring SPB Sender",
		historyloginId:0
	};

  constructor(private http: HttpClient) { }

  ngOnInit() {
	this.historyLoginStart();
	this.checkuserGlobalVar();
  }
  
  ngOnDestroy() { 
	this.historyLoginEnd();
 }

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	  .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	  .subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	  .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	  .subscribe((data: any) => {

		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	);
}

  private async checkuserGlobalVar() {
    if(UserGlobalVar.USER_ID === '') {
        setTimeout(() => {
            this.checkuserGlobalVar();
        }, 1000);
    }

    else{		
        var checkPriv = 0;
        await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
            if (value == "gms-spb"){
                checkPriv = 1;
            }
        });

        if (checkPriv == 1){
            this.menuPriv = true;
            this.menuForb = false;
        
        }
    }
}


	onFormSubmit(e) {
		if (this.searchForm.date !== null) {
			this.searchForm.dateString = this.searchForm.date.getFullYear() + '-' + (this.searchForm.date.getMonth() + 1) + '-' + this.searchForm.date.getDate();
		}

		// this.viaSrg.GetList(this.searchForm.dateString);
		// this.viaKalTim.GetList(this.searchForm.dateString);
		// this.viaHlm.GetList(this.searchForm.dateString);
		// this.viaLpKedoya.GetList(this.searchForm.dateString);
		// this.viaSrgTruck.GetList(this.searchForm.dateString);
		// this.viaAll.GetList(this.searchForm.dateString);
		this.allData.GetList(this.searchForm.dateString);
	}


}
