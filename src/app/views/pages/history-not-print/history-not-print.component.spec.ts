import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryNotPrintComponent } from './history-not-print.component';

describe('HistoryNotPrintComponent', () => {
  let component: HistoryNotPrintComponent;
  let fixture: ComponentFixture<HistoryNotPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryNotPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryNotPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
