import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';
import {AppGlobalVar} from '../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-history-not-print',
  templateUrl: './history-not-print.component.html',
  styleUrls: ['./history-not-print.component.scss']
})
export class HistoryNotPrintComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
	 
  ds: any = {};
  menuPriv = false;
  menuForb = true;

  constructor(private http: HttpClient) { }

  HistoryForm = {
	menu			: "History Not Print",
	historyloginId 	: 0
};


  ngOnInit() {
	  this.historyLoginStart();
	  this.checkuserGlobalVar();
  }

  private async checkuserGlobalVar() {
    if(UserGlobalVar.USER_ID === '') {
      setTimeout(() => {
        this.checkuserGlobalVar();
      }, 1000);
    }
  
    else{		
      var checkPriv = 0;
      await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
        if (value == "master-history-not-print"){
          checkPriv = 1;
        }
      });
  
      if (checkPriv == 1){
        this.menuPriv = true;
        this.menuForb = false;
        
      	this.GetList();
      }
    }
  }

  ngOnDestroy() { 
	this.historyLoginEnd();
 }

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	  .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	  .subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	  .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	  .subscribe((data: any) => {

		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	);
}
  

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  formSearch = {
		dateStart : new Date(),
		dateTo    : new Date()
	};

  GetList() {
		const params = {
			dateFrom : this.formSearch.dateStart.toDateString(),
			dateTo    : this.formSearch.dateTo.toDateString()
		}; 

		this.ds = new CustomStore({
			key: 'spbId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + "history-not-login/list", { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  onSearch(){
    this.GetList();
  }

}