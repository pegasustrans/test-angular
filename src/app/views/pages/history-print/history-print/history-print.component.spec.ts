import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryPrintComponent } from './history-print.component';

describe('HistoryPrintComponent', () => {
  let component: HistoryPrintComponent;
  let fixture: ComponentFixture<HistoryPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
