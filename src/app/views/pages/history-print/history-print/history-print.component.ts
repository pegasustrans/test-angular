import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-history-print',
  templateUrl: './history-print.component.html',
  styleUrls: ['./history-print.component.scss']
})
export class HistoryPrintComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
	 
  ds: any = {};
  menuPriv = false;
  menuForb = true;

	HistoryForm = {
		menu			: "History Print",
		historyloginId 	: 0
	};

	formSearch = {
		dateStart : new Date(),
		dateTo    : new Date()
	};

  constructor(private http: HttpClient) { }
  ngOnInit() {
	  this.historyLoginStart();
	  this.checkuserGlobalVar();
  }

  onSearch(){
    this.GetList();
  }

  private async checkuserGlobalVar() {
    if(UserGlobalVar.USER_ID === '') {
      setTimeout(() => {
        this.checkuserGlobalVar();
      }, 1000);
    }
  
    else{		
      var checkPriv = 0;
      await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
        if (value == "master-history-print"){
          checkPriv = 1;
        }
      });
  
      if (checkPriv == 1){
        this.menuPriv = true;
        this.menuForb = false;
      }
    }
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  GetList() {
	const params = {
		dateFrom : this.formSearch.dateStart.toDateString(),
		dateTo    : this.formSearch.dateTo.toDateString()
	}; 

		this.ds = new CustomStore({
			key: 'username',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + "HistoryLogin/list", { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

}
