import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightRouteCreateComponent } from './flight-route-create.component';

describe('FlightRouteCreateComponent', () => {
  let component: FlightRouteCreateComponent;
  let fixture: ComponentFixture<FlightRouteCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightRouteCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightRouteCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
