import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-flight-route-create',
  templateUrl: './flight-route-create.component.html',
  styleUrls: ['./flight-route-create.component.scss']
})
export class FlightRouteCreateComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
  originStationDs         : any;
  firstTransitStationDs   : any;
  secondTransitStationDs  : any;
  thirdTransitStationDs   : any;
  destinationStationDs    : any;
  carrierDs               : any;
  vendorCarrierDs         : any;
  firstFlightDs           : any;
  secondFlightDs          : any;
  thirdFlightDs           : any;
  fourthFlightDs          : any;

  HistoryForm = {
	menu: "Master - Flight Route - Create",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  formCreate = {
    originStationId       : null,
    destinationStationId  : null,
    carrierId             : null,
    vendorCarrierId       : null,
    firstFlightId         : null,
    firstTransitStationId : null,
    secondFlightId        : null,
    secondTransitStationId: null,
    thirdFlightId         : null,
    thirdTransitStationId : null,
    fourthFlightId        : null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.readOriginStation();
    this.readDestinationStation();
    this.readCarrier();
    this.readFirstTransitStation();
    this.readSecondTransitStation();
    this.readThirdTransitStation();
	this.historyLoginStart();
	this.secondFlight();
	this.thirdFlight();
	this.fourthFlight();
  }

  readOriginStation(){
    const params = {};
    
		this.originStationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-read-origin-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

   readDestinationStation(){
    const params = {};
    
		this.destinationStationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-read-destination-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  readCarrier(){
    const params = {};
    
		this.carrierDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-read-carrier', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  readVendorCarrier(carrierId){
    const params = {
      carrierId     : carrierId
    };
    
		this.vendorCarrierDs = new DataSource({
			store: new CustomStore({
				key: 'vendorCarrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-read-vendor-carrier', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  readFirstTransitStation(){
    const params = {};
    
		this.firstTransitStationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-read-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  readSecondTransitStation(){
    const params = {};
    
		this.secondTransitStationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-read-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  readThirdTransitStation(){
    const params = {};
    
		this.thirdTransitStationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-read-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  firstFlight(carrierId){
	const params = {
		carrierId     : carrierId
	};
	  
	this.firstFlightDs = new DataSource({
		store: new CustomStore({
			key: 'flightId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-read-flight', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});
  }

  secondFlight(){
	const params = {
		
	};
	  
	this.secondFlightDs = new DataSource({
		store: new CustomStore({
			key: 'flightId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-flight-list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});
  }

  thirdFlight(){
	const params = {
		
	};
	  
	this.thirdFlightDs = new DataSource({
		store: new CustomStore({
			key: 'flightId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-flight-list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});
  }

  fourthFlight(){
	const params = {
		
	};
	  
	this.fourthFlightDs = new DataSource({
		store: new CustomStore({
			key: 'flightId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-flight-list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});
  }
  
  onTriggerChangeOriginStation(e){
    console.log(e.value);    
  }

  onTriggerChangeCarrier(e){
    this.readVendorCarrier(e.value);
	this.firstFlight(e.value);
	
  }

  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}
  onSave(){

    if(this.formCreate.originStationId == null){
		notify({
			message: 'Bandara Asal harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.destinationStationId == null){
		notify({
			message: 'Bandara Tujuan harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.carrierId == null){
		notify({
			message: 'Carrier harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.vendorCarrierId == null){
		notify({
			message: 'Vendor Carrier harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.firstFlightId == null){
		notify({
			message: 'First Flight harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else {

      var params = {
		originStationId       	: this.formCreate.originStationId       
		, destinationStationId  : this.formCreate.destinationStationId  
		, carrierId             : this.formCreate.carrierId             
		, vendorCarrierId       : this.formCreate.vendorCarrierId       
		, firstFlightId         : this.formCreate.firstFlightId         
		, secondFlightId        : this.formCreate.secondFlightId        
		, thirdFlightId         : this.formCreate.thirdFlightId         
		, fourthFlightId        : this.formCreate.fourthFlightId        
      }
	  console.log(params);

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-create', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/flight-route']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }

  onBack(){
    this.router.navigate(['/flight-route']);
  }

}
