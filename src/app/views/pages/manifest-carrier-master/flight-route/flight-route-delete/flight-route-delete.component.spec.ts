import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightRouteDeleteComponent } from './flight-route-delete.component';

describe('FlightRouteDeleteComponent', () => {
  let component: FlightRouteDeleteComponent;
  let fixture: ComponentFixture<FlightRouteDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightRouteDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightRouteDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
