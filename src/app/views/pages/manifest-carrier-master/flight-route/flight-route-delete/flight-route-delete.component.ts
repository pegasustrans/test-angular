import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-flight-route-delete',
  templateUrl: './flight-route-delete.component.html',
  styleUrls: ['./flight-route-delete.component.scss']
})
export class FlightRouteDeleteComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

  originStationName       : any;      
  destinationStationName  : any;
  carrierName             : any;      
  vendorCarrierName       : any;      
  firstFlightNo           : any;      
  secondFlightNo          : any;      
  thirdFlightNo           : any;      
  fourthFlightNo          : any;
  flightRouteId           : any;
  
  HistoryForm = {
		menu: "Master - Flight Route - Delete",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.originStationName       = params.p1;      
        this.destinationStationName  = params.p2;
        this.carrierName             = params.p3;      
        this.vendorCarrierName       = params.p4;      
        this.firstFlightNo           = params.p5;      
        this.secondFlightNo          = params.p6;      
        this.thirdFlightNo           = params.p7;      
        this.fourthFlightNo          = params.p8;
        this.flightRouteId           = params.p9;
      }
    );
  }

  onSave(){
      var params = {
        flightRouteId : parseInt(this.flightRouteId)
      }

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/flight-route-delete', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/flight-route']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}
  onBack(){
    this.router.navigate(['/flight-route']);
  }

}
