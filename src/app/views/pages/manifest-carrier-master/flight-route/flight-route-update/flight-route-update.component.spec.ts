import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightRouteUpdateComponent } from './flight-route-update.component';

describe('FlightRouteUpdateComponent', () => {
  let component: FlightRouteUpdateComponent;
  let fixture: ComponentFixture<FlightRouteUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightRouteUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightRouteUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
