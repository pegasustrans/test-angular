import { Component, HostListener, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import { Router } from '@angular/router';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-flight-route',
  templateUrl: './flight-route.component.html',
  styleUrls: ['./flight-route.component.scss']
})
export class FlightRouteComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
	appGlobalVar = AppGlobalVar;

	dsFlight: any = {};
	menuPriv = false;
	menuForb = true;

	HistoryForm = {
		menu: "Master - Flight Route",
		historyloginId:0
	};

	constructor(private http: HttpClient, private router: Router) { }

	ngOnInit() {
		this.checkuserGlobalVar();
		this.historyLoginStart();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
		  setTimeout(() => {
			this.checkuserGlobalVar();
		  }, 1000);
		}
	  
		else{		
		  var checkPriv = 0;
		  await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-flight-route"){
			  checkPriv = 1;
			}
		  });
	  
		  if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
			
			  this.GetList();
		  }
		}
	  }
	  
	GetList() {
		this.dsFlight = new CustomStore({
			key: 'flightRouteId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/list-flight-route')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	addRoute(){
		this.router.navigate(['/flight-route-create']);
	}

	actionCLick = (e) => {
		if (e.itemData.key === 'delete') {
			this.router.navigate(['/flight-route-delete'], 
			{ 
				queryParams: { 
					p1: e.itemData.data.originStationName      
					, p2: e.itemData.data.destinationStationName  
					, p3: e.itemData.data.carrierName             
					, p4: e.itemData.data.vendorCarrierName       
					, p5: e.itemData.data.firstFlightNo         
					, p6: e.itemData.data.secondFlightNo        
					, p7: e.itemData.data.thirdFlightNo         
					, p8: e.itemData.data.fourthFlightNo   
					, p9: e.itemData.data.flightRouteId   
				} 
			});
		}
	}
}
