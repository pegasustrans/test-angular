import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostCarrierCreateComponent } from './manifest-carrier-cost-carrier-create.component';

describe('ManifestCarrierCostCarrierCreateComponent', () => {
  let component: ManifestCarrierCostCarrierCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostCarrierCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostCarrierCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostCarrierCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
