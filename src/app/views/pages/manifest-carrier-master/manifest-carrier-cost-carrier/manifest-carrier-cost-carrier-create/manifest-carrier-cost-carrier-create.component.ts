import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-manifest-carrier-cost-carrier-create',
  templateUrl: './manifest-carrier-cost-carrier-create.component.html',
  styleUrls: ['./manifest-carrier-cost-carrier-create.component.scss']
})
export class ManifestCarrierCostCarrierCreateComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
  carrierDs           	: any;
  originStationDs      	: any;
  firstFlightDs  	      : any;
  lastFlightDs      		: any;
  vendorCarrierDs 		  : any;
  destinationStationDs 	: any;

  HistoryForm = {
	menu: "Manifest Carrier - Cost Carrier - Create",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  formCreate = {
    carrierId           	: null,
    originStationId      	: null,
    firstflightId 	      : null,
    lastflightId      	  : null,
    vendorCarrierId 	    : null,
    destinationStationId  : null,
    baseCost            	: null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.readOriginStation();
    this.readDestinationStation();
	this.historyLoginStart();
    // this.readCarrier();
    // this.readFirstFlight();
    // this.readLastFlight();
    // this.readStationDestination();
    // this.readVendorCarrier();
  }

  readOriginStation(){
    const params = {};

		this.originStationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-carrier-read-origin-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onTriggerChangeOriginStation(e){
    // console.log(e.value, this.formCreate.destinationStationId);    
    this.readCarrier(e.value, this.formCreate.destinationStationId);
  }

  readDestinationStation(){
    const params = {};

		this.destinationStationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-carrier-read-destination-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onTriggerChangeDestinationStation(e){
    // console.log(this.formCreate.originStationId, e.value);
    this.readCarrier(this.formCreate.originStationId, e.value);
  }

  readCarrier(originStationId, destinationStationId){
    const params = {
      originStationId       : originStationId,
      destinationStationId  : destinationStationId
    };

		this.carrierDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-carrier-read-carrier', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onTriggerChangeCarrier(e){
    this.readFirstFlight(e.value);
    this.readVendorCarrier(e.value);
  }

  readFirstFlight(carrierId){
    const params = {
      carrierId : carrierId
    };

		this.firstFlightDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-carrier-read-first-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onTriggerChangeLast(e){
        this.readLastFlight(this.formCreate.carrierId, e.value);
  }

  readLastFlight(carrierId, firstFlightId){
    const params = {
      carrierId     : carrierId,
      firstFlightId : firstFlightId
    };
	console.log(params);

		this.lastFlightDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-carrier-read-last-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  readVendorCarrier(carrierId){
    const params = {
      carrierId : carrierId
    };

		this.vendorCarrierDs = new DataSource({
			store: new CustomStore({
				key: 'vendorCarrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-carrier-read-vendor-carrier', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onSave(){
	  console.log(this.formCreate);
	  
    if(this.formCreate.originStationId == null){
		notify({
			message: 'Bandara Asal harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.destinationStationId == null){
		notify({
			message: 'Bandara Tujuan harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.carrierId == null){
		notify({
			message: 'Carrier harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.vendorCarrierId == null){
		notify({
			message: 'Vendor Carrier harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
  }else if(this.formCreate.firstflightId == null){
		notify({
			message: 'Nomor Penerbangan Pertama harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
  }else if(this.formCreate.lastflightId == null){
		notify({
			message: 'Nomor Penerbangan Terakhir harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.baseCost == null){
		notify({
			message: 'Base Cost harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else {
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-carrier-create', JSON.stringify(this.formCreate), config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-cost-carrier']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }
  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}


  onBack(){
    this.router.navigate(['/manifest-carrier-cost-carrier']);
    }

}
