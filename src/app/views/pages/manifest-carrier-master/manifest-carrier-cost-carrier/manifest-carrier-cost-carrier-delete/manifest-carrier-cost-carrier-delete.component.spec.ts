import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostCarrierDeleteComponent } from './manifest-carrier-cost-carrier-delete.component';

describe('ManifestCarrierCostCarrierDeleteComponent', () => {
  let component: ManifestCarrierCostCarrierDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierCostCarrierDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostCarrierDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostCarrierDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
