import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';
import { param } from 'jquery';


@Component({
  selector: 'kt-manifest-carrier-cost-carrier-delete',
  templateUrl: './manifest-carrier-cost-carrier-delete.component.html',
  styleUrls: ['./manifest-carrier-cost-carrier-delete.component.scss']
})
export class ManifestCarrierCostCarrierDeleteComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }


 HistoryForm = {
  menu: "Manifest Carrier - Cost Carrier - Delete",
  historyloginId:0
};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  costCarrierId       : any;
  costCarrierDetailId : any;
  baseCost            : any;
  carrierName         : any;
  originStation       : any;
  firstFlightNo       : any;
  lastFlightNo        : any;
  vendorName          : any;
  destinationStation  : any;

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
       this.costCarrierId         = params.p1;
       this.costCarrierDetailId   = params.p2;
       this.baseCost              = params.p3;
       this.carrierName           = params.p4;
       this.originStation         = params.p5;
       this.firstFlightNo         = params.p6;
       this.lastFlightNo          = params.p7;
       this.vendorName            = params.p8;
       this.destinationStation    = params.p9;
      }
    );
  }

  onSave(){
    var params = {
      costCarrierId       : parseInt(this.costCarrierId),
      costCarrierDetailId : parseInt(this.costCarrierDetailId)
    }
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-carrier-delete', params, config)
      .subscribe((data: any) => {

        if(data.data[0].result == 'false'){
          notify({
            message: data.data[0].message,
            type: 'error',
            displayTime: 5000,
            width: 400,
          });
        }else{
          notify({
            message: data.data[0].message,
            type: 'success',
            displayTime: 5000,
            width: 400,
          });

          setTimeout(() => {
            this.router.navigate(['/manifest-carrier-cost-carrier']);
          }, 1000);					
        }
        
        },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
    );
  }
  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}
  onBack(){
    this.router.navigate(['/manifest-carrier-cost-carrier']);
    }

}