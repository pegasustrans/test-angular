import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostCarrierUpdateComponent } from './manifest-carrier-cost-carrier-update.component';

describe('ManifestCarrierCostCarrierUpdateComponent', () => {
  let component: ManifestCarrierCostCarrierUpdateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostCarrierUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostCarrierUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostCarrierUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
