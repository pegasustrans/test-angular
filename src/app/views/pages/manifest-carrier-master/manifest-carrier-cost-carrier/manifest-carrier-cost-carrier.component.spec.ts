import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostCarrierComponent } from './manifest-carrier-cost-carrier.component';

describe('ManifestCarrierCostCarrierComponent', () => {
  let component: ManifestCarrierCostCarrierComponent;
  let fixture: ComponentFixture<ManifestCarrierCostCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
