import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostDestinationCreateComponent } from './manifest-carrier-cost-destination-create.component';

describe('ManifestCarrierCostDestinationCreateComponent', () => {
  let component: ManifestCarrierCostDestinationCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostDestinationCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostDestinationCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostDestinationCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
