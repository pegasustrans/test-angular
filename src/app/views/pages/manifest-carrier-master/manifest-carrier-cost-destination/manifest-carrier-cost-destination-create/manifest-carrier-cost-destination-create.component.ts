import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-manifest-carrier-cost-destination-create',
  templateUrl: './manifest-carrier-cost-destination-create.component.html',
  styleUrls: ['./manifest-carrier-cost-destination-create.component.scss']
})
export class ManifestCarrierCostDestinationCreateComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
  carrierDs           		: any;
  stationOriginDs      		: any;
  stationDestinationDs  	: any;
  vendorOriginDs      		: any;
  vendorDestinationDs 		: any;
  cityDestinationDs   		: any;

  HistoryForm = {
	menu: "Manifest Carrier - Cost Destination - Create",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  formCreate = {
    carrierId           	: null,
    stationOriginId      	: null,
    stationDestinationId 	: null,
    vendorOriginId      	: null,
    vendorDestinationId 	: null,
    cityDestinationId   	: null,
    baseCost            	: null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.readCarrier();
    this.readStationOrigin();
    this.readStationDestination();
	this.historyLoginStart();
  }

  readCarrier(){
    const params = {};

		this.carrierDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-destination-read-carrier', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  readStationOrigin(){
	const params = {
		//originVendorId : this.fromCreate.vendorOriginId
	};

	this.stationOriginDs = new DataSource({
		store: new CustomStore({
			key: 'stationId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-destination-read-station-origin', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});

  }

  readStationDestination(){
	const params = {
		//originVendorId : this.fromCreate.vendorOriginId
	};

	this.stationDestinationDs = new DataSource({
		store: new CustomStore({
			key: 'stationId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-destination-read-station-destination', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});

  }

  readVendorOrigin(stationId) {
		const params = {
			stationOriginId : stationId
		};

		this.vendorOriginDs = new DataSource({
			store: new CustomStore({
				key: 'vendorId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-destination-read-vendor-origin', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  readVendorDestination(stationId) {
		const params = {
			stationDestinationId : stationId
		};

		this.vendorDestinationDs = new DataSource({
			store: new CustomStore({
				key: 'vendorId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-destination-read-vendor-destination', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  readCityDestination(stationId) {
		const params = {
			stationDestinationId : stationId
		};

		this.cityDestinationDs = new DataSource({
			store: new CustomStore({
				key: 'masterCityId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-destination-read-city', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							console.log(result.data);
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeOrigin(e){
		this.readVendorOrigin(e.value);
	}

	onTriggerChangeDestination(e){
		this.readVendorDestination(e.value);
		this.readCityDestination(e.value);
	}
  
  onSave(){
	  console.log(this.formCreate);
	  
    if(this.formCreate.carrierId == null){
		notify({
			message: 'Carrier harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.vendorOriginId == null){
		notify({
			message: 'Vendor Origin harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.vendorDestinationId == null){
		notify({
			message: 'Vendor Tujuan harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.cityDestinationId == null){
		console.log(this.formCreate);
		notify({
			message: 'Kota Tujuan harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else if(this.formCreate.baseCost == null){
		notify({
			message: 'Base Cost harus di isi terlebih dahulu',
			type: 'error',
			displayTime: 5000,
			width: 400,
		});
	}else {
		var params = {
			carrierId   : parseInt(this.formCreate.carrierId)
			, stationOriginId  : parseInt(this.formCreate.stationOriginId)
			, stationDestinationId       : parseInt(this.formCreate.stationDestinationId)
			, vendorOriginId       : parseInt(this.formCreate.vendorOriginId)
			, vendorDestinationId       : parseInt(this.formCreate.vendorDestinationId)
			, cityDestinationId       : String(this.formCreate.cityDestinationId)
			, baseCost        : this.formCreate.baseCost
		  }
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-destination-create', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-cost-destination']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }

  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}
  onBack(){
	this.router.navigate(['/manifest-carrier-cost-destination']);
  }

}