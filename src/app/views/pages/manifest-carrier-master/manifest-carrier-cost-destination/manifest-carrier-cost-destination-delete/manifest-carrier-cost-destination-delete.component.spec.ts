import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostDestinationDeleteComponent } from './manifest-carrier-cost-destination-delete.component';

describe('ManifestCarrierCostDestinationDeleteComponent', () => {
  let component: ManifestCarrierCostDestinationDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierCostDestinationDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostDestinationDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostDestinationDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
