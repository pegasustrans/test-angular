import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';
import { param } from 'jquery';

@Component({
  selector: 'kt-manifest-carrier-cost-destination-delete',
  templateUrl: './manifest-carrier-cost-destination-delete.component.html',
  styleUrls: ['./manifest-carrier-cost-destination-delete.component.scss']
})
export class ManifestCarrierCostDestinationDeleteComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	 HistoryForm = {
		menu: "Manifest Carrier - Cost Destination - Delete",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  costDestinationId         : any;
  costDestinationDetailId   : any;
  baseCost                  : any;
  carrierId                 : any;
  carrierName               : any;
  cityDestinationId         : any;
  cityDestinationName       : any;
  vendorDestinationId       : any;
  vendorDestinationName     : any;
  vendorOriginId            : any;
  vendorOriginName          : any;

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.costDestinationId         = params.p1;
        this.costDestinationDetailId   = params.p2;
        this.baseCost                  = params.p3;
        this.carrierId                 = params.p4;
        this.carrierName               = params.p5;
        this.cityDestinationId         = params.p6;
        this.cityDestinationName       = params.p7;
        this.vendorDestinationId       = params.p8;
        this.vendorDestinationName     = params.p9;
        this.vendorOriginId            = params.p10;
        this.vendorOriginName          = params.p11;
      }
    );
  }

  onSave(){
    if(this.carrierId == null){
			notify({
				message: 'Carrier harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.vendorOriginId == null){
			notify({
				message: 'Vendor Asal harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.vendorDestinationId == null){
			notify({
				message: 'Vendor Tujuan harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.cityDestinationId == null){
			notify({
				message: 'Kota Tujuan harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.baseCost == null){
			notify({
				message: 'Base Cost harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
      let params = {
        costDestinationId       : parseInt(this.costDestinationId),
        costDestinationDetailId : parseInt(this.costDestinationDetailId)
      }
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-destination-delete', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-cost-destination']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }


  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}
  onBack(){
    this.router.navigate(['/manifest-carrier-cost-destination']);
    }

}