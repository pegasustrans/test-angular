import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostDestinationUpdateComponent } from './manifest-carrier-cost-destination-update.component';

describe('ManifestCarrierCostDestinationUpdateComponent', () => {
  let component: ManifestCarrierCostDestinationUpdateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostDestinationUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostDestinationUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostDestinationUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
