import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostDestinationComponent } from './manifest-carrier-cost-destination.component';

describe('ManifestCarrierCostDestinationComponent', () => {
  let component: ManifestCarrierCostDestinationComponent;
  let fixture: ComponentFixture<ManifestCarrierCostDestinationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostDestinationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostDestinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
