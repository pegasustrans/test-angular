import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostGeraiCreateComponent } from './manifest-carrier-cost-gerai-create.component';

describe('ManifestCarrierCostGeraiCreateComponent', () => {
  let component: ManifestCarrierCostGeraiCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostGeraiCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostGeraiCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostGeraiCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
