import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostGeraiDeleteComponent } from './manifest-carrier-cost-gerai-delete.component';

describe('ManifestCarrierCostGeraiDeleteComponent', () => {
  let component: ManifestCarrierCostGeraiDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierCostGeraiDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostGeraiDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostGeraiDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
