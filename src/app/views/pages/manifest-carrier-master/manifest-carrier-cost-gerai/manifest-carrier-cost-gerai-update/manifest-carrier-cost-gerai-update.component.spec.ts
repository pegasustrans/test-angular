import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostGeraiUpdateComponent } from './manifest-carrier-cost-gerai-update.component';

describe('ManifestCarrierCostGeraiUpdateComponent', () => {
  let component: ManifestCarrierCostGeraiUpdateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostGeraiUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostGeraiUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostGeraiUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
