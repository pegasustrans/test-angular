import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-manifest-carrier-cost-gerai-update',
  templateUrl: './manifest-carrier-cost-gerai-update.component.html',
  styleUrls: ['./manifest-carrier-cost-gerai-update.component.scss']
})
export class ManifestCarrierCostGeraiUpdateComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }



	 HistoryForm = {
		menu: "Manifest Carrier - Cost Gerai - Update",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  formCreate = {
    costGeraiId           : null,
    costGeraiDetailId     : null,
    manifestOriginCity    : null,
    manifestOriginCityId  : null,
    baseCost              : null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.formCreate.costGeraiId           = params.p1;
        this.formCreate.costGeraiDetailId     = params.p2;
        this.formCreate.baseCost              = params.p3;
        this.formCreate.manifestOriginCityId  = params.p4;
        this.formCreate.manifestOriginCity    = params.p5;

      }
    );
  }

  onSave(){
    if(this.formCreate.baseCost == null){
			notify({
				message: 'Base Cost harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-gerai-update', JSON.stringify(this.formCreate), config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-cost-gerai']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }
  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

  onBack(){
    this.router.navigate(['/manifest-carrier-cost-gerai']);
  }

}
