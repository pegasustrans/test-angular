import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostGeraiComponent } from './manifest-carrier-cost-gerai.component';

describe('ManifestCarrierCostGeraiComponent', () => {
  let component: ManifestCarrierCostGeraiComponent;
  let fixture: ComponentFixture<ManifestCarrierCostGeraiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostGeraiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostGeraiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
