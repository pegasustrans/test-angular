import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostInlandOriginCreateComponent } from './manifest-carrier-cost-inland-origin-create.component';

describe('ManifestCarrierCostInlandOriginCreateComponent', () => {
  let component: ManifestCarrierCostInlandOriginCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostInlandOriginCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostInlandOriginCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostInlandOriginCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
