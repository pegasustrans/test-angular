import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostInlandOriginDeleteComponent } from './manifest-carrier-cost-inland-origin-delete.component';

describe('ManifestCarrierCostInlandOriginDeleteComponent', () => {
  let component: ManifestCarrierCostInlandOriginDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierCostInlandOriginDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostInlandOriginDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostInlandOriginDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
