import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostInlandOriginUpdateComponent } from './manifest-carrier-cost-inland-origin-update.component';

describe('ManifestCarrierCostInlandOriginUpdateComponent', () => {
  let component: ManifestCarrierCostInlandOriginUpdateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostInlandOriginUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostInlandOriginUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostInlandOriginUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
