import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostInlandOriginComponent } from './manifest-carrier-cost-inland-origin.component';

describe('ManifestCarrierCostInlandOriginComponent', () => {
  let component: ManifestCarrierCostInlandOriginComponent;
  let fixture: ComponentFixture<ManifestCarrierCostInlandOriginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostInlandOriginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostInlandOriginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
