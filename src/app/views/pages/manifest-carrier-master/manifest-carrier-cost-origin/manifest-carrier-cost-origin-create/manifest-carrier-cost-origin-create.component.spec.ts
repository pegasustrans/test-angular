import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostOriginCreateComponent } from './manifest-carrier-cost-origin-create.component';

describe('ManifestCarrierCostOriginCreateComponent', () => {
  let component: ManifestCarrierCostOriginCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostOriginCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostOriginCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostOriginCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
