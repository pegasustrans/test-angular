import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-manifest-carrier-cost-origin-create',
  templateUrl: './manifest-carrier-cost-origin-create.component.html',
  styleUrls: ['./manifest-carrier-cost-origin-create.component.scss']
})
export class ManifestCarrierCostOriginCreateComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
  stationOriginDs : any; carrierDs : any; vendorOriginDs : any;

  HistoryForm = {
	menu: "Manifest Carrier - Cost Origin - Create",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  formCreate = {
    stationOriginId : null,
    stationName     : null,
    carrierId       : null,
    vendorOriginId  : null,
    vendorName      : null,
    baseCost        : null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.readStation();
    this.readCarrier();
	this.historyLoginStart();
  }

  readStation(){
    const params = {};

		this.stationOriginDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-origin-read-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onTriggerChange(e){
	this.readVendor(e.value);
  }

  readCarrier(){
	const params = {};

	this.carrierDs = new DataSource({
		store: new CustomStore({
			key: 'carrierId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-origin-read-carrier', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});
  }

  readVendor(stationId){
    const params = {
		stationOriginId :stationId
	};

	this.vendorOriginDs = new DataSource({
		store: new CustomStore({
			key: 'vendorId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-origin-read-vendor', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});
  } 
  
  onSave(){
    if(this.formCreate.stationOriginId == null){
			notify({
				message: 'Bandara Asal harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.vendorOriginId == null){
			notify({
				message: 'Vendor Asal harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.carrierId == null){
			notify({
				message: 'Carrier harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.baseCost == null){
			notify({
				message: 'Base Cost harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
      var params = {
        stationOriginId   : parseInt(this.formCreate.stationOriginId)
        , vendorOriginId  : parseInt(this.formCreate.vendorOriginId)
        , carrierId       : parseInt(this.formCreate.carrierId)
        , baseCost        : this.formCreate.baseCost
      }

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-origin-create', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-cost-origin']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }

  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}
  onBack(){
    this.router.navigate(['/manifest-carrier-cost-origin']);
  }
}
