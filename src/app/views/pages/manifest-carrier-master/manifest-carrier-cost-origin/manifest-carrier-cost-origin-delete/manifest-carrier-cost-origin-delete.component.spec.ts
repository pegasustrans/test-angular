import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostOriginDeleteComponent } from './manifest-carrier-cost-origin-delete.component';

describe('ManifestCarrierCostOriginDeleteComponent', () => {
  let component: ManifestCarrierCostOriginDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierCostOriginDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostOriginDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostOriginDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
