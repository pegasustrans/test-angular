import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-manifest-carrier-cost-origin-delete',
  templateUrl: './manifest-carrier-cost-origin-delete.component.html',
  styleUrls: ['./manifest-carrier-cost-origin-delete.component.scss']
})
export class ManifestCarrierCostOriginDeleteComponent implements OnInit {

	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }


	 HistoryForm = {
		menu: "Manifest Carrier - Cost Origin - Delete",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  costOriginId        : any;
  costOriginDetailId  : any;
  baseCost            : any;
  stationName         : any;
  carrierName         : any;
  vendorName          : any;

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.costOriginId   	 = params.p1;
        this.costOriginDetailId  = params.p2;
        this.baseCost   		 = params.p3;
        this.stationName         = params.p4;
		this.carrierName    	 = params.p5;
        this.vendorName          = params.p6;
       
      }
    );
  }

  onSave(){
	const params = { 
		costOriginId 		: parseInt(this.costOriginId),   		
		costOriginDetailId  : parseInt(this.costOriginDetailId)     
	};
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-origin-delete', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-cost-origin']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);    
  }

  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}
  onBack(){
	  this.router.navigate(['/manifest-carrier-cost-origin']);
	}

}