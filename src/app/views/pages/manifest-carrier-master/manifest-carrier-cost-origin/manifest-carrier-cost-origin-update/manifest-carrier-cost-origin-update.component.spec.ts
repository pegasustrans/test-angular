import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostOriginUpdateComponent } from './manifest-carrier-cost-origin-update.component';

describe('ManifestCarrierCostOriginUpdateComponent', () => {
  let component: ManifestCarrierCostOriginUpdateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostOriginUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostOriginUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostOriginUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
