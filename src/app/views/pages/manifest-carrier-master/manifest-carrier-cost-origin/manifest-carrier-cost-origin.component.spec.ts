import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostOriginComponent } from './manifest-carrier-cost-origin.component';

describe('ManifestCarrierCostOriginComponent', () => {
  let component: ManifestCarrierCostOriginComponent;
  let fixture: ComponentFixture<ManifestCarrierCostOriginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostOriginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostOriginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
