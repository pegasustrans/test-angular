import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostPenerusCreateComponent } from './manifest-carrier-cost-penerus-create.component';

describe('ManifestCarrierCostPenerusCreateComponent', () => {
  let component: ManifestCarrierCostPenerusCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostPenerusCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostPenerusCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostPenerusCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
