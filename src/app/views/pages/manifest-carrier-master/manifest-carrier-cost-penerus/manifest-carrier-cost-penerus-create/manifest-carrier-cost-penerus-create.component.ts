import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-manifest-carrier-cost-penerus-create',
  templateUrl: './manifest-carrier-cost-penerus-create.component.html',
  styleUrls: ['./manifest-carrier-cost-penerus-create.component.scss']
})
export class ManifestCarrierCostPenerusCreateComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
  stationDestinationDs  : any;
  cityDestinationDs     : any;
  vendorDestinationDs   : any;


  HistoryForm = {
	menu: "Manifest Carrier - Cost Penerus - Create",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  formCreate = {
    stationId   : null,
    cityId      : null,
    vendorId    : null,
    baseCost    : null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.readStation();
	this.historyLoginStart();
    // this.readCity();s
    // this.readVendor();
  }

  readStation(){
    const params = {
			//originVendorId : this.fromCreate.vendorOriginId
		};
    
		this.stationDestinationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-penerus-read-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onTriggerChange(e){
	this.readVendor(e.value);
	this.readCity(e.value);
  }

  readCity(stationId){
	const params = {
		stationId :stationId
	};

	this.cityDestinationDs = new DataSource({
		store: new CustomStore({
			key: 'cityId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-penerus-read-city', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});
  }

  readVendor(stationId){
    const params = {
		stationId :stationId
	};

	this.vendorDestinationDs = new DataSource({
		store: new CustomStore({
			key: 'vendorId',
			loadMode: 'raw',
			load: (loadOptions) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-penerus-read-vendor', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((result: any) => {
						return result.data;
					});
			}
		})
	});
  } 
  
  onSave(){
    if(this.formCreate.stationId == null){
			notify({
				message: 'Bandara Tujuan harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.cityId == null){
			notify({
				message: 'Kota Tujuan harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.vendorId == null){
			notify({
				message: 'Vendor Tujuan harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.baseCost == null){
			notify({
				message: 'Base Cost harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
      var params = {
        stationId   : parseInt(this.formCreate.stationId)
        , cityId    : this.formCreate.cityId
        , vendorId  : parseInt(this.formCreate.vendorId)
        , baseCost  : parseInt(this.formCreate.baseCost)
      }

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-penerus-create', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-cost-penerus']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }


  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

  onBack(){
    this.router.navigate(['/manifest-carrier-cost-penerus']);
  }
}
