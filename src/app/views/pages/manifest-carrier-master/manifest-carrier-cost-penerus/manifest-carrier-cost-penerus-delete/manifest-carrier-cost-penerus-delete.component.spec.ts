import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostPenerusDeleteComponent } from './manifest-carrier-cost-penerus-delete.component';

describe('ManifestCarrierCostPenerusDeleteComponent', () => {
  let component: ManifestCarrierCostPenerusDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierCostPenerusDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostPenerusDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostPenerusDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
