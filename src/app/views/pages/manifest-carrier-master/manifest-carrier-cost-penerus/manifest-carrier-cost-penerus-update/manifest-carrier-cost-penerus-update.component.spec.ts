import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostPenerusUpdateComponent } from './manifest-carrier-cost-penerus-update.component';

describe('ManifestCarrierCostPenerusUpdateComponent', () => {
  let component: ManifestCarrierCostPenerusUpdateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostPenerusUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostPenerusUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostPenerusUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
