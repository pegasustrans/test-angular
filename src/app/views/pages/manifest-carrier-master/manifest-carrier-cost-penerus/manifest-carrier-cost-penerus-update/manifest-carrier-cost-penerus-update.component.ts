import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-manifest-carrier-cost-penerus-update',
  templateUrl: './manifest-carrier-cost-penerus-update.component.html',
  styleUrls: ['./manifest-carrier-cost-penerus-update.component.scss']
})
export class ManifestCarrierCostPenerusUpdateComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

  baseCost                : any;
  cityDestinationId       : any;
  cityDestinationName     : any;
  costGroup               : any;
  costPenerusDetailId     : any;
  costPenerusId           : any;
  costType                : any;
  stationDestinationId    : any;
  stationDestinationName  : any;
  unit                    : any;
  useWeight               : any;
  vendorDestinationId     : any;
  vendorDestinationName   : any;
  

  HistoryForm = {
		menu: "Manifest Carrier - Cost Penerus - Update",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.baseCost                = params.p1;
        this.cityDestinationId       = params.p2;
        this.cityDestinationName     = params.p3;
        this.costGroup               = params.p4;
        this.costPenerusDetailId     = params.p5;
        this.costPenerusId           = params.p6;
        this.costType                = params.p7;
        this.stationDestinationId    = params.p8;
        this.stationDestinationName  = params.p9;
        this.unit                    = params.p10;
        this.useWeight               = params.p11;
        this.vendorDestinationId     = params.p12;
        this.vendorDestinationName   = params.p13;
      }
    );
  }

  onSave(){
    if(this.baseCost == null){
			notify({
				message: 'Base Cost harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
      var params = {
        costPenerusDetailId : parseInt(this.costPenerusDetailId),
        costPenerusId       : parseInt(this.costPenerusId), 
        baseCost            : parseInt(this.baseCost)
      }

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-penerus-update', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-cost-penerus']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }


  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  onBack(){
    this.router.navigate(['/manifest-carrier-cost-penerus']);
  }

}
