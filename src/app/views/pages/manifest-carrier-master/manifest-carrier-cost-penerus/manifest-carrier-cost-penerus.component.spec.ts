import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostPenerusComponent } from './manifest-carrier-cost-penerus.component';

describe('ManifestCarrierCostPenerusComponent', () => {
  let component: ManifestCarrierCostPenerusComponent;
  let fixture: ComponentFixture<ManifestCarrierCostPenerusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostPenerusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostPenerusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
