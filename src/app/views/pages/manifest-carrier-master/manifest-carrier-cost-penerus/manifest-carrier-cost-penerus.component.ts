import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-manifest-carrier-cost-penerus',
  templateUrl: './manifest-carrier-cost-penerus.component.html',
  styleUrls: ['./manifest-carrier-cost-penerus.component.scss']
})
export class ManifestCarrierCostPenerusComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
  appGlobalVar = AppGlobalVar;

	dsCost: any = {};
	menuPriv = false;
	menuForb = true;


	HistoryForm = {
		menu: "Manifest Carrier - Cost Penerus",
		historyloginId:0
	};

	constructor(private http: HttpClient, private router: Router) { }

	ngOnInit() {
		this.checkuserGlobalVar();
		this.historyLoginStart();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
		  setTimeout(() => {
			this.checkuserGlobalVar();
		  }, 1000);
		}
	  
		else{		
		  var checkPriv = 0;
		  await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-cost-penerus"){
			  checkPriv = 1;
			}
		  });
	  
		  if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
			
			this.GetList();
		  }
		}
	  }
	  
	GetList() {
		this.dsCost = new CustomStore({
			key: 'costPenerusId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/list-cost-penerus')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	addCost(){
		this.router.navigate(['/manifest-carrier-cost-penerus-create']);
	}

	actionCLick = (e) => {
		if (e.itemData.key === 'update') {			
			this.router.navigate(['/manifest-carrier-cost-penerus-update'], 
			{ 
				queryParams: { 
					p1 	: e.itemData.data.baseCost,
					p2 	: e.itemData.data.cityDestinationId,
					p3 	: e.itemData.data.cityDestinationName,
					p4 	: e.itemData.data.costGroup,
					p5 	: e.itemData.data.costPenerusDetailId,
					p6 	: e.itemData.data.costPenerusId,
					p7 	: e.itemData.data.costType,
					p8 	: e.itemData.data.stationDestinationId,
					p9 	: e.itemData.data.stationDestinationName,
					p10 : e.itemData.data.unit,
					p11 : e.itemData.data.useWeight,
					p12 : e.itemData.data.vendorDestinationId,
					p13 : e.itemData.data.vendorDestinationName
				} 
			});
		}else if (e.itemData.key === 'delete') {
			this.router.navigate(['/manifest-carrier-cost-penerus-delete'], 
			{ 
				queryParams: { 
					p1 	: e.itemData.data.baseCost,
					p2 	: e.itemData.data.cityDestinationId,
					p3 	: e.itemData.data.cityDestinationName,
					p4 	: e.itemData.data.costGroup,
					p5 	: e.itemData.data.costPenerusDetailId,
					p6 	: e.itemData.data.costPenerusId,
					p7 	: e.itemData.data.costType,
					p8 	: e.itemData.data.stationDestinationId,
					p9 	: e.itemData.data.stationDestinationName,
					p10 : e.itemData.data.unit,
					p11 : e.itemData.data.useWeight,
					p12 : e.itemData.data.vendorDestinationId,
					p13 : e.itemData.data.vendorDestinationName
				} 
			});
		}
	}

}
