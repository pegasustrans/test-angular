import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostTransitCreateComponent } from './manifest-carrier-cost-transit-create.component';

describe('ManifestCarrierCostTransitCreateComponent', () => {
  let component: ManifestCarrierCostTransitCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostTransitCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostTransitCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostTransitCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
