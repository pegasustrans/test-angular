import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostTransitDeleteComponent } from './manifest-carrier-cost-transit-delete.component';

describe('ManifestCarrierCostTransitDeleteComponent', () => {
  let component: ManifestCarrierCostTransitDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierCostTransitDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostTransitDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostTransitDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
