import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostTransitUpdateComponent } from './manifest-carrier-cost-transit-update.component';

describe('ManifestCarrierCostTransitUpdateComponent', () => {
  let component: ManifestCarrierCostTransitUpdateComponent;
  let fixture: ComponentFixture<ManifestCarrierCostTransitUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostTransitUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostTransitUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
