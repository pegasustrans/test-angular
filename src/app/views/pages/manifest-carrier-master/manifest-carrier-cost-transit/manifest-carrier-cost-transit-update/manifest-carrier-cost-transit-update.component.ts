import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-manifest-carrier-cost-transit-update',
  templateUrl: './manifest-carrier-cost-transit-update.component.html',
  styleUrls: ['./manifest-carrier-cost-transit-update.component.scss']
})
export class ManifestCarrierCostTransitUpdateComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }


	 HistoryForm = {
		menu: "Manifest Carrier - Cost Transit - Update",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  formCreate = {
    costTransitId         : null,
	costTransitDetailId   : null,
	baseCost              : null,
    stationName           : null,
	carrierName           : null,
	vendorName            : null
}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.formCreate.costTransitId         = params.p1;
        this.formCreate.costTransitDetailId   = params.p2;
        this.formCreate.baseCost              = params.p3;
        this.formCreate.stationName           = params.p6;
        this.formCreate.carrierName           = params.p7;
        this.formCreate.vendorName            = params.p8;

      }
    );
  }

  onSave(){
	  console.log(this.formCreate.baseCost);
	  
    if(this.formCreate.baseCost == null || this.formCreate.baseCost == ""){
			notify({
				message: 'Base Cost harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {

			const params = { 
				CostTransitId 		: parseInt(this.formCreate.costTransitId),   		
				CostTransitDetailId : parseInt(this.formCreate.costTransitDetailId),     
				BaseCost 			: parseInt(this.formCreate.baseCost)     
			};

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/cost-transit-update', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-cost-transit']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }
  

  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

  onBack(){
	// this.router.navigate(['manifest-carrier']);
	this.router.navigate(['/manifest-carrier-cost-transit']);
	  }

}
