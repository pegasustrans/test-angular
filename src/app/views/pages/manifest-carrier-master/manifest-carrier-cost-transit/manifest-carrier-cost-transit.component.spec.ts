import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierCostTransitComponent } from './manifest-carrier-cost-transit.component';

describe('ManifestCarrierCostTransitComponent', () => {
  let component: ManifestCarrierCostTransitComponent;
  let fixture: ComponentFixture<ManifestCarrierCostTransitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierCostTransitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierCostTransitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
