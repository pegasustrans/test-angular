import { Component, HostListener, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import { Router } from '@angular/router';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-manifest-carrier-cost-transit',
  templateUrl: './manifest-carrier-cost-transit.component.html',
  styleUrls: ['./manifest-carrier-cost-transit.component.scss']
})
export class ManifestCarrierCostTransitComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
	appGlobalVar = AppGlobalVar;

	dsCost: any = {};
	menuPriv = false;
	menuForb = true;

	HistoryForm = {
		menu: "Manifest Carrier - Cost Transit",
		historyloginId:0
	};

	constructor(private http: HttpClient, private router: Router) { }

	ngOnInit() {
		this.checkuserGlobalVar();
		this.historyLoginStart();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
		  setTimeout(() => {
			this.checkuserGlobalVar();
		  }, 1000);
		}
	  
		else{		
		  var checkPriv = 0;
		  await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-cost-transit"){
			  checkPriv = 1;
			}
		  });
	  
		  if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
			
			this.GetList();
		  }
		}
	  }

	GetList() {
		this.dsCost = new CustomStore({
			key: 'costTransitId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/list-cost-transit')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}


	addCost(){
		this.router.navigate(['/manifest-carrier-cost-transit-create']);
	}

	actionCLick = (e) => {
		if (e.itemData.key === 'update') {
			this.router.navigate(['/manifest-carrier-cost-transit-update'], 
			{ 
				queryParams: { 
					p1: e.itemData.data.costTransitId,
					p2: e.itemData.data.costTransitDetailId,
					p3: e.itemData.data.baseCost,
					p4: e.itemData.data.manifestOriginCityId,
					p5: e.itemData.data.masterCityCode,
					p6: e.itemData.data.stationName,
					p7: e.itemData.data.carrierName,
					p8: e.itemData.data.vendorName
				} 
			});
		}else if (e.itemData.key === 'delete') {
			console.log(e.itemData);
			
			this.router.navigate(['/manifest-carrier-cost-transit-delete'], 
			{ 
				queryParams: { 
					p1: e.itemData.data.costTransitId,
					p2: e.itemData.data.costTransitDetailId,
					p3: e.itemData.data.baseCost,
					p4: e.itemData.data.stationName,
					p5: e.itemData.data.carrierName,
					p6: e.itemData.data.vendorName
				} 
			});
		}
	}


}
