import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterCarrierCreateComponent } from './manifest-carrier-master-carrier-create.component';

describe('ManifestCarrierMasterCarrierCreateComponent', () => {
  let component: ManifestCarrierMasterCarrierCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterCarrierCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterCarrierCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterCarrierCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
