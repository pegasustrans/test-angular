import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterCarrierDeleteComponent } from './manifest-carrier-master-carrier-delete.component';

describe('ManifestCarrierMasterCarrierDeleteComponent', () => {
  let component: ManifestCarrierMasterCarrierDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterCarrierDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterCarrierDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterCarrierDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
