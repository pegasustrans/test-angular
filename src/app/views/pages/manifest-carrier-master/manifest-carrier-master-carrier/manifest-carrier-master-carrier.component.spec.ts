import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterCarrierComponent } from './manifest-carrier-master-carrier.component';

describe('ManifestCarrierMasterCarrierComponent', () => {
  let component: ManifestCarrierMasterCarrierComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
