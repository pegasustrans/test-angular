import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterFlightCreateComponent } from './manifest-carrier-master-flight-create.component';

describe('ManifestCarrierMasterFlightCreateComponent', () => {
  let component: ManifestCarrierMasterFlightCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterFlightCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterFlightCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterFlightCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
