import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterFlightDeleteComponent } from './manifest-carrier-master-flight-delete.component';

describe('ManifestCarrierMasterFlightDeleteComponent', () => {
  let component: ManifestCarrierMasterFlightDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterFlightDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterFlightDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterFlightDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
