import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterFlightComponent } from './manifest-carrier-master-flight.component';

describe('ManifestCarrierMasterFlightComponent', () => {
  let component: ManifestCarrierMasterFlightComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterFlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterFlightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterFlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
