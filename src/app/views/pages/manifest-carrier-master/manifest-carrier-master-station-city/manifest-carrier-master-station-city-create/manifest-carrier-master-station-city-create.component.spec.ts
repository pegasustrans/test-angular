import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterStationCityCreateComponent } from './manifest-carrier-master-station-city-create.component';

describe('ManifestCarrierMasterStationCityCreateComponent', () => {
  let component: ManifestCarrierMasterStationCityCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterStationCityCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterStationCityCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterStationCityCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
