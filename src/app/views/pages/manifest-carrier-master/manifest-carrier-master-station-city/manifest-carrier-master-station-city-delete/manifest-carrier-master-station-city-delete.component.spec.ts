import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterStationCityDeleteComponent } from './manifest-carrier-master-station-city-delete.component';

describe('ManifestCarrierMasterStationCityDeleteComponent', () => {
  let component: ManifestCarrierMasterStationCityDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterStationCityDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterStationCityDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterStationCityDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
