import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterStationCityComponent } from './manifest-carrier-master-station-city.component';

describe('ManifestCarrierMasterStationCityComponent', () => {
  let component: ManifestCarrierMasterStationCityComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterStationCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterStationCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterStationCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
