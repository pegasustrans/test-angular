import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterStationCreateComponent } from './manifest-carrier-master-station-create.component';

describe('ManifestCarrierMasterStationCreateComponent', () => {
  let component: ManifestCarrierMasterStationCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterStationCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterStationCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterStationCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
