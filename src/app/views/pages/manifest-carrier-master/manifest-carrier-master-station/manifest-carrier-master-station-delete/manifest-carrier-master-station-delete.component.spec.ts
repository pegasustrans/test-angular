import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterStationDeleteComponent } from './manifest-carrier-master-station-delete.component';

describe('ManifestCarrierMasterStationDeleteComponent', () => {
  let component: ManifestCarrierMasterStationDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterStationDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterStationDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterStationDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
