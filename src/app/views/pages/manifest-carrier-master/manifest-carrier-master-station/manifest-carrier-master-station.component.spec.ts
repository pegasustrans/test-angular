import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterStationComponent } from './manifest-carrier-master-station.component';

describe('ManifestCarrierMasterStationComponent', () => {
  let component: ManifestCarrierMasterStationComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
