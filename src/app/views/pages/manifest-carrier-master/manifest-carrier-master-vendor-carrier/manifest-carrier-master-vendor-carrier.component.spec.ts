import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorCarrierComponent } from './manifest-carrier-master-vendor-carrier.component';

describe('ManifestCarrierMasterVendorCarrierComponent', () => {
  let component: ManifestCarrierMasterVendorCarrierComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
