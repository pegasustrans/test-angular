import { Component, HostListener, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import { Router } from '@angular/router';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-manifest-carrier-master-vendor-carrier',
  templateUrl: './manifest-carrier-master-vendor-carrier.component.html',
  styleUrls: ['./manifest-carrier-master-vendor-carrier.component.scss']
})
export class ManifestCarrierMasterVendorCarrierComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

  appGlobalVar = AppGlobalVar;
	dsList: any = {};
	menuPriv = false;
	menuForb = true;


	HistoryForm = {
		menu: "Master - Vendor Carrier",
		historyloginId:0
	};

	constructor(private http: HttpClient, private router: Router) { }

	ngOnInit() {
		this.historyLoginStart();		
		this.checkuserGlobalVar();
		// this.GetList();
	}

	

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
		  setTimeout(() => {
			this.checkuserGlobalVar();
		  }, 1000);
		}
	  
		else{		
		  var checkPriv = 0;
		  await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-vendor-carrier"){
			  checkPriv = 1;
			}
		  });
	  
		  if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
			
			  this.GetList();
		  }
		}
	  }

	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	GetList() {
		this.dsList = new CustomStore({
			key: 'vendorCarrierId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-carrier-list')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}


	addCost(){
		this.router.navigate(['/mc-master-vendor-carrier-create']);
	}

	actionCLick = (e) => {
		this.router.navigate(['/mc-master-vendor-carrier-delete'], 
			{ 
				queryParams: { 
					p1: e.itemData.data.vendorCarrierId,
					p2: e.itemData.data.vendorId,
					p3: e.itemData.data.vendorName,
					p4: e.itemData.data.carrierId,
					p5: e.itemData.data.carrierName
				} 
			});
	}


}

