import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MCMasterVendorCarrierCreateComponent } from './mc-master-vendor-carrier-create.component';

describe('MCMasterVendorCarrierCreateComponent', () => {
  let component: MCMasterVendorCarrierCreateComponent;
  let fixture: ComponentFixture<MCMasterVendorCarrierCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MCMasterVendorCarrierCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MCMasterVendorCarrierCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
