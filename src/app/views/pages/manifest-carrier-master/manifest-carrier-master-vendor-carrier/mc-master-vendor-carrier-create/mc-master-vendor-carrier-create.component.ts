import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';


@Component({
  selector: 'kt-mc-master-vendor-carrier-create',
  templateUrl: './mc-master-vendor-carrier-create.component.html',
  styleUrls: ['./mc-master-vendor-carrier-create.component.scss']
})
export class MCMasterVendorCarrierCreateComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

  vendorDs:any; carrierDs:any;

  HistoryForm = {
	menu: "Master - Vendor Carrier - Create",
	historyloginId:0
};

  constructor(private http: HttpClient, private router: Router) { }

  formCreate = {
    vendorId  : null,
  	carrierId : null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.ReadVendor();
    this.ReadCarrier();
	this.historyLoginStart();
  }

  ReadVendor() {
		const params = {};

		this.vendorDs = new DataSource({
			store: new CustomStore({
				key: 'vendorId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-carrier-list-vendor', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	ReadCarrier() {
		const params = {};

		this.carrierDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-carrier-list-carrier', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  onSave(){
    if(this.formCreate.vendorId == null){
			notify({
				message: 'Nama Vendor harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});

		}else if(this.formCreate.carrierId == null){
			notify({
				message: 'Carrier harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});

		}else {
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-carrier-create', JSON.stringify(this.formCreate), config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-master-vendor-carrier']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }


  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

  onBack(){
    this.router.navigate(['/manifest-carrier-master-vendor-carrier']);
  }

}
