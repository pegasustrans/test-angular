import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MCMasterVendorCarrierDeleteComponent } from './mc-master-vendor-carrier-delete.component';

describe('MCMasterVendorCarrierDeleteComponent', () => {
  let component: MCMasterVendorCarrierDeleteComponent;
  let fixture: ComponentFixture<MCMasterVendorCarrierDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MCMasterVendorCarrierDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MCMasterVendorCarrierDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
