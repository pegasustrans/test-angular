import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-mc-master-vendor-carrier-delete',
  templateUrl: './mc-master-vendor-carrier-delete.component.html',
  styleUrls: ['./mc-master-vendor-carrier-delete.component.scss']
})
export class MCMasterVendorCarrierDeleteComponent implements OnInit {
  
  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  vendorCarrierId   : any;
  vendorId          : any;
  vendorName        : any;
  carrierId         : any;
  carrierName       : any;

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.vendorCarrierId    = params.p1;
        this.vendorId           = params.p2;
        this.vendorName   	    = params.p3;       
        this.carrierId   	      = params.p4;       
        this.carrierName   	    = params.p5;       
      }
    );
  }

  onSave(){
	const params = { 
		vendorCarrierId 		: parseInt(this.vendorCarrierId)
	};
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-carrier-delete', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-master-vendor-carrier']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);    
  }

  onBack(){
    this.router.navigate(['/manifest-carrier-master-vendor-carrier']);
  }


}
