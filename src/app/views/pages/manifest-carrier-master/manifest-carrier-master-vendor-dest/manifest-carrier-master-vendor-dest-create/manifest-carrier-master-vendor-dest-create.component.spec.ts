import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorDestCreateComponent } from './manifest-carrier-master-vendor-dest-create.component';

describe('ManifestCarrierMasterVendorDestCreateComponent', () => {
  let component: ManifestCarrierMasterVendorDestCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorDestCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorDestCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorDestCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
