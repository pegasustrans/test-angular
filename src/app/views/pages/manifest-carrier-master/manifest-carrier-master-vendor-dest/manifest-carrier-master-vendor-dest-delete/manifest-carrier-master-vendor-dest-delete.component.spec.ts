import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorDestDeleteComponent } from './manifest-carrier-master-vendor-dest-delete.component';

describe('ManifestCarrierMasterVendorDestDeleteComponent', () => {
  let component: ManifestCarrierMasterVendorDestDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorDestDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorDestDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorDestDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
