import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorDestComponent } from './manifest-carrier-master-vendor-dest.component';

describe('ManifestCarrierMasterVendorDestComponent', () => {
  let component: ManifestCarrierMasterVendorDestComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorDestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorDestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorDestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
