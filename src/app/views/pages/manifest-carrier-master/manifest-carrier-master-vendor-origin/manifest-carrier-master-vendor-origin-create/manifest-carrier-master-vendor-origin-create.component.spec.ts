import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorOriginCreateComponent } from './manifest-carrier-master-vendor-origin-create.component';

describe('ManifestCarrierMasterVendorOriginCreateComponent', () => {
  let component: ManifestCarrierMasterVendorOriginCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorOriginCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorOriginCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorOriginCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
