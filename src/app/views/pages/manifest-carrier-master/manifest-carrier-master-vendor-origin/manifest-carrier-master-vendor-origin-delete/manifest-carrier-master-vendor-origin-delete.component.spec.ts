import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorOriginDeleteComponent } from './manifest-carrier-master-vendor-origin-delete.component';

describe('ManifestCarrierMasterVendorOriginDeleteComponent', () => {
  let component: ManifestCarrierMasterVendorOriginDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorOriginDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorOriginDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorOriginDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
