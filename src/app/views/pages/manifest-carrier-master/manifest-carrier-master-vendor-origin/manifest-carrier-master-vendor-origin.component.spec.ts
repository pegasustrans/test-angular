import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorOriginComponent } from './manifest-carrier-master-vendor-origin.component';

describe('ManifestCarrierMasterVendorOriginComponent', () => {
  let component: ManifestCarrierMasterVendorOriginComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorOriginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorOriginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorOriginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
