import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorTransitComponent } from './manifest-carrier-master-vendor-transit.component';

describe('ManifestCarrierMasterVendorTransitComponent', () => {
  let component: ManifestCarrierMasterVendorTransitComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorTransitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorTransitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorTransitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
