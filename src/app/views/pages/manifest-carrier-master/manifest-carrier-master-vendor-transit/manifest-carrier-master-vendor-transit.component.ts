import { Component, HostListener, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import { Router } from '@angular/router';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-manifest-carrier-master-vendor-transit',
  templateUrl: './manifest-carrier-master-vendor-transit.component.html',
  styleUrls: ['./manifest-carrier-master-vendor-transit.component.scss']
})
export class ManifestCarrierMasterVendorTransitComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

  	appGlobalVar = AppGlobalVar;
	dsList: any = {};
	menuPriv = false;
	menuForb = true;


	HistoryForm = {
		menu: "Master - Vendor Transit",
		historyloginId:0
	};

	constructor(private http: HttpClient, private router: Router) { }

	ngOnInit() {
	  	this.checkuserGlobalVar();
		this.historyLoginStart();
	}


	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
		  setTimeout(() => {
			this.checkuserGlobalVar();
		  }, 1000);
		}
	  
		else{		
		  var checkPriv = 0;
		  await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-vendor-transit"){
			  checkPriv = 1;
			}
		  });
	  
		  if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
			
			this.GetList();
		  }
		}
	  }

	GetList() {
		this.dsList = new CustomStore({
			key: 'vendorId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-transit-list')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}


	addCost(){
		this.router.navigate(['/mc-master-vendor-transit-create']);
	}

	actionCLick = (e) => {
		this.router.navigate(['/mc-master-vendor-transit-delete'], 
			{ 
				queryParams: { 
					p1: e.itemData.data.vendorTransitId,
					p2: e.itemData.data.vendorId,
					p3: e.itemData.data.vendorName,
					p4: e.itemData.data.stationId,
					p5: e.itemData.data.stationName
				} 
			});
	}


}

