import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MCMasterVendorTransitCreateComponent } from './mc-master-vendor-transit-create.component';

describe('MCMasterVendorTransitCreateComponent', () => {
  let component: MCMasterVendorTransitCreateComponent;
  let fixture: ComponentFixture<MCMasterVendorTransitCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MCMasterVendorTransitCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MCMasterVendorTransitCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
