import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'kt-mc-master-vendor-transit-create',
  templateUrl: './mc-master-vendor-transit-create.component.html',
  styleUrls: ['./mc-master-vendor-transit-create.component.scss']
})
export class MCMasterVendorTransitCreateComponent implements OnInit {
  vendorDs:any; stationDs:any;

  constructor(private http: HttpClient, private router: Router) { }

  formCreate = {
    vendorId  : null,
  	stationId : null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.ReadVendor();
    this.ReadStation();
  }

  ReadVendor() {
		const params = {};

		this.vendorDs = new DataSource({
			store: new CustomStore({
				key: 'vendorId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-transit-list-vendor', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	ReadStation() {
		const params = {};

		this.stationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-transit-list-station', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  onSave(){
    if(this.formCreate.vendorId == null){
			notify({
				message: 'Nama Vendor harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});

		}else if(this.formCreate.stationId == null){
			notify({
				message: 'Nama Bandara harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});

		}else {
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-transit-create', JSON.stringify(this.formCreate), config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-master-vendor-transit']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }

  onBack(){
    this.router.navigate(['/manifest-carrier-master-vendor-transit']);
  }

}
