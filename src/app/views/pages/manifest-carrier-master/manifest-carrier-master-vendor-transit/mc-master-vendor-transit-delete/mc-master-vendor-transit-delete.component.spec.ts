import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MCMasterVendorTransitDeleteComponent } from './mc-master-vendor-transit-delete.component';

describe('MCMasterVendorTransitDeleteComponent', () => {
  let component: MCMasterVendorTransitDeleteComponent;
  let fixture: ComponentFixture<MCMasterVendorTransitDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MCMasterVendorTransitDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MCMasterVendorTransitDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
