import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-mc-master-vendor-transit-delete',
  templateUrl: './mc-master-vendor-transit-delete.component.html',
  styleUrls: ['./mc-master-vendor-transit-delete.component.scss']
})
export class MCMasterVendorTransitDeleteComponent implements OnInit {

	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }


	 HistoryForm = {
		menu: "Master - Vendor Transit - Delete",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  vendorTransitId : any;
  vendorId        : any;
  vendorName      : any;
  stationId       : any;
  stationName     : any;

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.vendorTransitId  = params.p1;
        this.vendorId         = params.p2;
        this.vendorName   	  = params.p3;       
        this.stationId   	    = params.p4;       
        this.stationName   	  = params.p5;       
      }
    );
  }

  onSave(){
	const params = { 
		vendorTransitId 		: parseInt(this.vendorTransitId)
	};
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-vendor-transit-delete', params, config)
			  .subscribe((data: any) => {

					if(data.data[0].result == 'false'){
						notify({
							message: data.data[0].message,
							type: 'error',
							displayTime: 5000,
							width: 400,
						});
					}else{
						notify({
							message: data.data[0].message,
							type: 'success',
							displayTime: 5000,
							width: 400,
						});

						setTimeout(() => {
							this.router.navigate(['/manifest-carrier-master-vendor-transit']);
						}, 1000);					
					}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);    
  }


  ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

  onBack(){
    this.router.navigate(['/manifest-carrier-master-vendor-transit']);
  }


}