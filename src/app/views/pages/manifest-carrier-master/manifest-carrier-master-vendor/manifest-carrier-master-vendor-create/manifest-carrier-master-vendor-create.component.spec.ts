import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorCreateComponent } from './manifest-carrier-master-vendor-create.component';

describe('ManifestCarrierMasterVendorCreateComponent', () => {
  let component: ManifestCarrierMasterVendorCreateComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
