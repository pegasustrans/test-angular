import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorDeleteComponent } from './manifest-carrier-master-vendor-delete.component';

describe('ManifestCarrierMasterVendorDeleteComponent', () => {
  let component: ManifestCarrierMasterVendorDeleteComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
