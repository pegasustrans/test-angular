import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierMasterVendorComponent } from './manifest-carrier-master-vendor.component';

describe('ManifestCarrierMasterVendorComponent', () => {
  let component: ManifestCarrierMasterVendorComponent;
  let fixture: ComponentFixture<ManifestCarrierMasterVendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierMasterVendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierMasterVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
