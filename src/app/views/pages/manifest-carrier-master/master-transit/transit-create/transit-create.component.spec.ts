import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransitCreateComponent } from './transit-create.component';

describe('TransitCreateComponent', () => {
  let component: TransitCreateComponent;
  let fixture: ComponentFixture<TransitCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransitCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransitCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
