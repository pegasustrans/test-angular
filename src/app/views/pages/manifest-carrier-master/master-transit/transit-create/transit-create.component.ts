import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-transit-create',
  templateUrl: './transit-create.component.html',
  styleUrls: ['./transit-create.component.scss']
})
export class TransitCreateComponent implements OnInit {
	httpOptions = {
	  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
	};
  
	originStationDs: DataSource;
	firstFlightDs: DataSource;
	stationTransitDs: DataSource;
	lastFlightDs: DataSource;
	finalDestinationStationDs: DataSource;
	vendorTransitDs: DataSource;
	sequenceTransitDs: number[] = [];
  
	formCreate: any = {
	  originStationId: null,
	  firstFlightId: null,
	  stationTransitId: null,
	  lastFlightId: null,
	  finalDestinationStationId: null,
	  sequenceTransit: null,
	  vendorTransitId: null,
	};
  
	constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }
  
	ngOnInit() {
	  this.loadOriginStation();
	  this.loadFirstFlight();
	  this.loadStationTransit();
	  this.loadLastFlight();
	  this.loadFinalDestinationStation();
	  this.sequenceTransit();
	}
  
	loadOriginStation() {
	  this.originStationDs = new DataSource({
		store: new CustomStore({
		  key: 'stationId',
		  loadMode: 'raw',
		  load: () => this.fetchData('manifest-carrier-master/manifest-carrier-master-station-city-list')
		})
	  });
	}
  
	loadFirstFlight() {
		this.firstFlightDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: () => {
					return this.fetchData('manifest-carrier-master/manifest-carrier-master-flight-list').then(data => {
						console.log("First Flight Data:", data); // Debugging: log the data
						return data;
					});
				}
			})
		});
	}
  
	loadStationTransit() {
	  this.stationTransitDs = new DataSource({
		store: new CustomStore({
		  key: 'stationId',
		  loadMode: 'raw',
		  load: () => this.fetchData('manifest-carrier-master/manifest-carrier-master-station-city-list')
		})
	  });
	}
  
	loadLastFlight() {
		this.lastFlightDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: () => {
					return this.fetchData('manifest-carrier-master/manifest-carrier-master-flight-list').then(data => {
						console.log("Last Flight Data:", data); // Debugging: log the data
						return data;
					});
				}
			})
		});
	}
  
	loadFinalDestinationStation() {
	  this.finalDestinationStationDs = new DataSource({
		store: new CustomStore({
		  key: 'stationId',
		  loadMode: 'raw',
		  load: () => this.fetchData('manifest-carrier-master/manifest-carrier-master-station-city-list')
		})
	  });
	}
  
	loadVendorTransit(stationId: number) {
	  this.vendorTransitDs = new DataSource({
		store: new CustomStore({
		  key: 'vendorTransitId',
		  loadMode: 'raw',
		  load: () => {
			return this.http.get(`${AppGlobalVar.BASE_API_URL}manifest-carrier-master/manifest-carrier-master-vendor-transit-list`, {
			  headers: this.httpOptions.headers,
			  params: { stationId: stationId.toString() }
			}).toPromise().then((result: any) => result.data);
		  }
		})
	  });
	}
  
	fetchData(endpoint: string) {
	  return this.http.get(`${AppGlobalVar.BASE_API_URL}${endpoint}`, {
		headers: this.httpOptions.headers
	  }).toPromise().then((result: any) => result.data);
	}
  
	onStationTransitChange(event) {
	  const selectedStationId = event.value;
	  this.formCreate.vendorTransitId = null; // Reset vendorTransitId when stationTransitId changes
	  this.loadVendorTransit(selectedStationId);
	}

	sequenceTransit() {
		this.sequenceTransitDs = [1, 2, 3]; // Rentang angka statis 1 hingga 3
	}	
	
	onCustomItemCreating(e: any): void {
		const customValue = parseInt(e.text, 10);
	  
		// Validasi input (opsional: angka valid dari 1 hingga 10)
		if (!isNaN(customValue) && customValue >= 1 && customValue <= 10) {
		  e.customItem = customValue; // Menambahkan nilai ke dropdown
		  this.formCreate.sequenceTransit = customValue; // Mengupdate form model
		} else {
		  notify({
			message: 'Input harus berupa angka antara 1 hingga 10',
			type: 'warning',
			displayTime: 3000,
		  });
		  e.customItem = null; // Abaikan input jika tidak valid
		}
	  }

	  onSave(){
	
		  const params = {
			originStationId: this.formCreate.originStationId,
			// firstFlightCodeId: this.formCreate.firstFlightCodeId,
			firstFlightId: this.formCreate.firstFlightId,
			stationTransitId: this.formCreate.stationTransitId,
			// lastFlightCodeId: this.formCreate.lastFlightCodeId,
			lastFlightId: this.formCreate.lastFlightId,
			finalDestinationStationId: this.formCreate.finalDestinationStationId,
			sequenceTransit: this.formCreate.sequenceTransit,
			vendorTransitId: this.formCreate.vendorTransitId
		  }
	
		  console.log(params)
	
		  const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		  this.http
			.post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/master-transit-create', params, config)
			.subscribe((data: any) => {
			  console.log(data.data);
	
			  this.router.navigate(['/transit-list']);
	
			  notify({
				message: 'Data telah di Tambahkan',
				type: 'success',
				displayTime: 5000,
				width: 400,
			  });
	
			},
			  (error) => {
				alert(error.statusText + '. ' + error.message);
			  }
			);
		
		
	  }	
	  
	  onBack(){
		this.router.navigate(['/transit-list']);
	  }	
}

// Fetch final destination city
// finalDestinationCity() {
// 	this.finalDestinationCityDs = new DataSource({
// 	  store: new CustomStore({
// 		key: 'stationId',
// 		loadMode: 'raw',
// 		load: () => {
// 		  return this.http
// 			.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/master-transit-read-destination-station', {
// 			  headers: this.httpOptions.headers
// 			})
// 			.toPromise()
// 			.then((result: any) => result.data);
// 		}
// 	  })
// 	});
// }