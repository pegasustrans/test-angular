import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransitDeleteComponent } from './transit-delete.component';

describe('TransitDeleteComponent', () => {
  let component: TransitDeleteComponent;
  let fixture: ComponentFixture<TransitDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransitDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransitDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
