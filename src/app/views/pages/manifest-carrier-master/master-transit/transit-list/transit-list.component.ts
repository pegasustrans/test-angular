import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-transit-list',
  templateUrl: './transit-list.component.html',
  styleUrls: ['./transit-list.component.scss']
})
export class TransitListComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

  transitListDs: CustomStore;

  menuPriv = false;
  menuForb = true;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
		})
  };
  

  HistoryForm = {
		menu: "Master - Flight Transit Route",
		historyloginId:0
	};

  constructor(private http: HttpClient,private router: Router) { }

  ngOnInit() {
    this.historyLoginStart();
    this.checkuserGlobalVar();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
		  setTimeout(() => {
			this.checkuserGlobalVar();
		  }, 1000);
		}
	  
		else{		
		  var checkPriv = 0;
		  await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-flight-transit-route"){
			  checkPriv = 1;
			}
		  });
	  
		  if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
			
			  this.GetList();
		  }
		}
	  }

  GetList(){
    const params = {
		};

		this.transitListDs = new CustomStore({
			key: 'transitRouteId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/master-transit-read-transit', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
            return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
  }

  addTransit(){
		this.router.navigate(['/transit-create']);
	}

	actionCLick = (e) => {
    console.log(e.itemData.data);
    
		if (e.itemData.key === 'delete') {
			this.router.navigate(['/transit-delete'], 
			{ 
				queryParams: { 
          p1: e.itemData.data.transitRouteId   
          ,p2 : e.itemData.data.originStationName
          ,p3: e.itemData.data.firstFlightCode
          ,p4: e.itemData.data.transitStationName
          ,p5: e.itemData.data.lastFlightCode
          ,p6: e.itemData.data.finalDestinationStationName
				} 
			});
		}
	}
}
