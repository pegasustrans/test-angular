import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransitUpdateComponent } from './transit-update.component';

describe('TransitUpdateComponent', () => {
  let component: TransitUpdateComponent;
  let fixture: ComponentFixture<TransitUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransitUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransitUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
