import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-transit-update',
  templateUrl: './transit-update.component.html',
  styleUrls: ['./transit-update.component.scss']
})
export class TransitUpdateComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }
  
  cityTransitDs : any;


  HistoryForm = {
		menu: "Master - Flight Transit Route - Update",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  fromUpdate = {
    transitRouteId          : null,
    originCityId            : null,
    firstFlightCodeId       : null,
    firstFlightId           : null,
    cityTransitId           : null,
    lastFlightCodeId        : null,
    lastFlightId            : null,
    finalDestinationCityId  : null,

    originCity              : null,
    firstFlight             : null,
    cityTransit             : null,
    lastFlight              : null,
    finalDestinationCity    : null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};


  ngOnInit() {
    this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        console.log(params); 

        this.fromUpdate.transitRouteId          = params.p1;
        this.fromUpdate.originCityId            = params.p2;
        this.fromUpdate.firstFlightCodeId       = params.p3;
        this.fromUpdate.firstFlightId           = params.p4;
        this.fromUpdate.cityTransitId           = params.p5;
        this.fromUpdate.lastFlightCodeId        = params.p6;
        this.fromUpdate.lastFlightId            = params.p7;
        this.fromUpdate.finalDestinationCityId  = params.p8;

        this.fromUpdate.originCity              = params.p9;
        this.fromUpdate.firstFlight             = params.p10;
        this.fromUpdate.cityTransit             = params.p11;
        this.fromUpdate.lastFlight              = params.p12;
        this.fromUpdate.finalDestinationCity    = params.p13;
                
      }
    );

    this.readDataCityTransit();
  }

  readDataCityTransit() {
		const params = {
			cityTransitId		: this.fromUpdate.cityTransitId
		};

     console.log(params);
     this.cityTransitDs = new DataSource({
			store: new CustomStore({
				key: 'cityTransitId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'master-transit/read-city-transit', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
     
	}

  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  createSubmit(e) {
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'master-transit/update-city-transit', JSON.stringify(this.fromUpdate), config)
      .subscribe((data: any) => {
        
        console.log(data);
        if(data.status == 200){
          this.router.navigate(['transit-list']);
        }else if(data.status == 201){
            notify({
              message: "Rute Transit sudah terdaftar di system sebelumnya.",
              type: 'error',
              displayTime: 5000,
              width: 400,
            });
        }else{
          notify({
            message: "Failed, Error Update",
            type: 'error',
            displayTime: 5000,
            width: 400,
          });
        }

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
    
	}

}
