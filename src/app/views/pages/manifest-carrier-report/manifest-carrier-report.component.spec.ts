import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierReportComponent } from './manifest-carrier-report.component';

describe('ManifestCarrierReportComponent', () => {
  let component: ManifestCarrierReportComponent;
  let fixture: ComponentFixture<ManifestCarrierReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
