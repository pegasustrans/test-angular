import {Component, HostListener, OnInit} from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AppGlobalVar} from '../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../core/globalvar/userGlobalVar'; 
import * as AspNetData from 'devextreme-aspnet-data-nojquery';

@Component({
	selector: "kt-manifest-carrier-report",
	templateUrl: "./manifest-carrier-report.component.html",
	styleUrls: ["./manifest-carrier-report.component.scss"],
})
export class ManifestCarrierReportComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;
  
	sales: any;
	dataSource: any;
  data: any;
  
  formSearch = {
		flightDate: new Date()
	};


	HistoryForm = {
		menu: "Manifest Carrier Report",
		historyloginId:0
	};

	constructor(private http: HttpClient) {
		
  }

  ngOnInit() { 
	this.historyLoginStart();
        
    // this.getList();
  }
  
  httpOptions = {
	headers: new HttpHeaders({
		'Content-Type': 'application/json'
	})
};

  getList() {
	var flightDateUtcYear 			= this.formSearch.flightDate.getUTCFullYear();
	var flightDateUtcMonth 			= this.formSearch.flightDate.getUTCMonth() + 1;
	var flightDateUtcDay 			= this.formSearch.flightDate.getUTCDate();
	var flightDateUtcHour 			= this.formSearch.flightDate.getUTCHours();
	var flightDateUtcMinute 		= this.formSearch.flightDate.getMinutes();
	var flightDateUtcSecond 		= this.formSearch.flightDate.getUTCSeconds();
	var flightDateUtcMillisecond 	= this.formSearch.flightDate.getUTCMilliseconds();

	const params = { 
		flightDateUtcYear 			: flightDateUtcYear.toString(),	
		flightDateUtcMonth 			: flightDateUtcMonth.toString(), 		
		flightDateUtcDay 			: flightDateUtcDay.toString(), 		
		flightDateUtcHour 			: flightDateUtcHour.toString(), 		
		flightDateUtcMinute 		: flightDateUtcMinute.toString(), 		
		flightDateUtcSecond 		: flightDateUtcSecond.toString(), 		
		flightDateUtcMillisecond 	: flightDateUtcMillisecond.toString() 
	 };

    this.dataSource = {
			fields: [
				{
					caption: "Kota Tujuan",
					dataField: "destination",
          area: "row",
          width: 60,
        },
        {
					caption: "Flight Date",
					dataField: "flightDateLocal",
          area: "row",
          width: 80,
        },
				{
					dataField: "costGroup", 
					caption: "Tipe Kelompok Biaya",
					area: "column",
				},
				{
					dataField: "costType", 
					caption: "Tipe Biaya",
					area: "column",
				},
				{
					caption: "Biaya",
					dataField: "cost",
					dataType: "number",
					summaryType: "sum",
					format: { style: "currency", currency: "IDR" },
					area: "data",
        },
			],
			store:  new CustomStore({
        key: 's.manifestId',
        load:  (loadOptions: any) => {
          return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/list-report', { headers: this.httpOptions.headers, params  })
            .toPromise()
            .then((data: any) => {
              return {
                data: data.data
              };
            })
            .catch(error => { console.log(error); });
        }
      })
		};
	}
  
  onSearchFormSubmit(e) { 
    this.getList();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	formatDate(date: Date) {
		var month = '' + (date.getMonth() + 1),
			day = '' + date.getDate(),
			year = date.getFullYear();
	
		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;
	
		return [year, month, day].join('-');
	}

}
