import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateManifestCarrierComponent } from './create-manifest-carrier.component';

describe('CreateManifestCarrierComponent', () => {
  let component: CreateManifestCarrierComponent;
  let fixture: ComponentFixture<CreateManifestCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateManifestCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateManifestCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
