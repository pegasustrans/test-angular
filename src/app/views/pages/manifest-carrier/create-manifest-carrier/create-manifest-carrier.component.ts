import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, OnInit, Output, enableProdMode, ViewChild} from '@angular/core';
import { confirm } from 'devextreme/ui/dialog';
import { custom } from 'devextreme/ui/dialog';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { DxDataGridComponent, DxDateBoxModule,DxSelectBoxModule } from "devextreme-angular";
import { Observable } from 'rxjs';
import { forEach } from 'lodash';

@Component({
	selector: 'kt-create-manifest-carrier',
	templateUrl: './create-manifest-carrier.component.html',
	styleUrls: ['./create-manifest-carrier.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateManifestCarrierComponent implements OnInit {
    allMode: string;
    checkBoxesMode: string;
	selectedRows: number[] = [];
    selectionChangedBySelectbox: boolean;

	@ViewChild('grid', { static: false }) dataGrid: DxDataGridComponent;
	
	constructor(private http: HttpClient, private cd: ChangeDetectorRef) {
        this.allMode = 'allPages';
        this.checkBoxesMode = 'onClick'
	}
	@Output() getList = new EventEmitter();

	createWindowVisible 		= false;
	approvalWindowVisible 		= false;
	confirmWindowVisible 		= false;
	koliManifestVisible			= false;

	ppnDs						: any;
	modaDs						: any;
	carrierDs					: any;

	firstFlightNoDs				: any;
	secondFlightNoDs			: any;
	thirdFlightNoDs				: any;
	fourthFlightNoDs			: any;

	firstCarrierCodeDs			: any;
	secondCarrierCodeDs			: any;
	thirdCarrierCodeDs			: any;
	fourthCarrierCodeDs			: any;

	stationOriginDs				: any;
	stationDestinationDs		: any;
	stationTransitDs			: any;
	dsList: any = {};

	vendorCarrierDs				: any;
	vendorId				: any;

	vendorOriginDs				: any;
	vendorDestinationDs			: any;

	cityDestinationDs			: any;

	koliManifestDs				: any;

	// manifest	
	// ///// ///// /////	
	manifest1Ds					: any;
	manifest1Id					: any;
	selectedRowsDataManifest	= [];
	popoverManifestVisible 		= false;
	reasonKoli					= false;
	reasonManifest				= false;
	koliManifest				: any;
	

	lastDays : any;

	formGetCostCarrier = {
		costSMU 	: null,
		vendorId	: null,
		firstCarrierId : null,
		firstFlightId : null,
		secondCarrierId : null,
		secondFlightId : null,
		lastCarrierId : null,
		lastFlightId	: null
	}

	fromCreate = {
		startDate					: new Date(),
		endDate						: new Date(),
		flightDate					: new Date(),

		//pecah start date
		startDateUtcYear			: null,
		startDateUtcMonth			: null,
		startDateUtcDay				: null,
		startDateUtcHour			: null,
		startDateUtcMinute			: null,
		startDateUtcSecond			: null,
		startDateUtcMillisecond		: null,

		//pecah end date
		endDateUtcYear				: null,
		endDateUtcMonth				: null,
		endDateUtcDay				: null,
		endDateUtcHour				: null,
		endDateUtcMinute			: null,
		endDateUtcSecond			: null,
		endDateUtcMillisecond		: null,

		//station & city
		originStationId				: null,
		originCityId				: null,
		originCityName				: null,
		destinationStationId		: null,
		destinationCityName			: null,

		//city	
		cityDestinationId			: null,

		//ppn
		ppnId						: null,

		//moda	
		modaId						: null,
		carrierId					: null,
		
		//flight
		firstFlightId				: null,
		secondFlightId				: null,
		thirdFlightId				: null,
		fourthFlightId				: null,

		//carrier
		firstCarrierCodeId			: null,
		secondCarrierCodeId			: null,
		thirdCarrierCodeId			: null,
		fourthCarrierCodeId			: null,

		//vendor
		vendorCarrierId				: null,
		vendorOriginId				: null,
		vendorDestinationId			: null,

		//transit
		firstCityTransitId 			: null,
		firstVendorTransitId 		: null,
		firstVendorTransitName		: null,
		firstStationTransitName		: null,
		firstStationTransitId		: null,
		
		secondCityTransitId 		: null,
		secondVendorTransitId 		: null,
		secondVendorTransitName		: null,
		secondStationTransitName	: null,
		secondStationTransitId		: null,
		
		thirdCityTransitId 			: null,
		thirdVendorTransitId 		: null,
		thirdVendorTransitName		: null,
		thirdStationTransitName		: null,
		thirdStationTransitId		: null,
		
		fourthCityTransitId 		: null,
		fourthVendorTransitId 		: null,


		noSmu						: null,
		finalWeight					: null,
		manifestTotalWeight			: null,
		manifestTotalKoli			: null,

		costInland					: null,
		beratBTB					: null,

		lookupKey					: null,

		reasonKoli					: null,
		reasonManifest				: null,
		reasonSMU					: null,

		approvedUser				: null,
		approvedPassword			: null,
		
		manifest1					: null,
		manifest2					: null,
		manifests					: [],
		manifestkoli				: [],
		revise						: 0,
		statRevise					: 'use'
	};

	// SumTotalSmuPerManifest()
	// -- -- --
	sumKoli = 0;
	sumSmu = 0;
	originCityId 			= "";
	

	ngOnInit() {
		this.formInit();
		this.getFunction();
		// this.readOriginStation();
		// this.readDestinationStation();

		this.lastDays = this.addDays(this.fromCreate.startDate, -3);
	}

	addDays(date: Date, days: number): Date {
        // date.setDate(date.getDate() + days);
        date.setDate(date.getDate() + (days));
        return date;
    }

	setWindowVisible(status) {
		this.formInit();
		this.getFunction();
		
		this.createWindowVisible = status;
		this.cd.detectChanges();
	}

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	readModa(){
		this.fromCreate.modaId = null;
		const params = {
			// DestinationStationId	: this.fromCreate.destinationStationId
		};

		this.modaDs = new DataSource({
			store: new CustomStore({
				key: 'modaId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-moda', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	readPpn(){
		this.fromCreate.ppnId = null;
		const params = {};

		this.ppnDs = new DataSource({
			store: new CustomStore({
				key: 'ppnId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-ppn', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeVendor(e){
		this.fromCreate.vendorCarrierId = null;
		
		// if (e.value === null) { this.vendorCarrierDs = null; return; }
		this.readDataVendor();
	}


	// Manifest
	// ///// ///// /////
	readManifest() {
	 	const params = {
			originStationId					: this.fromCreate.originStationId,
			destinationStationId			: this.fromCreate.destinationStationId
		};

		this.manifest1Ds = new DataSource({
			store: new CustomStore({
				key: 'manifestId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/data-manifest', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	readOriginStation() {
		const params = {
			//originVendorId : this.fromCreate.vendorOriginId
		};

		this.stationOriginDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-station-origin', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	GetListStationTransit() {
		this.dsList = new CustomStore({
			key: 'stationId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/manifest-carrier-master-station-city-list?isTransit=1')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	readOriginVendor(){
		const params = {
			originStationId : this.fromCreate.originStationId
		};

		this.vendorOriginDs = new DataSource({
			store: new CustomStore({
				key: 'vendorId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-vendor-origin', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeOriginStation(e){
		this.fromCreate.vendorOriginId = null;
		
		if (e.value === null) { this.vendorOriginDs = null; return; }
		this.readOriginVendor();
		this.readDestinationStation();
	}

	readDestinationStation() {
		const params = {
			originStationId : this.fromCreate.originStationId
		};

		this.stationDestinationDs = new DataSource({
			store: new CustomStore({
				key: 'stationId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-station-destination', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	readDestinationVendor(){
		const params = {
			destinationStationId : this.fromCreate.destinationStationId
		};

		this.vendorDestinationDs = new DataSource({
			store: new CustomStore({
				key: 'vendorId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-vendor-destination', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeDestinationStation(e){
		this.fromCreate.vendorDestinationId = null;
		
		if (e.value === null) { this.vendorDestinationDs = null; return; }
		this.readDestinationVendor();
	}
	

	readDataVendor() {
		const params = {
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			ModaId					: this.fromCreate.modaId
		};

		this.vendorCarrierDs = new DataSource({
			store: new CustomStore({
				key: 'vendorId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-vendor-carrier', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
		

	}

	onTriggerChangeOriginCity(e){
		this.originCityId = null;
		this.fromCreate.originCityName = null;
		
		const params = { stationId: e.value	};
		
		this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/get-city-origin', { headers: this.httpOptions.headers, params }).subscribe((response: any) =>{
			response.data.forEach( (item, index) => {
				document.getElementById("fromCreate.originCityName").innerHTML = item.originCityName;
				this.originCityId = item.originCityId;
			});
		});

		this.onTriggerChangeOriginStation(e);
	}

	onTriggerChangeDestinationCity(e){
		this.fromCreate.cityDestinationId = null;

		if (e.value === null) { this.cityDestinationDs = null; return; }
		this.onTriggerChangeManifest(e);
		this.onTriggerChangeDestinationStation(e);
	}

	onTriggerChangeManifest(e){
		
		this.getDataManifest(e);
	}

	getDataManifest(e){
		this.fromCreate.manifest1 = null;
		
		if (e.value === null) { this.manifest1Ds = null; return; }

		this.readManifest();
	}

	//-------------------------------------- First Flight---------------------------------------------

	onTriggerChangeFirstCarrierCode(e){
		this.fromCreate.firstCarrierCodeId = null;

		if (e.value === null) { this.firstCarrierCodeDs = null; return; }
		this.readFirstCarrierCode();
		this.formGetCostCarrier.vendorId = e.value;
		console.log(this.formGetCostCarrier.vendorId);
	}

	readFirstCarrierCode() {
		const params = {
			vendorCarrierId  		: this.fromCreate.vendorCarrierId,
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			modaId 					: this.fromCreate.modaId
		};

		this.firstCarrierCodeDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-first-carrier-code', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeFirstFlightNo(e){
		this.fromCreate.firstFlightId = null;
		
		if (e.value === null) { this.firstFlightNoDs = null; return; }
		this.readFirstFlight();
		
		this.formGetCostCarrier.firstCarrierId = e.value;
		console.log(this.formGetCostCarrier.firstCarrierId)
	}

	readFirstFlight() {
		const params = {
			vendorCarrierId			: this.fromCreate.vendorCarrierId,
			firstCarrierCodeId  	: this.fromCreate.firstCarrierCodeId,
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			modaId 					: this.fromCreate.modaId
		};


		this.firstFlightNoDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-first-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});

	}

	//-------------------------------------- Second Flight---------------------------------------------
	onTriggerChangeSecondCarrierCode(e){
		this.fromCreate.secondCarrierCodeId = null;

		if (e.value === null) { this.secondCarrierCodeDs = null; return; }
		this.readSecondCarrierCode();
		this.readCarrier();
		this.formGetCostCarrier.firstFlightId = e.value;
		this.formGetCostCarrier.lastFlightId = e.value;
		this.getBaseCostCarrierDetail();
	}

	getBaseCostCarrierDetail(){
		const params = {
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			carrierId				: this.formGetCostCarrier.firstCarrierId,
			vendorId				: this.formGetCostCarrier.vendorId,
			lastFlightId			: this.formGetCostCarrier.lastFlightId,
			firstFlightId			: this.formGetCostCarrier.firstFlightId
		};

		console.log(params);

		this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier-master/get-cost-carrier-detail', { headers: this.httpOptions.headers, params })
    .subscribe(
        (response: any) => {
            console.log("Response from API:", response); // Debug: print entire response
            if (response.data && Array.isArray(response.data)) {
                response.data.forEach((item, index) => {
                    console.log("Item:", item); // Debug: print each item in data array
                    this.formGetCostCarrier.costSMU = item.baseCost; // Assign baseCost to costSMU
                    const costElement = document.getElementById("formGetCostCarrier.costSMU");
                    if (costElement) {
                        costElement.innerHTML = item.baseCost;
                    }
                });
            } else {
                console.warn("Data array is missing or not an array in response.");
            }
        },
        (error) => {
            console.error("API call failed:", error); // Handle API errors
        }
    );

	}

	readSecondCarrierCode() {
		const params = {
			vendorCarrierId  		: this.fromCreate.vendorCarrierId,
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			firstCarrierCodeId		: this.fromCreate.firstCarrierCodeId,
			firstFlightId			: this.fromCreate.firstFlightId,
			modaId 					: this.fromCreate.modaId
		};

		this.secondCarrierCodeDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-second-carrier-code', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeSecondFlightNo(e){
		this.fromCreate.secondFlightId = null;

		if (e.value === null) { this.secondFlightNoDs = null; return; }
		this.readSecondFlight();
		this.formGetCostCarrier.secondCarrierId = e.value;
		this.formGetCostCarrier.lastCarrierId = e.value;
		console.log("dsa" + this.formGetCostCarrier.secondCarrierId);
		console.log("dsa" + this.formGetCostCarrier.lastCarrierId);
	}

	readSecondFlight() {
		const params = {
			vendorCarrierId			: this.fromCreate.vendorCarrierId,
			firstCarrierCodeId  	: this.fromCreate.firstCarrierCodeId,
			firstFlightId  			: this.fromCreate.firstFlightId,
			secondCarrierCodeId 	: this.fromCreate.secondCarrierCodeId,
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			modaId 					: this.fromCreate.modaId
		};


		this.secondFlightNoDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-second-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							console.log(result.data);
							return result.data;
						});
				}
			})
		});
	}

	//-------------------------------------- Third Flight---------------------------------------------
	onTriggerChangeThirdCarrierCode(e){
		this.fromCreate.thirdCarrierCodeId = null;

		if (e.value === null) { this.thirdCarrierCodeDs = null; return; }
		this.readThirdCarrierCode();
	}

	readThirdCarrierCode() {
		const params = {
			vendorCarrierId  		: this.fromCreate.vendorCarrierId,
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			firstCarrierCodeId		: this.fromCreate.firstCarrierCodeId,
			firstFlightId			: this.fromCreate.firstFlightId,
			secondCarrierCodeId		: this.fromCreate.secondCarrierCodeId,
			secondFlightId			: this.fromCreate.secondFlightId,
			modaId 					: this.fromCreate.modaId
		};

		this.thirdCarrierCodeDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-third-carrier-code', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeThirdFlightNo(e){
		this.fromCreate.thirdFlightId = null;
		
		if (e.value === null) { this.thirdFlightNoDs = null; return; }
		this.formGetCostCarrier.lastCarrierId = e.value;
		this.readThirdFlight();
	}

	readThirdFlight() {
		const params = {
			vendorCarrierId			: this.fromCreate.vendorCarrierId,
			firstCarrierCodeId  	: this.fromCreate.firstCarrierCodeId,
			firstFlightId  			: this.fromCreate.firstFlightId,
			secondCarrierCodeId 	: this.fromCreate.secondCarrierCodeId,
			secondFlightId 			: this.fromCreate.secondFlightId,
			thirdCarrierCodeId 		: this.fromCreate.thirdCarrierCodeId,
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			modaId 					: this.fromCreate.modaId
		};

		this.thirdFlightNoDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-third-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	//-------------------------------------- Fourth Flight---------------------------------------------
	onTriggerChangeFourthCarrierCode(e){
		this.fromCreate.fourthCarrierCodeId = null;

		if (e.value === null) { this.fourthCarrierCodeDs = null; return; }
		this.readFourthCarrierCode();
	}

	readFourthCarrierCode() {
		const params = {
			vendorCarrierId  		: this.fromCreate.vendorCarrierId,
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			firstCarrierCodeId		: this.fromCreate.firstCarrierCodeId,
			firstFlightId			: this.fromCreate.firstFlightId,
			secondCarrierCodeId		: this.fromCreate.secondCarrierCodeId,
			secondFlightId			: this.fromCreate.secondFlightId,
			thirdCarrierCodeId		: this.fromCreate.thirdCarrierCodeId,
			thirdFlightId			: this.fromCreate.thirdFlightId,
			modaId 					: this.fromCreate.modaId
		};

		this.fourthCarrierCodeDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-fourth-carrier-code', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeFourthFlightNo(e){
		this.fromCreate.fourthFlightId = null;

		if (e.value === null) { this.fourthFlightNoDs = null; return; }
		this.readFourthFlight();
	}

	readFourthFlight() {
		const params = {
			vendorCarrierId			: this.fromCreate.vendorCarrierId,
			firstCarrierCodeId  	: this.fromCreate.firstCarrierCodeId,
			firstFlightId  			: this.fromCreate.firstFlightId,
			secondCarrierCodeId 	: this.fromCreate.secondCarrierCodeId,
			secondFlightId 			: this.fromCreate.secondFlightId,
			thirdCarrierCodeId 		: this.fromCreate.thirdCarrierCodeId,
			thirdFlightId 			: this.fromCreate.thirdFlightId,
			fourthCarrierCodeId 	: this.fromCreate.fourthCarrierCodeId,
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			modaId 					: this.fromCreate.modaId
		};


		this.fourthFlightNoDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-fourth-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	//-------------------------------------- First Transit---------------------------------------------
	onTriggerChangeFirstTransit(e){
		this.fromCreate.firstVendorTransitName  = null;
		this.fromCreate.firstStationTransitName = null;
		this.formGetCostCarrier.secondFlightId = e.value;
		this.formGetCostCarrier.lastFlightId = e.value;
		this.getBaseCostCarrierDetail();

		const params = {
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			firstFlightId			: this.formGetCostCarrier.firstFlightId,
			firstCarrierCodeId		: this.formGetCostCarrier.firstCarrierId,
			lastFlightId			: this.formGetCostCarrier.lastFlightId,
			lastCarrierCodeId		: this.formGetCostCarrier.lastCarrierId
		};

		console.log(params);

		this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/vendor-station-transit-first', { headers: this.httpOptions.headers, params }).subscribe((response: any) =>{
			response.data.forEach( (item, index) => {
				document.getElementById("fromCreate.firstVendorTransitName").innerHTML = item.vendorName;
				document.getElementById("fromCreate.firstStationTransitName").innerHTML = item.stationName;
				this.fromCreate.firstVendorTransitId	= item.vendorId;
				this.fromCreate.firstStationTransitId	= item.stationId;
			});
		});

		this.onTriggerChangeThirdCarrierCode(e);
		this.readCarrier();
	}

	//-------------------------------------- Second Transit---------------------------------------------
	onTriggerChangeSecondTransit(e){
		this.fromCreate.secondVendorTransitName  = null;
		this.fromCreate.secondStationTransitName = null;
		this.formGetCostCarrier.lastFlightId = e.value;
		
		this.getBaseCostCarrierDetail();

		const params = {
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			firstFlightId			: this.formGetCostCarrier.secondFlightId,
			firstCarrierCodeId		: this.formGetCostCarrier.secondCarrierId,
			lastFlightId			: this.formGetCostCarrier.lastFlightId,
			lastCarrierCodeId		: this.formGetCostCarrier.lastCarrierId
		};

		console.log(params);

		this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/vendor-station-transit-second', { headers: this.httpOptions.headers, params })
		.subscribe((response: any) => {
			console.log("Response data:", response); // Menampilkan respons lengkap di konsol
	
			// Jika respons berisi data, iterasi untuk mengambil item di dalamnya
			response.data.forEach((item, index) => {
				console.log("Vendor Name:", item.vendorName); // Cek nilai vendorName
				console.log("Station Name:", item.stationName); // Cek nilai stationName
				console.log("Vendor ID:", item.vendorId); // Cek nilai vendorId
				console.log("Station ID:", item.stationId); // Cek nilai stationId
				
				document.getElementById("fromCreate.secondVendorTransitName").innerHTML = item.vendorName;
				document.getElementById("fromCreate.secondStationTransitName").innerHTML = item.stationName;
				this.fromCreate.secondVendorTransitId = item.vendorId;
				this.fromCreate.secondStationTransitId = item.stationId;
			});
		});
	

		this.onTriggerChangeFourthCarrierCode(e);
		this.readCarrier();
	}

	//-------------------------------------- Third Transit---------------------------------------------
	onTriggerChangeThirdTransit(e){
		this.fromCreate.thirdVendorTransitName  = null;
		this.fromCreate.thirdStationTransitName = null;
		this.formGetCostCarrier.lastFlightId = e.value;
		this.getBaseCostCarrierDetail();

		const params = {
			originStationId			: this.fromCreate.secondStationTransitId,
			destinationStationId	: this.fromCreate.destinationStationId,
			firstFlightId			: this.fromCreate.thirdFlightId,
			firstCarrierCodeId		: this.fromCreate.thirdCarrierCodeId,
			lastFlightId			: this.fromCreate.fourthFlightId,
			lastCarrierCodeId		: this.fromCreate.fourthCarrierCodeId,
			modaId 					: this.fromCreate.modaId
		};

		this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/vendor-station-transit', { headers: this.httpOptions.headers, params }).subscribe((response: any) =>{
			response.data.forEach( (item, index) => {
				document.getElementById("fromCreate.thirdVendorTransitName").innerHTML = item.vendorName;
				document.getElementById("fromCreate.thirdStationTransitName").innerHTML = item.stationName;
				this.fromCreate.thirdCityTransitId		= item.cityTransitCode;
				this.fromCreate.thirdVendorTransitId	= item.vendorId;
				this.fromCreate.thirdStationTransitId	= item.stationId;
			});
		});

		this.readCarrier();
	}
	
	//------------------------------------------------- Carrier ---------------------------------------------
	readCarrier(){
		const params = {
			originStationId			: this.fromCreate.originStationId,
			destinationStationId	: this.fromCreate.destinationStationId,
			firstFlightId			: this.fromCreate.firstFlightId,
			firstCarrierCodeId		: this.fromCreate.firstCarrierCodeId,
			secondFlightId			: this.fromCreate.secondFlightId,
			secondCarrierCodeId		: this.fromCreate.secondCarrierCodeId, 
			thirdFlightId			: this.fromCreate.thirdFlightId,
			thirdCarrierCodeId		: this.fromCreate.thirdCarrierCodeId,
			fourthFlightId			: this.fromCreate.fourthFlightId,
			fourthCarrierCodeId		: this.fromCreate.fourthCarrierCodeId,
			modaId 					: this.fromCreate.modaId,
			vendorCarrierId 		: this.fromCreate.vendorCarrierId
		};

		this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-carrier', { headers: this.httpOptions.headers, params }).subscribe((response: any) =>{
			response.data.forEach( (item, index) => {
				this.fromCreate.carrierId = item.carrierId;
				
			});
		});
	}

	ColManifestNo(rowData) {
		return rowData.manifestNo;
	}

	onValueChanged(e, dataField) {
		if (e.value === null) { this.fromCreate[dataField] = null; return; }
		this.fromCreate[dataField] = this.manifest1Id;
	}

	onSelectionChangedManifest(e) {
		// selected
		if ( e.currentDeselectedRowKeys.length === 0 ) {
			const selectedRowsData = e.selectedRowsData.filter(element => element.manifestId === e.currentSelectedRowKeys[0])[0];
			this.selectedRowsDataManifest.push(selectedRowsData);
			//
			console.log(selectedRowsData);
			this.fromCreate.manifests.push({
				manifestId 				: selectedRowsData.manifestId,
				destinationCityId	 	: selectedRowsData.destinationCityId,
				destinationCityName	 	: selectedRowsData.destinationCityName,
				aw	 					: selectedRowsData.sumAw,
				caw 					: selectedRowsData.sumCaw,
				koli 					: selectedRowsData.countKoli,
				sisaCaw 				: selectedRowsData.sisaCaw,
				sisaKoli 				: selectedRowsData.sisaKoli,
				sisaCawFinal 			: selectedRowsData.sisaCawFinal,
				sisaKoliFinal			: selectedRowsData.sisaKoliFinal,
				weightManifest			: null,
				finalKoli				: null,
				weightSmu				: null,
				vendorDestinationId		: null
			});
		}

		// deselected
		if ( e.currentDeselectedRowKeys.length !== 0 ) {
			this.selectedRowsDataManifest = this.selectedRowsDataManifest.filter((element) => {
				if (element.manifestId !== e.currentDeselectedRowKeys[0]) {
					return element;
				}
			});

			this.fromCreate.manifests = this.fromCreate.manifests.filter((element) => {
				if (element.manifestId !== e.currentDeselectedRowKeys[0]) {
					return element;
				}
			});

			//
		}
	}

	manifestNoFormat(rowData) {
		return rowData.manifestNo;
	}

	setConfirmWindowVisible(status) {
		this.confirmWindowVisible = status;
	}

	OnConfirm(){
		this.setConfirmWindowVisible(false);
		this.setApprovalWindowVisible(true);
	}

	setApprovalWindowVisible(status) {
		this.approvalWindowVisible = status;
	}

	approvalSubmit(e){
		var user 	= this.fromCreate.approvedUser;
		var pass 	= this.fromCreate.approvedPassword;
		var reason 	= this.fromCreate.reasonSMU;

		console.log(user);
		console.log(pass);
		console.log(reason);
		

		if(user == null || user == ''){
			console.log('user');
			
				notify({
					message: 'Approval User harus di isi',
					type: 'error',
					displayTime: 5000,
					width: 400,
				});
		}else if(pass == null || pass == ''){
			console.log('pass');
			
			notify({
				message: 'Approval Password harus di isi',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(reason == null || reason == ''){
			console.log('reason');
			
			notify({
				message: 'Alasan harus di isi',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else{
			this.saveData();	
		}
	}

	createSubmit(e) {
		if (this.validate() === false) { return false; }
		
		this.fromCreate.originCityId 			= this.originCityId;

		this.fromCreate.startDateUtcYear 		= this.fromCreate.startDate.getUTCFullYear();
		this.fromCreate.startDateUtcMonth 		= this.fromCreate.startDate.getUTCMonth() + 1;
		this.fromCreate.startDateUtcDay 		= this.fromCreate.startDate.getUTCDate();
		this.fromCreate.startDateUtcHour 		= this.fromCreate.startDate.getUTCHours();
		this.fromCreate.startDateUtcMinute 		= this.fromCreate.startDate.getMinutes();
		this.fromCreate.startDateUtcSecond 		= this.fromCreate.startDate.getUTCSeconds();
		this.fromCreate.startDateUtcMillisecond = this.fromCreate.startDate.getUTCMilliseconds();

		this.fromCreate.endDateUtcYear 			= this.fromCreate.endDate.getUTCFullYear();
		this.fromCreate.endDateUtcMonth 		= this.fromCreate.endDate.getUTCMonth() + 1;
		this.fromCreate.endDateUtcDay 			= this.fromCreate.endDate.getUTCDate();
		this.fromCreate.endDateUtcHour 			= this.fromCreate.endDate.getUTCHours();
		this.fromCreate.endDateUtcMinute 		= this.fromCreate.endDate.getMinutes();
		this.fromCreate.endDateUtcSecond 		= this.fromCreate.endDate.getUTCSeconds();
		this.fromCreate.endDateUtcMillisecond 	= this.fromCreate.endDate.getUTCMilliseconds();

		var tCaw 	 = 0;
		var tKoli 	 = 0;
		var tSumCaw  = 0;
		var tSumKoli = 0;
		
		this.fromCreate.manifests.forEach((item) => {

			if(tKoli == 0){
				if(item.finalKoli != item.sisaKoli){
					tKoli = 1;
				}
			}

			if(tCaw == 0){
				if(item.weightManifest != item.sisaCaw){
					tCaw = 1;
				}
			}
			
		});

		console.log('jumlah koli '+tKoli);
		console.log(this.fromCreate.reasonKoli);
		console.log('------------------------------------------------------------- koli');
		

		if(tKoli > 0){
			this.reasonKoli 	= true;
			tSumKoli = 1;
			if(this.fromCreate.reasonKoli != null && this.fromCreate.reasonKoli != ''){
				tSumKoli = 0;
				this.reasonKoli 	= false;
			}
		}else{
			tSumKoli = 0;
			this.reasonKoli 	= false;
		}  
		

		console.log('berat manifest '+tCaw);
		console.log(this.fromCreate.reasonManifest);
		console.log('------------------------------------------------------------- manifest');
		
		if(tCaw > 0){
			this.reasonManifest = true;
			tSumCaw = 1;
			if(this.fromCreate.reasonManifest != null && this.fromCreate.reasonManifest != ''){
				tSumCaw = 0;
				this.reasonManifest 	= false;
			}
		}else{
			tSumCaw = 0;
			this.reasonManifest = false;
		}
		

		if(tSumCaw == 0 && tSumKoli == 0){
			if(this.fromCreate.manifestTotalWeight > 400){
				this.setConfirmWindowVisible(true);
			}else{
				this.saveData();
			}
		}
		

		// if(sKoli > 0){
		// 	notify({
		// 		message: 'Berat Koli di Kenakan Lebih Besar daripada Sisa Koli',
		// 		type: 'error',
		// 		displayTime: 5000,
		// 		width: 400,
		// 	});
		// } else if(sAw > 0){
		// 	notify({
		// 		message: 'Berat Manifest di Kenakan Lebih Besar daripada Sisa Aw',
		// 		type: 'error',
		// 		displayTime: 5000,
		// 		width: 400,
		// 	});
		// } else if(min_sAf > this.fromCreate.manifestTotalWeight){
		// 	notify({
		// 		message: 'Total Berat Final SMU harus minimal paling sedikit, 5% dari jumlah Berat Manifest yang di kenakan',
		// 		type: 'error',
		// 		displayTime: 5000,
		// 		width: 400,
		// 	});	
		// }else{
		
		// }

	}

	saveData(){
			const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier/create', JSON.stringify(this.fromCreate), config)
			  .subscribe((data: any) => {
					console.log(data);
					
					if(data.data.counterOrigin == 0){
							notify({
								message: 'Data Belum Terdaftar di Master Cost Origin',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterDestination == 0){
							notify({
								message: 'Data Belum Terdaftar di Master Cost Destination',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterCostCarrier == 0){
							notify({
								message: 'Data Belum Terdaftar di Master Cost Carrier',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterManifest == 0){
							notify({
								message: 'Data Manifest Belum di pilih',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterApproval == 0){
							notify({
								message: 'Data User Approval Salah',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterPrivilegeApproval == 0){
							notify({
								message: 'Privilege User tidak bisa melakukan Approve',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else{
							if (data != null) {
								notify({
									message: 'Data telah tersimpan..',
									type: 'success',
									displayTime: 5000,
									width: 400,
								});
								this.setWindowVisible(false);
								this.formInit();
								this.getList.next();
								this.cd.detectChanges();
								window.location.reload();
							}
						}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
	}

	validate() {
		// if (this.fromCreate.manifestTotalWeight !== this.fromCreate.finalWeight) {
		// 	notify({
		// 		message: 'Berat Final tidak sama dengan berat total manifest',
		// 		type: 'error',
		// 		displayTime: 5000,
		// 		width: 400,
		// 	});
		//
		// 	return false;
		// }

		return true;
	}

	formInit() {
	
		this.fromCreate = {
			startDate					: new Date(),
			endDate						: new Date(),
			flightDate					: new Date(),
	
			//pecah start date
			startDateUtcYear			: null,
			startDateUtcMonth			: null,
			startDateUtcDay				: null,
			startDateUtcHour			: null,
			startDateUtcMinute			: null,
			startDateUtcSecond			: null,
			startDateUtcMillisecond		: null,
	
			//pecah end date
			endDateUtcYear				: null,
			endDateUtcMonth				: null,
			endDateUtcDay				: null,
			endDateUtcHour				: null,
			endDateUtcMinute			: null,
			endDateUtcSecond			: null,
			endDateUtcMillisecond		: null,
	
			//station & city
			originStationId				: null,
			originCityId				: null,
			originCityName				: null,
			destinationStationId		: null,
			destinationCityName			: null,
	
			//city	
			cityDestinationId			: null,
	
			//ppn
			ppnId						: null,
	
			//moda	
			modaId						: null,
			carrierId					: null,
			
			//flight
			firstFlightId				: null,
			secondFlightId				: null,
			thirdFlightId				: null,
			fourthFlightId				: null,
	
			//carrier
			firstCarrierCodeId			: null,
			secondCarrierCodeId			: null,
			thirdCarrierCodeId			: null,
			fourthCarrierCodeId			: null,
	
			//vendor
			vendorCarrierId				: null,
			vendorOriginId				: null,
			vendorDestinationId			: null,
	
			//transit
			firstCityTransitId 			: null,
			firstVendorTransitId 		: null,
			firstVendorTransitName		: null,
			firstStationTransitName		: null,
			firstStationTransitId		: null,
			
			secondCityTransitId 		: null,
			secondVendorTransitId 		: null,
			secondVendorTransitName		: null,
			secondStationTransitName	: null,
			secondStationTransitId		: null,
			
			thirdCityTransitId 			: null,
			thirdVendorTransitId 		: null,
			thirdVendorTransitName		: null,
			thirdStationTransitName		: null,
			thirdStationTransitId		: null,
			
			fourthCityTransitId 		: null,
			fourthVendorTransitId 		: null,
	
	
			noSmu						: null,
			finalWeight					: null,
			manifestTotalWeight			: null,
			manifestTotalKoli			: null,

			costInland					: null,
			beratBTB					: null,
	
			lookupKey					: null,
	
			reasonKoli					: null,
			reasonManifest				: null,
			reasonSMU					: null,
	
			approvedUser				: null,
			approvedPassword			: null,
	
			manifest1					: null,
			manifest2					: null,
			manifests					: [],
			manifestkoli				: [],
			revise						: 0,
			statRevise					: 'use'
		};

		this.manifest1Id = null;
		this.selectedRowsDataManifest = [];

	}

	SumTotalSmuPerManifest($event) {

		// reset
		// -- -- --
		this.sumSmu = 0

		// foreach manifests
		// -- -- --
		this.fromCreate.manifests.forEach((item) => {
			this.sumSmu += item.weightSmu !== null ? item.weightSmu : 0;
		});

		this.fromCreate.manifestTotalWeight = this.sumSmu;
	}

	SumTotalKoliPerManifest($event) {

		// reset
		// -- -- --
		this.sumKoli = 0

		// foreach manifests
		// -- -- --
		this.fromCreate.manifests.forEach((item) => {
			this.sumKoli += item.finalKoli !== null ? item.finalKoli : 0;
		});

		this.fromCreate.manifestTotalKoli = this.sumKoli;
	}

	getFunction(){
		this.readModa();
		// this.readOriginVendor();
		this.readOriginStation();
		// this.readDestinationVendor();
		this.GetListStationTransit();
		this.readPpn();		
	}

	onAddKoli(rowData, rows){
		this.koliManifest = rows;

		this.koliManifestVisible = true;
		this.ManifestKoli(rowData.manifestId);
	}

	ManifestKoli(manifestId){
		const params = { manifestId : manifestId};

		this.koliManifestDs = new DataSource({
			store: new CustomStore({
				key: 'spbGoodsId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-koli-manifest', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	// getSelectedRowKeys () {
    //     return this.dataGrid.instance.getSelectedRowKeys();
    // }
    getSelectedRowsData () {
        return this.dataGrid.instance.getSelectedRowsData();
    }

	OnCheckKoliManifest(){
		var getData = this.getSelectedRowsData();
		this.koliManifestVisible = false;

		var filtered = this.fromCreate.manifestkoli.filter(function(value, index, arr){ 
			return value != getData[0].manifestId;
		});

		var kolix = 0;
		var mf = 0;
		getData.forEach( (item, index) => {
			console.log(item);		
			mf = mf+(item.caw*item.koli);
			kolix = kolix+item.koli;	

			this.fromCreate.manifestkoli.push({
				manifestId 	: item.manifestId,
				spbId	 	: item.spbId,
				spbGoodsId	: item.spbGoodsId,
				koli	 	: item.koli
			});
		});

		// console.log('Koli Manifest di Kenakan : '+koli);
		// console.log('Berat Manifest di Kenakan : ' + mf);
		// console.log('Koli Manifest : '+this.koliManifest);

		this.fromCreate.manifests[this.koliManifest].finalKoli = kolix;
		this.fromCreate.manifests[this.koliManifest].weightManifest = mf;		

		console.log(this.fromCreate.manifestkoli);
		console.log(filtered);
		
		
	}
}
