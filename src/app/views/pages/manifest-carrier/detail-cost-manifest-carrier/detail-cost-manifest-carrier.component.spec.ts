import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCostManifestCarrierComponent } from './detail-cost-manifest-carrier.component';

describe('DetailCostManifestCarrierComponent', () => {
  let component: DetailCostManifestCarrierComponent;
  let fixture: ComponentFixture<DetailCostManifestCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCostManifestCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailCostManifestCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
