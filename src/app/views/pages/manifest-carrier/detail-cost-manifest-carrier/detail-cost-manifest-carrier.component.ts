import { Component, HostListener, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';
import { DxPivotGridModule, DxSelectBoxModule } from 'devextreme-angular';

@Component({
  selector: 'kt-detail-cost-manifest-carrier',
  templateUrl: './detail-cost-manifest-carrier.component.html',
  styleUrls: ['./detail-cost-manifest-carrier.component.scss']
})
export class DetailCostManifestCarrierComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
		this.historyLoginEnd();
	}

	HistoryForm = {
		historyloginId 	: 0,
		menu			: "Manifest Carrier - Detail Cost",
	};

  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;

  ds:any;
  manifestDs:any;
  PivotDs:any;
  pivotListDs:any;

  manifestCarrierId:any;

  applyChangesModes: any;
  applyChangesMode: any;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }
  
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.historyLoginStart();
    this.route.queryParams
		.subscribe(params => {
		
		this.manifestCarrierId = params.p1;

		this.GetList(params.p1);
		this.GetManifestList(params.p1);
		this.GetCostManifest(params.p1);
				
		}
	);
  }

  ngOnDestroy() { 
	this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  	onCellPrepared(e) {
	  if(e.column.dataField === "totalFinalWeightSMU") {
		e.cellElement.style.background = "lightgreen";
	  }
	}

	onCellPivotPrepared(e) {
		// if(e.area === 'data' && e.rowIndex === 1 && e.cell.dataIndex === 0){
		// 	e.cellElement.style.color = "transparent";//e.cell.value >= 1000 ? "green" : "red";
		// }
	}

	onPivotCellClick(e){
		// console.log(e);
		// e.cellElement.style.color = "black";
		
		// if(e.area === 'data' && e.rowIndex === 1 && e.cell.dataIndex === 0){
		// 	e.cellElement.style.color = "transparent";//e.cell.value >= 1000 ? "green" : "red";
		// }
	}

  GetList(manifestCarrierId){
    const params = { manifestCarrierId : manifestCarrierId };

		this.ds = new CustomStore({
			key: 'manifestCarrierId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/list-detail-cost', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
  }

  GetManifestList(manifestCarrierId){
			this.manifestDs = new CustomStore({
				key: 'manifestCarrierItemId',
				load:  (loadOptions: any) => {
					const httpParams = new HttpParams().set('manifestCarrierId', manifestCarrierId);
					const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8'), params: httpParams };
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/list-item', config)
						.toPromise()
						.then((data: any) => {
							return {
								data: data.data
							};
						})
						.catch(error => { console.log(error); });
				},
			});
  }

  GetCostManifest(manifestCarrierId){
    this.PivotDs =  {
			fields: [{
				caption: 'Kota Tujuan',
				// width: 120,
				dataField: 'cityCodeDestination',
				area: 'row'
			}, {
				caption: 'Manifest No',
				dataField: 'manifestNo',
				width: 100,
				area: 'row'
			}, {
				dataField: 'costName',
				dataType: 'string',
				area: 'column'
			},
			{
				caption: 'Biaya Satuan',
				dataField: 'baseCost',
				dataType: 'number',
				summaryType: 'sum',
				format: 'fixedPoint',
				area: 'data'
			}, {
				caption: 'Berat',
				dataField: 'weight',
				dataType: 'number',
				summaryType: 'sum',
				format: 'fixedPoint',
				area: 'data'
			}, {
				caption: 'Biaya Total',
				dataField: 'cost',
				dataType: 'number',
				summaryType: 'sum',
				format: 'fixedPoint',
				area: 'data'
			}],
			store: this.pivotList(manifestCarrierId)
		}

		this.applyChangesModes = ["instantly", "onDemand"];
		this.applyChangesMode = this.applyChangesModes[0];
  }

  pivotList(manifestCarrierId){
    this.pivotListDs = new CustomStore({
      key: 'costType',
      load:  (loadOptions: any) => {
        const httpParams = new HttpParams().set('manifestCarrierId', manifestCarrierId);
        const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8'), params: httpParams };
        return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/list-cost', config)
          .toPromise()
          .then((data: any) => {
            return {
              data: data.data
            };
          })
          .catch(error => { console.log(error); });
      },
    });

    return this.pivotListDs;
  }

  detailData = (e) => {
		this.router.navigate(['/detail-manifest-carrier'], 
        { 
            queryParams: { 
               p1: e.row.data.manifestCarrierId,
			   p2: 'use',
			   p3: 'front'
            } 
        });
	}

  onBack(){
    this.router.navigate(['manifest-carrier']);
  }

}
