import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailManifestCarrierComponent } from './detail-manifest-carrier.component';

describe('DetailManifestCarrierComponent', () => {
  let component: DetailManifestCarrierComponent;
  let fixture: ComponentFixture<DetailManifestCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailManifestCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailManifestCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
