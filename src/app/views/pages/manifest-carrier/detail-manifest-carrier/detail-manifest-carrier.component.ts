import { Component, OnInit, Renderer2, ElementRef, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { param } from 'jquery';

@Component({
  selector: 'kt-detail-manifest-carrier',
  templateUrl: './detail-manifest-carrier.component.html',
  styleUrls: ['./detail-manifest-carrier.component.scss']
})
export class DetailManifestCarrierComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
		this.historyLoginEnd();
	}

  HistoryForm = {
		historyloginId 	: 0,
		menu			: "Manifest Carrier - Detail Manifest Carrier",
	};

  // @ViewChild('div') div: ElementRef;
  @ViewChild('div', {static: false}) div: ElementRef;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router, private renderer: Renderer2) { }

  manifestCarrierItemData : any;

  formDetail = {
    manifestCarrierId   : null,
    periode_start       : null,
    periode_end         : null,
    originStation       : null,
    originVendor        : null,
    originCity          : null,
    destinationStation  : null,
    noSmu               : null,
    flightDate          : null,
    modaName            : null,
    vendorCarrier       : null,
    firstFlightCode     : null,
    firstFlightNo       : null,
    secondFlightCode    : null,
    secondFlightNo      : null,
    thirdFlightCode     : null,
    thirdFlightNo       : null,
    fourthFlightCode    : null,
    fourthFlightNo      : null,
    firstStationTransit : null,
    firstVendorTransit  : null,
    secondStationTransit: null,
    secondVendorTransit : null,
    thirdStationTransit : null,
    thirdVendorTransit  : null,
    reasonKoli          : null,
    reasonManifest      : null,
    reasonSMU           : null,
    approvedBy          : null,
    approvedDate        : null,
    totalFinalWeightSMU : null,
    totalKoliFinal      : null,
    ppn                 : null,
    cost_smu            : null,
    cost_inland         : null,
    btbWeight           : null
  }

  side: any;
  manifestCarrierId: any;
  manifestCarrierNo: any;

  isButtonVisible = false;

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        
        this.manifestCarrierId = params.p1;
        this.side = params.p3;

        if(params.p2 == 'use'){
          this.isButtonVisible = true;
        }

        this.GetList(params.p1);
        this.GetManifestList(params.p1);
                
      }
    );
  }

  ngOnDestroy() { 
    this.historyLoginEnd();
    }
  
    historyLoginStart(){
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
      .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
      .subscribe((data: any) => {
          this.HistoryForm.historyloginId = data.data;
        },
        (error) => {
        alert(error.statusText + '. ' + error.message);
        }
      );
    }
  
    historyLoginEnd(){
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
      .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
      .subscribe((data: any) => {
  
        },
        (error) => {
        alert(error.statusText + '. ' + error.message);
        }
      );
    }

  GetList(manifestCarrierId){
		this.formDetail.manifestCarrierId = manifestCarrierId;

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-detail/list-detail', JSON.stringify(this.formDetail), config)
      .subscribe((data: any) => {
        
        var dataDetail = data.data;
        dataDetail.forEach((item) => {
          document.getElementById("formDetail.manifestCarrierNo").innerHTML = "Manifest Carrier No : "+item.manifestCarrierNo;
          this.manifestCarrierNo              = item.manifestCarrierNo;
          this.formDetail.periode_start       = item.fromSendDate;
          this.formDetail.periode_end         = item.toSendDate;
          this.formDetail.originStation       = item.originStation;
          this.formDetail.originVendor        = item.originVendor;
          this.formDetail.originCity          = item.originCity;
          this.formDetail.destinationStation  = item.destinationStation;
          this.formDetail.noSmu               = item.noSmu;
          this.formDetail.flightDate          = item.flightDate;
          this.formDetail.modaName            = item.modaName;
          this.formDetail.vendorCarrier       = item.vendorCarrier;
          this.formDetail.firstFlightCode     = item.firstFlightCode;
          this.formDetail.firstFlightNo       = item.firstFlightNo;
          this.formDetail.secondFlightCode    = item.secondFlightCode;
          this.formDetail.secondFlightNo      = item.secondFlightNo;
          this.formDetail.thirdFlightCode     = item.thirdFlightCode;
          this.formDetail.thirdFlightNo       = item.thirdFlightNo;
          this.formDetail.fourthFlightCode    = item.fourthFlightCode;
          this.formDetail.fourthFlightNo      = item.fourthFlightNo;
          this.formDetail.firstStationTransit = item.firstStationTransit;
          this.formDetail.firstVendorTransit  = item.firstVendorTransit;
          this.formDetail.secondStationTransit= item.secondStationTransit;
          this.formDetail.secondVendorTransit = item.secondVendorTransit;
          this.formDetail.thirdStationTransit = item.thirdStationTransit;
          this.formDetail.thirdVendorTransit  = item.thirdVendorTransit;
          this.formDetail.reasonKoli          = item.reasonKoli;
          this.formDetail.reasonManifest      = item.reasonManifest;
          this.formDetail.reasonSMU           = item.reasonSMU;
          this.formDetail.approvedBy          = item.approvedBy;
          this.formDetail.approvedDate        = item.approvedDate; 
          this.formDetail.ppn                 = item.ppn; 
          this.formDetail.cost_smu            = item.cost_smu; 
          this.formDetail.totalFinalWeightSMU = item.totalFinalWeightSMU; 
          this.formDetail.totalKoliFinal      = item.totalKoliFinal; 
          this.formDetail.cost_inland         = item.cost_inland; 
          this.formDetail.btbWeight           = item.btbWeight; 
          
          console.log(item);
          
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  GetManifestList(manifestCarrierId){
    const body = {
			manifestCarrierId : manifestCarrierId
		};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-detail/list-item-detail',  body, this.httpOptions)
				.subscribe((data: any) => {
          console.log(data.data);
          
						 this.manifestCarrierItemData = data.data;
             console.log(this.manifestCarrierItemData);
             
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);

  }

  onBack(){
    // this.router.navigate(['manifest-carrier']);
    if(this.side == 'front'){
        this.router.navigate(['/detail-cost-manifest-carrier'], 
          { 
              queryParams: { 
                  p1:  this.manifestCarrierId
              } 
          });
      }else{
        this.router.navigate(['/list-revisi-manifest-carrier'], 
        { 
            queryParams: { 
               p1: this.manifestCarrierId
               , p2: this.manifestCarrierNo
            } 
        });
      }
  }

  onRevise() {
		this.router.navigate(['revisi-manifest-carrier'], 
        { 
            queryParams: { 
               p1: this.manifestCarrierId
            } 
        });
	}

  detailData(){
		this.router.navigate(['/list-revisi-manifest-carrier'], 
        { 
            queryParams: { 
               p1: this.manifestCarrierId
               , p2: this.manifestCarrierNo
            } 
        });
	}


}
