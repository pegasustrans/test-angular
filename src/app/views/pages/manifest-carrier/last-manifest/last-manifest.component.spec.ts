import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastManifestComponent } from './last-manifest.component';

describe('LastManifestComponent', () => {
  let component: LastManifestComponent;
  let fixture: ComponentFixture<LastManifestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastManifestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastManifestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
