import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-last-manifest',
  templateUrl: './last-manifest.component.html',
  styleUrls: ['./last-manifest.component.scss']
})
export class LastManifestComponent implements OnInit {
  ds: any = {};
  constructor(private http: HttpClient) { }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	  this.GetList();
  }
  
  GetList() {
		const params = {}; 
		this.ds = new CustomStore({
			key: 'manifestId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/last-manifest', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  onCellPrepared(e){
    if(e.row.data.columnColour === "yellow") {
      e.cellElement.style.background = "lightyellow";
    }

	if(e.row.data.columnColour === "green") {
		e.cellElement.style.background = "lightgreen";
	  }
    
    if(e.row.data.columnColour === "red") {
      e.cellElement.style.background = "pink";
    }
  }

}
