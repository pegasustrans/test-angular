import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRevisiManifestCarrierComponent } from './list-revisi-manifest-carrier.component';

describe('ListRevisiManifestCarrierComponent', () => {
  let component: ListRevisiManifestCarrierComponent;
  let fixture: ComponentFixture<ListRevisiManifestCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRevisiManifestCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRevisiManifestCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
