import { Component, HostListener, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-list-revisi-manifest-carrier',
  templateUrl: './list-revisi-manifest-carrier.component.html',
  styleUrls: ['./list-revisi-manifest-carrier.component.scss']
})
export class ListRevisiManifestCarrierComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
		this.historyLoginEnd();
	}

	HistoryForm = {
		historyloginId 	: 0,
		menu			: "Manifest Carrier - List Revisi",
	};
  
  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;

  manifestCarrierId : any;

  dsRevisiList: any = {};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {

        this.manifestCarrierId = params.p1;
        this.GetList(params.p2);
        document.getElementById("manifestCarrierNo").innerHTML = "Manifest Carrier No : "+params.p2;
                
      }
    );
   
  }

  ngOnDestroy() { 
    this.historyLoginEnd();
    }
  
    historyLoginStart(){
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
      .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
      .subscribe((data: any) => {
          this.HistoryForm.historyloginId = data.data;
        },
        (error) => {
        alert(error.statusText + '. ' + error.message);
        }
      );
    }
  
    historyLoginEnd(){
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
      .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
      .subscribe((data: any) => {
  
        },
        (error) => {
        alert(error.statusText + '. ' + error.message);
        }
      );
    }

  GetList(manifestCarrierNo) {
		const params = { manifestCarrierNo : manifestCarrierNo };

		this.dsRevisiList = new CustomStore({
			key: 'manifestCarrierId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/revise-list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  onBack(){
		this.router.navigate(['/detail-manifest-carrier'], 
        { 
            queryParams: { 
               p1: this.manifestCarrierId
               , p2: 'use'
               , p3: 'front'
            } 
        });
	}

  detailDataRevisi = (e) => {

    this.router.navigate(['/detail-manifest-carrier'], 
        { 
            queryParams: { 
               p1: e.row.data.manifestCarrierId,
			         p2: e.row.data.statRevise,
               p3: 'back'
            } 
        });
	}

}