import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestCarrierComponent } from './manifest-carrier.component';

describe('ManifestCarrierComponent', () => {
  let component: ManifestCarrierComponent;
  let fixture: ComponentFixture<ManifestCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
