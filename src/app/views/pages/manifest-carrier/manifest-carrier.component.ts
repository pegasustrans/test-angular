import {ChangeDetectorRef, Component, HostListener, OnInit, ChangeDetectionStrategy} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AppGlobalVar} from '../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';

@Component({
	selector: 'kt-manifest-carrier',
	templateUrl: './manifest-carrier.component.html',
	styleUrls: ['./manifest-carrier.component.scss']
})
export class ManifestCarrierComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	 constructor(private http: HttpClient) { }

	menuPriv = false;
	menuForb = true;

	HistoryForm = {
		menu: "Manifest Carrier List",
		historyloginId : 0
	};


	ngOnInit() {
		this.historyLoginStart();
		this.checkuserGlobalVar();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	
	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "manifest-carrier"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;
			
			}
		}
	}

}
