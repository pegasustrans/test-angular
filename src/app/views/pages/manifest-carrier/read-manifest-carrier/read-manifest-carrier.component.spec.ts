import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadManifestCarrierComponent } from './read-manifest-carrier.component';

describe('ManifestCarrierReadComponent', () => {
  let component: ReadManifestCarrierComponent;
  let fixture: ComponentFixture<ReadManifestCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadManifestCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadManifestCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
