import { Component, HostListener, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';
import { Router } from '@angular/router';
import { DxPivotGridModule, DxSelectBoxModule } from 'devextreme-angular';

@Component({
  selector: 'kt-read-manifest-carrier',
  templateUrl: './read-manifest-carrier.component.html',
  styleUrls: ['./read-manifest-carrier.component.scss']
})
export class ReadManifestCarrierComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
	appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

	ds: any = {};
	dsPivot: any = {};
	maanifestCarrierId: any;

	manifestDs: object = {};
	costListDs: object = {};
	pivotListDs: object = {}; 

	pivotGridDataSource: any;

	dataSource: any;
    applyChangesModes: any;
    applyChangesMode: any;

	HistoryForm = {
		menu: "Read Manifest Carrier",
		historyloginId:0
	};

  constructor(private http: HttpClient,private router: Router) { }
	ngOnInit() {
		this.GetList();
		this.historyLoginStart();
	}

	listItem(mcId) {
  		if(this.manifestDs[mcId.data.manifestCarrierId] === undefined) {
			this.manifestDs[mcId.data.manifestCarrierId] = new CustomStore({
				key: 'manifestCarrierItemId',
				load:  (loadOptions: any) => {
					const httpParams = new HttpParams().set('manifestCarrierId', mcId.data.manifestCarrierId);
					const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8'), params: httpParams };
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/list-item', config)
						.toPromise()
						.then((data: any) => {
							return {
								data: data.data
							};
						})
						.catch(error => { console.log(error); });
				},
			});
		}

  		return this.manifestDs[mcId.data.manifestCarrierId];

	}

	costList(mcId) {
		if(this.costListDs[mcId.data.manifestCarrierId] === undefined) {
			this.costListDs[mcId.data.manifestCarrierId] = new CustomStore({
				key: 'costType',
				load:  (loadOptions: any) => {
					const httpParams = new HttpParams().set('manifestCarrierId', mcId.data.manifestCarrierId);
					const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8'), params: httpParams };
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/list-cost', config)
						.toPromise()
						.then((data: any) => {
							return {
								data: data.data
							};
						})
						.catch(error => { console.log(error); });
				},
			});
		}

		return this.costListDs[mcId.data.manifestCarrierId];

	}

	pivotList(mcId) {
		console.log('maanifestCarrierId',this.maanifestCarrierId);
		console.log('mcId',mcId.data.manifestCarrierId);
		
		if(this.maanifestCarrierId != mcId.data.manifestCarrierId){
			console.log('masuk');
			
			this.maanifestCarrierId = mcId.data.manifestCarrierId;
			if(this.pivotListDs[mcId.data.manifestCarrierId] === undefined) {
				this.pivotListDs[mcId.data.manifestCarrierId] = new CustomStore({
					key: 'costType',
					load:  (loadOptions: any) => {
						const httpParams = new HttpParams().set('manifestCarrierId', mcId.data.manifestCarrierId);
						const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8'), params: httpParams };
						return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/list-cost', config)
							.toPromise()
							.then((data: any) => {
								return {
									data: data.data
								};
							})
							.catch(error => { console.log(error); });
					},
				});
			}
		}

		this.dsPivot =  {
			fields: [{
				caption: 'Kota Tujuan',
				// width: 120,
				dataField: 'cityCodeDestination',
				area: 'row'
			}, {
				caption: 'Manifest No',
				dataField: 'manifestNo',
				width: 100,
				area: 'row'
			}, {
				dataField: 'costName',
				dataType: 'string',
				area: 'column'
			},
			{
				caption: 'Biaya Satuan',
				dataField: 'baseCost',
				dataType: 'number',
				summaryType: 'sum',
				format: 'fixedPoint',
				showGrandTotals: false,
				showRowTotals:false,
				showRowGrandTotals:false,
				area: 'data'
			}, {
				caption: 'Berat',
				dataField: 'weight',
				dataType: 'number',
				summaryType: 'sum',
				format: 'fixedPoint',
				area: 'data'
			}, {
				caption: 'Biaya Total',
				dataField: 'cost',
				dataType: 'number',
				summaryType: 'sum',
				format: 'fixedPoint',
				area: 'data'
			}],
			store: this.pivotListDs[mcId.data.manifestCarrierId]
		}

		this.applyChangesModes = ["instantly", "onDemand"];
		this.applyChangesMode = this.applyChangesModes[0];

		return this.dsPivot;
	}

	kiloSelector(data) {
		// console.log(data);
		return data.unitValue;
		
		// return data.city + ' (' + data.country + ')';
	}
	
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};


	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	GetList() {
		this.ds = new CustomStore({
			key: 'manifestCarrierId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/list')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			},

			// remove: (key) => {
			// 	const body = { manifestCarrierId:key };
			// 	return this.http.post(AppGlobalVar.BASE_API_URL + 'manifest-carrier/delete',  body, this.httpOptions)
			// 		.toPromise()
			// 		.then((data: any) => {
			// 			// return method === "GET" ? data.data : data;
			// 			return data.data;
			// 			if (data != null) {
			// 				notify({
			// 					message: 'Data telah terhapus..',
			// 					type: 'success',
			// 					displayTime: 5000,
			// 					width: 400,
			// 				});
			// 			}
			// 		})
			// 		.catch(error => { console.log(error); });
			// },
		});
	}

	detailData = (e) => {
		this.router.navigate(['/detail-cost-manifest-carrier'], 
        { 
            queryParams: { 
               p1: e.row.data.manifestCarrierId,
            } 
        });
	}

	// detailData = (e) => {
	// 	this.router.navigate(['/detail-manifest-carrier'], 
    //     { 
    //         queryParams: { 
    //            p1: e.row.data.manifestCarrierId,
	// 		   p2: 'use'
    //         } 
    //     });
	// }

}
