import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisiManifestCarrierComponent } from './revisi-manifest-carrier.component';

describe('RevisiManifestCarrierComponent', () => {
  let component: RevisiManifestCarrierComponent;
  let fixture: ComponentFixture<RevisiManifestCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisiManifestCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisiManifestCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
