import { Component, OnInit, Renderer2, ElementRef, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-revisi-manifest-carrier',
  templateUrl: './revisi-manifest-carrier.component.html',
  styleUrls: ['./revisi-manifest-carrier.component.scss']
})
export class RevisiManifestCarrierComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
		this.historyLoginEnd();
	}

	HistoryForm = {
		historyloginId 	: 0,
		menu			: "Manifest Carrier - Form Revisi",
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  manifestCarrierItemData 				: any;
	vendorCarrierDs				  		: any;
  	firstFlightNoDs				  		: any;
	secondFlightNoDs			  		: any;
	thirdFlightNoDs				  		: any;
	fourthFlightNoDs			  		: any;
	firstCarrierCodeDs					: any;
	secondCarrierCodeDs					: any;
	thirdCarrierCodeDs					: any;
	fourthCarrierCodeDs					: any;

  formDetail = {
    manifestCarrierId         			: null,
    manifestCarrierNo        			: null,
    periode_start             			: null,
    periode_end               			: null,
    originStation             			: null,
    originVendor              			: null,
    originCity                			: null,
    destinationStation        			: null,
    noSmu                     			: null,
    flightDate                			: null,
    modaName                  			: null,
    vendorCarrier             			: null,
    firstFlightCode           			: null,
    firstFlightNo             			: null,
    secondFlightCode          			: null,
    secondFlightNo            			: null,
    thirdFlightCode           			: null,
    thirdFlightNo             			: null,
    fourthFlightCode          			: null,
    fourthFlightNo            			: null,
    firstStationTransit       			: null,
    firstVendorTransit        			: null,
    secondStationTransit      			: null,
    secondVendorTransit       			: null,
    thirdStationTransit       			: null,
    thirdVendorTransit        			: null,
    reasonKoli                			: null,
    reasonManifest            			: null,
    reasonSMU                 			: null,
    approvedBy                			: null,
    approvedDate              			: null,
    totalFinalWeightSMU       			: null,
    totalKoliFinal            			: null,
    ppn                       			: null,
    costSMU                  			: null,

	//------------------------------------------------------------------------------------------------------------------------------------//

	startDate							: new Date(),
	endDate								: new Date(),

    startDateUtcYear			    	: null,
	startDateUtcMonth					: null,
	startDateUtcDay						: null,
	startDateUtcHour					: null,
	startDateUtcMinute					: null,
	startDateUtcSecond					: null,
	startDateUtcMillisecond				: null,
	//pecah end date	
	endDateUtcYear				 		: null,
	endDateUtcMonth				 		: null,
	endDateUtcDay				 		: null,
	endDateUtcHour				 		: null,
	endDateUtcMinute			 		: null,
	endDateUtcSecond			 		: null,
	endDateUtcMillisecond		 		: null,
	//station & city  		
	originStationId				 		: null,
	originCityId				 		: null,
	originCityName				 		: null,
	destinationStationId		 		: null,
	destinationCityName			 		: null,
	//city	  		
	cityDestinationId			 		: null,
	//ppn 		
	ppnId						 		: null,
	//moda	  		
	modaId						 		: null,
	carrierId					 		: null,

	//flight  		
	firstFlightId				 		: null,
	secondFlightId				 		: null,
	thirdFlightId				 		: null,
	fourthFlightId				 		: null,
	//carrier 	
	firstCarrierCodeId			  		: null,
	secondCarrierCodeId			  		: null,
	thirdCarrierCodeId			  		: null,
	fourthCarrierCodeId			  		: null,
	//vendor  		
	vendorCarrierId				  		: null,
	vendorOriginId				  		: null,
	vendorDestinationId			  		: null,
	//transit		
	firstCityTransitId 			  		: null,
	firstVendorTransitId 		  		: null,
	firstVendorTransitName		  		: null,
	firstStationTransitName		  		: null,
	firstStationTransitId		  		: null,
	secondCityTransitId 		  		: null,
	secondVendorTransitId 		  		: null,
	secondVendorTransitName		  		: null,
	secondStationTransitName	  		: null,
	secondStationTransitId		  		: null,
	thirdCityTransitId 			  		: null,
	thirdVendorTransitId 		  		: null,
	thirdVendorTransitName		  		: null,
	thirdStationTransitName		  		: null,
	thirdStationTransitId		  		: null,
	fourthCityTransitId 		  		: null,
	fourthVendorTransitId 		  		: null,
	finalWeight					  		: null,
	manifestTotalWeight			  		: null,
	manifestTotalKoli			  		: null,
	lookupKey					  		: null,
	approvedUserId				  		: null,
	approvedPassword			  		: null,
	manifest1					  		: null,
	manifest2					  		: null,
	manifests					  		: [],
	manifestkoli				  		: [],
	revise						  		: 0,
	statRevise					  		: 'use'
  }	

  manifestCarrierId						: any;
  
  vendorCarrierId						: any;
  firstCarrierCodeId  					: any;
  firstFlightId  						: any;
  secondCarrierCodeId 					: any;
  secondFlightId 						: any;
  thirdCarrierCodeId 					: any;
  thirdFlightId 						: any;
  fourthCarrierCodeId 					: any;
  fourthFlightId      					: any;
  originStationId						: any;
  destinationStationId					: any;
  modaId 				    			: any;

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {

        this.manifestCarrierId = params.p1;
        this.GetList(params.p1);
        this.GetManifestList(params.p1);
                
      }
    );
  }
  
  ngOnDestroy() { 
	this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  GetList(manifestCarrierId){
		this.formDetail.manifestCarrierId = manifestCarrierId;

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-detail/list-detail', JSON.stringify(this.formDetail), config)
      .subscribe((data: any) => {
        
        var dataDetail = data.data;

        dataDetail.forEach((item) => {
          
          document.getElementById("formDetail.manifestCarrierNo").innerHTML = "Manifest Carrier No : "+item.manifestCarrierNo;

			this.formDetail.periode_start       				= item.fromSendDate;
			this.formDetail.periode_end         				= item.toSendDate;
			this.formDetail.originStation       				= item.originStation;
			this.formDetail.originVendor        				= item.originVendor;
			this.formDetail.originCity          				= item.originCity;
			this.formDetail.destinationStation  				= item.destinationStation;
			this.formDetail.noSmu               				= item.noSmu;
			this.formDetail.flightDate          				= item.flightDate;
			this.formDetail.modaName            				= item.modaName;
			this.formDetail.reasonKoli          				= item.reasonKoli;
			this.formDetail.reasonManifest      				= item.reasonManifest;
			this.formDetail.reasonSMU           				= item.reasonSMU;
			this.formDetail.approvedBy          				= item.approvedBy;
			this.formDetail.approvedDate        				= item.approvedDate; 
			this.formDetail.ppn                 				= item.ppn; 
			this.formDetail.costSMU            					= parseInt(item.cost_smu); 
			this.formDetail.manifestTotalWeight 				= parseFloat(item.totalFinalWeightSMU); 
			this.formDetail.manifestTotalKoli      				= parseInt(item.totalKoliFinal); 
			this.formDetail.manifestCarrierNo					= item.manifestCarrierNo;

			// this.vendorCarrierId			   					= item.vendorCarrierId;
			// this.firstCarrierCodeId      						= item.firstFlightCodeId;
			// this.firstFlightId  			   					= item.firstFlightNoId;
			// this.secondCarrierCodeId     						= item.secondFlightCodeId;
			// this.secondFlightId 			   					= item.secondFlightNoId;
			// this.thirdCarrierCodeId 	   						= item.thirdFlightCodeId;
			// this.thirdFlightId 			     					= item.thirdFlightNoId;
			// this.fourthCarrierCodeId     						= item.fourthFlightCodeId;
			// this.fourthFlightId          						= item.fourthFlightNoId;
			// this.originStationId			   					= item.originStationId;
			// this.destinationStationId    						= item.destinationStationId;
			// this.modaId 				    	   				= item.modaId;

			//---------------------------------------------------------------------------------------------------------------------------//

			this.formDetail.startDate				  			= new Date(item.fromSendDate);
			this.formDetail.endDate								= new Date(item.toSendDate);

			this.formDetail.startDateUtcYear 					= this.formDetail.startDate.getUTCFullYear();
			this.formDetail.startDateUtcMonth 					= this.formDetail.startDate.getUTCMonth() + 1;
			this.formDetail.startDateUtcDay 					= this.formDetail.startDate.getUTCDate();
			this.formDetail.startDateUtcHour 					= this.formDetail.startDate.getUTCHours();
			this.formDetail.startDateUtcMinute 					= this.formDetail.startDate.getMinutes();
			this.formDetail.startDateUtcSecond 					= this.formDetail.startDate.getUTCSeconds();
			this.formDetail.startDateUtcMillisecond 			= this.formDetail.startDate.getUTCMilliseconds();

			this.formDetail.endDateUtcYear 						= this.formDetail.endDate.getUTCFullYear();
			this.formDetail.endDateUtcMonth 					= this.formDetail.endDate.getUTCMonth() + 1;
			this.formDetail.endDateUtcDay 						= this.formDetail.endDate.getUTCDate();
			this.formDetail.endDateUtcHour 						= this.formDetail.endDate.getUTCHours();
			this.formDetail.endDateUtcMinute 					= this.formDetail.endDate.getMinutes();
			this.formDetail.endDateUtcSecond 					= this.formDetail.endDate.getUTCSeconds();
			this.formDetail.endDateUtcMillisecond 				= this.formDetail.endDate.getUTCMilliseconds();

			//station & city  		
			this.formDetail.originStationId				 		= item.originStationId;
			this.formDetail.originCityId				 		= item.originCityId;
			this.formDetail.originCityName				 		= item.originCity;
			this.formDetail.destinationStationId		 		= item.destinationStationId;
			this.formDetail.destinationStation			 		= item.destinationStation;
			
			//ppn 		
			this.formDetail.ppnId						 		= item.ppnId;
			//moda	  		
			this.formDetail.modaId						 		= item.modaId;
			this.formDetail.carrierId					 		= item.carrierId;

			//flight  		
			//vendor  		
			this.formDetail.vendorOriginId				  		= item.vendorOriginId;
		

			// this.formDetail.vendorCarrierId				  		: null,
			// //transit		
			// this.formDetail.firstCityTransitId 			  		: null,
			// this.formDetail.firstVendorTransitId 		  		: null,
			// this.formDetail.firstVendorTransitName		  		: null,
			// this.formDetail.firstStationTransitName		  		: null,
			// this.formDetail.firstStationTransitId		  		: null,
			// this.formDetail.secondCityTransitId 		  		: null,
			// this.formDetail.secondVendorTransitId 		  		: null,
			// this.formDetail.secondVendorTransitName		  		: null,
			// this.formDetail.secondStationTransitName	  		: null,
			// this.formDetail.secondStationTransitId		  		: null,
			// this.formDetail.thirdCityTransitId 			  		: null,
			// this.formDetail.thirdVendorTransitId 		  		: null,
			// this.formDetail.thirdVendorTransitName		  		: null,
			// this.formDetail.thirdStationTransitName		  		: null,
			// this.formDetail.thirdStationTransitId		  		: null,
			// this.formDetail.fourthCityTransitId 		  		: null,
			// this.formDetail.fourthVendorTransitId 		  		: null,

			
			this.formDetail.approvedUserId				  		= item.approvedById;
			this.formDetail.approvedPassword			  		= item.approvedByPassword;
			
			this.formDetail.revise						  		= item.revise;
			this.formDetail.statRevise					  		= 'use';

	//---------------------------------------------------------------------------------------------------//
          
          this.readDataVendor(item.originStationId, item.destinationStationId, item.modaId);
        //   this.formDetail.vendorCarrierId     	= item.vendorCarrierId;
          
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  GetManifestList(manifestCarrierId){
    const body = {
			manifestCarrierId : manifestCarrierId
		};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'manifest-carrier-detail/list-item-detail',  body, this.httpOptions)
				.subscribe((data: any) => {
          console.log(data.data);
          
				 	this.manifestCarrierItemData = data.data;
					this.manifestCarrierItemData.forEach((item) => {

							this.formDetail.manifests.push({
								manifestId 				: parseInt(item.manifestId),
								destinationCityId	 	: item.destinationCityId,
								aw	 					: parseInt(item.aw),
								caw 					: parseInt(item.caw),
								koli 					: parseInt(item.koli),
								finalKoli				: parseInt(item.koliFinal),
								weightManifest			: parseInt(item.cawFinal),
								weightSmu				: parseFloat(item.finalWeightSMU),
								vendorDestinationId		: parseInt(item.vendorDestinationId)
							});
						
					});
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);

  }

  //-------------------------------------- Vendor Carrier ---------------------------------------------
  readDataVendor(originStationId, destinationStationId,modaId) {
		const params = {
			originStationId			  	: originStationId,
			destinationStationId		: destinationStationId,
			ModaId					    : modaId
		};

		this.vendorCarrierDs = new DataSource({
			store: new CustomStore({
				key: 'vendorId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-vendor-carrier', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  //-------------------------------------- First Flight---------------------------------------------
  onTriggerChangeFirstCarrierCode(e){
		this.formDetail.firstCarrierCodeId = null;

		if (e.value === null) { this.firstCarrierCodeDs = null; return; }
		this.readFirstCarrierCode();
   		// this.formDetail.firstCarrierCodeId  = this.firstCarrierCodeId;
	}

  readFirstCarrierCode() {
		const params = {
			vendorCarrierId  		  	: this.formDetail.vendorCarrierId,
			originStationId			  	: this.formDetail.originStationId,
			destinationStationId		: this.formDetail.destinationStationId,
			modaId 					    : this.formDetail.modaId
		};

		this.firstCarrierCodeDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-first-carrier-code', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  onTriggerChangeFirstFlightNo(e){
		this.formDetail.firstFlightId = null;

		if (e.value === null) { this.firstFlightNoDs = null; return; }
		this.readFirstFlight();
    	// this.formDetail.firstFlightId       = this.firstFlightId;
	}

  readFirstFlight() {
		const params = {
			vendorCarrierId			  	: this.formDetail.vendorCarrierId,
			firstCarrierCodeId  		: this.formDetail.firstCarrierCodeId,
			originStationId			  	: this.formDetail.originStationId,
			destinationStationId		: this.formDetail.destinationStationId,
			modaId 					    : this.formDetail.modaId
		};


		this.firstFlightNoDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-first-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  //-------------------------------------- Second Flight---------------------------------------------
	onTriggerChangeSecondCarrierCode(e){
		this.formDetail.secondCarrierCodeId = null;

		if (e.value === null) { this.secondCarrierCodeDs = null; return; }
		this.readSecondCarrierCode();
	}

	readSecondCarrierCode() {
		const params = {
			vendorCarrierId  		  	: this.formDetail.vendorCarrierId,
			firstCarrierCodeId			: this.formDetail.firstCarrierCodeId,
			firstFlightId			    : this.formDetail.firstFlightId,
			originStationId			  	: this.formDetail.originStationId,
			destinationStationId		: this.formDetail.destinationStationId,
			modaId 					    : this.formDetail.modaId
		};

		this.secondCarrierCodeDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-second-carrier-code', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeSecondFlightNo(e){
		this.formDetail.secondFlightId = null;

		if (e.value === null) { this.secondFlightNoDs = null; return; }
		this.readSecondFlight();
	}

	readSecondFlight() {
		const params = {
			vendorCarrierId			  	: this.formDetail.vendorCarrierId,
			firstCarrierCodeId  		: this.formDetail.firstCarrierCodeId,
			firstFlightId  			  	: this.formDetail.firstFlightId,
			secondCarrierCodeId 		: this.formDetail.secondCarrierCodeId,
			originStationId			  	: this.formDetail.originStationId,
			destinationStationId		: this.formDetail.destinationStationId,
			modaId 					    : this.formDetail.modaId
		};


		console.log(params, 'flight 2');
		

		this.secondFlightNoDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-second-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  //-------------------------------------- First Transit---------------------------------------------
	onTriggerChangeFirstTransit(e){
		this.formDetail.firstVendorTransitName  = null;
		this.formDetail.firstStationTransitName = null;

		const params = {
			originStationId			: this.formDetail.originStationId,
			destinationStationId	: this.formDetail.destinationStationId,
			firstFlightId			: this.formDetail.firstFlightId,
			firstCarrierCodeId		: this.formDetail.firstCarrierCodeId,
			lastFlightId			: this.formDetail.secondFlightId,
			lastCarrierCodeId		: this.formDetail.secondCarrierCodeId,
			modaId 					: this.formDetail.modaId
		};

		console.log(params);		

		this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/vendor-station-transit', { headers: this.httpOptions.headers, params }).subscribe((response: any) =>{
			response.data.forEach( (item, index) => {
				console.log(item,'TRANSIT 1');
				
				this.formDetail.firstVendorTransitName = item.vendorName;
				this.formDetail.firstStationTransitName = item.stationName;
				
				this.formDetail.firstVendorTransitId	= item.vendorId;
				this.formDetail.firstStationTransitId	= item.stationId;
			});
		});

		this.onTriggerChangeThirdCarrierCode(e);
	}

	//-------------------------------------- Third Flight---------------------------------------------
	onTriggerChangeThirdCarrierCode(e){
		this.formDetail.thirdCarrierCodeId = null;

		if (e.value === null) { this.thirdCarrierCodeDs = null; return; }
		this.readThirdCarrierCode();
	}

	readThirdCarrierCode() {
		const params = {
			vendorCarrierId  		: this.formDetail.vendorCarrierId,
			originStationId			: this.formDetail.originStationId,
			destinationStationId	: this.formDetail.destinationStationId,
			firstCarrierCodeId		: this.formDetail.firstCarrierCodeId,
			firstFlightId			: this.formDetail.firstFlightId,
			secondCarrierCodeId		: this.formDetail.secondCarrierCodeId,
			secondFlightId			: this.formDetail.secondFlightId,
			modaId 					: this.formDetail.modaId
		};

		this.thirdCarrierCodeDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-third-carrier-code', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeThirdFlightNo(e){
		this.formDetail.thirdFlightId = null;

		if (e.value === null) { this.thirdFlightNoDs = null; return; }
		this.readThirdFlight();
	}

	readThirdFlight() {
		const params = {
			vendorCarrierId			: this.formDetail.vendorCarrierId,
			firstCarrierCodeId  	: this.formDetail.firstCarrierCodeId,
			firstFlightId  			: this.formDetail.firstFlightId,
			secondCarrierCodeId 	: this.formDetail.secondCarrierCodeId,
			secondFlightId 			: this.formDetail.secondFlightId,
			thirdCarrierCodeId 		: this.formDetail.thirdCarrierCodeId,
			originStationId			: this.formDetail.originStationId,
			destinationStationId	: this.formDetail.destinationStationId,
			modaId 					: this.formDetail.modaId
		};

		console.log(params, 'flight 3');
		

		this.thirdFlightNoDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-third-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}
	//-------------------------------------- Second Transit---------------------------------------------
	onTriggerChangeSecondTransit(e){
		this.formDetail.secondVendorTransitName  = null;
		this.formDetail.secondStationTransitName = null;

		const params = {
			originStationId			: this.formDetail.firstStationTransitId,
			destinationStationId	: this.formDetail.destinationStationId,
			firstFlightId			: this.formDetail.secondFlightId,
			firstCarrierCodeId		: this.formDetail.secondCarrierCodeId,
			lastFlightId			: this.formDetail.thirdFlightId,
			lastCarrierCodeId		: this.formDetail.thirdCarrierCodeId,
			modaId 					: this.formDetail.modaId
		};

		console.log(params);		


		this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/vendor-station-transit', { headers: this.httpOptions.headers, params }).subscribe((response: any) =>{
			response.data.forEach( (item, index) => {
				console.log(item,'TRANSIT 2');
				
				this.formDetail.secondVendorTransitName = item.vendorName;
				this.formDetail.secondStationTransitName = item.stationName;
				this.formDetail.secondVendorTransitId		= item.vendorId;
				this.formDetail.secondStationTransitId		= item.stationId;
			});
		});

		this.onTriggerChangeFourthCarrierCode(e);
	}

	//-------------------------------------- Fourth Flight---------------------------------------------
	onTriggerChangeFourthCarrierCode(e){
		this.formDetail.fourthCarrierCodeId = null;

		if (e.value === null) { this.fourthCarrierCodeDs = null; return; }
		this.readFourthCarrierCode();
	}

	readFourthCarrierCode() {
		const params = {
			vendorCarrierId  		: this.formDetail.vendorCarrierId,
			originStationId			: this.formDetail.originStationId,
			destinationStationId	: this.formDetail.destinationStationId,
			firstCarrierCodeId		: this.formDetail.firstCarrierCodeId,
			firstFlightId			: this.formDetail.firstFlightId,
			secondCarrierCodeId		: this.formDetail.secondCarrierCodeId,
			secondFlightId			: this.formDetail.secondFlightId,
			thirdCarrierCodeId		: this.formDetail.thirdCarrierCodeId,
			thirdFlightId			: this.formDetail.thirdFlightId,
			modaId 					: this.formDetail.modaId
		};

		this.fourthCarrierCodeDs = new DataSource({
			store: new CustomStore({
				key: 'carrierId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-fourth-carrier-code', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	onTriggerChangeFourthFlightNo(e){
		this.formDetail.fourthFlightId = null;

		if (e.value === null) { this.fourthFlightNoDs = null; return; }
		this.readFourthFlight();
		// this.formDetail.fourthFlightId  = this.fourthFlightId;
	}

	readFourthFlight() {
		const params = {
			vendorCarrierId			: this.formDetail.vendorCarrierId,
			firstCarrierCodeId  	: this.formDetail.firstCarrierCodeId,
			firstFlightId  			: this.formDetail.firstFlightId,
			secondCarrierCodeId 	: this.formDetail.secondCarrierCodeId,
			secondFlightId 			: this.formDetail.secondFlightId,
			thirdCarrierCodeId 		: this.formDetail.thirdCarrierCodeId,
			thirdFlightId 			: this.formDetail.thirdFlightId,
			fourthCarrierCodeId 	: this.formDetail.fourthCarrierCodeId,
			originStationId			: this.formDetail.originStationId,
			destinationStationId	: this.formDetail.destinationStationId,
			modaId 					: this.formDetail.modaId
		};

		console.log(params, 'flight 4');
		

		this.fourthFlightNoDs = new DataSource({
			store: new CustomStore({
				key: 'flightId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-fourth-flight', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	//-------------------------------------- Third Transit---------------------------------------------
	onTriggerChangeThirdTransit(e){
		this.formDetail.thirdVendorTransitName  = null;
		this.formDetail.thirdStationTransitName = null;

		const params = {
			originStationId			: this.formDetail.secondStationTransitId,
			destinationStationId	: this.formDetail.destinationStationId,
			firstFlightId			: this.formDetail.thirdFlightId,
			firstCarrierCodeId		: this.formDetail.thirdCarrierCodeId,
			lastFlightId			: this.formDetail.fourthFlightId,
			lastCarrierCodeId		: this.formDetail.fourthCarrierCodeId,
			modaId 					: this.formDetail.modaId
		};

		console.log(params);		


		this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/vendor-station-transit', { headers: this.httpOptions.headers, params }).subscribe((response: any) =>{
			response.data.forEach( (item, index) => {
				console.log(item,'TRANSIT 3');
				
				this.formDetail.thirdVendorTransitName	= item.vendorName;
				this.formDetail.thirdStationTransitName	= item.stationName;
				this.formDetail.thirdCityTransitId		= item.cityTransitCode;
				this.formDetail.thirdVendorTransitId	= item.vendorId;
				this.formDetail.thirdStationTransitId	= item.stationId;
			});
		});
	}

  onBack(){
    this.router.navigate(['manifest-carrier']);
  }

  onSave() {
	  	
		if(this.formDetail.vendorCarrierId == null){
			notify({
				message: 'Vendor Carrier harus di pilih dulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formDetail.firstFlightId == null){
			notify({
				message: 'No Penerbangan harus di pilih dulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
			this.formDetail.manifestCarrierId = parseInt(this.formDetail.manifestCarrierId);

			const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'manifest-carrier/create', JSON.stringify(this.formDetail), config)
			  .subscribe((data: any) => {
					console.log(data);
					
					if(data.data.counterOrigin == 0){
							notify({
								message: 'Data Belum Terdaftar di Master Cost Origin',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterDestination == 0){
							notify({
								message: 'Data Belum Terdaftar di Master Cost Destination',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterCostCarrier == 0){
							notify({
								message: 'Data Belum Terdaftar di Master Cost Carrier',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterManifest == 0){
							notify({
								message: 'Data Manifest Belum di pilih',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterApproval == 0){
							notify({
								message: 'Data User Approval Salah',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else if(data.data.counterPrivilegeApproval == 0){
							notify({
								message: 'Privilege User tidak bisa melakukan Approve',
								type: 'error',
								displayTime: 5000,
								width: 400,
							});
						}else{
							if (data != null) {
								notify({
									message: 'Data telah tersimpan..',
									type: 'success',
									displayTime: 5000,
									width: 400,
								});

								this.router.navigate(['manifest-carrier']);
							}
						}
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
		}
		// this.router.navigate(['revisi-manifest-carrier'], 
        // { 
        //     queryParams: { 
        //        p1: this.manifestCarrierId
        //     } 
        // });
		console.log(this.formDetail);
		
	}

}
