import {OnDestroy, Pipe, PipeTransform} from '@angular/core';
import {Subject} from 'rxjs';
import {ManifestService} from './manifest.service';
import {takeUntil} from 'rxjs/operators';

@Pipe({
  name: 'calculateTunaiAsal'
})
export class CalculateTunaiAsalPipe implements PipeTransform, OnDestroy {

	sumTunaiAsal: number;
	sumTunaiAsalSubs: Subject<any> = new Subject<any>();

	constructor(private manifestService: ManifestService) {

	}

	transform(value: any, ...args: any[]): any {
		this.manifestService.sumTunaiAsal
			.pipe(takeUntil(this.sumTunaiAsalSubs))
			.subscribe(data => {
				this.sumTunaiAsal = data;
			});

    	return this.CalculateTunaiAsal(value);
  }


	CalculateTunaiAsal(ta) {
		// this.totalTunaiAsal = this.totalTunaiAsal + ta;
		this.manifestService.TunaiAsalSet(ta);
		return ta;
	}


	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.sumTunaiAsalSubs.next();
		this.sumTunaiAsalSubs.complete();
		this.sumTunaiAsalSubs.unsubscribe();
	}

}
