import {OnDestroy, Pipe, PipeTransform} from '@angular/core';
import {Subject} from 'rxjs';
import {ManifestService} from './manifest.service';
import {takeUntil} from 'rxjs/operators';

@Pipe({
  name: 'calculateTunaiTujuan'
})
export class CalculateTunaiTujuanPipe implements PipeTransform, OnDestroy {

	sumTunaiTujuan: number;
	sumTunaiTujuanSubs: Subject<any> = new Subject<any>();

	constructor(private manifestService: ManifestService) {

	}

  transform(value: any, ...args: any[]): any {
	  this.manifestService.sumTunaiTujuan
		  .pipe(takeUntil(this.sumTunaiTujuanSubs))
		  .subscribe(data => {
			  this.sumTunaiTujuan = data;
		  });

	  return this.CalculateTunaiTujuan(value);
  }

	CalculateTunaiTujuan(ta) {
		this.manifestService.TunaiTujuanSet(ta);
		return ta;
	}


	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.sumTunaiTujuanSubs.next();
		this.sumTunaiTujuanSubs.complete();
		this.sumTunaiTujuanSubs.unsubscribe();
	}

}
