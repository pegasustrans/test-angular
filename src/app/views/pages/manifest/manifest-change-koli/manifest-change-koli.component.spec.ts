import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestChangeKoliComponent } from './manifest-change-koli.component';

describe('ManifestChangeKoliComponent', () => {
  let component: ManifestChangeKoliComponent;
  let fixture: ComponentFixture<ManifestChangeKoliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestChangeKoliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestChangeKoliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
