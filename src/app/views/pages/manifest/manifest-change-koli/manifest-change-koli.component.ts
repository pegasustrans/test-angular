import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import notify from 'devextreme/ui/notify';

@Component({
	selector: 'kt-manifest-change-koli',
	templateUrl: './manifest-change-koli.component.html',
	styleUrls: ['./manifest-change-koli.component.scss']
})
export class ManifestChangeKoliComponent implements OnInit {

	@Input() manifestId: number;
	@Input() manifestKoliSpbId: number;
	@Output() popupVisible = new EventEmitter();

	manifestDdbDs: any;
	manifestKoliId: number;

	formUpdate = {
		nextManifestKoliId : null,
		manifestKoliSpbId: null
	}

	constructor(private http: HttpClient) { }

	ngOnInit() {
		this.readManifestDdbDs();
	}

	readManifestDdbDs() {
		this.manifestDdbDs = new DataSource({
			store: new CustomStore({
				key: 'manifestKoliId',
				loadMode: 'raw',
				load: (loadOptions) => {

					const httpParams = new HttpParams()
						.set('manifestId', this.manifestId.toString());
					const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8'), params: httpParams };

					return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest/ddb-manifest-withtotalkoli', config)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	gridBox_displayExpr(item) {
		return item.manifestNo + ' [ Koli : ' + item.koliNo + ']';
	}

	submitUpdate(e) {
		this.formUpdate.nextManifestKoliId = this.manifestKoliId[0];
		this.formUpdate.manifestKoliSpbId = this.manifestKoliSpbId;

		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
			.post(AppGlobalVar.BASE_API_URL + 'manifest/change-kolino', JSON.stringify(this.formUpdate), config)
			.subscribe((data: any) => {
					if (data != null) {
						notify({
							message: 'Data telah terupdate..',
							type: 'success',
							displayTime: 5000,
							width: 400,
						});
						this.popupVisible.next();
					}
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				}
			);


	}

}
