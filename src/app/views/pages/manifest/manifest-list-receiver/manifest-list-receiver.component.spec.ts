import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestListReceiverComponent } from './manifest-list-receiver.component';

describe('ManifestListReceiverComponent', () => {
  let component: ManifestListReceiverComponent;
  let fixture: ComponentFixture<ManifestListReceiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestListReceiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestListReceiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
