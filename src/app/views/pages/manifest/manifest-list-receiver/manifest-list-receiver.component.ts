import {Component, HostListener, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import {ManifestPrintComponent} from '../manifest-print/manifest-print.component';
import {SpbPrintComponent} from '../../spb/spb-print/spb-print.component';
import {SpbPrintV2Component} from '../../spb/spb-print-v2/spb-print-v2.component';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-manifest-list-receiver',
  templateUrl: './manifest-list-receiver.component.html',
  styleUrls: ['./manifest-list-receiver.component.scss']
})
export class ManifestListReceiverComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;
	popupVisible = false;

	ds: any = {};

	spbNoList = '';

	public innerWidth: any = window.innerWidth;
	public innerHeight: any = window.innerHeight;

	menuPriv = false;
	menuForb = true;

	printPopupVisible = false;
	printPrivResult: any;
	isiPrint : any;
	dataPrint : any;

	choosePrint = {
		isiPrint: null
	};

	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.innerWidth = window.innerWidth;
		this.innerHeight = window.innerHeight;
	}

	HistoryForm = {
		spbNo		: null,
		typePrint	: null,
		menu: "Manifest List Receiver",
		menuAction: "",
		printed: true,
		desc: null,
		historyloginId:0
	};

	formSearch = {
		manifestDate : new Date(),
	};

  constructor(private http: HttpClient, private matDialog: MatDialog) { }
  
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};
	ngOnInit() {
		this.historyLoginStart();
		this.checkuserGlobalVar();
	}

	onSearch(){
		this.GetList();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyPrint(spbNo, typePrint){
		this.HistoryForm.spbNo 		= spbNo.toString();
		this.HistoryForm.typePrint 	= typePrint;

		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-print/create', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "manifest-list-receiver"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;
			
			}
		}

		this.printPrivCheck();
	}

	printPrivCheck(){
		let printPrivResult: string[] = [];

		const params = {};
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'distribusi-print/list', params, config)
		.subscribe((data: any) => {

			// console.log(data);
			data.data.forEach(function(value){
				printPrivResult.push(value.printValue);
			});

			this.printPrivResult = printPrivResult;
		},
		(error) => {
			alert(error.statusText + '. ' + error.message);
		}
		);
		
	}

	GetList() {
		// const params = {
		// 	//createdAt: this.formSearch.dateString
		// }; 
		// this.ds = new CustomStore({
		// 	key: 's.manifestId',
		// 	load:  (loadOptions: any) => {
		// 		return this.http.get(AppGlobalVar.BASE_API_URL + 'manifestreceiver/list', { headers: this.httpOptions.headers, params  })
		// 			.toPromise()
		// 			.then((data: any) => {
		// 				return {
		// 					data: data.data
		// 				};
		// 			})
		// 			.catch(error => { console.log(error); });
		// 	}
		// });

		const params = {
			createdAt: this.formSearch.manifestDate.toDateString()
			, type : 'receiver'
		}; 
		this.ds = new CustomStore({
			key: 'manifestId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring/list-manifest', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						// console.log(data.data);
						
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	//
	// column setting
	ColManifestNo(rowData) {
		return rowData.destinationCityCode + ' ' + rowData.alphabet + ' # ' + rowData.manifestId;
	}

	//
	// action

	Print2 = (e) => {
		this.Print(e);
	}


	Print(data) {
		this.HistoryForm.menuAction = "Print Manifest";
		this.historyPrint(data.manifestId, 'manifest');

		const dialogConfig = new MatDialogConfig();
		dialogConfig.data = data;
		dialogConfig.minWidth = this.innerWidth + 'px';
		dialogConfig.minHeight = this.innerHeight + 'px';
		let dialogRef = this.matDialog.open(ManifestPrintComponent, dialogConfig);

		dialogRef.afterClosed().subscribe(value => {
			// console.log(`Dialog terima nehhh: ${value}`);
		});
	}
	

	printSpb = (e, isiPrint, choosePrint) => {
		this.printSpbGet(e, isiPrint, choosePrint);
	}

	actionCLick = (e) => {
		this.dataPrint = null;
		if (e.itemData.key === 'print_manifest') {
			this.Print2(e.itemData.data)
		}else if (e.itemData.key === 'print_spb') {
			// this.printSpb(e.itemData.data);
			this.isiPrint = e.itemData.data;
			if (this.printPrivResult != null){
				if (this.printPrivResult.length < 1){
					notify({
						message: 'Anda tidak punya akses untuk print SPB',
						type: 'warning',
						displayTime: 5000,
						width: 800
					});
				}

				else{
					this.dataPrint = e.itemData.data;
					this.printPopupVisible = true;
				}	
			}
		}
	}

	PrintResult(e){
		
		if (this.choosePrint.isiPrint == null || this.choosePrint.isiPrint == ''){
			alert("Silahkan pilih jenis print terlebih dahulu");
		}
		else{
			this.printPopupVisible = false;
			// this.PrintSpbV2(this.isiPrint, this.choosePrint.isiPrint);
			// console.log(this.isiPrint, this.choosePrint.isiPrint);
			this.printSpb(this.dataPrint, this.isiPrint, this.choosePrint.isiPrint);
		}
	}

	// PrintSpbV2(data, teks) {
	// 	this.HistoryForm.desc = teks;
	// 	this.historyPrint();

	// 	const dialogConfig = new MatDialogConfig();
	// 	// dialogConfig.data = data.spbNo;
	// 	dialogConfig.data = {
	// 		urlReport: this.appGlobalVar.BASE_REPORT_URL + 'spb-receiver/',
	// 		manifestId: data.spbId,
	// 		workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
	// 		workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
	// 		username: this.userGlobalVar.USERNAME,
	// 		teks: teks
	// 	};
	// 	dialogConfig.minWidth = this.innerWidth + 'px';
	// 	dialogConfig.minHeight = this.innerHeight + 'px';
	// 	let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);

	// 	dialogRef.afterClosed().subscribe(value => {
	// 		console.log(`Dialog sent: ${value}`);
	// 	});
	// }

	printSpbGet(e, isiPrint, choosePrint) {
		this.HistoryForm.menuAction = "Print SPB";
		// get all spb no by manifest id
		this.http
			.get(AppGlobalVar.BASE_API_URL + 'manifestreceiver/list-spb?manifestId=' + e.manifestId)
			.subscribe((data: any) => {
					if (data != null ) {
						if (data.data.length > 0) {
							// this.spbNoList = data.data;
							this.historyPrint(data.data,'spb');
							this.PrintSpbBulkV2(isiPrint, choosePrint);
						} else {
							alert('data SPB berada pada manifest yang lain.');
						}
					}
				},
				(error) => {

				}
			);
	}

	PrintSpbBulk() {
		// const dialogConfig = new MatDialogConfig();
		// // dialogConfig.data = this.spbNoList;
		// dialogConfig.data = {
		// 	spbNo: this.spbNoList,
		// 	urlSpb: 'manifestreceiver/spb-customer-print',
		// 	urlSpbDetail: 'spbcreate/read-spb-goods-print',
		// };
		// dialogConfig.minWidth = this.innerWidth + 'px';
		// dialogConfig.minHeight = this.innerHeight + 'px';
		// let dialogRef = this.matDialog.open(SpbPrintComponent, dialogConfig);
		// // let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);
		//
		// dialogRef.afterClosed().subscribe(value => {
		// 	console.log(`Dialog sent: ${value}`);
		// });
	}

	PrintSpbBulkV2(data, teks) {
		// console.log(data, teks);
		
		const dialogConfig = new MatDialogConfig();
		// dialogConfig.data = this.spbNoList;
		dialogConfig.data = {
			urlReport: this.appGlobalVar.BASE_REPORT_URL + 'manifest-receiver/',
			manifestId: data.manifestId,
			workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
			workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
			username: this.userGlobalVar.USERNAME,
			teks: teks + '#toolbar=0'  // Pati 26 Jan 22
			//teks: teks
		};
		dialogConfig.minWidth = this.innerWidth + 'px';
		dialogConfig.minHeight = this.innerHeight + 'px';
		let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);

		dialogRef.afterClosed().subscribe(value => {
			console.log(`Dialog sent: ${value}`);
		});
	}

}
