import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestPrintComponent } from './manifest-print.component';

describe('ManifestPrintComponent', () => {
  let component: ManifestPrintComponent;
  let fixture: ComponentFixture<ManifestPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
