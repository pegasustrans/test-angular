import {
	AfterContentChecked,
	ChangeDetectionStrategy,
	Component,
	EventEmitter,
	HostListener,
	Inject,
	OnDestroy,
	OnInit,
	Output,
	Pipe,
	PipeTransform
} from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';

import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import printJS from 'print-js';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subject} from 'rxjs';
import {ManifestService} from '../manifest.service';
import {takeUntil} from 'rxjs/operators';
import * as Enumerable from 'linq';
import notify from 'devextreme/ui/notify';


// import * as $ from 'jquery';

// https://stackblitz.com/edit/angular-printing-solution-example-3?file=src%2Fstyles.css
// https://codesandbox.io/s/wq7pplxznl



@Component({
	selector: 'kt-manifest-print',
	templateUrl: './manifest-print.component.html',
	styleUrls: ['./manifest-print.component.scss'],
	// changeDetection: ChangeDetectionStrategy.OnPush // this line
})
export class ManifestPrintComponent implements OnInit , OnDestroy  {

	sumTunaiAsal = 0;
	// sumTunaiAsalSubs: Subject<any> = new Subject<any>();

	sumTunaiTujuan = 0;
	// sumTunaiTujuanSubs: Subject<any> = new Subject<any>();


	popupVisible = false;
	popupOpenClose = false;
	dtOptions: any = {};
	itemData: any = {};
	manifestList;
	dtTrigger: Subject<any> = new Subject();

	manifestData: any;
	manifestId = 0;
	manifestKoliSpbId = 0;
	sumAw = 0;
	sumCaw = 0;
	sumCawBDJ = 0;
	sumCawBJU = 0;
	sumCawMTP = 0;
	sumCawSRI = 0;
	sumCawBNT = 0;
	sumCawSGT = 0;
	// sumTa = 0;
	// sumTt = 0;
	prevKoliNo = 1;
	sameWithPrevKoli = false;
	totalTunaiAsal = 0;


	httpOptions = {
		headers: new HttpHeaders({
		'Content-Type': 'application/json'
		})
  	};

	  manifestCarrierDs: any;

	constructor(public dialogRef: MatDialogRef<ManifestPrintComponent>,
				private manifestService: ManifestService,
				@Inject(MAT_DIALOG_DATA) public data: any, private http: HttpClient, ) { }

	ngOnInit() {

		this.manifestData = this.data;
		this.manifestId = this.data.manifestId;
		this.DtOptions();
		this.GetManifestDetail();
		this.getManifestCarrier();
	}

	getManifestCarrier(){
		const body = {
			ManifestId : this.manifestId
		};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'manifest-carrier/get-detail',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.manifestCarrierDs = data.data;
						
						// this.manifestData.st_cntData 				= this.manifestCarrierDs[0].cntData;
						// this.manifestData.st_flightNo 				= this.manifestCarrierDs[0].carrierNo;
						// this.manifestData.st_noSmu 					= this.manifestCarrierDs[0].noSmu;
						// this.manifestData.st_totalFinalWeightSMU 	= this.manifestCarrierDs[0].totalFinalWeightSMU;
						// this.manifestData.st_flightDate 			= this.manifestCarrierDs[0].flightDate;
						// this.manifestData.st_firstFlight 			= this.manifestCarrierDs[0].firstFlight;
						// this.manifestData.st_lastFlight 			= this.manifestCarrierDs[0].lastFlight;

						// this.manifestData.nd_cntData 				= this.manifestCarrierDs[1].cntData;
						// this.manifestData.nd_flightNo 				= this.manifestCarrierDs[1].carrierNo;
						// this.manifestData.nd_noSmu 					= this.manifestCarrierDs[1].noSmu;
						// this.manifestData.nd_totalFinalWeightSMU 	= this.manifestCarrierDs[1].totalFinalWeightSMU;
						// this.manifestData.nd_flightDate 			= this.manifestCarrierDs[1].flightDate;
						// this.manifestData.nd_firstFlight 			= this.manifestCarrierDs[1].firstFlight;
						// this.manifestData.nd_lastFlight 			= this.manifestCarrierDs[1].lastFlight;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);
	}



	close() {
		this.dialogRef.close('Thanks for using me!');
	}

	DtOptions() {
		this.dtOptions = {
			// Declare the use of the extension in the dom parameter
			dom: 'Bfrtip',
			rowGroup: [0],
			ordering: false,
			paging: false,
			// Configure the buttons
			buttons: [
				'copy',
				{
					extend: 'print',
					title: '',
					messageTop:  () => {
						return '<table border="0" width="100%">\n' +
							'<tr>\n' +
							'<td width="20%" >No. Manifest : ' + this.manifestData.manifestId + ' <br/> Date: ' + this.manifestData.createdAtLocal + ' </td>\n' +
							'<td style="text-align: center; font-size: 20px; font-weight: bold">Manifest <br/> ' + this.manifestData.destinationCityCode + ' ' + this.manifestData.alphabet + ' </td>\n' +
							'<td width="20%" >SMU : '+ this.manifestData.noSmu+'<br/> No. Flight : '+ this.manifestData.flightNo+'</td>\n' +
							'</tr>\n' +
							'\n' +
							'</table>';
					},
					messageBottom: null,
					customize: ( win ) => {
						// $(win.document.body)
						// 	.css( 'font-size', '10pt' )
						// 	.prepend(
						// 		'<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
						// 	);

						$(win.document.body)
							.css( 'font-size', '10pt' )
							.prepend();

						$(win.document.body).find( 'table' )
							.addClass( 'cell-border compact stripe' )
							.css( 'font-size', 'inherit' );

					}
				},
				{
					extend: 'excelHtml5',
					title: '',
					messageTop:  () => {
						return '';
					},
				},
			]
		};
	}



	printPage() {
		window.print();
	}


	GetManifestDetail() {

		this.http.get(AppGlobalVar.BASE_API_URL + 'manifestsender/list-detail?manifestId=' + this.manifestId)
			.toPromise()
			.then((data: any) => {
				this.manifestList = data.data;
				this.dtTrigger.next();
				console.log(this.manifestList);	
				Enumerable.from(this.manifestList).forEach(
					(obj: any) => {
						
						this.sumAw = this.sumAw + obj.aw;
						this.sumCaw = this.sumCaw + obj.caw;
						this.sumCawBDJ = this.sumCawBDJ + obj.cawBDJ;
						this.sumCawBJU = this.sumCawBJU + obj.cawBJU;
						this.sumCawMTP = this.sumCawMTP + obj.cawMTP;
						this.sumCawSRI = this.sumCawSRI + obj.cawSRI;
						this.sumCawBNT = this.sumCawBNT + obj.cawBNT;
						this.sumCawSGT = this.sumCawSGT + obj.cawSGT;
						if (obj.isagree === 1) {obj.tunaiAsal = 0}
						if (obj.isagree === 1) {obj.tunaiTujuan = 0}
						this.sumTunaiAsal = this.sumTunaiAsal + obj.tunaiAsal;
						this.sumTunaiTujuan = this.sumTunaiTujuan + obj.tunaiTujuan;
					}
				);

			})
			.catch(error => { console.log(error); });
	}

	CompareWithPrevKoli(currentKoli = 0) {
		return true;
		if (this.prevKoliNo !== currentKoli) {
			this.prevKoliNo = currentKoli;
			// this.sameWithPrevKoli =  true;
			return true;
		} else {
			// this.sameWithPrevKoli = false;
			return 'nnnn';
		}
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		// this.dtTrigger.unsubscribe();
		// this.sumTunaiAsalSubs.next();
		// this.sumTunaiAsalSubs.complete();
		// this.sumTunaiAsalSubs.unsubscribe();
		//
		// this.sumTunaiTujuanSubs.next();
		// this.sumTunaiTujuanSubs.complete();
		// this.sumTunaiTujuanSubs.unsubscribe();
	}


	Update = (e, row) => {
		this.manifestKoliSpbId = row.manifestKoliSpbId;
		this.popupVisible = true;
	}

	setPopupVisible = () => {
		this.popupVisible = !this.popupVisible;
		// this.GetManifestDetail();
	}

	actionItem(i, row) {

		if (i === 0) {
			return [
				{icon: 'refresh' , key: 'change_koli', value: 'Pindah Koli', data: row},
				{icon: 'close' , key: 'close_koli', value: 'Open / Close', data: row}
			];
		}

		if (i !== 0 && row.koliNo !== this.manifestList[ i - 1 ].koliNo) {
			return [
				{icon: 'refresh' , key: 'change_koli', value: 'Pindah Koli', data: row},
				{icon: 'close' , key: 'close_koli', value: 'Open / Close', data: row}
			];
		}

		return [
			{icon: 'refresh' , key: 'change_koli', value: 'Pindah Koli', data: row}
		];
	}

	actionCLick(e) {
		this.itemData = e.itemData;
		if (e.itemData.key === 'change_koli') { this.Update(e, e.itemData.data); }
		if (e.itemData.key === 'close_koli') { this.popupOpenClose = true; }
	}

	setOpenCloseKoli(action) {
		const form = {
			action,
			manifestKoliId: this.itemData.data.manifestKoliId
		};

		// console.log(this.itemData.data.manifestKoliId);
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
			.post(AppGlobalVar.BASE_API_URL + 'manifest/openclosekoli', JSON.stringify(form), config)
			.subscribe((result: any) => {
					if (result != null) {
						notify({
							message: 'Berhasil..',
							type: 'success',
							displayTime: 5000,
							width: 400,
						});
						// this.ds.load();
						this.popupOpenClose = false;
					}
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				}
			);
	}

}

