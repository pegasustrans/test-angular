import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestSenderComponent } from './manifest-sender.component';

describe('ManifestSenderComponent', () => {
  let component: ManifestSenderComponent;
  let fixture: ComponentFixture<ManifestSenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestSenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestSenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
