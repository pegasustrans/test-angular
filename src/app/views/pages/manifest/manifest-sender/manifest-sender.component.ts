import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnInit} from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {MatDialog, MatDialogConfig } from '@angular/material/dialog';
import {ManifestPrintComponent} from '../manifest-print/manifest-print.component';
import {SpbPrintComponent} from '../../spb/spb-print/spb-print.component';
import {SpbPrintV2Component} from '../../spb/spb-print-v2/spb-print-v2.component';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
	selector: 'kt-manifest-sender',
	templateUrl: './manifest-sender.component.html',
	styleUrls: ['./manifest-sender.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManifestSenderComponent implements OnInit {

	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	constructor(private http: HttpClient, private matDialog: MatDialog, private cd: ChangeDetectorRef) { }

	HistoryForm = {
		historyloginId:0,
		spbNo		: null,
		typePrint	: null,
		menu: "Manifest List Sender",
		menuAction: "",
		printed: true,
		desc: null
	};

	appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;
	popupVisible = false;
	rowData : any;

	ds: any = {};

	spbNoList = '';

	menuPriv = true;
	menuForb = false;
	
	printPopupVisible = false;
	printPrivResult: any;
	isiPrint : any;
	dataPrint : any;

	choosePrint = {
		isiPrint: null
	};

	public innerWidth: any = window.innerWidth;
	public innerHeight: any = window.innerHeight;
	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.innerWidth = window.innerWidth;
		this.innerHeight = window.innerHeight;
	}

	formSearch = {
		manifestDate : new Date(),
	};

	ngOnInit() {
		this.checkuserGlobalVar();
		this.historyLoginStart();
	}

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	onSearch(){
		this.GetList();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyPrint(spbNo, typePrint){
		this.HistoryForm.spbNo 		= spbNo.toString();
		this.HistoryForm.typePrint 	= typePrint;

		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-print/create', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	private async checkuserGlobalVar() {
		if(await UserGlobalVar.USER_ID == '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				// console.log(value);
				if (value == "manifest-list-sender"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv != 1){
				this.menuPriv = false;
				this.menuForb = true;
			
			}
		}
		this.printPrivCheck();
	}	

	printPrivCheck(){
		let printPrivResult: string[] = [];

		const params = {};
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'distribusi-print/list', params, config)
		.subscribe((data: any) => {

			// console.log(data);
			data.data.forEach(function(value){
				printPrivResult.push(value.printValue);
			});

			this.printPrivResult = printPrivResult;
		},
		(error) => {
			alert(error.statusText + '. ' + error.message);
		}
		);
		
	}

	GetList() {
		// const params = {
		// 	//createdAt: this.formSearch.dateString
		// }; 
		
		// this.ds = new CustomStore({
		// 	key: 's.manifestId',
		// 	load:  (loadOptions: any) => {
		// 		return this.http.get(AppGlobalVar.BASE_API_URL + 'manifestsender/list', { headers: this.httpOptions.headers, params  })
		// 			.toPromise()
		// 			.then((data: any) => {
		// 				return {
		// 					data: data.data
		// 				};
		// 			})
		// 			.catch(error => { console.log(error); });
		// 	}
		// });

		const params = {
			createdAt: this.formSearch.manifestDate.toDateString()
			, type : 'sender'
		}; 
		this.ds = new CustomStore({
			key: 'manifestId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring/list-manifest', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						console.log(data.data);
						
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	//
	// column setting
	ColManifestNo(rowData) {
		return rowData.destinationCityCode + ' ' + rowData.alphabet + ' # ' + rowData.manifestId;
	}

	onRowPrepared(e) {
		if (e.rowType === 'data' && e.data.isOpen === false) {
			e.rowElement.style.backgroundColor = '#c4c3c3';
			e.rowElement.className = e.rowElement.className.replace('dx-row-alt', '');
		}
	}

	openOrCLose(rowData) {
		if (rowData.isOpen === true) { return 'Open'; }
		if (rowData.isOpen === false) { return 'CLose'; }
		if (rowData.isOpen === null) { return ''; }
	}

	//
	// action

	actionCLick(e) {
		this.rowData = e.itemData.data;
		if (e.itemData.key === 'print_manifest') { this.Print2(e.itemData.data); }
		if (e.itemData.key === 'print_spb') { 
			// this.printSpb(e.itemData.data); 
			this.isiPrint = e.itemData.data;
			if (this.printPrivResult != null){
				if (this.printPrivResult.length < 1){
					notify({
						message: 'Anda tidak punya akses untuk print SPB',
						type: 'warning',
						displayTime: 5000,
						width: 800
					});
				}

				else{
					this.dataPrint = e.itemData.data;
					this.printPopupVisible = true;
				}	
			}
		}
		if (e.itemData.key === 'close_manifest') { this.popupVisible = true; }
	}

	Print2 = (data) => {
		// console.log(data);
		this.Print(data);
	}

	PrintResult(e){
		
		if (this.choosePrint.isiPrint == null || this.choosePrint.isiPrint == ''){
			alert("Silahkan pilih jenis print terlebih dahulu");
		}
		else{
			this.printPopupVisible = false;
			// this.PrintSpbV2(this.isiPrint, this.choosePrint.isiPrint);
			// console.log(this.isiPrint, this.choosePrint.isiPrint);
			this.printSpb(this.dataPrint, this.isiPrint, this.choosePrint.isiPrint);
		}
	}

	Print(data) {
		this.HistoryForm.menuAction = "Print Manifest";
			
		this.historyPrint(data.manifestId, 'manifest');

		const dialogConfig = new MatDialogConfig();
		dialogConfig.data = data;
		dialogConfig.minWidth = this.innerWidth + 'px';
		dialogConfig.minHeight = this.innerHeight + 'px';
		let dialogRef = this.matDialog.open(ManifestPrintComponent, dialogConfig);


		dialogRef.afterClosed().subscribe(value => {
			// console.log(`Dialog terima nehhh: ${value}`);
		});
	}

	// printSpb = (e) => {
	// 	this.printSpbGet(e);
	// }
	printSpb = (data, isiPrint, choosePrint) => {
		this.printSpbGet(data, isiPrint, choosePrint);
	}


	printSpbGet(data, isiPrint, choosePrint) {
		
		
		// get all spb no by manifest id
		this.http
			.get(AppGlobalVar.BASE_API_URL + 'manifestsender/list-spb?manifestId=' + data.manifestId)
			.subscribe((data: any) => {
					if (data != null ) {
						if (data.data.length > 0) {
							// this.spbNoList = data.data;
							this.PrintSpbBulkV2(data, isiPrint, choosePrint);
						} else {
							alert('data SPB berada pada manifest yang lain.');
						}
					}
				},
				(error) => {

				}
			);


	}

	PrintSpbBulk() {
		const dialogConfig = new MatDialogConfig();
		// dialogConfig.data = this.spbNoList;
		dialogConfig.data = {
			spbNo: this.spbNoList,
			urlSpb: 'manifestsender/spb-customer-print',
			urlSpbDetail: 'spbcreate/read-spb-goods-print',
		};
		dialogConfig.minWidth = this.innerWidth + 'px';
		dialogConfig.minHeight = this.innerHeight + 'px';
		// let dialogRef = this.matDialog.open(SpbPrintComponent, dialogConfig);
		let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);

		dialogRef.afterClosed().subscribe(value => {
			console.log(`Dialog sent: ${value}`);
		});
	}

	PrintSpbBulkV2(data, isiPrint, choosePrint) {
		this.HistoryForm.menuAction = "Print SPB";
		
		// data.data.split(",", 1000000).forEach(function(value){
		// 	console.log(value.toString());
			
			this.historyPrint(data.data,'spb');
			// this.historyPrint(value, 'spb');
		// });

		const dialogConfig = new MatDialogConfig();
		// dialogConfig.data = this.spbNoList;
		dialogConfig.data = {
			urlReport: this.appGlobalVar.BASE_REPORT_URL + 'manifest-sender/',
			manifestId: isiPrint.manifestId,
			workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
			workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
			username: this.userGlobalVar.USERNAME,
			teks: choosePrint + '#toolbar=0'  // Pati 26 Jan 22
			//teks: choosePrint
		};
		dialogConfig.minWidth = this.innerWidth + 'px';
		dialogConfig.minHeight = this.innerHeight + 'px';
		let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);

		dialogRef.afterClosed().subscribe(value => {
			console.log(`Dialog sent: ${value}`);
		});
	}

	setOpen() {

	}

	setOpenClose(action) {
		const form = {
			action,
			manifestId: this.rowData.manifestId
		};

		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
			.post(AppGlobalVar.BASE_API_URL + 'manifest/openclose', JSON.stringify(form), config)
			.subscribe((result: any) => {
					if (result != null) {
						notify({
							message: 'Berhasil..',
							type: 'success',
							displayTime: 5000,
							width: 400,
						});
						// this.ds.load();
						this.popupVisible = false;
						this.GetList();
						this.cd.detectChanges();
					}
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				}
			);
	}

}
