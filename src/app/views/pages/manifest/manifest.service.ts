import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManifestService {

  constructor() { }

	//
	// sum tunai asal
	private sumTunaiAsalSubs = new BehaviorSubject(0);
	sumTunaiAsal = this.sumTunaiAsalSubs.asObservable();
	TunaiAsalReset() { this.sumTunaiAsalSubs.next(0); }
	TunaiAsalSet(message: number) {
		const latestValue = this.sumTunaiAsalSubs.getValue();
		this.sumTunaiAsalSubs.next(latestValue + message);
	}

	//
	// sum tunai TUJUAN
	private sumTunaiTujuanSubs = new BehaviorSubject(0);
	sumTunaiTujuan = this.sumTunaiTujuanSubs.asObservable();
	TunaiTujuanReset() { this.sumTunaiTujuanSubs.next(0); }
	TunaiTujuanSet(message: number) {
		const latestValue = this.sumTunaiTujuanSubs.getValue();
		this.sumTunaiTujuanSubs.next(latestValue + message);
	}
}
