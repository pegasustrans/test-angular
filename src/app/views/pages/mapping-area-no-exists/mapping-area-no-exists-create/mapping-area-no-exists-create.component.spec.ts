import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingAreaNoExistsCreateComponent } from './mapping-area-no-exists-create.component';

describe('MappingAreaNoExistsCreateComponent', () => {
  let component: MappingAreaNoExistsCreateComponent;
  let fixture: ComponentFixture<MappingAreaNoExistsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingAreaNoExistsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingAreaNoExistsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
