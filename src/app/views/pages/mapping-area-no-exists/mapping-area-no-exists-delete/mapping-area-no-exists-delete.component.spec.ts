import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingAreaNoExistsDeleteComponent } from './mapping-area-no-exists-delete.component';

describe('MappingAreaNoExistsDeleteComponent', () => {
  let component: MappingAreaNoExistsDeleteComponent;
  let fixture: ComponentFixture<MappingAreaNoExistsDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingAreaNoExistsDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingAreaNoExistsDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
