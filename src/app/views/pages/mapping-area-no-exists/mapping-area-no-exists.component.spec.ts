import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingAreaNoExistsComponent } from './mapping-area-no-exists.component';

describe('MappingAreaNoExistsComponent', () => {
  let component: MappingAreaNoExistsComponent;
  let fixture: ComponentFixture<MappingAreaNoExistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingAreaNoExistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingAreaNoExistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
