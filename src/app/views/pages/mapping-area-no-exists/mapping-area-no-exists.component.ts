import { Component, HostListener, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import { Router } from '@angular/router';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';


@Component({
  selector: 'kt-mapping-area-no-exists',
  templateUrl: './mapping-area-no-exists.component.html',
  styleUrls: ['./mapping-area-no-exists.component.scss']
})
export class MappingAreaNoExistsComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

  appGlobalVar = AppGlobalVar;

	dsList: any = {};
	menuPriv = false;
	menuForb = true;

	HistoryForm = {
		menu: "Master - Mapping Area No Exist",
		historyloginId:0
	};

	constructor(private http: HttpClient, private router: Router) { }

	ngOnInit() {
		this.historyLoginStart();
		this.checkuserGlobalVar();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
		  setTimeout(() => {
			this.checkuserGlobalVar();
		  }, 1000);
		}
	  
		else{		
		  var checkPriv = 0;
		  await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "mapping-area-no-exists"){
			  checkPriv = 1;
			}
		  });
	  
		  if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
			
			  this.GetList();
		  }
		}
	  }

	GetList() {
		this.dsList = new CustomStore({
			key: 'mappingAreaNoExistsId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'master-mapping-area-no-exists/list-mapping-area-no-exist')
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}


	addCost(){
		this.router.navigate(['/mapping-area-no-exists-create']);
	}

	actionCLick = (e) => {
    if (e.itemData.key === 'delete') {
        this.router.navigate(['/mapping-area-no-exists-delete'], 
        { 
          queryParams: { 
            p1: e.itemData.data.mappingAreaNoExistsId,
            p2: e.itemData.data.areaName,
            p3: e.itemData.data.cityName,
            p4: e.itemData.data.subDistrictName
          } 
        });
      }
	}


}
