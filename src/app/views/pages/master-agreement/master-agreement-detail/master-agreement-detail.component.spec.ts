import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterAgreementDetailComponent } from './master-agreement-detail.component';

describe('MasterAgreementDetailComponent', () => {
  let component: MasterAgreementDetailComponent;
  let fixture: ComponentFixture<MasterAgreementDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterAgreementDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterAgreementDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
