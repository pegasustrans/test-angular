import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';


@Component({
  selector: 'kt-master-agreement-detail',
  templateUrl: './master-agreement-detail.component.html',
  styleUrls: ['./master-agreement-detail.component.scss']
})
export class MasterAgreementDetailComponent implements OnInit {
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  subBranchId         : any;  
  subBranchCode       : any;
  subBranchName       : any;
  subBranchAddress    : any;
  isActive            : any;
  isAgree             : any;
  selectionId         : any;

  statusDs: any;
  dataItems = [];

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.subBranchId      = params.p1;
        this.subBranchCode    = params.p2;
        this.subBranchName    = params.p3;
        this.subBranchAddress = params.p4;
        this.isAgree          = params.p5;
        this.isActive         = params.p6;

        this.readStatus();
      }
    );
  }

  readStatus() {
    this.dataItems = [];
    const params = { type : 'status_agreement' };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'master-agreement/list-status', params, config)
      .subscribe((data: any) => {
        
        data.data.forEach((item) => {
          this.dataItems.push(item);
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
    
    this.selectionId = this.isAgree;
	}

  onValueChanged($event){
    const params = { 
      isAgree       : parseInt($event.value)
      , subBranchId : parseInt(this.subBranchId) 
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'master-agreement/update-status', params, config)
      .subscribe((data: any) => {
        
        notify({
          message: 'Data telah Update',
          type: 'success',
          displayTime: 5000,
          width: 400,
        });       

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    ); 
  }

  onBack(){
    this.router.navigate(['master-agreement']);
  }

}
