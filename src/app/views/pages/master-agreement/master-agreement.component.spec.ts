import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterAgreementComponent } from './master-agreement.component';

describe('MasterAgreementComponent', () => {
  let component: MasterAgreementComponent;
  let fixture: ComponentFixture<MasterAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
