import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';;
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-master-agreement',
  templateUrl: './master-agreement.component.html',
  styleUrls: ['./master-agreement.component.scss']
})
export class MasterAgreementComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

  ds: any = {};
  menuPriv = false;
  menuForb = true;

  HistoryForm = {
	menu: "Master - As Agreed/Kesepakatan",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.checkuserGlobalVar();
	this.GetList();
	this.historyLoginStart();
  }

  private async checkuserGlobalVar() {
	if(UserGlobalVar.USER_ID === '') {
		setTimeout(() => {
			this.checkuserGlobalVar();
		}, 1000);
	}

	else{		
		var checkPriv = 0;
		await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-agreement"){
				checkPriv = 1;
			}
		});

		if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
		}
	}
}


ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

  GetList() {
		const params = {
			//createdAt: this.formSearch.dateString
		}; 
		this.ds = new CustomStore({
			key: 'subBranchId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'master-agreement/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  detail = (e) => {
		this.router.navigate(['/master-agreement-detail'], 
        { 
            queryParams: { 
               p1: e.row.data.subBranchId,
               p2: e.row.data.subBranchCode,
               p3: e.row.data.subBranchName,
               p4: e.row.data.subBranchAddress,
               p5: e.row.data.isAgree,
               p6: e.row.data.isActive
            } 
        });
	}


}
