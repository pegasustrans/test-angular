import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCityCreateComponent } from './master-city-create.component';

describe('MasterCityCreateComponent', () => {
  let component: MasterCityCreateComponent;
  let fixture: ComponentFixture<MasterCityCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCityCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCityCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
