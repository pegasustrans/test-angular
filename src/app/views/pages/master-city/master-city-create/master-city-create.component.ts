import { Component, HostListener, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-master-city-create',
  templateUrl: './master-city-create.component.html',
  styleUrls: ['./master-city-create.component.scss']
})

export class MasterCityCreateComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }
  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  provinceDs: any;
  typeKotaDs: any;

  formCreate = {
		province  : null,
		typeKota  : null,
    kodeKota  : null,
		namaKota  : null,
		ibuKota   : null,
	};

  

  
  HistoryForm = {
		menu: "Master - Kota - Create",
		historyloginId:0
	};

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  ngOnInit() {
    this.readDataProvince();
    this.readDataTypeKota();
    this.historyLoginStart();
  }

  readDataProvince() {
    const params = {};

		this.provinceDs = new DataSource({
			store: new CustomStore({
				key: 'id',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'master-city/list-province', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  readDataTypeKota() {
    const params = {};

		this.typeKotaDs = new DataSource({
			store: new CustomStore({
				key: 'id',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'master-city/list-typeKota', { headers: this.httpOptions.headers, params  })
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

  onSave(){
  
      const params = {
        masterProvinceId : parseInt(this.formCreate.province),
        masterCityType : this.formCreate.typeKota,
        masterCityCode : this.formCreate.kodeKota,
        masterCityName : this.formCreate.namaKota,
        masterCityCapital  : this.formCreate.ibuKota
      };

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
        .post(AppGlobalVar.BASE_API_URL + 'master-city/create-city', params, config)
        .subscribe((data: any) => {
          console.log(data.data);

          this.router.navigate(['/master-city']);

          notify({
            message: 'Data telah di Tambahkan',
            type: 'success',
            displayTime: 5000,
            width: 400,
          });

        },
          (error) => {
            alert(error.statusText + '. ' + error.message);
          }
        );
    // }

  }

  onBack(){
    this.router.navigate(['/master-city']);
  }
  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

}
