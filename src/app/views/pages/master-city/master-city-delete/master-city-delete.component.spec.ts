import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCityDeleteComponent } from './master-city-delete.component';

describe('MasterCityDeleteComponent', () => {
  let component: MasterCityDeleteComponent;
  let fixture: ComponentFixture<MasterCityDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCityDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCityDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
