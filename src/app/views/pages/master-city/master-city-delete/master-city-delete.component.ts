import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-master-city-delete',
  templateUrl: './master-city-delete.component.html',
  styleUrls: ['./master-city-delete.component.scss']
})
export class MasterCityDeleteComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

  masterCityCapital  : any;
  masterCityCode     : any;
  masterCityId       : any;
  masterCityName     : any;
  masterCityType     : any;
  masterProvinceId   : any;
  masterProvinceName : any;


  HistoryForm = {
		menu: "Master - Kota - Delete",
		historyloginId:0
	};

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.masterCityCapital    = params.p1;         
        this.masterCityCode       = params.p2;         
        this.masterCityId         = params.p3;         
        this.masterCityName       = params.p4;         
        this.masterCityType       = params.p5;         
        this.masterProvinceId     = params.p6;       
        this.masterProvinceName   = params.p7;       
      }
      
    );
  }


  onDelete(){
  
    const params = {
      masterProvinceId  : parseInt(this.masterProvinceId),
      masterCityId      : this.masterCityId
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'master-city/delete-city', params, config)
      .subscribe((data: any) => {
        console.log(data.data);

        this.router.navigate(['/master-city']);

        notify({
          message: 'Data di Hapus',
          type: 'success',
          displayTime: 5000,
          width: 400,
        });

      },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
      );
  // }

}

onBack(){
  this.router.navigate(['/master-city']);
}

ngOnDestroy() { 
  this.historyLoginEnd();
}

historyLoginStart(){
  const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
  this.http
  .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
  .subscribe((data: any) => {
      this.HistoryForm.historyloginId = data.data;
    },
    (error) => {
    alert(error.statusText + '. ' + error.message);
    }
  );
}

historyLoginEnd(){
  const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
  this.http
  .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
  .subscribe((data: any) => {

    },
    (error) => {
    alert(error.statusText + '. ' + error.message);
    }
  );
}


}
