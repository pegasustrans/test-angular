import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';;
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-master-city',
  templateUrl: './master-city.component.html',
  styleUrls: ['./master-city.component.scss']
})


export class MasterCityComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

  ds: any = {};
  menuPriv = false;
  menuForb = true;

  editTarifPopupVisible 	= false;
  addTarifPopupVisible 		= false;
  hapusTarifPopupVisible 	= false;



  HistoryForm = {
	menu: "Master - Kota",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  ngOnInit() {
	this.checkuserGlobalVar();
	this.historyLoginStart();
    
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};



	ChooseAction = (e) => {
		// if(e.itemData.key == "update"){
		//    this.editTarif(e.itemData.data);
		// }
		if(e.itemData.key == "delete"){
			this.deleteCity (e.itemData.data);
		 }
	  }

	  deleteCity(e){
		this.router.navigate(['/master-city-delete'], 
		{ 
			queryParams: { 
			  p1: e.masterCityCapital,
			  p2: e.masterCityCode,
			  p3: e.masterCityId,
			  p4: e.masterCityName,
			  p5: e.masterCityType,
			  p6: e.masterProvinceId,
			  p7: e.masterProvinceName
			} 
		});
	 }

	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
		  setTimeout(() => {
			this.checkuserGlobalVar();
		  }, 1000);
		}
	  
		else{		
		  var checkPriv = 0;
		  await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-city"){
			  checkPriv = 1;
			}
		  });
	  
		  if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
			
			this.GetList();
		  }
		}
	  }

  GetList() {
		const params = {
			//createdAt: this.formSearch.dateString
		}; 
		this.ds = new CustomStore({
			key: 'masterCityId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'master-city/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	addCity(){
		this.router.navigate(['/master-city-create']);
	}

	// receiverOneRowSelected(){
	// 	this.IsReceiverDisabled = false;
	// 	this.receiverPopUp = false;
	// 	this.Receiver_ResetAll();
	// 	this.spbForm.patchValue({
	// 		receiverId: null,
	// 		receiver: null
	// 	});
	// 	this.ReceiverPhoneToNew();
	// 	if (this.spbForm.get('destCity').value != null){
	// 		this.ReceiverCity_Reload();
	// 		if (this.spbForm.get('destArea').value != null){
	// 			this.ReceiverSubDistrict_Reload();
	// 			this.ReceiverUrban_Reload();
	// 		}
	// 	}
	// }



	// detailMasterDataCustomer = (e) => {
	//   console.log(e.row.data.nomor);
	  
	// 	this.router.navigate(['/master-data-customer-detail'], 
  //       { 
  //           queryParams: { 
  //              p1: e.row.data.nomor
  //           } 
  //       });
	// }


	

}