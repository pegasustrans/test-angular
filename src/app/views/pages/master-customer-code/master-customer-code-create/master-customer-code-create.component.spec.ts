import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCustomerCodeCreateComponent } from './master-customer-code-create.component';

describe('MasterCustomerCodeCreateComponent', () => {
  let component: MasterCustomerCodeCreateComponent;
  let fixture: ComponentFixture<MasterCustomerCodeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCustomerCodeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCustomerCodeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
