import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCustomerCodeDeleteComponent } from './master-customer-code-delete.component';

describe('MasterCustomerCodeDeleteComponent', () => {
  let component: MasterCustomerCodeDeleteComponent;
  let fixture: ComponentFixture<MasterCustomerCodeDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCustomerCodeDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCustomerCodeDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
