import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCustomerCodeComponent } from './master-customer-code.component';

describe('MasterCustomerCodeComponent', () => {
  let component: MasterCustomerCodeComponent;
  let fixture: ComponentFixture<MasterCustomerCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCustomerCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCustomerCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
