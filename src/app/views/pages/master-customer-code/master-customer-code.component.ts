import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-master-customer-code',
  templateUrl: './master-customer-code.component.html',
  styleUrls: ['./master-customer-code.component.scss']
})
export class MasterCustomerCodeComponent implements OnInit {
  ds: any = {};
  cityDs          : any;
  menuPriv = false;
  menuForb = true;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	HistoryForm = {
		menu			: "Master Customer Code",
		historyloginId 	: 0
	};

	formSearch = {
		cityId   : null
	};

	ngOnInit() {
    this.historyLoginStart();
	this.checkuserGlobalVar();
	this.getCity();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

  private async checkuserGlobalVar() {
	if(UserGlobalVar.USER_ID === '') {
		setTimeout(() => {
			this.checkuserGlobalVar();
		}, 1000);
	}

	else{		
		var checkPriv = 0;
		await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-customer-code"){
				checkPriv = 1;
			}
		});

		if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
		}
	}
}

onSearch(){
	this.GetList();
}

getCity(){
    const body = {};

		this.http
		.post(AppGlobalVar.BASE_API_URL + 'master-customer-code/list-city',  body, this.httpOptions)
		.subscribe((data: any) => {
				this.cityDs = data.data;
				return data.data;
			},
			(error) => {
				alert(error.statusText + '. ' + error.message);
			}
		);
  }

  GetList() {
		const params = {
			cityId : this.formSearch.cityId
		}; 
		this.ds = new CustomStore({
			key: 'customerHeaderId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'master-customer-code/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  addCustomerCode(){
    this.router.navigate(['/master-customer-code-create']);    
  }

  generate(){
	const params = { 
		// customerHeaderId    : e.itemData.data.customerHeaderId
		// , cityCode          : e.itemData.data.cityCode
	  };
  
	  const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	  this.http
		.post(AppGlobalVar.BASE_API_URL + 'master-customer-code/generate-code', params, config)
		.subscribe((data: any) => {
			if(data.data[0].result == 'true'){
			notify({
			  message: data.data[0].message,
			  type: 'success',
			  displayTime: 5000,
			  width: 400,
			});   
		  }else{
			notify({
			  message: data.data[0].message,
			  type: 'error',
			  displayTime: 5000,
			  width: 400,
			});   
		  }    
		  
		  this.GetList();
		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	  ); 
  }
//   ChooseAction = (e) => {
// 	if(e.itemData.key == "generate"){
// 		  console.log(e.itemData.data);

		  
//     }
//   }
}