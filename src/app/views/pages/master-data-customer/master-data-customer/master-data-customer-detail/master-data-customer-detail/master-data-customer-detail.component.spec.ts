import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDataCustomerDetailComponent } from './master-data-customer-detail.component';

describe('MasterDataCustomerDetailComponent', () => {
  let component: MasterDataCustomerDetailComponent;
  let fixture: ComponentFixture<MasterDataCustomerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterDataCustomerDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDataCustomerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
