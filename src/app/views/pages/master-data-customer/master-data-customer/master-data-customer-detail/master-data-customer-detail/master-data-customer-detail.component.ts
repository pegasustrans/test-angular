import {ChangeDetectorRef, Component, HostListener, OnInit, ChangeDetectionStrategy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { AppGlobalVar } from '../../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../core/globalvar/userGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';


@Component({
  selector: 'kt-master-data-customer-detail',
  templateUrl: './master-data-customer-detail.component.html',
  styleUrls: ['./master-data-customer-detail.component.scss']
})

export class MasterDataCustomerDetailComponent implements OnInit {
	//router: any;
	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}


	menuPriv = false;
	menuForb = true;


	ngOnInit() {
		this.checkuserGlobalVar();
	}

	
	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "master-data-customer-detail"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;
			
			}
		}
	}
	onBack(){
		this.router.navigate(['/master-data-customer']);
	  }

	
}
