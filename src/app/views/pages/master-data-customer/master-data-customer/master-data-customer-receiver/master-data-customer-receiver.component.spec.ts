import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDataCustomerReceiverComponent } from './master-data-customer-receiver.component';

describe('MasterDataCustomerReceiverComponent', () => {
  let component: MasterDataCustomerReceiverComponent;
  let fixture: ComponentFixture<MasterDataCustomerReceiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterDataCustomerReceiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDataCustomerReceiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
