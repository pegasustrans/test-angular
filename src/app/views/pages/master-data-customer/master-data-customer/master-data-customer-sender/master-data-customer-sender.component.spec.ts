import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDataCustomerSenderComponent } from './master-data-customer-sender.component';

describe('MasterDataCustomerSenderComponent', () => {
  let component: MasterDataCustomerSenderComponent;
  let fixture: ComponentFixture<MasterDataCustomerSenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterDataCustomerSenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDataCustomerSenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
