import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';;
import { Component, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import DataSource from 'devextreme/data/data_source';

@Component({
  selector: 'kt-master-data-customer-sender',
  templateUrl: './master-data-customer-sender.component.html',
  styleUrls: ['./master-data-customer-sender.component.scss']
})


export class MasterDataCustomerSenderComponent implements OnInit {

	ds: any = {};
	menuPriv = true;
	menuForb = false;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  	customerId			: any;
	customerNameId		: any;
	customerStoreId		: any;
	customerPlaceId		: any;
	customerAddressId	: any;

	totalKoli			: any;
  	totalCAW			: any;
  
//<!-- SEARCH BUTTON START HERE -->
  formSearch = {
	dateStart		: new Date(),
	dateEnd 		: new Date()
  };
//<!-- SEARCH BUTTON END -->


  ngOnInit() {
	this.route.queryParams
	.subscribe(params => {
	  	this.customerId			= params.p1;   
		this.customerNameId		= params.p2;   
		this.customerStoreId	= params.p3;   
		this.customerPlaceId	= params.p4;   
		this.customerAddressId	= params.p5;   
	});

    // this.GetList();
    
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	onSearch(){
		this.GetList();
	}

  GetList() {
	  var awaltahun 	= this.formSearch.dateStart.getUTCFullYear();
   	  var awalbulan 	= this.formSearch.dateStart.getUTCMonth() + 1;
	  var awaltanggal 	= this.formSearch.dateStart.getUTCDate();

	  var akhirtahun 	= this.formSearch.dateEnd.getUTCFullYear();
	  var akhirbulan 	= this.formSearch.dateEnd.getUTCMonth() + 1;
   	  var akhirtanggal 	= this.formSearch.dateEnd.getUTCDate();

	  var dateStart 	= awaltahun + '-' + awalbulan + '-' + awaltanggal;
	  var dateEnd 		= akhirtahun + '-' + akhirbulan + '-' + akhirtanggal;

		const params = {
			customerId			: this.customerId,
			customerNameId		: this.customerNameId,
			customerStoreId		: this.customerStoreId,
			customerPlaceId		: this.customerPlaceId,
			customerAddressId	: this.customerAddressId,
			dateStart			: dateStart,
			dateEnd				: dateEnd
		}; 

		this.ds = new CustomStore({
			key: 'spbId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'mdata-customer-sender/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	thousandSeparator(data) {
		var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		roundUp = Math.round(roundUp);
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	}





	detailMasterDataCustomer = (e) => {
	  
		this.router.navigate(['/master-data-customer-detail'], 
        { 
            queryParams: { 
               p1: e.row.data.privilegeId,
               p2: e.row.data.privilegeName
            } 
        });
	}


	

}