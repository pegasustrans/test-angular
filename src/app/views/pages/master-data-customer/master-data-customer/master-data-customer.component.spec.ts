import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDataCustomerComponent } from './master-data-customer.component';

describe('MasterDataCustomerComponent', () => {
  let component: MasterDataCustomerComponent;
  let fixture: ComponentFixture<MasterDataCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterDataCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDataCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
