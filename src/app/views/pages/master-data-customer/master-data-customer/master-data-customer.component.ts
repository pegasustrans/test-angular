import { ActivatedRoute, Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';;
import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-master-data-customer',
  templateUrl: './master-data-customer.component.html',
  styleUrls: ['./master-data-customer.component.scss']
})


export class MasterDataCustomerComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

  ds: any = {};
  menuPriv = false;
menuForb = true;

HistoryForm = {
	menu			: "Data Pelanggan",
	historyloginId 	: 0
};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  ngOnInit() {
   	this.GetList();
	this.CheckPrivilege();
	this.historyLoginStart();

	// window.onbeforeunload = () => this.ngOnDestroy();
  }

  ngOnDestroy() { 
	this.historyLoginEnd();
 }

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	  .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	  .subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	  .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	  .subscribe((data: any) => {

		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	);
}

 

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};
	CheckPrivilege(){
		const params = {};
	  
		  const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		  this.http
			.post(AppGlobalVar.BASE_API_URL + 'master-data-customer/check-privilege', params, config)
			.subscribe((data: any) => {

				if(data.data.length > 0){
				this.menuForb = false;
				this.menuPriv = true;
			};
			  
			}, 
			  (error) => {
				alert(error.statusText + '. ' + error.message);
			  }
			);
	}

  GetList() {
		const params = {
			//createdAt: this.formSearch.dateString
		}; 
		this.ds = new CustomStore({
			key: 'customerId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'master-data-customer/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	detailMasterDataCustomer = (e) => {
		this.router.navigate(['/master-data-customer-detail'], 
        { 
            queryParams: { 
               p1: e.row.data.customerId,
			   p2: e.row.data.customerNameId,
			   p3: e.row.data.customerStoreId,
			   p4: e.row.data.customerPlaceId,
			   p5: e.row.data.customerAddressId
            } 
        });
	}


	

}