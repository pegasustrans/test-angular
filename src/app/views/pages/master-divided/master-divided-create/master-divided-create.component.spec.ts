import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDividedCreateComponent } from './master-divided-create.component';

describe('MasterDividedCreateComponent', () => {
  let component: MasterDividedCreateComponent;
  let fixture: ComponentFixture<MasterDividedCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterDividedCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDividedCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
