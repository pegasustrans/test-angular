import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDividedDeleteComponent } from './master-divided-delete.component';

describe('MasterDividedDeleteComponent', () => {
  let component: MasterDividedDeleteComponent;
  let fixture: ComponentFixture<MasterDividedDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterDividedDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDividedDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
