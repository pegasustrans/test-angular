import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDividedComponent } from './master-divided.component';

describe('MasterDividedComponent', () => {
  let component: MasterDividedComponent;
  let fixture: ComponentFixture<MasterDividedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterDividedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDividedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
