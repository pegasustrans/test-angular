import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-master-divided',
  templateUrl: './master-divided.component.html',
  styleUrls: ['./master-divided.component.scss']
})
export class MasterDividedComponent implements OnInit {

  ds: any = {};
  menuPriv = false;
  menuForb = true;

  HistoryForm = {
	menu: "Master - Moda",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.historyLoginStart();
	  this.checkuserGlobalVar();
    this.GetList();
  }

  private async checkuserGlobalVar() {
    if(UserGlobalVar.USER_ID === '') {
      setTimeout(() => {
        this.checkuserGlobalVar();
      }, 1000);
    }
  
    else{		
      var checkPriv = 0;
      await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
        if (value == "master-divided"){
          checkPriv = 1;
        }
      });
  
      if (checkPriv == 1){
        this.menuPriv = true;
        this.menuForb = false;
      }
    }
  }
  
  ngOnDestroy() { 
    this.historyLoginEnd();
  }
  
  historyLoginStart(){
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
    .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
    .subscribe((data: any) => {
        this.HistoryForm.historyloginId = data.data;
      },
      (error) => {
      alert(error.statusText + '. ' + error.message);
      }
    );
  }
  
  historyLoginEnd(){
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
    .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
    .subscribe((data: any) => {
  
      },
      (error) => {
      alert(error.statusText + '. ' + error.message);
      }
    );
  }
  
    GetList() {
      const params = {}; 
      this.ds = new CustomStore({
        key: 'transportId',
        load:  (loadOptions: any) => {
          return this.http.get(AppGlobalVar.BASE_API_URL + 'master-divided/list', { headers: this.httpOptions.headers, params  })
            .toPromise()
            .then((data: any) => {
              return {
                data: data.data
                
              };
            })
            .catch(error => { console.log(error); });
        }
      });
    }
  
    addRound(){
      this.router.navigate(['/master-divided-create']);
      
    }
  
    ChooseAction = (e) => {
    console.log(e.itemData.data);
    
      if(e.itemData.key == "hapus"){
        this.router.navigate(['/master-divided-delete'], 
          { 
              queryParams: { 
                p1: e.itemData.data.roundId,
                 p2: e.itemData.data.carrier,
                 p3: e.itemData.data.first_value,
                 p4: e.itemData.data.last_value,
                 p5: e.itemData.data.final_value,
              } 
          });
      }
    }

}
