import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterMethodPaymentCreateComponent } from './master-method-payment-create.component';

describe('MasterMethodPaymentCreateComponent', () => {
  let component: MasterMethodPaymentCreateComponent;
  let fixture: ComponentFixture<MasterMethodPaymentCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterMethodPaymentCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterMethodPaymentCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
