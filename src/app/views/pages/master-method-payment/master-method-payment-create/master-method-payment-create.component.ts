import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-master-method-payment-create',
  templateUrl: './master-method-payment-create.component.html',
  styleUrls: ['./master-method-payment-create.component.scss']
})
export class MasterMethodPaymentCreateComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

  cityDs          : any;
  paymentMethodDs : any;

  formCreate = {
    masterCityId  : null,
    lookupId      : null
  }

  HistoryForm = {
		menu: "Master - Metode Pembayaran - Create",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.getCity();
    this.getPaymentMethod();
    this.historyLoginStart();
  }

  onBack(){
    this.router.navigate(['master-method-payment']);
  }

  getCity(){
    const body = {};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'master-payment-method/get-city',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.cityDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);
  }

  getPaymentMethod(){
    const body = {};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'master-payment-method/get-payment-method',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.paymentMethodDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);
  }
  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}
  onSave(){
    const params = { 
      masterCityId  : this.formCreate.masterCityId
      , lookupId    : this.formCreate.lookupId
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'master-payment-method/create-payment-method', params, config)
      .subscribe((data: any) => {

        if(data.data[0].result == 'true'){
          notify({
            message: data.data[0].message,
            type: 'success',
            displayTime: 5000,
            width: 400,
          });   

          this.onBack();
        }else{
          notify({
            message: data.data[0].message,
            type: 'error',
            displayTime: 5000,
            width: 400,
          });   
        }    
        
      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    ); 
  }

}
