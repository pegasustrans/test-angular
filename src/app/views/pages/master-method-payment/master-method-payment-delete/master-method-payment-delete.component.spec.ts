import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterMethodPaymentDeleteComponent } from './master-method-payment-delete.component';

describe('MasterMethodPaymentDeleteComponent', () => {
  let component: MasterMethodPaymentDeleteComponent;
  let fixture: ComponentFixture<MasterMethodPaymentDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterMethodPaymentDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterMethodPaymentDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
