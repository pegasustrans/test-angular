import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-master-method-payment-delete',
  templateUrl: './master-method-payment-delete.component.html',
  styleUrls: ['./master-method-payment-delete.component.scss']
})
export class MasterMethodPaymentDeleteComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

  paymentMethodId   : any;
  masterCityId      : any;  
  masterCityCustom  : any;
  lookupId          : any;
  lookupValue       : any;

  HistoryForm = {
		menu: "Master - Metode Pembayaran - Delete",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.paymentMethodId    = params.p1;
        this.lookupId           = params.p2;
        this.lookupValue        = params.p3;
        this.masterCityId       = params.p4;
        this.masterCityCustom   = params.p5;
      }
    );
  }

  onBack(){
    this.router.navigate(['master-method-payment']);
  }
  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}
  onDelete(){
    const params = { 
      masterCityId  : this.masterCityId
      , lookupId    : parseInt(this.lookupId)
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'master-payment-method/delete-payment-method', params, config)
      .subscribe((data: any) => {

        if(data.data[0].result == 'true'){
          notify({
            message: data.data[0].message,
            type: 'success',
            displayTime: 5000,
            width: 400,
          });   

          this.onBack();
        }else{
          notify({
            message: data.data[0].message,
            type: 'error',
            displayTime: 5000,
            width: 400,
          });   
        }    
        
      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    ); 
  }

}
