import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterMethodPaymentComponent } from './master-method-payment.component';

describe('MasterMethodPaymentComponent', () => {
  let component: MasterMethodPaymentComponent;
  let fixture: ComponentFixture<MasterMethodPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterMethodPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterMethodPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
