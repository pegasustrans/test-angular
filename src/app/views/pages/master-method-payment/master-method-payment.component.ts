import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-master-method-payment',
  templateUrl: './master-method-payment.component.html',
  styleUrls: ['./master-method-payment.component.scss']
})
export class MasterMethodPaymentComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
  ds: any = {};
  menuPriv = false;
  menuForb = true;

  HistoryForm = {
	menu: "Master - Metode Pembayaran",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.checkuserGlobalVar();
	this.GetList();
	this.historyLoginStart();
  }

  private async checkuserGlobalVar() {
	if(UserGlobalVar.USER_ID === '') {
		setTimeout(() => {
			this.checkuserGlobalVar();
		}, 1000);
	}

	else{		
		var checkPriv = 0;
		await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-payment-method"){
				checkPriv = 1;
			}
		});

		if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
		}
	}
}
ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}
  GetList() {
		const params = {}; 
		this.ds = new CustomStore({
			key: 'paymentMethodId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'master-payment-method/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  addPaymentMethod(){
    this.router.navigate(['/master-method-payment-create']);
    
  }

  ChooseAction = (e) => {
	  if(e.itemData.key == "hapus"){
      this.router.navigate(['/master-method-payment-delete'], 
        { 
            queryParams: { 
              p1: e.itemData.data.paymentMethodId,
               p2: e.itemData.data.lookupId,
               p3: e.itemData.data.lookupValue,
               p4: e.itemData.data.masterCityId,
               p5: e.itemData.data.masterCityCustom,
            } 
        });
    }
  }


}
