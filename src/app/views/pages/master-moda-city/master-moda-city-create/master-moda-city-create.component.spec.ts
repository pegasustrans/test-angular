import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterModaCityCreateComponent } from './master-moda-city-create.component';

describe('MasterModaCityCreateComponent', () => {
  let component: MasterModaCityCreateComponent;
  let fixture: ComponentFixture<MasterModaCityCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterModaCityCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterModaCityCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
