import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-master-moda-city-create',
  templateUrl: './master-moda-city-create.component.html',
  styleUrls: ['./master-moda-city-create.component.scss']
})
export class MasterModaCityCreateComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

  cityDs          : any;
  modaCityDs      : any;

  formCreate = {
    originCityId        : null,
    destinationCityId   : null,
    lookupId            : null
  }

  HistoryForm = {
		menu: "Master - Moda Kota - Create",
		historyloginId:0
	};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.getCity();
    this.getModaCity();
    this.historyLoginStart();
  }

  onBack(){
    this.router.navigate(['master-moda-city']);
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}
  getCity(){
    const body = {};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'master-moda-city/get-city',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.cityDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);
  }

  getModaCity(){
    const body = {};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'master-moda-city/get-moda-city',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.modaCityDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);
  }

  onSave(){
    const params = { 
      originCityId          : this.formCreate.originCityId
      , destinationCityId   : this.formCreate.destinationCityId
      , lookupId            : this.formCreate.lookupId
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'master-moda-city/create-moda-city', params, config)
      .subscribe((data: any) => {

        if(data.data[0].result == 'true'){
          notify({
            message: data.data[0].message,
            type: 'success',
            displayTime: 5000,
            width: 400,
          });   

          this.onBack();
        }else{
          notify({
            message: data.data[0].message,
            type: 'error',
            displayTime: 5000,
            width: 400,
          });   
        }    
        
      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    ); 
  }

}
