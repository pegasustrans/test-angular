import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterModaCityDeleteComponent } from './master-moda-city-delete.component';

describe('MasterModaCityDeleteComponent', () => {
  let component: MasterModaCityDeleteComponent;
  let fixture: ComponentFixture<MasterModaCityDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterModaCityDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterModaCityDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
