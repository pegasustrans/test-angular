import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterModaCityComponent } from './master-moda-city.component';

describe('MasterModaCityComponent', () => {
  let component: MasterModaCityComponent;
  let fixture: ComponentFixture<MasterModaCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterModaCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterModaCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
