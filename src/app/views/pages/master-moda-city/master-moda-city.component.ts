import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-master-moda-city',
  templateUrl: './master-moda-city.component.html',
  styleUrls: ['./master-moda-city.component.scss']
})
export class MasterModaCityComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
  ds: any = {};
  menuPriv = false;
  menuForb = true;

  HistoryForm = {
	menu: "Master - Moda Kota",
	historyloginId:0
};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
	this.checkuserGlobalVar();
	this.GetList();
	this.historyLoginStart();
  }

  private async checkuserGlobalVar() {
	if(UserGlobalVar.USER_ID === '') {
		setTimeout(() => {
			this.checkuserGlobalVar();
		}, 1000);
	}

	else{		
		var checkPriv = 0;
		await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
			if (value == "master-moda-city"){
				checkPriv = 1;
			}
		});

		if (checkPriv == 1){
			this.menuPriv = true;
			this.menuForb = false;
		}
	}
}

ngOnDestroy() { 
	this.historyLoginEnd();
}

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	.subscribe((data: any) => {

		},
		(error) => {
		alert(error.statusText + '. ' + error.message);
		}
	);
}

  GetList() {
		const params = {}; 
		this.ds = new CustomStore({
			key: 'modaCityId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'master-moda-city/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  addCity(){
    this.router.navigate(['/master-moda-city-create']);    
  }

  ChooseAction = (e) => {
	  if(e.itemData.key == "hapus"){
		  
      this.router.navigate(['/master-moda-city-delete'], 
        { 
            queryParams: { 
              p1: e.itemData.data.modaCityId,
               p2: e.itemData.data.lookupId,
               p3: e.itemData.data.lookupValue,
               p4: e.itemData.data.originCityId,
               p5: e.itemData.data.originCityCustom,
			   p6: e.itemData.data.destinationCityId,
               p7: e.itemData.data.destinationCityCustom,
            } 
        });
    }
  }


}