import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterPorterCodeCreateComponent } from './master-porter-code-create.component';

describe('MasterPorterCodeCreateComponent', () => {
  let component: MasterPorterCodeCreateComponent;
  let fixture: ComponentFixture<MasterPorterCodeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterPorterCodeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterPorterCodeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
