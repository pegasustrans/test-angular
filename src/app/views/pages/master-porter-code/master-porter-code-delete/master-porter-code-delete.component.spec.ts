import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterPorterCodeDeleteComponent } from './master-porter-code-delete.component';

describe('MasterPorterCodeDeleteComponent', () => {
  let component: MasterPorterCodeDeleteComponent;
  let fixture: ComponentFixture<MasterPorterCodeDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterPorterCodeDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterPorterCodeDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
