import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterPorterCodeComponent } from './master-porter-code.component';

describe('MasterPorterCodeComponent', () => {
  let component: MasterPorterCodeComponent;
  let fixture: ComponentFixture<MasterPorterCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterPorterCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterPorterCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
