import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-master-porter-code',
  templateUrl: './master-porter-code.component.html',
  styleUrls: ['./master-porter-code.component.scss']
})
export class MasterPorterCodeComponent implements OnInit {
  ds: any = {};
  menuPriv = false;
  menuForb = true;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  HistoryForm = {
		menu			: "Master Porter Code",
		historyloginId 	: 0
	};

  ngOnInit() {
    this.historyLoginStart();
	  this.checkuserGlobalVar();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}
  
  private async checkuserGlobalVar() {
    if(UserGlobalVar.USER_ID === '') {
      setTimeout(() => {
        this.checkuserGlobalVar();
      }, 1000);
    }
  
    else{		
      var checkPriv = 0;
      await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
        if (value == "master-porter-code"){
          checkPriv = 1;
        }
      });
  
      if (checkPriv == 1){
        this.menuPriv = true;
        this.menuForb = false;
        
      	this.GetList();
      }
    }
  }
  GetList() {
		const params = {}; 

		this.ds = new CustomStore({
			key: 'porterId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'master-porter/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  addPorterCode(){
    this.router.navigate(['/master-porter-code-create']);    
  }

  generate(){
    const params = {};
  
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
    .post(AppGlobalVar.BASE_API_URL + 'master-porter-code/generate-code', params, config)
    .subscribe((data: any) => {
      if(data.data[0].result == 'true'){
      notify({
        message: data.data[0].message,
        type: 'success',
        displayTime: 5000,
        width: 400,
      });   
      }else{
      notify({
        message: data.data[0].message,
        type: 'error',
        displayTime: 5000,
        width: 400,
      });   
      }    
      
      this.GetList();
    },
    (error) => {
      alert(error.statusText + '. ' + error.message);
    }
    ); 
  }
  }

  // ChooseAction = (e) => {
  //   if(e.itemData.key == "generate"){
  //       console.log(e.itemData.data);
  
        
  //   }

// }
