import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterRoundCreateComponent } from './master-round-create.component';

describe('MasterRoundCreateComponent', () => {
  let component: MasterRoundCreateComponent;
  let fixture: ComponentFixture<MasterRoundCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterRoundCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterRoundCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
