import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-master-round-create',
  templateUrl: './master-round-create.component.html',
  styleUrls: ['./master-round-create.component.scss']
})
export class MasterRoundCreateComponent implements OnInit {

  carrierDs : any;

  formCreate = {
    carrierId     : null,
    first_value   : null,
    last_value    : null,
    final_value   : null
  }

  HistoryForm = {
		menu: "Master - Pembulatan - Create",
		historyloginId:0
	};


  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.historyLoginStart();
    this.getCarrier();
  }

  onBack(){
    this.router.navigate(['master-round']);
  }

  getCarrier(){
    const body = {};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'master-round/list-carrier',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.carrierDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  onSave(){
    const params = { 
      carrierId       : this.formCreate.carrierId
      , first_value   : this.formCreate.first_value
      , last_value    : this.formCreate.last_value
      , final_value   : this.formCreate.final_value
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'master-round/create-round', params, config)
      .subscribe((data: any) => {

        if(data.data[0].result == 'true'){
          notify({
            message: data.data[0].message,
            type: 'success',
            displayTime: 5000,
            width: 400,
          });   

          this.onBack();
        }else{
          notify({
            message: data.data[0].message,
            type: 'error',
            displayTime: 5000,
            width: 400,
          });   
        }    
        
      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    ); 
  }

}
