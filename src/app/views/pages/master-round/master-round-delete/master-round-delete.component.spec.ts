import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterRoundDeleteComponent } from './master-round-delete.component';

describe('MasterRoundDeleteComponent', () => {
  let component: MasterRoundDeleteComponent;
  let fixture: ComponentFixture<MasterRoundDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterRoundDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterRoundDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
