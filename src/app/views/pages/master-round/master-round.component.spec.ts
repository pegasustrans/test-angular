import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterRoundComponent } from './master-round.component';

describe('MasterRoundComponent', () => {
  let component: MasterRoundComponent;
  let fixture: ComponentFixture<MasterRoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterRoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterRoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
