import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringCapacityInlandSubComponent } from './monitoring-capacity-inland-sub.component';

describe('MonitoringCapacityInlandSubComponent', () => {
  let component: MonitoringCapacityInlandSubComponent;
  let fixture: ComponentFixture<MonitoringCapacityInlandSubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringCapacityInlandSubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringCapacityInlandSubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
