import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringCapacityComponent } from './monitoring-capacity.component';

describe('MonitoringCapacityComponent', () => {
  let component: MonitoringCapacityComponent;
  let fixture: ComponentFixture<MonitoringCapacityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringCapacityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringCapacityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
