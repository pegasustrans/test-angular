import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-monitoring-capacity',
  templateUrl: './monitoring-capacity.component.html',
  styleUrls: ['./monitoring-capacity.component.scss']
})
export class MonitoringCapacityComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

  carrierDs       : any;
  inlandSubListDs : any;

  menuPriv = false;
	menuForb = true;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  formSearch = {
		manifestDate  : new Date(),
		carrierCode   : null
	};

  HistoryForm = {
		historyloginId 	: 0,
		menu			      : "Monitoring Capacity"
	};

  ngOnInit() {
    this.historyLoginStart();
    this.checkuserGlobalVar();
    this.getCarrier();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

  private async checkuserGlobalVar() {
    if(UserGlobalVar.USER_ID === '') {
        setTimeout(() => {
            this.checkuserGlobalVar();
        }, 1000);
    }

    else{		
        var checkPriv = 0;
        await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
            if (value == "monitoring-capacity"){
                checkPriv = 1;
            }
        });

        if (checkPriv == 1){
            this.menuPriv = true;
            this.menuForb = false;
        
        }
    }
}

  getCarrier(){
    var params = {};

    this.carrierDs = new CustomStore({
			key: 'carrierCode',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring/monitoring-capacity-list-carrier', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
              data: data.data
            };
					})
					.catch(error => { console.log(error); });
			}
		});
  }

  thousandSeparator(data) {
    var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		//roundUp = Math.round(roundUp);
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
		// return data.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	}

  onSearch(){
    var manifesttahun 	  = this.formSearch.manifestDate.getUTCFullYear();
    var manifestbulan 	  = this.formSearch.manifestDate.getUTCMonth() + 1;
	  var manifesttanggal 	= this.formSearch.manifestDate.getUTCDate();

    const params = {
			manifestYear  : manifesttahun.toString(),
			manifestMonth : manifestbulan.toString(),
			manifestDay   : manifesttanggal.toString(),
			carrierCode	  : this.formSearch.carrierCode
		}; 
    
		this.inlandSubListDs = new CustomStore({
			key: 'moda',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring/monitoring-capacity-list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
  }

}
