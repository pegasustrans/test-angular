import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringShipmentCreateComponent } from './monitoring-shipment-create.component';

describe('MonitoringShipmentCreateComponent', () => {
  let component: MonitoringShipmentCreateComponent;
  let fixture: ComponentFixture<MonitoringShipmentCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringShipmentCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringShipmentCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
