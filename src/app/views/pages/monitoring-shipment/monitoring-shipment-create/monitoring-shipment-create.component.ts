import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'kt-monitoring-shipment-create',
  templateUrl: './monitoring-shipment-create.component.html',
  styleUrls: ['./monitoring-shipment-create.component.scss']
})
export class MonitoringShipmentCreateComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
  }
  
	viaDs					    : any;
	flightDs				  : any;
	carrierDs				  : any;
	corporateDs				: any;
	originCityDs			: any;
	destinationCityDs	: any;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  formCreate = {
    viaId                 : null,
    originCityId         	: null,
    corporateId		    	  : null,
    destinationCityId    	: null,
    sttPegasus           	: null,
    sttLP                	: null,
    smuNo                	: null,
    koli                 	: null,
    caw                  	: null,
    carrierId             : null,
    flightId             	: null,
    flightDate           	: new Date(),
    flightDateYear        : null,
    flightDateMonth       : null,
    flightDateDay         : null,
    etd                  	: new Date(),
    etdYear               : null,
    etdMonth              : null,
    etdDay               	: null,
    etdHour               : null,
    etdMinute             : null,
    etdSecond             : null,
    eta                  	: new Date(),
    etaYear               : null,
    etaMonth              : null,
    etaDay               	: null,
    etaMinute             : null,
    etaSecond             : null,
    status               	: null,
    pod                  	: null,
    podDate              	: null,
    podDateYear          	: null,
    podDateMonth         	: null,
    podDateDay           	: null,
	  description				    : null
  }
  	
  httpOptions = {
	headers: new HttpHeaders({
		'Content-Type': 'application/json'
	})
  };

  HistoryForm = {
		historyloginId 	: 0,
		menu			: "LP-Monitoring Create",
	};

  ngOnInit() {
    this.historyLoginStart();
    this.via();
    this.originCity();
    this.destinationCity();
    this.carrier();
  }

  ngOnDestroy() { 
    this.historyLoginEnd();
    }
  
    historyLoginStart(){
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
      .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
      .subscribe((data: any) => {
          this.HistoryForm.historyloginId = data.data;
        },
        (error) => {
        alert(error.statusText + '. ' + error.message);
        }
      );
    }
  
    historyLoginEnd(){
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
      .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
      .subscribe((data: any) => {
  
        },
        (error) => {
        alert(error.statusText + '. ' + error.message);
        }
      );
    }

  via(){
      const params = {
      };
      this.viaDs = new DataSource({
        store: new CustomStore({
          key: 'stationId',
          loadMode: 'raw',
          load: (loadOptions) => {
            return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring-shipment/monitoring-shipment-read-via', { headers: this.httpOptions.headers, params  })
              .toPromise()
              .then((result: any) => {
                return result.data;
              });
          }
        })
      });
  }

  originCity(){
    const params = {
    };
    this.originCityDs = new DataSource({
      store: new CustomStore({
        key: 'cityId',
        loadMode: 'raw',
        load: (loadOptions) => {
          return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring-shipment/monitoring-shipment-read-origin-city', { headers: this.httpOptions.headers, params  })
            .toPromise()
            .then((result: any) => {
              return result.data;
            });
        }
      })
    });
  }

  onTriggerChange(e){
    this.corporate(e.value);    
  }

  destinationCity(){
    const params = {
    };
    this.destinationCityDs = new DataSource({
      store: new CustomStore({
        key: 'cityId',
        loadMode: 'raw',
        load: (loadOptions) => {
          return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring-shipment/monitoring-shipment-read-destination-city', { headers: this.httpOptions.headers, params  })
            .toPromise()
            .then((result: any) => {
              return result.data;
            });
        }
      })
    });
  }

  corporate(cityId){
    const params = {
      cityId : cityId
    };
    this.corporateDs = new DataSource({
      store: new CustomStore({
        key: 'corporateId',
        loadMode: 'raw',
        load: (loadOptions) => {
          return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring-shipment/monitoring-shipment-read-corporate', { headers: this.httpOptions.headers, params  })
            .toPromise()
            .then((result: any) => {
              return result.data;
            });
        }
      })
    });
  }

  carrier(){
    const params = {
    };
    this.carrierDs = new DataSource({
      store: new CustomStore({
        key: 'carrierId',
        loadMode: 'raw',
        load: (loadOptions) => {
          return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring-shipment/monitoring-shipment-read-carrier', { headers: this.httpOptions.headers, params  })
            .toPromise()
            .then((result: any) => {
              return result.data;
            });
        }
      })
    });
  }

  onTriggerChangeCarrier(e){
    this.flight(e.value);    
  }

  flight(carrierId){
    const params = {
      carrierId : carrierId
    };
    this.flightDs = new DataSource({
      store: new CustomStore({
        key: 'flightId',
        loadMode: 'raw',
        load: (loadOptions) => {
          return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring-shipment/monitoring-shipment-read-flight', { headers: this.httpOptions.headers, params  })
            .toPromise()
            .then((result: any) => {
              return result.data;
            });
        }
      })
    });
  }

  onSave(){
    if(this.formCreate.viaId == null){
			notify({
				message: 'Via harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		
		}else if(this.formCreate.originCityId == null){
			notify({
				message: 'Origin harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.destinationCityId == null ){
			notify({
				message: 'Tujuan harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.sttLP == null){
			notify({
				message: 'STT Lion Parcel harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.smuNo == null){
			notify({
				message: 'SMU harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.koli == null){
			notify({
				message: 'Koli harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.caw == null){
			notify({
				message: 'Kilo harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.flightId == null){
			notify({
				message: 'Flight harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.flightDate == null){
			notify({
				message: 'Flight Date harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.etd == null){
			notify({
				message: 'ETD harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.eta == null){
			notify({
				message: 'ETA harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});	
    }else if(this.formCreate.status == null){
			notify({
				message: 'Status Pengiriman harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});		
		}else {

			this.formCreate.koli 	= parseInt(this.formCreate.koli);	
			this.formCreate.caw 	= parseFloat(this.formCreate.caw);	

      console.log(this.formCreate);
      
			const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
					this.http
					.post(AppGlobalVar.BASE_API_URL + 'monitoring-shipment/create', JSON.stringify(this.formCreate), config)
					.subscribe((data: any) => {
							if(data.data[0].result == 'false'){
								notify({
									message: data.data[0].message,
									type: 'error',
									displayTime: 5000,
									width: 400,
								});
							}else{
								notify({
									message: data.data[0].message,
									type: 'success',
									displayTime: 5000,
									width: 400,
								});
								setTimeout(() => {
									this.router.navigate(['/monitoring-shipment']);
								}, 1000);					
							}
				
						},
						(error) => {
						alert(error.statusText + '. ' + error.message);
						}
					);
			}
 
  }

  onBack(){
	this.router.navigate(['/monitoring-shipment']);
  }
}
