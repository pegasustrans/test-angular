import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringShipmentDeleteComponent } from './monitoring-shipment-delete.component';

describe('MonitoringShipmentDeleteComponent', () => {
  let component: MonitoringShipmentDeleteComponent;
  let fixture: ComponentFixture<MonitoringShipmentDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringShipmentDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringShipmentDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
