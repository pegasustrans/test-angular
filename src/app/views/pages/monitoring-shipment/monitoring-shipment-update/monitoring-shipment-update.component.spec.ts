import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringShipmentUpdateComponent } from './monitoring-shipment-update.component';

describe('MonitoringShipmentUpdateComponent', () => {
  let component: MonitoringShipmentUpdateComponent;
  let fixture: ComponentFixture<MonitoringShipmentUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringShipmentUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringShipmentUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
