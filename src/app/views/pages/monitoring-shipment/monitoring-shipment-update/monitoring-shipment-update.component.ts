import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-monitoring-shipment-update',
  templateUrl: './monitoring-shipment-update.component.html',
  styleUrls: ['./monitoring-shipment-update.component.scss']
})
export class MonitoringShipmentUpdateComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
  }
  
  caw                     : any;
  corporateName           : any;
  desc                    : any;
  destinationCityName     : any;
  eta                     : any;
  etaLocal                : any;
  etd                     : any;
  etdLocal                : any;
  fifthDayAfternoon       : any;
  fifthDayMorning         : any;
  firstDayAfternoon       : any;
  firstDayMorning         : any;
  flightDate              : any;
  flightDateLocal         : any;
  carrierCode             : any;
  flightNo                : any;
  fourthDayAfternoon      : any;
  fourthDayMorning        : any;
  koli                    : any;
  monitoringShipmentId    : any;
  originCityName          : any;
  pod                     : any;
  podDate                 : any;
  secondDayAfternoon      : any;
  secondDayMorning        : any;
  smuNo                   : any;
  sttLP                   : any;
  sttPegasus              : any;
  thirdDayAfternoon       : any;
  thirdDayMorning         : any;
  via                     : any;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  HistoryForm = {
		historyloginId 	: 0,
		menu			: "LP-Monitoring Update",
	};

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  formCreate = {
    status               	: null,
    pod                  	: null,
    podDate              	: null,
    podDateYear          	: null,
    podDateMonth         	: null,
    podDateDay           	: null,
	  description				    : null
  }

  ngOnInit() {
    this.historyLoginStart();
    this.route.queryParams
      .subscribe(params => {
        this.caw                     = params.q1 ;
        this.corporateName           = params.q2 ;
        this.desc                    = params.q3 ;
        this.destinationCityName     = params.q4 ;
        this.eta                     = params.q5 ;
        this.etaLocal                = params.q6 ;
        this.etd                     = params.q7 ;
        this.etdLocal                = params.q8 ;
        this.fifthDayAfternoon       = params.q9 ;
        this.fifthDayMorning         = params.q10;
        this.firstDayAfternoon       = params.q11;
        this.firstDayMorning         = params.q12;
        this.flightDate              = params.q13;
        this.flightDateLocal         = params.q14;
        this.flightNo                = params.q15;
        this.fourthDayAfternoon      = params.q16;
        this.fourthDayMorning        = params.q17;
        this.koli                    = params.q18;
        this.monitoringShipmentId    = params.q19;
        this.originCityName          = params.q20;
        this.pod                     = params.q21;
        this.podDate                 = params.q22;
        this.secondDayAfternoon      = params.q23;
        this.secondDayMorning        = params.q24;
        this.smuNo                   = params.q25;
        this.sttLP                   = params.q26;
        this.sttPegasus              = params.q27;
        this.thirdDayAfternoon       = params.q28;
        this.thirdDayMorning         = params.q29;
        this.via                     = params.q30;
        this.carrierCode             = params.q31;
      }
    );
  }

  ngOnDestroy() { 
    this.historyLoginEnd();
    }
  
    historyLoginStart(){
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
      .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
      .subscribe((data: any) => {
          this.HistoryForm.historyloginId = data.data;
        },
        (error) => {
        alert(error.statusText + '. ' + error.message);
        }
      );
    }
  
    historyLoginEnd(){
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
      this.http
      .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
      .subscribe((data: any) => {
  
        },
        (error) => {
        alert(error.statusText + '. ' + error.message);
        }
      );
    }

  onBack(){
    this.router.navigate(['monitoring-shipment']);
  }

  onSave(){
    const params = { 
      monitoringShipmentId : parseInt(this.monitoringShipmentId)                     
      , status             : this.formCreate.status                     
      , pod                : this.formCreate.pod                    
      , podDate            : this.formCreate.podDate            
      , description        : this.formCreate.description                   
    };

    if(this.formCreate.status == null){
			notify({
				message: 'Status Pengiriman harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});		
		}else {

			const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
					this.http
					.post(AppGlobalVar.BASE_API_URL + 'monitoring-shipment/update', params, config)
					.subscribe((data: any) => {
							if(data.data[0].result == 'false'){
								notify({
									message: data.data[0].message,
									type: 'error',
									displayTime: 5000,
									width: 400,
								});
							}else{
								notify({
									message: data.data[0].message,
									type: 'success',
									displayTime: 5000,
									width: 400,
								});
								setTimeout(() => {
									this.router.navigate(['/monitoring-shipment']);
								}, 1000);					
							}
				
						},
						(error) => {
						alert(error.statusText + '. ' + error.message);
						}
					);
			}
  }

}
