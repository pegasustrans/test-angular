import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringShipmentComponent } from './monitoring-shipment.component';

describe('MonitoringShipmentComponent', () => {
  let component: MonitoringShipmentComponent;
  let fixture: ComponentFixture<MonitoringShipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringShipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringShipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
