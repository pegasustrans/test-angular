import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';

@Component({
  selector: 'kt-monitoring-shipment',
  templateUrl: './monitoring-shipment.component.html',
  styleUrls: ['./monitoring-shipment.component.scss']
})


export class MonitoringShipmentComponent implements OnInit {
@HostListener('window:beforeunload', ["$event"]) unload(event) {
	this.historyLoginEnd();
}

  ds: any = {};
  menuPriv = false;
  menuForb = true;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	HistoryForm = {
		historyloginId 	: 0,
		menu			: "LP-Monitoring List",
	};


  ngOnInit() {
	this.historyLoginStart();
	this.checkuserGlobalVar();
	this.GetList();
  }

  ngOnDestroy() { 
	this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
		.subscribe((data: any) => {

			},
			(error) => {
			alert(error.statusText + '. ' + error.message);
			}
		);
	}

  onCellPrepared(e) {
	//   console.log(e.column.dataField);
	  
	if(e.column.dataField === "firstDayMorning" || e.column.dataField === "firstDayAfternoon") {
	  e.cellElement.style.background = "#22d800";
	}

	if(e.column.dataField === "secondDayMorning" || e.column.dataField === "secondDayAfternoon") {
		e.cellElement.style.background = "#00BFFF";
	}

	if(e.column.dataField === "thirdDayMorning" || e.column.dataField === "thirdDayAfternoon") {
		e.cellElement.style.background = "yellow";
	}

	if(e.column.dataField === "fourthDayMorning" || e.column.dataField === "fourthDayAfternoon") {
		e.cellElement.style.background = "#FFA500";
	}

	if(e.column.dataField === "fifthDayMorning" || e.column.dataField === "fifthDayAfternoon") {
		e.cellElement.style.background = "salmon";
	}

	  
  }

  private async checkuserGlobalVar() {
    if(UserGlobalVar.USER_ID === '') {
      setTimeout(() => {
        this.checkuserGlobalVar();
      }, 1000);
    }
  
    else{		
      var checkPriv = 0;
      await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
        if (value == "monitoring-shipment"){
          checkPriv = 1;
        }
      });
  
      if (checkPriv == 1){
        this.menuPriv = true;
        this.menuForb = false;
      }
    }
  }

  GetList() {
		const params = {}; 
		this.ds = new CustomStore({
			key: 'monitoringShipmentId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring-shipment/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						console.log(data.data);
						
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  addShipment(){
    this.router.navigate(['/monitoring-shipment-create']);
    
  }

  ChooseAction = (e) => {
	if(e.itemData.key == "edit"){
		this.router.navigate(['/monitoring-shipment-update'], 
		    { 
		        queryParams: { 
					  q1 : e.itemData.data.caw
					, q2 : e.itemData.data.corporateName
					, q3 : e.itemData.data.desc
					, q4 : e.itemData.data.destinationCityName
					, q5 : e.itemData.data.eta
					, q6 : e.itemData.data.etaLocal
					, q7 : e.itemData.data.etd
					, q8 : e.itemData.data.etdLocal
					, q9 : e.itemData.data.fifthDayAfternoon
					, q10 : e.itemData.data.fifthDayMorning
					, q11 : e.itemData.data.firstDayAfternoon
					, q12 : e.itemData.data.firstDayMorning
					, q13 : e.itemData.data.flightDate
					, q14 : e.itemData.data.flightDateLocal
					, q15 : e.itemData.data.flightNo
					, q16 : e.itemData.data.fourthDayAfternoon
					, q17 : e.itemData.data.fourthDayMorning
					, q18 : e.itemData.data.koli
					, q19 : e.itemData.data.monitoringShipmentId
					, q20 : e.itemData.data.originCityName
					, q21 : e.itemData.data.pod
					, q22 : e.itemData.data.podDate
					, q23 : e.itemData.data.secondDayAfternoon
					, q24 : e.itemData.data.secondDayMorning
					, q25 : e.itemData.data.smuNo
					, q26 : e.itemData.data.sttLP
					, q27 : e.itemData.data.sttPegasus
					, q28 : e.itemData.data.thirdDayAfternoon
					, q29 : e.itemData.data.thirdDayMorning
					, q30 : e.itemData.data.via
					, q31 : e.itemData.data.carrierCode
			} 
        });
	}
	
	if(e.itemData.key == "delete"){
		this.router.navigate(['/monitoring-shipment-delete'], 
		    { 
		        queryParams: { 
					q1 : e.itemData.data.caw
					, q2 : e.itemData.data.corporateName
					, q3 : e.itemData.data.desc
					, q4 : e.itemData.data.destinationCityName
					, q5 : e.itemData.data.eta
					, q6 : e.itemData.data.etaLocal
					, q7 : e.itemData.data.etd
					, q8 : e.itemData.data.etdLocal
					, q9 : e.itemData.data.fifthDayAfternoon
					, q10 : e.itemData.data.fifthDayMorning
					, q11 : e.itemData.data.firstDayAfternoon
					, q12 : e.itemData.data.firstDayMorning
					, q13 : e.itemData.data.flightDate
					, q14 : e.itemData.data.flightDateLocal
					, q15 : e.itemData.data.flightNo
					, q16 : e.itemData.data.fourthDayAfternoon
					, q17 : e.itemData.data.fourthDayMorning
					, q18 : e.itemData.data.koli
					, q19 : e.itemData.data.monitoringShipmentId
					, q20 : e.itemData.data.originCityName
					, q21 : e.itemData.data.pod
					, q22 : e.itemData.data.podDate
					, q23 : e.itemData.data.secondDayAfternoon
					, q24 : e.itemData.data.secondDayMorning
					, q25 : e.itemData.data.smuNo
					, q26 : e.itemData.data.sttLP
					, q27 : e.itemData.data.sttPegasus
					, q28 : e.itemData.data.thirdDayAfternoon
					, q29 : e.itemData.data.thirdDayMorning
					, q30 : e.itemData.data.via
					, q31 : e.itemData.data.carrierCode
					, q32 : e.itemData.data.podDateLocal
			} 
        });
    }
  }


}