// import * as $ from 'jquery';
// import 'datatables.net';

// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MatDialogModule, MatTabsModule} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';
import { MailModule } from './apps/mail/mail.module';
import { ECommerceModule } from './apps/e-commerce/e-commerce.module';
import { UserManagementModule } from './user-management/user-management.module';
import { MyPageComponent } from './my-page/my-page.component';
import { SpbCreateComponent } from './spb/spb-create/spb-create.component';
import { LocationComponent } from './spb/spb-create/location/location.component';

import { DataTablesModule } from 'angular-datatables';

// Devextreme
import { DxPopupModule } from 'devextreme-angular/ui/popup';
import { DxPopoverModule } from 'devextreme-angular/ui/popover';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxTextAreaModule } from 'devextreme-angular/ui/text-area';
import { DxTextBoxModule } from 'devextreme-angular/ui/text-box';
import { DxPivotGridModule } from 'devextreme-angular/ui/pivot-grid';
import { DxNumberBoxModule } from 'devextreme-angular/ui/number-box';
import { DxCheckBoxModule } from 'devextreme-angular/ui/check-box';
import { DxSwitchModule } from 'devextreme-angular/ui/switch';
import { DxSelectBoxModule } from 'devextreme-angular/ui/select-box';
import { DxRadioGroupModule } from 'devextreme-angular/ui/radio-group';
import { DxValidatorModule } from 'devextreme-angular/ui/validator';
import { DxScrollViewModule } from 'devextreme-angular/ui/scroll-view';
import { DxDropDownBoxModule } from 'devextreme-angular/ui/drop-down-box';
import { DxAutocompleteModule } from 'devextreme-angular/ui/autocomplete';
import { DxDateBoxModule } from 'devextreme-angular/ui/date-box';
import { DxValidationSummaryModule } from 'devextreme-angular/ui/validation-summary';
import { SpbPrintComponent } from './spb/spb-print/spb-print.component';
import { GoodsComponent } from './spb/spb-create/goods/goods.component';
import { ServiceTypeComponent } from './spb/spb-create/service-type/service-type.component';
import { PriceComponent } from './spb/spb-create/price/price.component';
import { CustomerComponent } from './spb/spb-create/customer/customer.component';
import { ManifestSenderComponent } from './manifest/manifest-sender/manifest-sender.component';
import { ManifestPrintComponent } from './manifest/manifest-print/manifest-print.component';
import { SpbListComponent } from './spb/spb-list/spb-list.component';
import { CalculateTunaiAsalPipe } from './manifest/calculate-tunai-asal.pipe';
import { CalculateTunaiTujuanPipe } from './manifest/calculate-tunai-tujuan.pipe';
import { DxDropDownButtonModule } from 'devextreme-angular/ui/drop-down-button';
import { SpbListReceiverComponent } from './spb/spb-list-receiver/spb-list-receiver.component';
import { ManifestListReceiverComponent } from './manifest/manifest-list-receiver/manifest-list-receiver.component';
import { SpbPrintV2Component } from './spb/spb-print-v2/spb-print-v2.component';
import { GoodsMonitoringComponent } from './goods-monitoring/goods-monitoring.component';
import { GoodsMonitoringDatagridComponent } from './goods-monitoring/goods-monitoring-datagrid/goods-monitoring-datagrid.component';
import { ManifestCarrierComponent } from './manifest-carrier/manifest-carrier.component';
import { SpbStatisticInputComponent } from './statistik/spb-statistic-input/spb-statistic-input.component';
import { ReadManifestCarrierComponent } from './manifest-carrier/read-manifest-carrier/read-manifest-carrier.component';
import { CreateManifestCarrierComponent } from './manifest-carrier/create-manifest-carrier/create-manifest-carrier.component';
import { ManifestChangeKoliComponent } from './manifest/manifest-change-koli/manifest-change-koli.component';
import { ManifestCarrierReportComponent } from './manifest-carrier-report/manifest-carrier-report.component';
import { SpbHourlyListComponent } from './spb-hourly/spb-hourly-list/spb-hourly-list.component';
import { SpbHourlyReceiverListComponent } from './spb-hourly-receiver/spb-hourly-receiver-list/spb-hourly-receiver-list.component';
import { GoodsMonitoringReceiverListComponent } from './goods-monitoring-receiver/goods-monitoring-receiver-list/goods-monitoring-receiver-list.component';
import { PrivilegeMasterListComponent } from './privilege-master/privilege-master-list/privilege-master-list.component';
import { PrivilegeMasterDetailListComponent } from './privilege-master/privilege-master-detail-list/privilege-master-detail-list.component';
import { PrivilegeUserComponent } from './privilege-master/privilege-user/privilege-user.component';
import { PrivilegeMenuComponent } from './privilege-master/privilege-menu/privilege-menu.component';
import { UpdatePasswordComponent } from './privilege-user/update-password/update-password.component';
import { RatesComponent } from './rates/rates.component';
import { RatesDatagridComponent } from './rates/rates-datagrid/rates-datagrid.component';
import { TransitListComponent } from './manifest-carrier-master/master-transit/transit-list/transit-list.component';
import { TransitUpdateComponent } from './manifest-carrier-master/master-transit/transit-update/transit-update.component';
import { DetailManifestCarrierComponent } from './manifest-carrier/detail-manifest-carrier/detail-manifest-carrier.component';
import { PrivilegeUserListComponent } from './configuration/privilege-user/privilege-user-list/privilege-user-list.component';
import { PrivilegeListComponent } from './configuration/privilege-user/privilege-list/privilege-list.component';
import { UserListComponent } from './configuration/privilege-user/user-list/user-list.component';
import { PrivilegeSettingComponent } from './configuration/privilege-user/privilege-list/privilege-setting/privilege-setting.component';
import { DetailPrivilegeSettingComponent } from './configuration/privilege-user/privilege-list/detail-privilege-setting/detail-privilege-setting.component';
import { RoleUserListComponent } from './configuration/privilege-user/privilege-list/role-user-list/role-user-list.component';
import { DetailRoleListComponent } from './configuration/privilege-user/privilege-list/detail-role-list/detail-role-list.component';
import { RoleUserSettingComponent } from './configuration/privilege-user/privilege-list/role-user-setting/role-user-setting.component';
import { DeleteRoleCityComponent } from './configuration/privilege-user/privilege-list/delete-role-city/delete-role-city.component';
import { AddRoleCityComponent } from './configuration/privilege-user/privilege-list/add-role-city/add-role-city.component';
import { PrivilegeUpdateSettingComponent } from './configuration/privilege-user/privilege-list/privilege-update-setting/privilege-update-setting.component';
import { RoleListComponent } from './configuration/privilege-user/role-list/role-list.component';
import { AddUserComponent } from './configuration/privilege-user/user-list/add-user/add-user.component';
import { UpdateUnitUserComponent } from './configuration/privilege-user/user-list/update-unit-user/update-unit-user.component';
import { UpdateRolePrivilegeComponent } from './configuration/privilege-user/user-list/update-role-privilege/update-role-privilege.component';
import { PrivilegeCreateComponent } from './configuration/privilege-user/privilege-list/privilege-create/privilege-create.component';
import { RoleCreateComponent } from './configuration/privilege-user/role-list/role-create/role-create.component';
import { UnitPlaceCreateComponent } from './configuration/privilege-user/user-list/unit-place-create/unit-place-create.component';
import { UnitSubbranchPlaceCreateComponent } from './configuration/privilege-user/user-list/unit-subbranch-place-create/unit-subbranch-place-create.component';
import { UpdateUnitSubbranchUserComponent } from './configuration/privilege-user/user-list/update-unit-subbranch-user/update-unit-subbranch-user.component';
import { UpdateUnitCompanyComponent } from './configuration/privilege-user/user-list/update-unit-company/update-unit-company.component';
import { UpdateUnitBranchComponent } from './configuration/privilege-user/user-list/update-unit-branch/update-unit-branch.component';
import { UpdateUnitSubbranchComponent } from './configuration/privilege-user/user-list/update-unit-subbranch/update-unit-subbranch.component';
import { UserRoleListComponent } from './configuration/privilege-user/role-list/user-role-list/user-role-list.component';
import { UserDetailComponent } from './configuration/privilege-user/user-list/user-detail/user-detail.component';
import { UserSettingComponent } from './configuration/privilege-user/user-list/user-setting/user-setting.component';
import { MasterDataCustomerComponent } from './master-data-customer/master-data-customer/master-data-customer.component';
import { MasterDataCustomerDetailComponent } from './master-data-customer/master-data-customer/master-data-customer-detail/master-data-customer-detail/master-data-customer-detail.component';
import { MasterDataCustomerSenderComponent } from './master-data-customer/master-data-customer/master-data-customer-sender/master-data-customer-sender.component';
import { MasterDataCustomerReceiverComponent } from './master-data-customer/master-data-customer/master-data-customer-receiver/master-data-customer-receiver.component';
import { RevisiManifestCarrierComponent } from './manifest-carrier/revisi-manifest-carrier/revisi-manifest-carrier.component';
import { ListRevisiManifestCarrierComponent } from './manifest-carrier/list-revisi-manifest-carrier/list-revisi-manifest-carrier.component';
import { DetailCostManifestCarrierComponent } from './manifest-carrier/detail-cost-manifest-carrier/detail-cost-manifest-carrier.component';
import { SuperVisionComponent } from './super-vision/super-vision.component';
import { SuperVisionCreateComponent } from './super-vision/super-vision-create/super-vision-create.component';
import { SuperVisionHistoryComponent } from './super-vision/super-vision-history/super-vision-history.component';
import { SuperVisionListComponent } from './super-vision/super-vision-list/super-vision-list.component';
import { SuperVisionApprovalSpbComponent } from './super-vision/super-vision-approval/super-vision-approval-spb/super-vision-approval-spb.component';
import { SuperVisionApprovalVoidComponent } from './super-vision/super-vision-approval/super-vision-approval-void/super-vision-approval-void.component';
import { SuperVisionApprovalReprintComponent } from './super-vision/super-vision-approval/super-vision-approval-reprint/super-vision-approval-reprint.component';
import { SuperVisionCreateReprintComponent } from './super-vision/super-vision-create/super-vision-create-reprint/super-vision-create-reprint.component';
import { SuperVisionCreateVoidComponent } from './super-vision/super-vision-create/super-vision-create-void/super-vision-create-void.component';
import { SuperVisionCreateSpbComponent } from './super-vision/super-vision-create/super-vision-create-spb/super-vision-create-spb.component';
import { SuperVisionCreateFocComponent } from './super-vision/super-vision-create/super-vision-create-foc/super-vision-create-foc.component';
import { SuperVisionApprovalFocComponent } from './super-vision/super-vision-approval/super-vision-approval-foc/super-vision-approval-foc.component';
import { SuperVisionCreateKiloComponent } from './super-vision/super-vision-create/super-vision-create-kilo/super-vision-create-kilo.component';
import { SuperVisionApprovalKiloComponent } from './super-vision/super-vision-approval/super-vision-approval-kilo/super-vision-approval-kilo.component';
import { HistoryAksiComponent } from './HistoryAksi/history-aksi/history-aksi.component';
import { HistoryLoginComponent } from './history-login/history-login/history-login.component';
import { HistoryPrintComponent } from './history-print/history-print/history-print.component';
import { MasterAgreementComponent } from './master-agreement/master-agreement.component';
import { MasterAgreementDetailComponent } from './master-agreement/master-agreement-detail/master-agreement-detail.component';
import { MasterMethodPaymentComponent } from './master-method-payment/master-method-payment.component';
import { MasterMethodPaymentCreateComponent } from './master-method-payment/master-method-payment-create/master-method-payment-create.component';
import { MasterMethodPaymentDeleteComponent } from './master-method-payment/master-method-payment-delete/master-method-payment-delete.component';
import { MasterModaCityComponent } from './master-moda-city/master-moda-city.component';
import { MasterModaCityCreateComponent } from './master-moda-city/master-moda-city-create/master-moda-city-create.component';
import { MasterModaCityDeleteComponent } from './master-moda-city/master-moda-city-delete/master-moda-city-delete.component';
import { ManifestCarrierCostCarrierComponent } from './manifest-carrier-master/manifest-carrier-cost-carrier/manifest-carrier-cost-carrier.component';
import { ManifestCarrierCostOriginComponent } from './manifest-carrier-master/manifest-carrier-cost-origin/manifest-carrier-cost-origin.component';
import { ManifestCarrierCostInlandOriginComponent } from './manifest-carrier-master/manifest-carrier-cost-inland-origin/manifest-carrier-cost-inland-origin.component';
import { ManifestCarrierCostDestinationComponent } from './manifest-carrier-master/manifest-carrier-cost-destination/manifest-carrier-cost-destination.component';
import { ManifestCarrierCostGeraiComponent } from './manifest-carrier-master/manifest-carrier-cost-gerai/manifest-carrier-cost-gerai.component';
import { ManifestCarrierCostPenerusComponent } from './manifest-carrier-master/manifest-carrier-cost-penerus/manifest-carrier-cost-penerus.component';
import { ManifestCarrierCostTransitComponent } from './manifest-carrier-master/manifest-carrier-cost-transit/manifest-carrier-cost-transit.component';
import { ManifestCarrierCostGeraiCreateComponent } from './manifest-carrier-master/manifest-carrier-cost-gerai/manifest-carrier-cost-gerai-create/manifest-carrier-cost-gerai-create.component';
import { ManifestCarrierCostGeraiUpdateComponent } from './manifest-carrier-master/manifest-carrier-cost-gerai/manifest-carrier-cost-gerai-update/manifest-carrier-cost-gerai-update.component';
import { ManifestCarrierCostGeraiDeleteComponent } from './manifest-carrier-master/manifest-carrier-cost-gerai/manifest-carrier-cost-gerai-delete/manifest-carrier-cost-gerai-delete.component';
import { ManifestCarrierCostCarrierDeleteComponent } from './manifest-carrier-master/manifest-carrier-cost-carrier/manifest-carrier-cost-carrier-delete/manifest-carrier-cost-carrier-delete.component';
import { ManifestCarrierCostCarrierCreateComponent } from './manifest-carrier-master/manifest-carrier-cost-carrier/manifest-carrier-cost-carrier-create/manifest-carrier-cost-carrier-create.component';
import { ManifestCarrierCostCarrierUpdateComponent } from './manifest-carrier-master/manifest-carrier-cost-carrier/manifest-carrier-cost-carrier-update/manifest-carrier-cost-carrier-update.component';
import { ManifestCarrierCostDestinationUpdateComponent } from './manifest-carrier-master/manifest-carrier-cost-destination/manifest-carrier-cost-destination-update/manifest-carrier-cost-destination-update.component';
import { ManifestCarrierCostDestinationCreateComponent } from './manifest-carrier-master/manifest-carrier-cost-destination/manifest-carrier-cost-destination-create/manifest-carrier-cost-destination-create.component';
import { ManifestCarrierCostDestinationDeleteComponent } from './manifest-carrier-master/manifest-carrier-cost-destination/manifest-carrier-cost-destination-delete/manifest-carrier-cost-destination-delete.component';
import { ManifestCarrierCostInlandOriginDeleteComponent } from './manifest-carrier-master/manifest-carrier-cost-inland-origin/manifest-carrier-cost-inland-origin-delete/manifest-carrier-cost-inland-origin-delete.component';
import { ManifestCarrierCostInlandOriginUpdateComponent } from './manifest-carrier-master/manifest-carrier-cost-inland-origin/manifest-carrier-cost-inland-origin-update/manifest-carrier-cost-inland-origin-update.component';
import { ManifestCarrierCostInlandOriginCreateComponent } from './manifest-carrier-master/manifest-carrier-cost-inland-origin/manifest-carrier-cost-inland-origin-create/manifest-carrier-cost-inland-origin-create.component';
import { ManifestCarrierCostOriginCreateComponent } from './manifest-carrier-master/manifest-carrier-cost-origin/manifest-carrier-cost-origin-create/manifest-carrier-cost-origin-create.component';
import { ManifestCarrierCostOriginUpdateComponent } from './manifest-carrier-master/manifest-carrier-cost-origin/manifest-carrier-cost-origin-update/manifest-carrier-cost-origin-update.component';
import { ManifestCarrierCostOriginDeleteComponent } from './manifest-carrier-master/manifest-carrier-cost-origin/manifest-carrier-cost-origin-delete/manifest-carrier-cost-origin-delete.component';
import { ManifestCarrierCostPenerusDeleteComponent } from './manifest-carrier-master/manifest-carrier-cost-penerus/manifest-carrier-cost-penerus-delete/manifest-carrier-cost-penerus-delete.component';
import { ManifestCarrierCostPenerusUpdateComponent } from './manifest-carrier-master/manifest-carrier-cost-penerus/manifest-carrier-cost-penerus-update/manifest-carrier-cost-penerus-update.component';
import { ManifestCarrierCostPenerusCreateComponent } from './manifest-carrier-master/manifest-carrier-cost-penerus/manifest-carrier-cost-penerus-create/manifest-carrier-cost-penerus-create.component';
import { ManifestCarrierCostTransitCreateComponent } from './manifest-carrier-master/manifest-carrier-cost-transit/manifest-carrier-cost-transit-create/manifest-carrier-cost-transit-create.component';
import { ManifestCarrierCostTransitUpdateComponent } from './manifest-carrier-master/manifest-carrier-cost-transit/manifest-carrier-cost-transit-update/manifest-carrier-cost-transit-update.component';
import { ManifestCarrierCostTransitDeleteComponent } from './manifest-carrier-master/manifest-carrier-cost-transit/manifest-carrier-cost-transit-delete/manifest-carrier-cost-transit-delete.component';
import { LastManifestComponent } from './manifest-carrier/last-manifest/last-manifest.component';
import { MasterCityComponent } from './master-city/master-city/master-city.component';
import { MasterCityCreateComponent } from './master-city/master-city-create/master-city-create.component';
import { MasterCityDeleteComponent } from './master-city/master-city-delete/master-city-delete.component';
import { FlightRouteComponent } from './manifest-carrier-master/flight-route/flight-route.component';
import { MappingAreaNoExistsComponent } from './mapping-area-no-exists/mapping-area-no-exists.component';
import { MappingAreaNoExistsCreateComponent } from './mapping-area-no-exists/mapping-area-no-exists-create/mapping-area-no-exists-create.component';
import { MappingAreaNoExistsDeleteComponent } from './mapping-area-no-exists/mapping-area-no-exists-delete/mapping-area-no-exists-delete.component';
import { MasterStatistikComponent } from './statistik/master-statistik/master-statistik.component';
import { MonitoringCapacityComponent } from './monitoring-capacity/monitoring-capacity.component';
import { MonitoringCapacityInlandSubComponent } from './monitoring-capacity/monitoring-capacity-inland-sub/monitoring-capacity-inland-sub.component';
import { ManifestCarrierMasterVendorComponent } from './manifest-carrier-master/manifest-carrier-master-vendor/manifest-carrier-master-vendor.component';
import { ManifestCarrierMasterVendorTransitComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-transit/manifest-carrier-master-vendor-transit.component';
import { ManifestCarrierMasterVendorCreateComponent } from './manifest-carrier-master/manifest-carrier-master-vendor/manifest-carrier-master-vendor-create/manifest-carrier-master-vendor-create.component';
import { ManifestCarrierMasterVendorDeleteComponent } from './manifest-carrier-master/manifest-carrier-master-vendor/manifest-carrier-master-vendor-delete/manifest-carrier-master-vendor-delete.component';
import { MCMasterVendorTransitDeleteComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-transit/mc-master-vendor-transit-delete/mc-master-vendor-transit-delete.component';
import { MCMasterVendorTransitCreateComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-transit/mc-master-vendor-transit-create/mc-master-vendor-transit-create.component';
import { ManifestCarrierMasterVendorOriginComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-origin/manifest-carrier-master-vendor-origin.component';
import { ManifestCarrierMasterVendorCarrierComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-carrier/manifest-carrier-master-vendor-carrier.component';
import { ManifestCarrierMasterVendorOriginCreateComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-origin/manifest-carrier-master-vendor-origin-create/manifest-carrier-master-vendor-origin-create.component';
import { ManifestCarrierMasterVendorOriginDeleteComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-origin/manifest-carrier-master-vendor-origin-delete/manifest-carrier-master-vendor-origin-delete.component';
import { MCMasterVendorCarrierCreateComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-carrier/mc-master-vendor-carrier-create/mc-master-vendor-carrier-create.component';
import { MCMasterVendorCarrierDeleteComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-carrier/mc-master-vendor-carrier-delete/mc-master-vendor-carrier-delete.component';
import { ManifestCarrierMasterFlightComponent } from './manifest-carrier-master/manifest-carrier-master-flight/manifest-carrier-master-flight.component';
import { ManifestCarrierMasterCarrierComponent } from './manifest-carrier-master/manifest-carrier-master-carrier/manifest-carrier-master-carrier.component';
import { ManifestCarrierMasterFlightCreateComponent } from './manifest-carrier-master/manifest-carrier-master-flight/manifest-carrier-master-flight-create/manifest-carrier-master-flight-create.component';
import { ManifestCarrierMasterFlightDeleteComponent } from './manifest-carrier-master/manifest-carrier-master-flight/manifest-carrier-master-flight-delete/manifest-carrier-master-flight-delete.component';
import { ManifestCarrierMasterCarrierCreateComponent } from './manifest-carrier-master/manifest-carrier-master-carrier/manifest-carrier-master-carrier-create/manifest-carrier-master-carrier-create.component';
import { ManifestCarrierMasterCarrierDeleteComponent } from './manifest-carrier-master/manifest-carrier-master-carrier/manifest-carrier-master-carrier-delete/manifest-carrier-master-carrier-delete.component';
import { FlightRouteCreateComponent } from './manifest-carrier-master/flight-route/flight-route-create/flight-route-create.component';
import { FlightRouteDeleteComponent } from './manifest-carrier-master/flight-route/flight-route-delete/flight-route-delete.component';
import { FlightRouteUpdateComponent } from './manifest-carrier-master/flight-route/flight-route-update/flight-route-update.component';
import { TransitCreateComponent } from './manifest-carrier-master/master-transit/transit-create/transit-create.component';
import { TransitDeleteComponent } from './manifest-carrier-master/master-transit/transit-delete/transit-delete.component';
import { HistoryNotPrintComponent } from './history-not-print/history-not-print.component';
import { MonitoringShipmentComponent } from './monitoring-shipment/monitoring-shipment.component';
import { MonitoringShipmentCreateComponent } from './monitoring-shipment/monitoring-shipment-create/monitoring-shipment-create.component';
import { MonitoringShipmentUpdateComponent } from './monitoring-shipment/monitoring-shipment-update/monitoring-shipment-update.component';
import { MonitoringShipmentDeleteComponent } from './monitoring-shipment/monitoring-shipment-delete/monitoring-shipment-delete.component';
import { ManifestCarrierMasterStationComponent } from './manifest-carrier-master/manifest-carrier-master-station/manifest-carrier-master-station.component';
import { ManifestCarrierMasterStationCityComponent } from './manifest-carrier-master/manifest-carrier-master-station-city/manifest-carrier-master-station-city.component';
import { ManifestCarrierMasterStationCreateComponent } from './manifest-carrier-master/manifest-carrier-master-station/manifest-carrier-master-station-create/manifest-carrier-master-station-create.component';
import { ManifestCarrierMasterStationDeleteComponent } from './manifest-carrier-master/manifest-carrier-master-station/manifest-carrier-master-station-delete/manifest-carrier-master-station-delete.component';
import { ManifestCarrierMasterStationCityDeleteComponent } from './manifest-carrier-master/manifest-carrier-master-station-city/manifest-carrier-master-station-city-delete/manifest-carrier-master-station-city-delete.component';
import { ManifestCarrierMasterStationCityCreateComponent } from './manifest-carrier-master/manifest-carrier-master-station-city/manifest-carrier-master-station-city-create/manifest-carrier-master-station-city-create.component';
import { ManifestCarrierMasterVendorDestComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-dest/manifest-carrier-master-vendor-dest.component';
import { ManifestCarrierMasterVendorDestCreateComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-dest/manifest-carrier-master-vendor-dest-create/manifest-carrier-master-vendor-dest-create.component';
import { ManifestCarrierMasterVendorDestDeleteComponent } from './manifest-carrier-master/manifest-carrier-master-vendor-dest/manifest-carrier-master-vendor-dest-delete/manifest-carrier-master-vendor-dest-delete.component';
import { LabelPrintComponent } from './spb/label-print/label-print.component';
import { ResetPasswordComponent } from './privilege-user/reset-password/reset-password.component';
import { MasterCustomerCodeComponent } from './master-customer-code/master-customer-code.component';
import { MasterCustomerCodeCreateComponent } from './master-customer-code/master-customer-code-create/master-customer-code-create.component';
import { MasterCustomerCodeDeleteComponent } from './master-customer-code/master-customer-code-delete/master-customer-code-delete.component';
import { MasterPorterCodeComponent } from './master-porter-code/master-porter-code.component';
import { MasterPorterCodeCreateComponent } from './master-porter-code/master-porter-code-create/master-porter-code-create.component';
import { MasterPorterCodeDeleteComponent } from './master-porter-code/master-porter-code-delete/master-porter-code-delete.component';
import { SpbUpdateComponent } from './spb-update/spb-update.component';
import { SpbScansComponent } from './spb-scans/spb-scans.component';
import { MasterRoundComponent } from './master-round/master-round.component';
import { MasterDividedComponent } from './master-divided/master-divided.component';
import { MasterRoundCreateComponent } from './master-round/master-round-create/master-round-create.component';
import { MasterRoundDeleteComponent } from './master-round/master-round-delete/master-round-delete.component';
import { MasterDividedCreateComponent } from './master-divided/master-divided-create/master-divided-create.component';
import { MasterDividedDeleteComponent } from './master-divided/master-divided-delete/master-divided-delete.component';
import { CoaCodeComponent } from './finance/coa-code/coa-code/coa-code.component';
import { CoaCodeCreateComponent } from './finance/coa-code/coa-code-create/coa-code-create.component';
import { BukuKasComponent } from './finance/buku-kas/buku-kas/buku-kas.component';
import { BukuKasCreateComponent } from './finance/buku-kas/buku-kas-create/buku-kas-create.component';
import { BukuKasPrintComponent } from './finance/buku-kas/buku-kas-print/buku-kas-print.component';
import { AccountReceivableComponent } from './finance/account-receivable/account-receivable/account-receivable.component';
import { NeracaComponent } from './finance/neraca/neraca.component';
import { GeneralLedgerComponent } from './finance/general-ledger/general-ledger.component';
import { GeneralLedgerDatagridComponent } from './finance/general-ledger/general-ledger-datagrid/general-ledger-datagrid.component';
import { AccountReceivableCreateComponent } from './finance/account-receivable/account-receivable-create/account-receivable-create.component';
import { ExpensesFixComponent } from './finance/expenses/expenses-fix/expenses-fix.component';
import { ExpensesFixCreateComponent } from './finance/expenses/expenses-fix-create/expenses-fix-create.component';
import { CostComponent } from './finance/cost/cost/cost.component';
import { AccountPayableComponent } from './finance/account-payable/account-payable/account-payable.component';
import { CoaArManagementComponent } from './finance/coa-code/coa-ar-management/coa-ar-management.component';
import { CoaArManagementCreateComponent } from './finance/coa-code/coa-ar-management-create/coa-ar-management-create.component';
import { SalesReportComponent } from './finance/sales-report/sales-report/sales-report.component';
import { ProfitLostStatementListComponent } from './finance/profit-loss-statement/profit-lost-statement-list/profit-lost-statement-list.component';
import { ProfitLostStatementMatTabComponent } from './finance/profit-loss-statement/profit-lost-statement-mat-tab/profit-lost-statement-mat-tab.component';
import { ProfitLostStatementPrintPerDayComponent } from './finance/profit-loss-statement/profit-lost-statement-print-per-day/profit-lost-statement-print-per-day.component';
import { CoaAccuredExpensesManagementComponent } from './finance/coa-code/coa-accured-expenses-management/coa-accured-expenses-management.component';
import { CoaAccuredExpensesManagementCreateComponent } from './finance/coa-code/coa-accured-expenses-management-create/coa-accured-expenses-management-create.component';

@NgModule({
	declarations: [
		MyPageComponent, 
		AddRoleCityComponent,
		AddUserComponent,
		CalculateTunaiAsalPipe,
		CalculateTunaiTujuanPipe,
		CreateManifestCarrierComponent,
		CustomerComponent,
		DeleteRoleCityComponent,
		DetailCostManifestCarrierComponent,
		DetailManifestCarrierComponent,
		DetailPrivilegeSettingComponent,
		DetailRoleListComponent,
		GoodsComponent,
		GoodsMonitoringComponent,
		GoodsMonitoringDatagridComponent,
		GoodsMonitoringReceiverListComponent,
		HistoryAksiComponent,
		HistoryLoginComponent,
		HistoryPrintComponent,
		ListRevisiManifestCarrierComponent,
		LocationComponent,
		ManifestCarrierComponent,
		ManifestCarrierReportComponent,
		ManifestChangeKoliComponent,
		ManifestListReceiverComponent,
		ManifestPrintComponent,
		ManifestSenderComponent,
		MasterDataCustomerComponent,
		MasterDataCustomerDetailComponent,
		MasterDataCustomerReceiverComponent,
		MasterDataCustomerSenderComponent,
		PriceComponent,
		PrivilegeCreateComponent,
		PrivilegeListComponent,
		PrivilegeMasterDetailListComponent,
		PrivilegeMasterListComponent,
		PrivilegeMenuComponent,
		PrivilegeSettingComponent,
		PrivilegeUpdateSettingComponent,
		PrivilegeUserComponent,
		PrivilegeUserListComponent,
		RatesComponent,
		RatesDatagridComponent,
		ReadManifestCarrierComponent,
		RevisiManifestCarrierComponent,
		RoleCreateComponent,
		RoleListComponent,
		RoleUserListComponent,
		RoleUserSettingComponent,
		ServiceTypeComponent,
		SpbCreateComponent,
		SpbHourlyListComponent,
		SpbHourlyReceiverListComponent,
		SpbListComponent,
		SpbListReceiverComponent,
		SpbPrintComponent,
		SpbPrintV2Component,
		SpbStatisticInputComponent,
		SuperVisionApprovalFocComponent,
		SuperVisionApprovalKiloComponent,
		SuperVisionApprovalReprintComponent,
		SuperVisionApprovalSpbComponent,
		SuperVisionApprovalVoidComponent,
		SuperVisionComponent,
		SuperVisionCreateComponent,
		SuperVisionCreateFocComponent,
		SuperVisionCreateKiloComponent,
		SuperVisionCreateReprintComponent,
		SuperVisionCreateSpbComponent,
		SuperVisionCreateVoidComponent,
		SuperVisionHistoryComponent,
		SuperVisionListComponent,
		TransitListComponent,
		TransitCreateComponent,
		TransitDeleteComponent,
		TransitUpdateComponent,
		UnitPlaceCreateComponent,
		UnitSubbranchPlaceCreateComponent,
		UpdatePasswordComponent,
		UpdateRolePrivilegeComponent,
		UpdateUnitBranchComponent,
		UpdateUnitCompanyComponent,
		UpdateUnitSubbranchComponent,
		UpdateUnitSubbranchUserComponent,
		UpdateUnitUserComponent,
		UserDetailComponent,
		UserListComponent,
		UserRoleListComponent,
		UserSettingComponent,
		MasterAgreementComponent,
		MasterAgreementDetailComponent,
		MasterMethodPaymentComponent,
		MasterMethodPaymentCreateComponent,
		MasterMethodPaymentDeleteComponent,
		MasterModaCityComponent,
		MasterModaCityCreateComponent,
		MasterModaCityDeleteComponent,
		MasterMethodPaymentDeleteComponent,
		MasterAgreementDetailComponent,
		ManifestCarrierCostCarrierComponent,
		ManifestCarrierCostOriginComponent,
		ManifestCarrierCostInlandOriginComponent,
		ManifestCarrierCostDestinationComponent,
		ManifestCarrierCostGeraiComponent,
		ManifestCarrierCostPenerusComponent,
		ManifestCarrierCostTransitComponent,
		ManifestCarrierCostGeraiCreateComponent,
		ManifestCarrierCostGeraiUpdateComponent,
		ManifestCarrierCostGeraiDeleteComponent,
		ManifestCarrierCostCarrierDeleteComponent,
		ManifestCarrierCostCarrierCreateComponent,
		ManifestCarrierCostCarrierUpdateComponent,
		ManifestCarrierCostDestinationUpdateComponent,
		ManifestCarrierCostDestinationCreateComponent,
		ManifestCarrierCostDestinationDeleteComponent,
		ManifestCarrierCostInlandOriginDeleteComponent,
		ManifestCarrierCostInlandOriginUpdateComponent,
		ManifestCarrierCostInlandOriginCreateComponent,
		ManifestCarrierCostOriginCreateComponent,
		ManifestCarrierCostOriginUpdateComponent,
		ManifestCarrierCostOriginDeleteComponent,
		ManifestCarrierCostPenerusDeleteComponent,
		ManifestCarrierCostPenerusUpdateComponent,
		ManifestCarrierCostPenerusCreateComponent,
		ManifestCarrierCostTransitCreateComponent,
		ManifestCarrierCostTransitUpdateComponent,
		ManifestCarrierCostTransitDeleteComponent,
		LastManifestComponent,
		MasterCityComponent,
		MasterCityCreateComponent,
		MasterCityDeleteComponent,
		FlightRouteComponent,
		MappingAreaNoExistsComponent,
		MappingAreaNoExistsCreateComponent,
		MappingAreaNoExistsDeleteComponent,
		MasterStatistikComponent,
		MappingAreaNoExistsDeleteComponent,
		MonitoringCapacityComponent,
		MonitoringCapacityInlandSubComponent,
		ManifestCarrierMasterVendorComponent,
		ManifestCarrierMasterVendorTransitComponent,
		ManifestCarrierMasterVendorCreateComponent,
		ManifestCarrierMasterVendorDeleteComponent,
		MCMasterVendorTransitDeleteComponent,
		MCMasterVendorTransitCreateComponent,
		ManifestCarrierMasterVendorOriginComponent,
		ManifestCarrierMasterVendorCarrierComponent,
		ManifestCarrierMasterVendorOriginCreateComponent,
		ManifestCarrierMasterVendorOriginDeleteComponent,
		MCMasterVendorCarrierCreateComponent,
		MCMasterVendorCarrierDeleteComponent,
		ManifestCarrierMasterFlightComponent,
		ManifestCarrierMasterCarrierComponent,
		ManifestCarrierMasterFlightCreateComponent,
		ManifestCarrierMasterFlightDeleteComponent,
		ManifestCarrierMasterCarrierCreateComponent,
		ManifestCarrierMasterCarrierDeleteComponent,
		FlightRouteCreateComponent,
		FlightRouteDeleteComponent,
		FlightRouteUpdateComponent,
		HistoryNotPrintComponent,
		MappingAreaNoExistsDeleteComponent,
		MonitoringShipmentComponent,
		MonitoringShipmentCreateComponent,
		MonitoringShipmentUpdateComponent,
		MonitoringShipmentDeleteComponent,
		ManifestCarrierMasterStationComponent,
		ManifestCarrierMasterStationCityComponent,
		ManifestCarrierMasterStationCreateComponent,
		ManifestCarrierMasterStationDeleteComponent,
		ManifestCarrierMasterStationCityDeleteComponent,
		ManifestCarrierMasterStationCityCreateComponent,
		ManifestCarrierMasterVendorDestComponent,
		ManifestCarrierMasterVendorDestCreateComponent,
		ManifestCarrierMasterVendorDestDeleteComponent,
		LabelPrintComponent,
		ResetPasswordComponent,
		MasterCustomerCodeComponent,
		MasterCustomerCodeCreateComponent,
		MasterCustomerCodeDeleteComponent,
		MasterPorterCodeComponent,
		MasterPorterCodeCreateComponent,
		MasterPorterCodeDeleteComponent,
		SpbUpdateComponent,
		SpbScansComponent,
		MasterRoundComponent,
		MasterDividedComponent,
		MasterRoundCreateComponent,
		MasterRoundDeleteComponent,
		MasterDividedCreateComponent,
		MasterDividedDeleteComponent,
		CoaCodeComponent,
		CoaCodeCreateComponent,
		BukuKasComponent,
		BukuKasCreateComponent, 
		BukuKasPrintComponent,
		AccountReceivableComponent,
		NeracaComponent,
		GeneralLedgerComponent,
		GeneralLedgerDatagridComponent,
		AccountReceivableCreateComponent,
		ExpensesFixComponent,
		ExpensesFixCreateComponent,
		CostComponent,
		AccountPayableComponent,
		CoaArManagementComponent,
		CoaArManagementCreateComponent,
		SalesReportComponent,
		ProfitLostStatementListComponent,
		ProfitLostStatementMatTabComponent,
		ProfitLostStatementPrintPerDayComponent,
		CoaAccuredExpensesManagementComponent,
		CoaAccuredExpensesManagementCreateComponent 

	],
	exports: [],
	imports: [
		CommonModule,
		HttpClientModule,
		FormsModule,
		CoreModule,
		PartialsModule,
		MailModule,
		ECommerceModule,
		UserManagementModule,
		MatDialogModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatRadioModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		DxButtonModule,
		DxDataGridModule, 
		DxFormModule, 
		DxTextAreaModule, 
		DxTextBoxModule, 
		DxSwitchModule, 
		DxNumberBoxModule,
		DxValidatorModule, 
		DxValidationSummaryModule, 
		DxCheckBoxModule, 
		DxSelectBoxModule, 
		DxAutocompleteModule, 
		DxRadioGroupModule, 
		ReactiveFormsModule,
		DxDropDownButtonModule, 
		DxDateBoxModule, 
		DxScrollViewModule,
		DxPopupModule, 
		DxDropDownBoxModule, 
		DxPopoverModule, 
		DxPivotGridModule,
		DataTablesModule, 
		MatTabsModule
	],
	providers: [],
	entryComponents: [SpbPrintComponent, ManifestPrintComponent, SpbPrintV2Component, LabelPrintComponent,BukuKasPrintComponent]
})
export class PagesModule {
}
