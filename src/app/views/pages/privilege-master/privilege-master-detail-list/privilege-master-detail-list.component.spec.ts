import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegeMasterDetailListComponent } from './privilege-master-detail-list.component';

describe('PrivilegeMasterDetailListComponent', () => {
  let component: PrivilegeMasterDetailListComponent;
  let fixture: ComponentFixture<PrivilegeMasterDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegeMasterDetailListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegeMasterDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
