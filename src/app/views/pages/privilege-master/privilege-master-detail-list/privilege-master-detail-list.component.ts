import { Component, OnInit , Input } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import { DxDataGridModule } from "devextreme-angular";

@Component({
  selector: 'kt-privilege-master-detail-list',
  templateUrl: './privilege-master-detail-list.component.html',
  styleUrls: ['./privilege-master-detail-list.component.scss']
})
export class PrivilegeMasterDetailListComponent implements OnInit {

	@Input()
	privilegeId: number = 0;

	ViewDetailListDs: CustomStore;
	SubDetailPrivilege = false;

	httpOptions = {
		headers: new HttpHeaders({
		  'Content-Type': 'application/json'
			})
	  };
	  
	constructor(private http: HttpClient, private matDialog: MatDialog) { }
	

	ngOnInit() {
		// console.log("-----------------------------------------");
		// console.log(this.privilegeId);
		this.readDetailList(this.privilegeId);
		// console.log("-----------------------------------------");
	}

	ngOnChanges() {
		// console.log(this.privilegeId);
		this.readDetailList(this.privilegeId);
	}   

	readDetailList(pId){
		const params = { 
			pId:  pId
		};

		this.ViewDetailListDs = new CustomStore({
			key: 'privilegeId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-master-detail/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	onRowPrepared(e) {
		if (e.rowType === 'data' && e.data.isVoid === true) {
			e.rowElement.style.backgroundColor = 'orange';
			e.rowElement.className = e.rowElement.className.replace('dx-row-alt', '');
		}
	}
}
