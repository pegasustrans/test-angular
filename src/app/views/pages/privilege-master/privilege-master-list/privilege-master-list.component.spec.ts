import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegeMasterListComponent } from './privilege-master-list.component';

describe('PrivilegeMasterListComponent', () => {
  let component: PrivilegeMasterListComponent;
  let fixture: ComponentFixture<PrivilegeMasterListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegeMasterListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegeMasterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
