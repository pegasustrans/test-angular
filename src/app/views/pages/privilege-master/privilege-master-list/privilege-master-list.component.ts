import {Component, AfterContentInit, OnInit, HostListener, ViewChild} from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import {Privilege, PrivilegeMasterListService} from './privilege-master-list.service';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { PrivilegeMenuComponent } from '../privilege-menu/privilege-menu.component';
import { PrivilegeUserComponent } from '../privilege-user/privilege-user.component';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';

@Component({
  selector: 'kt-privilege-master-list',
//   template: `<router-outlet></router-outlet>`,
  templateUrl: './privilege-master-list.component.html',
  styleUrls: ['./privilege-master-list.component.scss']
})
export class PrivilegeMasterListComponent implements OnInit {

  @ViewChild('userData', {static: false}) userData: PrivilegeUserComponent;
  @ViewChild('menuData', {static: false}) menuData: PrivilegeMenuComponent;
  
//   currentPrivilege: Privilege = new Privilege();

  public forbid = false;
  public privilege_data_list = true;

  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;
  privilegeListDs: CustomStore;
  privilegeNameDs: CustomStore;
//   DetailPopupVisible = false;


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
		})
  };
  
  constructor(private http: HttpClient, private matDialog: MatDialog, private privilegeMasterListService: PrivilegeMasterListService, private router: Router) {
	  console.log(this.router.events);
	// this.router.events.subscribe((event: Event) => {
	// 	// if (event instanceof NavigationStart) {
	// 	// 	// Show loading indicator
	// 	// 	console.log("Start : "+event.url);
	// 	// }

	// 	if (event instanceof NavigationEnd) {
	// 		// Hide loading indicator
	// 		console.log("End : "+event.urlAfterRedirects);
	// 		// this.historyClose();
	// 	}
	// 	this.router.resetConfig;

	// 	// if (event instanceof NavigationError) {
	// 	// 	// Hide loading indicator
	// 	// 	console.log("Error : "+event.url);

	// 	// 	// Present error to user
	// 	// 	console.log(event.error);
	// 	// }
	// });
   }

	searchForm = {
		privilegeId: null,
	};

	HistoryForm = {
		menu: "Privilege",
	};

	ubahDataPrivilegePopUp = false;

	ngOnInit() {
		this.historyLogin();
		this.GetPrivilegeList();
		this.listPrivilegeName();
	}

	// @HostListener('window:beforeunload', ['$event'])
	// unloadHandler(event: Event) {
	// 	// Your logic on beforeunload
	// 	this.historyClose();
	// }

// 	@HostListener('window:beforeunload', ['$event'])
// 		onWindowClose(event: any): void {
// 		// Do something

// 		this.historyClose();
	
// 		event.preventDefault();
// 		event.returnValue = false;
//    }

	// @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
	// 	let result = confirm("Apa Kamu Yakin ?");
	// 		if (result) {
	// 		// Do more processing...
	// 		this.historyClose();
	// 	}
	// 	return result;
	// 	// event.returnValue = false; // stay on same page
	// }

	// @HostListener("window:unload", ["$event"]) unloadHandler(event: Event) {
	// 	let result = confirm("Apa Kamu Yakin ?");
	// 	if (result) {
	// 	// Do more processing...
	// 	this.historyClose();
	// 	}
	// 	event.returnValue = false; // stay on same page
	// }

//    @HostListener('window:unload', [ '$event' ])
// 	unloadHandler(event) {
// 		this.historyClose();
 
// 	}

// 	@HostListener('window:beforeunload', [ '$event' ])
// 	beforeUnloadHandler(event) {
// 		alert('beforeunload');
// 	}

	
	GetPrivilegeList(){
		const params = {
		};

		this.privilegeListDs = new CustomStore({
			key: 'privilegeId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-master/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						if(data.data != null){
							return {
								data: data.data
							};
						}else{
							this.forbid = true;
							this.privilege_data_list = false;
						}
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	ubahDataPrivilege(){
		this.ubahDataPrivilegePopUp = true;
	}

	listPrivilegeName() {
		const params = {
		};

		this.privilegeNameDs = new CustomStore({
			key: 'privilegeId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-master/list-privilege', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	searchData(){
		this.userData.GetList(this.searchForm.privilegeId);
		this.menuData.GetList(this.searchForm.privilegeId);
	}

	historyLogin(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/create', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyClose(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/close', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	
	
}
