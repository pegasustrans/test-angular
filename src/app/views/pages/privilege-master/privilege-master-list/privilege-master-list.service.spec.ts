import { TestBed } from '@angular/core/testing';

import { PrivilegeMasterListService } from './privilege-master-list.service';

describe('PrivilegeMasterListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrivilegeMasterListService = TestBed.get(PrivilegeMasterListService);
    expect(service).toBeTruthy();
  });
});
