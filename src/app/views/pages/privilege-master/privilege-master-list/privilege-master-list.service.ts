import { Injectable } from '@angular/core';

export class Privilege {
  PrivilegeId: number;
  PrivilegeName: string;
}

export class State {
  ID: number;
  Name: string;
}

@Injectable({
  providedIn: 'root'
})
export class PrivilegeMasterListService {

  constructor() { }

}
