import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-privilege-menu',
  templateUrl: './privilege-menu.component.html',
  styleUrls: ['./privilege-menu.component.scss']
})
export class PrivilegeMenuComponent implements OnInit {

  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;
  privilegeMenuListDs: CustomStore;
  menuActionListDs: CustomStore;
  privilegeNameDs: CustomStore;
  menuNameDs: CustomStore;
  menuActionDs: CustomStore;
  rowData : any;
  rowDataAction : any;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
		})
  };

  detailPopUp = {
		privilegeId : null,
		menuId      : null
	};

  formAddMenu = {
		privilegeId   : null,
		privilegeName : null,
		menuId        : null
	};

  formAddMenuAction = {
		privilegeId   : null,
		menuId        : null,
		menuActionId  : null
	};

  formDelete = {
    privilegeName : null,
		menuName      : null,
		menuActionName: null,
		privilegeId   : null,
		menuId        : null,
		menuActionId  : null
	};

  constructor(private http: HttpClient) { }

  @ViewChild('targetDataGrid',{static: true}) dataGrid: DxDataGridComponent;

  viewDataMenuActionPopUp = false;
  addDataMenuPopUp = false;
  addDataMenuActionPopUp = false;
  deleteDataMenuActionPopUp = false;

  ngOnInit() {
  }

  GetList(privilegeId) {
    this.detailPopUp.privilegeId = privilegeId;
    const params = {
      privilegeId : privilegeId
		};

		this.privilegeMenuListDs = new CustomStore({
			key: 'menuId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-menu/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
              data: data.data
            };
					})
					.catch(error => { console.log(error); });
			}
		});
	}
  
  GetDataMenuActionList(dp){
    const params = {
      menuId      : dp.menuId,
      privilegeId : dp.privilegeId,
		};

		this.menuActionListDs = new CustomStore({
			key: 'menuActionId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-menu-action/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
              data: data.data
            };
					})
					.catch(error => { console.log(error); });
			}
		});
  }

  actionCLick(e) {
		this.rowData = e.itemData.data;
		if (e.itemData.key === 'view_data') { this.ViewDataMenu(e.itemData.data); }
		// if (e.itemData.key === 'delete_data') { this.DeleteDataMenu(e.itemData.data); }
	}

  actionMenuActionCLick(e) {
		this.rowDataAction = e.itemData.data;
    // console.log(e.itemData.key);
		if (e.itemData.key === 'delete_data_action') { this.DeleteDataMenuAction(e.itemData.data); }
	}

  ViewDataMenu(item){
    this.detailPopUp.menuId = item.menuId;
    this.viewDataMenuActionPopUp = true;
    this.GetDataMenuActionList(this.detailPopUp);
  }

  DeleteDataMenuAction(item){
    this.formDelete.menuName        = item.menuName;
    this.formDelete.menuActionName  = item.menuAction;
    this.formDelete.privilegeName   = item.privilegeName;
    this.formDelete.menuId        = item.menuId;
    this.formDelete.menuActionId  = item.menuActionId;
    this.formDelete.privilegeId   = item.privilegeId;
    this.deleteDataMenuActionPopUp  = true;

  }

  // addDataMenu(){
  //   this.listPrivilegeName();
  //   this.listMenu();
  //   this.addDataMenuPopUp = true;
  // }
  addDataMenuAction(){
    this.listPrivilegeName();
    this.listMenu();
    
    this.addDataMenuActionPopUp = true;
  }

  listPrivilegeName() {
		const params = {
		};

		this.privilegeNameDs = new CustomStore({
			key: 'privilegeId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-master/list-privilege', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  listMenu() {
		const params = {
		};

		this.menuNameDs = new CustomStore({
			key: 'menuId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-menu/list-menu', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});

    
	}

  onTriggerChange(e){
    this.listMenuAction(e.value);
	}

  listMenuAction(menuId) {
		const params = {
      menuId      : menuId
		};

		this.menuActionDs = new CustomStore({
			key: 'menuActionId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-menu-action/list-menu-action', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  createMenuActionSubmit(e) {
      var pid = this.formAddMenuAction.privilegeId;

      console.log(pid);
      if(this.formAddMenuAction.privilegeId == null){
        notify({
          message: 'Privilege Name Harus Di isi terlebih dahulu',
          type: 'error',
          displayTime: 5000,
          width: 400,
        });
      }else if(this.formAddMenuAction.menuId == null){
        notify({
          message: 'Menu Harus Di isi terlebih dahulu',
          type: 'error',
          displayTime: 5000,
          width: 400,
        });
      }else if(this.formAddMenuAction.menuActionId == null){
        notify({
          message: 'Menu Action Harus Di isi terlebih dahulu',
          type: 'error',
          displayTime: 5000,
          width: 400,
        });
      }else{

          const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
          this.http
            .post(AppGlobalVar.BASE_API_URL + 'privilege-menu-action/create', JSON.stringify(this.formAddMenuAction), config)
            .subscribe((data: any) => {

                if(data.status == 200){
                  notify({
                    message: 'Insert Privilege Menu Action Berhasil ',
                    type: 'success',
                    displayTime: 5000,
                    width: 400
                  });
                }else{
                  notify({
                    message: 'Insert Privilege Menu Action Gagal',
                    type: 'error',
                    displayTime: 5000,
                    width: 400,
                  });
                }
            
                
              },
              (error) => {
                alert(error.statusText + '. ' + error.message);
              }
            );

        }
      this.GetList(pid);
      this.addDataMenuActionPopUp = false;
      this.viewDataMenuActionPopUp = false;
  }

  deleteMenuActionSubmit(e) {
    console.log(e);
    console.log(this.formDelete);

    var pid = this.formDelete.privilegeId;

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'privilege-menu-action/delete', JSON.stringify(this.formDelete), config)
      .subscribe((data: any) => {

        console.log(data);
          if(data.status == 200){
            notify({
              message: 'Delete Privilege Menu Action Berhasil ',
              type: 'success',
              displayTime: 5000,
              width: 400
            });
          }else{
            notify({
              message: 'Delete Privilege Menu Action Gagal',
              type: 'error',
              displayTime: 5000,
              width: 400,
            });
          }
      
          
        },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
      );

    this.GetList(pid);
    this.GetDataMenuActionList(this.formDelete);
    this.deleteDataMenuActionPopUp = false;
}

}
