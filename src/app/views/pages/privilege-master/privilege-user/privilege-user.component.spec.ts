import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegeUserComponent } from './privilege-user.component';

describe('PrivilegeUserComponent', () => {
  let component: PrivilegeUserComponent;
  let fixture: ComponentFixture<PrivilegeUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegeUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegeUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
