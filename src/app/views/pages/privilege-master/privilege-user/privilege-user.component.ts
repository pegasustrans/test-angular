import {Component, OnInit, Input, ViewChild} from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-privilege-user',
  templateUrl: './privilege-user.component.html',
  styleUrls: ['./privilege-user.component.scss']
})
export class PrivilegeUserComponent implements OnInit {

  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;
  privilegeUserListDs: CustomStore;
  privilegeNameDs: CustomStore;
  usernameDs: CustomStore;
  updateUserPrivilegePopupVisible = false;
  rowData : any;

  formChange = {
		userId      : null,
		username    : null,
		privilegeId : null
	};
 
  formAddUserPrivilege = {
		userId      : null,
		privilegeId : null
	};

  formState = {
    privilegeId : null
  }


  addDataUserPrivilegePopUp = false;


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
		})
  };

  constructor(private http: HttpClient) { }

  @ViewChild('targetDataGrid',{static: true}) dataGrid: DxDataGridComponent;

  ngOnInit() {
  }

  GetList(privilegeId) {
    // console.log(privilegeId);
    this.formState.privilegeId = privilegeId;

    const params = {
      privilegeId : privilegeId
		};

		this.privilegeUserListDs = new CustomStore({
			key: 'userId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-user/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
              data: data.data
            };
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  actionCLick(e) {
		this.rowData = e.itemData.data;
		if (e.itemData.key === 'update_user_privilege') { this.UpdateDataUser(e.itemData.data); }
	}

  UpdateDataUser(e){
   this.formChange.username = e.username;
   this.formChange.userId = e.userId;
   this.formChange.privilegeId = null;
   this.listPrivilegeName();
   this.updateUserPrivilegePopupVisible = true;
  }

  listPrivilegeName() {
		const params = {
		};

		this.privilegeNameDs = new CustomStore({
			key: 'privilegeId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-master/list-privilege', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  listUserName() {
		const params = {
		};

		this.usernameDs = new CustomStore({
			key: 'userId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'privilege-master/list-user', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

  createSubmit(e) {
     var pid = this.formState.privilegeId;
  
      if(this.formChange.privilegeId == null){
        notify({
          message: 'Privilege Name Harus Di isi terlebih dahulu',
          type: 'error',
          displayTime: 5000,
          width: 400,
        });
      }else{
        const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'privilege-user/update', JSON.stringify(this.formChange), config)
          .subscribe((data: any) => {

              if(data.status == 200){
                notify({
                  message: 'Update Privilege User Berhasil ',
                  type: 'success',
                  displayTime: 5000,
                  width: 400
                });
              }else{
                notify({
                  message: 'Update Privilege User Gagal',
                  type: 'error',
                  displayTime: 5000,
                  width: 400,
                });
              }
          
              
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
          );
      }

      this.GetList(pid);
      this.updateUserPrivilegePopupVisible = false;
  }

  addDataUser(){
    
    this.formAddUserPrivilege.userId = null;
    this.formAddUserPrivilege.privilegeId = null;
    this.listPrivilegeName();
    this.listUserName();
    this.addDataUserPrivilegePopUp = true;
  }

  createUserSubmit(e) {
    var pid = this.formAddUserPrivilege.privilegeId;
 
    if(this.formAddUserPrivilege.userId == null){
      notify({
        message: 'Username Harus Di isi terlebih dahulu',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    }else if(this.formAddUserPrivilege.privilegeId == null){
      notify({
        message: 'Privilege Name Harus Di isi terlebih dahulu',
        type: 'error',
        displayTime: 5000,
        width: 400,
      });
    }else{

        const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'privilege-user/create', JSON.stringify(this.formAddUserPrivilege), config)
          .subscribe((data: any) => {

              if(data.status == 200){
                notify({
                  message: 'Insert Privilege User Berhasil ',
                  type: 'success',
                  displayTime: 5000,
                  width: 400
                });
              }else{
                notify({
                  message: 'Insert Privilege User Gagal',
                  type: 'error',
                  displayTime: 5000,
                  width: 400,
                });
              }
          
              
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
          );

      }
     this.GetList(pid);
     this.addDataUserPrivilegePopUp = false;
 }

}
