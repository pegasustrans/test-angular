import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';
import { I } from '@angular/cdk/keycodes';

@Component({
  selector: 'kt-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }
 
  ds: any = {};
  menuPriv = false;
  menuForb = true;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  HistoryForm = {
		menu			: "Reset Password",
		historyloginId 	: 0
	};

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.historyLoginStart();
	  this.checkuserGlobalVar();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

    private async checkuserGlobalVar() {
      if(UserGlobalVar.USER_ID === '') {
        setTimeout(() => {
          this.checkuserGlobalVar();
        }, 1000);
      }
    
      else{		
        var checkPriv = 0;
        await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
          if (value == "reset-password"){
            checkPriv = 1;
          }
        });
    
        if (checkPriv == 1){
          this.menuPriv = true;
          this.menuForb = false;
          
          this.GetList();
        }
      }
    }
  
    GetList() {
      const params = {}; 
      this.ds = new CustomStore({
        key: 'userId',
        load:  (loadOptions: any) => {
          return this.http.get(AppGlobalVar.BASE_API_URL + 'user-setting/list-user-password', { headers: this.httpOptions.headers, params  })
            .toPromise()
            .then((data: any) => {
              return {
                data: data.data
                
              };
            })
            .catch(error => { console.log(error); });
        }
      });
    }
  
    resetPassword(){
      const body = {};
		  this.http
      .post(AppGlobalVar.BASE_API_URL + 'user-setting/reset-password',  body, this.httpOptions)
      .subscribe((data: any) => {
        // console.log('res : ',data.data[0].result);
        this.GetList();
        },
        (error) => {
          alert(error.statusText + '. ' + error.message);
        }
      );
    }

}
