import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnInit} from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import {BrowserModule} from '@angular/platform-browser';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {DxSelectBoxModule,
        DxCheckBoxModule,
        DxTextBoxModule,
        DxDateBoxModule,
        DxButtonModule,
        DxValidatorModule,
        DxValidationSummaryModule} from 'devextreme-angular';
import notify from 'devextreme/ui/notify';

// const sendRequest = function(value) {
//   const validEmail = "test@dx-email.com";
//   return new Promise((resolve) => {
//       setTimeout(function() {
//           resolve(value === validEmail);
//       }, 1000);
//   });    
// }

@Component({
  selector: 'kt-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

  public new_password = false;
  passwordMode: string;
  passwordButton: any;

  constructor(private http: HttpClient) { 
    this.passwordMode = 'password';
    this.passwordButton = {
      icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAB7klEQVRYw+2YP0tcQRTFz65xFVJZpBBS2O2qVSrRUkwqYfUDpBbWQu3ELt/HLRQ/Q8RCGxVJrRDEwj9sTATxZ/Hugo4zL/NmV1xhD9xi59177pl9986fVwLUSyi/tYC+oL6gbuNDYtyUpLqkaUmfJY3a+G9JZ5J2JW1J2ivMDBSxeWCfeBxYTHSOWMcRYLOAEBebxtEVQWPASQdi2jgxro4E1YDTQIJjYM18hszGbew4EHNq/kmCvgDnHtI7YBko58SWgSXg1hN/btyFBM0AlwExczG1YDZrMS4uLUeUoDmgFfjLGwXEtG05wNXyTc4NXgzMCOAIGHD8q0ATuDZrempkwGJ9+AfUQ4K+A/eEseqZ/UbgdUw4fqs5vPeW+5mgBvBAPkLd8cPju+341P7D/WAaJGCdOFQI14kr6o/zvBKZYz11L5Okv5KGA89Kzu9K0b0s5ZXt5PjuOL6TRV5ZalFP4F+rrnhZ1Cs5vN6ijmn7Q162/ThZq9+YNW3MbfvDAOed5cxdGL+RFaUPKQtjI8DVAr66/u9i6+jJzTXm+HFEVqxVYBD4SNZNKzk109HxoycPaG0bIeugVDTp4hH2qdXJDu6xOAAWiuQoQdLHhvY1aEZSVdInG7+Q9EvSz9RrUKqgV0PP3Vz7gvqCOsUj+CxC9LB1Dc8AAAASdEVYdEVYSUY6T3JpZW50YXRpb24AMYRY7O8AAAAASUVORK5CYII=',
      type: 'default',
      onClick: () => {
        this.passwordMode = this.passwordMode === 'text' ? 'password' : 'text';
      },
    };
  }

  appGlobalVar  = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  formPassword = {
		currPassword     : null
	};

  formChange = {
		email           : null,
		currentPassword : null,
		newPassword     : null
	};

  HistoryForm = {
		menu			: "Update Password",
		historyloginId 	: 0
	};
    passwordComparison = () => {
        return this.formChange.newPassword;
    };
    checkComparison() {
        return true;
    }
    // asyncValidation(params) {
    //     return sendRequest(params.value);
    // }
    
  ngOnInit() {
    this.getEmailPassword();
    this.historyLoginStart();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  
  checkPassword(){
    if(this.formChange.currentPassword == this.formPassword.currPassword){
      this.new_password = true;
    }else{
      this.new_password = false;
          notify({
            message: 'Maaf Password salah',
            type: 'error',
            displayTime: 5000,
            width: 400,
          });
    }
  }
  
  getEmailPassword(){
    this.http.get(AppGlobalVar.BASE_API_URL + 'user-setting/list-email-password').subscribe((response: any) =>{
        response.data.forEach( (item, index) => {
          this.formChange.email = item.email;
          this.formPassword.currPassword = item.password;
        //  this.formChange.currentPassword = item.password;
        } );
			});
  }

  createSubmit(e) {

    var rx = /^[a-zA-Z0-9_]+$/;

    console.log(rx.test(this.formChange.newPassword));
    if (rx.test(this.formChange.newPassword) == false){
        notify({
          message: 'Maaf Format Password tidak boleh di isi dengan tanda baca',
          type: 'error',
          displayTime: 5000,
          width: 400,
        });
    }else{
        var re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
        if (re.test(this.formChange.newPassword) == false){
          notify({
            message: 'Maaf Format Password harus minimal 6 karakter, dan berupa campuran dari angka dan number',
            type: 'error',
            displayTime: 5000,
            width: 400,
          });
        }else{
          const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
          this.http
            .post(AppGlobalVar.BASE_API_URL + 'user-setting/update', JSON.stringify(this.formChange), config)
            .subscribe((data: any) => {
              this.new_password = false;
              notify({
                message: 'Update Password Berhasil...',
                type: 'success',
                displayTime: 5000,
                width: 400,
              });
              
                
              },
              (error) => {
                alert(error.statusText + '. ' + error.message);
              }
            );
        }
    }
    
	}

}
