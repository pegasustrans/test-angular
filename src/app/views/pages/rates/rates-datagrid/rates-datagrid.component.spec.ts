import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatesDatagridComponent } from './rates-datagrid.component';

describe('RatesDatagridComponent', () => {
  let component: RatesDatagridComponent;
  let fixture: ComponentFixture<RatesDatagridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatesDatagridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatesDatagridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
