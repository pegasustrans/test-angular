import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import DataSource from 'devextreme/data/data_source';
import notify from 'devextreme/ui/notify';
import { HighlightProcess } from '../../../../core/helper';

@Component({
  selector: 'kt-rates-datagrid',
  templateUrl: './rates-datagrid.component.html',
  styleUrls: ['./rates-datagrid.component.scss']
})

export class RatesDatagridComponent implements OnInit {

	highlightProcess = HighlightProcess;

	destCity: DataSource;
	originCity: DataSource;

 	ds							: CustomStore;
	cityDs						: CustomStore;

	togDs						: any;
	kosDs						: any;
	qosDs						: any;
	sdDs						: any;
	carrierDs					: any;
	destinationSubDistrictDs	: any;
	ratesTypeDs					: any;

	currentRates 				: any;
	deleteRates 				: any;
	newRates 					: any;

	destCityData 				: any;
	originCityData				: any;

	editTarifPopupVisible 		= false;
	addTarifPopupVisible 		= false;
	hapusTarifPopupVisible 		= false;

	min 						= false;
	flat 						= true;
	minVisible 					= false;

	formSearch = {
		originCity 				: null,
		destinationCity 		: null,
		destinationSubDistrict 	: null,
		ratesType 				: null,
		moda 					: null,
		kos 					: null,
		qos 					: null,
		rates 					: null,
		minWeight 				: null,
		minRates 				: null
	  };


	formAddRates = {
		NewRates				: null
		, OriginCity			: null
		, DestinationCity		: null
		, SubDistrict			: null
		, Qos					: null
		, Kos					: null
		, Carrier				: null
		, RatesType				: null
		, Min					: null
		, Max					: null
		, MinRates				: null
	};

  constructor(private http: HttpClient) { }

  ngOnInit() {
    // this.getTog();
	this.getDestCity();
	this.getOriginCity();
	this.getCarrier();
	this.getQos();
	this.getKos();
	this.getRatesType();
  }

  httpOptions = {
		headers: new HttpHeaders({
		'Content-Type': 'application/json'
		})
  };
  
  ChooseAction = (e) => {
    if(e.itemData.key == "update"){
       this.editTarif(e.itemData.data);
    }
	if(e.itemData.key == "hapus"){
		this.hapusTarif(e.itemData.data);
	 }
  }

  tipeTarif(e){
	if (e.value == "flat"){
		this.minVisible = false;
	  }

	if (e.value == "flatmin" || e.value == "flatminspb"){
		  this.minVisible = true;
	}
  }

  hapusTarif(e){
    this.deleteRates = e;
	
    this.hapusTarifPopupVisible = true;
  }

  hapusTarifSubmit(e){
	const body = { 
		ratesId: this.deleteRates.ratesId,
		subDistrict: this.deleteRates.subDistrict

	};

	this.http
		   .post(AppGlobalVar.BASE_API_URL + 'rates/delete',  body, this.httpOptions)
		   .subscribe((data: any) => {
				   if (data != null) {
					   notify({
						   message: 'Berhasil hapus tarif.',
						   type: 'success',
						   displayTime: 5000,
						   width: 400
					   });
					   location.reload();
				   }

				   if (data == null) {
					   notify({
						   message: 'Gagal menghapus tarif.',
						   type: 'warning',
						   displayTime: 5000,
						   width: 800
					   });
				   }
			   },
			   (error) => {
				   alert(error.statusText + '. ' + error.message);
			   }
		   );

   this.hapusTarifPopupVisible = false;
 }




  editTarif(e){
    this.currentRates = e;
	if (this.currentRates.ratesType == 'FLATMIN' || this.currentRates.ratesType == 'FLATMINSPB'){
		this.min = true;
		this.flat = false;
	}
	if (this.currentRates.ratesType == 'FLAT'){
		this.min = false;
		this.flat = true;
	}
    this.editTarifPopupVisible = true;
  }

  editTarifSubmit(e){
	const body = { 
		RatesId			: this.currentRates.ratesId
		, NewRates		: this.currentRates.rates
		, Min			: this.currentRates.min
		, Max			: this.currentRates.max
		, MinRates		: this.currentRates.minRates
		, subDistrict	: this.currentRates.subDistrict
	};

     this.http
			.post(AppGlobalVar.BASE_API_URL + 'rates/update',  body, this.httpOptions)
			.subscribe((data: any) => {
					if (data != null) {
						notify({
							message: 'Update tarif dari ' + this.currentRates.originCity + ' ke  ' +this.currentRates.destinationCity + ' berhasil.',
							type: 'success',
							displayTime: 5000,
							width: 400
						});
					}

					if (data == null) {
						notify({
							message: 'Gagal mengupdate tarif',
							type: 'warning',
							displayTime: 5000,
							width: 800
						});
					}
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				}
			);

		this.editTarifPopupVisible = false;

  }

  addTarif(){
	this.addTarifPopupVisible = true;
	this.getDestCity();
	this.getOriginCity();

	// this.GetCityList();
  }

  addTarifSubmit(e){
     this.http
			.post(AppGlobalVar.BASE_API_URL + 'rates/create',  this.formAddRates, this.httpOptions)
			.subscribe((data: any) => {
					if (data != null) {
						notify({
							message: 'Penambahan tarif berhasil.',
							type: 'success',
							displayTime: 5000,
							width: 400
						});
						// location.reload();
						this.GetList();
					}

					if (data == null) {
						notify({
							message: 'Gagal menambahkan tarif',
							type: 'warning',
							displayTime: 5000,
							width: 800
						});
					}
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				}
			);

	this.addTarifPopupVisible = false;
  }

  GetList() {
    const params = {
		originCity		: this.formSearch.originCity,
		destinationCity	: this.formSearch.destinationCity,
		subDistrict		: this.formSearch.destinationSubDistrict,
		ratesType		: this.formSearch.ratesType,
		moda			: this.formSearch.moda,
		kos				: this.formSearch.kos,
		qos				: this.formSearch.qos,
		rates			: this.formSearch.rates,
		minWeight		: this.formSearch.minWeight,
		minRates		: this.formSearch.minRates,
    }; 
    this.ds = new CustomStore({
			key: 'ratesId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'rates/read', { headers: this.httpOptions.headers, params})
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	// GetCityList() {
	// 	const params = {
	// 	  //createdAt: this.formSearch.dateString
	// 	}; 
	// 	this.cityDs = new CustomStore({
	// 			key: 'companyCityId',
	// 			load:  (loadOptions: any) => {
	// 				return this.http.get(AppGlobalVar.BASE_API_URL + 'rates/readCity', { headers: this.httpOptions.headers, params})
	// 					.toPromise()
	// 					.then((data: any) => {
	// 						return {
	// 							data: data.data
	// 						};
	// 					})
	// 					.catch(error => { console.log(error); });
	// 			}
	// 		});
	// 	}
	getDestCity() {
		this.destCity = new DataSource({
			store: new CustomStore({
				key: 'cityId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'company/city')
						.toPromise()
						.then((result: any) => {
							this.destCityData = result.data;
							return result.data;
						});
				}
			})
		});
	}

	getOriginCity() {
		this.originCity = new DataSource({
			store: new CustomStore({
				key: 'cityId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'company/city')
						.toPromise()
						.then((result: any) => {
							this.originCityData = result.data;
							return result.data;
						});
				}
			})
		});
	}

  ColRatesType(rowData) {
		if (rowData.ratesType == 'FLATMIN'){
			return 'MIN PER KOLI'
		}

		if (rowData.ratesType == 'FLATMINSPB'){
			return 'MIN PER SPB'
		}

    if (rowData.ratesType == 'FLAT'){
			return 'FLAT'
		}
	}

	getTog(){
		const body = {
			LookUpKeyParent : 'tog'
		};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'lookup-list/read',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.togDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);

	}

	getQos(){
		const body = {
			LookUpKeyParent : 'qos'
		};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'lookup-list/read',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.qosDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);

	}

	getKos(){
		const body = {
			LookUpKeyParent : 'tos'
		};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'lookup-list/read',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.kosDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);

	}

	getCarrier(){
		const body = {
			LookUpKeyParent : 'carrier'
		};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'lookup-list/read',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.carrierDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);

	}

	getRatesType(){
		const body = {
			LookUpKeyParent : 'rates_type'
		};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'lookup-list/read',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.ratesTypeDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);

	}

	OnRatesChange(e){
		const params = {
			cityId : e.value
		};

		this.formAddRates.SubDistrict = null;
		this.sdDs = null;

		this.http
				.get(AppGlobalVar.BASE_API_URL + 'rates/list-sub-district', { headers: this.httpOptions.headers, params})
				.subscribe((data: any) => {
						this.sdDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);

	}

	onSearch(){
		this.GetList();
	}
}
