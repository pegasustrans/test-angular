import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';
import { RatesDatagridComponent } from './rates-datagrid/rates-datagrid.component';
import { forEach } from 'jszip';

@Component({
  selector: 'kt-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss']
})
export class RatesComponent implements OnInit {
  @HostListener('window:beforeunload', ["$event"]) unload(event) {
    this.historyLoginEnd();
 }

	@ViewChild('allData', {static: false}) allData: RatesDatagridComponent;

  ds: any = {};
  privilegeListDs: CustomStore;

  permit = true;
  forbid = false;
  

  HistoryForm = {
		menu: "Tarif",
    historyloginId 	: 0
	};

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
		})
  };

  constructor(private http: HttpClient) { }

  ngOnInit() {
    // this.allData.ngOnInit();
    // this.CheckPriv();
		this.checkuserGlobalVar();

    this.historyLoginStart();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

 async checkuserGlobalVar() {
		if(await UserGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}

		else{		
			let count = 0;

      if  (UserGlobalVar.MENU_ACTION_LOGIN!=null){
        await UserGlobalVar.MENU_ACTION_LOGIN.forEach(function(value){
          if (value == '13~list'){
            count+=1;
          }
        });
      }

			if (count == 1){
				this.permit = true;
				this.forbid = false;
			}

      else if (count == 0){
        this.forbid = true;
				this.permit = false;
      }
		}
	}

  // CheckPriv(){
  //   let count = 0;

  //   if  (UserGlobalVar.MENU_ACTION_LOGIN!=null){
  //     UserGlobalVar.MENU_ACTION_LOGIN.forEach(function(value){
  //       if (value == '13~list'){
  //         count+=1;
  //       }
  //     });
  //   }

  //   if (count>0){return true;}

	// 	else {return false;}
		// if  (UserGlobalVar.MENU_ACTION_LOGIN_NAME!=null){
		// 	UserGlobalVar.MENU_ACTION_LOGIN_NAME.forEach(function(value){
    //     console.log(value)
		// 	});
		// }

    // if  (UserGlobalVar.MENU_ACTION_ID_LOGIN!=null){
		// 	UserGlobalVar.MENU_ACTION_ID_LOGIN.forEach(function(value){
    //     console.log(value)
		// 	});
		// }

    // if  (UserGlobalVar.MENU_KEY_LOGIN!=null){
		// 	UserGlobalVar.MENU_KEY_LOGIN.forEach(function(value){
    //     console.log(value)
		// 	});
		// }

    // if  (UserGlobalVar.MENU_KEY_LOGIN_NAME!=null){
		// 	UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
    //     console.log(value)
		// 	});
		// }
	// }

}
