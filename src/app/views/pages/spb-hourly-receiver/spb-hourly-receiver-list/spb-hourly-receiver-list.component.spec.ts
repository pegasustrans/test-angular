import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbHourlyReceiverListComponent } from './spb-hourly-receiver-list.component';

describe('SpbHourlyReceiverListComponent', () => {
  let component: SpbHourlyReceiverListComponent;
  let fixture: ComponentFixture<SpbHourlyReceiverListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbHourlyReceiverListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbHourlyReceiverListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
