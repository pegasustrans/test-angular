import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
	selector: 'kt-spb-hourly-receiver-list',
	templateUrl: './spb-hourly-receiver-list.component.html',
	styleUrls: ['./spb-hourly-receiver-list.component.scss']
})
export class SpbHourlyReceiverListComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	// --- --- ---
	// variable
	// --- --- ---

	// form
	searchForm = {
		date: new Date(),
		dateString: null,
	};

	HistoryForm = {
		menu: "Monitoring Barang Receiver",
		historyloginId:0
	};

	// data store
	spbListDs: CustomStore;

	// column visible
	cBranchOrigin: boolean;
	cBranchDestination: boolean;
	c12: boolean;
	c14: boolean;
	c1530: boolean;
	c16: boolean;
	cFinal: boolean;

	menuPriv = false;
	menuForb = true;	

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};


	constructor(private http: HttpClient) { }

	ngOnInit() {

		this.checkuserGlobalVar();
		this.historyLoginStart();

		// show all column
		this.cBranchOrigin = false;
		this.cBranchDestination = false;
		this.c12 = true;
		this.c14 = true;
		this.c1530 = true;
		this.c16 = true;
		this.cFinal = true;
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "spb-hourly-receiver"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;
			
			}
		}
	}
	
	onFormSubmit(e) {
		if (this.searchForm.date !== null) {
			this.searchForm.dateString = this.searchForm.date.getFullYear() + '-' + (this.searchForm.date.getMonth() + 1) + '-' + this.searchForm.date.getDate();
		}

		// this.spbListDs = new CustomStore({
		// 	key: 'manifestId',
		// 	load:  (loadOptions: any) => {
		// 		return this.http.get(AppGlobalVar.BASE_API_URL + 'spb-hourly-receiver/list?dateString=' + this.searchForm.dateString)
		// 			.toPromise()
		// 			.then((data: any) => {
		// 				return {
		// 					data: data.data
		// 				};
		// 			})
		// 			.catch(error => { console.log(error); });
		// 	}
		// });

		const params = {
			createdAt: this.searchForm.dateString
			, type : 'receiver'
		}; 
		this.spbListDs = new CustomStore({
			key: 'manifestId',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring/monitoring-barang', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						console.log(data.data);
						
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});


	}

	ColManifestNo(rowData) {
		return rowData.destination + ' ' + rowData.alphabet + ' # ' + rowData.manifestId;
	}

	thousandSeparator(data) {
		return data.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	}


	showHide(e, column) {
		if (column === 'branchOrigin') { this.cBranchOrigin = !this.cBranchOrigin; }
		if (column === 'branchDestination') { this.cBranchDestination = !this.cBranchDestination; }
		if (column === '12') { this.c12 = !this.c12; }
		if (column === '14') { this.c14 = !this.c14; }
		if (column === '1530') { this.c1530 = !this.c1530; }
		if (column === '16') { this.c16 = !this.c16; }
		if (column === 'final') { this.cFinal = !this.cFinal; }
	}

}
