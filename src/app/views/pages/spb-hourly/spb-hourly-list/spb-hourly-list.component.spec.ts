import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbHourlyListComponent } from './spb-hourly-list.component';

describe('SpbHourlyListComponent', () => {
  let component: SpbHourlyListComponent;
  let fixture: ComponentFixture<SpbHourlyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbHourlyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbHourlyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
