import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbScansComponent } from './spb-scans.component';

describe('SpbScansComponent', () => {
  let component: SpbScansComponent;
  let fixture: ComponentFixture<SpbScansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbScansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbScansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
