import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-spb-scans',
  templateUrl: './spb-scans.component.html',
  styleUrls: ['./spb-scans.component.scss']
})
export class SpbScansComponent implements OnInit {

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  formCreate = {
    spbNo   : null
  }
  
  menuPriv = false;
  menuForb = true;

  HistoryForm = {
		menu			: "SPB Edit - Scans",
		historyloginId 	: 0
	};
  
  ngOnInit() {
    this.historyLoginStart();
	  this.checkuserGlobalVar();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

  private async checkuserGlobalVar() {
    if(UserGlobalVar.USER_ID === '') {
      setTimeout(() => {
        this.checkuserGlobalVar();
      }, 1000);
    }
  
    else{		
      var checkPriv = 0;
      await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
        if (value == "spb-edit"){
          checkPriv = 1;
        }
      });
  
      if (checkPriv == 1){
        this.menuPriv = true;
        this.menuForb = false;
      }
    }
  }
  

  valueChanged(e){
    console.log(this.formCreate.spbNo);
    this.router.navigate(['/spb-update'], 
        { 
            queryParams: { 
               p1: this.formCreate.spbNo
            } 
        });    
  }

}
