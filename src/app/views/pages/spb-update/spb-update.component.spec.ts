import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbUpdateComponent } from './spb-update.component';

describe('SpbUpdateComponent', () => {
  let component: SpbUpdateComponent;
  let fixture: ComponentFixture<SpbUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
