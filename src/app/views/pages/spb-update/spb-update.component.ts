import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import Swal from 'sweetalert2';
import { AppGlobalVar } from '../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../core/globalvar/userGlobalVar';
import { SpbPrintV2Component } from '../spb/spb-print-v2/spb-print-v2.component';

@Component({
  selector: 'kt-spb-update',
  templateUrl: './spb-update.component.html',
  styleUrls: ['./spb-update.component.scss']
})
export class SpbUpdateComponent implements OnInit {

  spbNumber : any;
  carrier: any;
  carrierCode: any;
  description: any;
  destinationAreaCode: any;
  destinationAreaName: any;
  destinationCityCode: any;
  destinationCityName: any;
  discount: any;
  etc: any;
  originAreaCode: any;
  originAreaName: any;
  originCityCode: any;
  originCityName: any;
  packing: any;
  paymentMethod: any;
  ppn: 0;
  qualityOfService: any;
  quarantine: 0;
  rates: 0;
  spbId: any;
  totalPrice: 0;
  typeOfService: any;
  typesOfGoods: any;
  ppnValue: 0;
  discountValue: 0;
  PpymentMethod: any;
  senderPhone: any;
  senderName: any;
  senderStore: any;
  senderPlace: any;
  senderAddress: any;
  receiverPhone: any;
  receiverName: any;
  receiverStore: any;
  receiverPlace: any;
  receiverAddress: any;
  viaName: any;
  spbCawFinal: 0;
  spbTotalKoli: any;
  ratesType: any;
  
  ratesTS: any;
  totalPriceTS: 0;
  ppnValueTS: any;
  discountValueTS: any;
  spbCawFinalTS: any;
  packingTS: any;
  quarantineTS: any;
  etcTS: any;

  firstVisible  = false;
  secondVisible = false;
  thirdVisible  = false;
  fourthVisible = false;
  fifthVisible  = false;
  
  public innerWidth: any = window.innerWidth;
	public innerHeight: any = window.innerHeight;
	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.innerWidth = window.innerWidth;
		this.innerHeight = window.innerHeight;
	}


  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  HistoryForm = {
		menu			: "SPB Edit - Update Spb",
		historyloginId 	: 0
	};

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router, private matDialog: MatDialog) { }

  formCreate = {
    spbId                 : null

    , first_spbGoodsId    : 0
    , first_aw            : 0
    , first_caw           : 0
    , first_height        : 0
    , first_width         : 0
    , first_length        : 0
    , first_vw            : 0
    , first_cawFinal      : 0
    , first_koliNo        : 0
    , first_totalKoli     : 0
    , first_totalAw       : 0
    , first_min           : 0
    , first_minRates      : 0
    , first_price         : 0

    , second_spbGoodsId   : 0
    , second_aw           : 0
    , second_caw          : 0
    , second_height       : 0
    , second_width        : 0
    , second_length       : 0
    , second_vw           : 0
    , second_cawFinal     : 0
    , second_koliNo       : 0
    , second_totalKoli    : 0
    , second_totalAw      : 0
    , second_min          : 0
    , second_minRates     : 0
    , second_price        : 0

    , third_spbGoodsId    : 0
    , third_aw            : 0
    , third_caw           : 0
    , third_height        : 0
    , third_width         : 0
    , third_length        : 0
    , third_vw            : 0
    , third_cawFinal      : 0
    , third_koliNo        : 0
    , third_totalKoli     : 0
    , third_totalAw       : 0
    , third_min           : 0
    , third_minRates      : 0
    , third_price         : 0

    , fourth_spbGoodsId   : 0
    , fourth_aw           : 0
    , fourth_caw          : 0
    , fourth_height       : 0
    , fourth_width        : 0
    , fourth_length       : 0
    , fourth_vw           : 0
    , fourth_cawFinal     : 0
    , fourth_koliNo       : 0
    , fourth_totalKoli    : 0
    , fourth_totalAw      : 0
    , fourth_min          : 0
    , fourth_minRates     : 0
    , fourth_price        : 0

    , fifth_spbGoodsId    : 0
    , fifth_aw            : 0
    , fifth_caw           : 0
    , fifth_height        : 0
    , fifth_width         : 0
    , fifth_length        : 0
    , fifth_vw            : 0
    , fifth_cawFinal      : 0
    , fifth_koliNo        : 0
    , fifth_totalKoli     : 0
    , fifth_totalAw       : 0
    , fifth_min           : 0
    , fifth_minRates      : 0
    , fifth_price         : 0
  }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        
        this.spbNumber = params.p1;
        this.GetDataForm(params.p1);
        this.GetGoodsDataForm(params.p1);        
      }
    );

    this.historyLoginStart();
  }

  ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}


  GetDataForm(spbNo){
    const params = {
      spbNo : spbNo
    }
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'spb-update/spb-data-form', params, config)
      .subscribe((data: any) => {
        
        var dataDetail = data.data;
        dataDetail.forEach((item) => {
          // this.manifestCarrierNo              = item.manifestCarrierNo;
          // this.formDetail.periode_start       = item.fromSendDate;
          this.carrier             = item.carrier;
          this.carrierCode         = item.carrierCode;
          this.description         = item.description;
          this.destinationAreaCode = item.destinationAreaCode;
          this.destinationAreaName = item.destinationAreaName;
          this.destinationCityCode = item.destinationCityCode;
          this.destinationCityName = item.destinationCityName;
          this.discount            = item.discount;
          this.etc                 = item.etc;
          this.etcTS               = this.thousandSeparator(item.etc);
          this.originAreaCode      = item.originAreaCode;
          this.originAreaName      = item.originAreaName;
          this.originCityCode      = item.originCityCode;
          this.originCityName      = item.originCityName;
          this.packing             = item.packing;
          this.packingTS           = this.thousandSeparator(item.packing);
          this.paymentMethod       = item.paymentMethod;
          this.ppn                 = item.ppn;
          this.qualityOfService    = item.qualityOfService;
          this.quarantine          = item.quarantine;
          this.quarantineTS        = this.thousandSeparator(item.quarantine);
          this.rates               = item.rates;
          this.ratesTS             = this.thousandSeparator(item.rates);
          this.spbId               = item.spbId;
          this.totalPrice          = item.totalPrice;
          this.totalPriceTS        = this.thousandSeparator(item.totalPrice)
          this.typeOfService       = item.typeOfService;
          this.typesOfGoods        = item.typesOfGoods;          
          this.ppnValue            = item.ppnValue;       
          this.discountValue       = item.discountValue;         
          this.ppnValueTS          = this.thousandSeparator(item.ppnValue);      
          this.discountValueTS     = this.thousandSeparator(item.discountValue);  
          this.PpymentMethod       = item.PpymentMethod;  
          this.senderPhone         = item.senderPhone;    
          this.senderName          = item.senderName;     
          this.senderStore         = item.senderStore;    
          this.senderPlace         = item.senderPlace;    
          this.senderAddress       = item.senderAddress;  
          this.receiverPhone       = item.receiverPhone;  
          this.receiverName        = item.receiverName;   
          this.receiverStore       = item.receiverStore;  
          this.receiverPlace       = item.receiverPlace;  
          this.receiverAddress     = item.receiverAddress;
          this.viaName             = item.viaName;    
          this.spbCawFinal         = item.spbCawFinal;    
          this.spbCawFinalTS       = this.thousandSeparator(item.spbCawFinal);    
          this.spbTotalKoli        = item.spbTotalKoli;    
          this.ratesType           = item.ratesType;    
          
          this.formCreate.spbId    = item.spbId;          
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );

    // console.log(this.spbCawFinal);
    
  }

  GetGoodsDataForm(spbNo){  
    const params = {
      spbNo : spbNo
    }
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'spb-update/spb-goods-data-form', params, config)
      .subscribe((data: any) => {
        
        var dataDetail = data.data;
        var ix = 1;
        dataDetail.forEach((item) => {
         
          if(ix == 1){
            this.firstVisible = true;

            this.formCreate.first_spbGoodsId    = item.spbGoodsId;
            this.formCreate.first_aw            = item.aw;
            this.formCreate.first_caw           = item.caw;
            this.formCreate.first_height        = item.height;
            this.formCreate.first_width         = item.width;
            this.formCreate.first_length        = item.length;
            this.formCreate.first_vw            = item.vw;
            this.formCreate.first_cawFinal      = item.cawFinal;
            this.formCreate.first_koliNo        = item.koliNo;
            this.formCreate.first_totalKoli     = item.totalKoli;
            this.formCreate.first_totalAw       = item.totalAw;
            this.formCreate.first_min           = item.min;
            this.formCreate.first_minRates      = item.minRates;
            this.formCreate.first_price         = item.price;
          }

          if(ix == 2){
            this.secondVisible = true;
            this.formCreate.second_spbGoodsId    = item.spbGoodsId;
            this.formCreate.second_aw            = item.aw;
            this.formCreate.second_caw           = item.caw;
            this.formCreate.second_height        = item.height;
            this.formCreate.second_width         = item.width;
            this.formCreate.second_length        = item.length;
            this.formCreate.second_vw            = item.vw;
            this.formCreate.second_cawFinal      = item.cawFinal;
            this.formCreate.second_koliNo        = item.koliNo;
            this.formCreate.second_totalKoli     = item.totalKoli;
            this.formCreate.second_totalAw       = item.totalAw;
            this.formCreate.second_min           = item.min;
            this.formCreate.second_minRates      = item.minRates;
            this.formCreate.second_price         = item.price;
          }

          if(ix == 3){
            this.thirdVisible = true;
            this.formCreate.third_spbGoodsId    = item.spbGoodsId;
            this.formCreate.third_aw            = item.aw;
            this.formCreate.third_caw           = item.caw;
            this.formCreate.third_height        = item.height;
            this.formCreate.third_width         = item.width;
            this.formCreate.third_length        = item.length;
            this.formCreate.third_vw            = item.vw;
            this.formCreate.third_cawFinal      = item.cawFinal;
            this.formCreate.third_koliNo        = item.koliNo;
            this.formCreate.third_totalKoli     = item.totalKoli;
            this.formCreate.third_totalAw       = item.totalAw;
            this.formCreate.third_min           = item.min;
            this.formCreate.third_minRates      = item.minRates;
            this.formCreate.third_price         = item.price;
          }

          if(ix == 4){
            this.fourthVisible = true;
            this.formCreate.fourth_spbGoodsId    = item.spbGoodsId;
            this.formCreate.fourth_aw            = item.aw;
            this.formCreate.fourth_caw           = item.caw;
            this.formCreate.fourth_height        = item.height;
            this.formCreate.fourth_width         = item.width;
            this.formCreate.fourth_length        = item.length;
            this.formCreate.fourth_vw            = item.vw;
            this.formCreate.fourth_cawFinal      = item.cawFinal;
            this.formCreate.fourth_koliNo        = item.koliNo;
            this.formCreate.fourth_totalKoli     = item.totalKoli;
            this.formCreate.fourth_totalAw       = item.totalAw;
            this.formCreate.fourth_min           = item.min;
            this.formCreate.fourth_minRates      = item.minRates;
            this.formCreate.fourth_price         = item.price;
          }

          if(ix == 5){
            this.fifthVisible = true;
            this.formCreate.fifth_spbGoodsId    = item.spbGoodsId;
            this.formCreate.fifth_aw            = item.aw;
            this.formCreate.fifth_caw           = item.caw;
            this.formCreate.fifth_height        = item.height;
            this.formCreate.fifth_width         = item.width;
            this.formCreate.fifth_length        = item.length;
            this.formCreate.fifth_vw            = item.vw;
            this.formCreate.fifth_cawFinal      = item.cawFinal;
            this.formCreate.fifth_koliNo        = item.koliNo;
            this.formCreate.fifth_totalKoli     = item.totalKoli;
            this.formCreate.fifth_totalAw       = item.totalAw;
            this.formCreate.fifth_min           = item.min;
            this.formCreate.fifth_minRates      = item.minRates;
            this.formCreate.fifth_price         = item.price;
          }

          ix++;
          
        });
      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );

    // console.log(this.formCreate);
    
  }

  cekMinimumPerKoli(e, f){
    var totalKoli = 0;
    var aw        = e.target.value;
    var vw        = 0;

    if(f == 'first_aw'){
      this.formCreate.first_aw = e.target.value;

      totalKoli = this.formCreate.first_totalKoli;
      vw        = this.formCreate.first_vw;
    }

    if(f == 'second_aw'){
      this.formCreate.second_aw = e.target.value;
      
      totalKoli = this.formCreate.second_totalKoli;
      vw        = this.formCreate.second_vw;
    }

    if(f == 'third_aw'){
      this.formCreate.third_aw = e.target.value;
      
      totalKoli = this.formCreate.third_totalKoli;
      vw        = this.formCreate.third_vw;
    }

    if(f == 'fourth_aw'){
      this.formCreate.fourth_aw = e.target.value;

      totalKoli = this.formCreate.fourth_totalKoli;
      vw        = this.formCreate.fourth_vw;
    }

    if(f == 'fifth_aw'){
      this.formCreate.fifth_aw = e.target.value;
      
      totalKoli = this.formCreate.fifth_totalKoli;
      vw        = this.formCreate.fifth_vw;
    }

    // console.log(this.spbId);
    // console.log(totalKoli);
    // console.log(aw);
    // console.log(vw);
    // console.log(this.ratesType);
    // console.log(this.carrier);
 

    const params = {
      spbId     : this.spbId,
      totalKoli : totalKoli.toString(),
      aw        : aw.toString(),
      vw        : vw.toString(),
      ratesType : this.ratesType,
      carrier   : this.carrierCode
    }
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'spb-update/get-data-caw', params, config)
      .subscribe((data: any) => {
        
        var dataDetail = data.data;
        dataDetail.forEach((item) => {
          // console.log(item.caw);
          // console.log(item.cawFinal);
          if(f == 'first_aw'){
            this.formCreate.first_totalAw = item.caw;
            this.formCreate.first_cawFinal = item.cawFinal;
          }
      
          if(f == 'second_aw'){
            this.formCreate.second_totalAw = item.caw;
            this.formCreate.second_cawFinal = item.cawFinal;
          }
      
          if(f == 'third_aw'){
            this.formCreate.third_totalAw = item.caw;
            this.formCreate.third_cawFinal = item.cawFinal;
          }
      
          if(f == 'fourth_aw'){
            this.formCreate.fourth_totalAw = item.caw;
            this.formCreate.fourth_cawFinal = item.cawFinal;
          }
      
          if(f == 'fifth_aw'){
            this.formCreate.fifth_totalAw = item.caw;
            this.formCreate.fifth_cawFinal = item.cawFinal;
          }
           
          this.cawFinalSpb();
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  cekTotalKoli(e, f){
    if(f == "first"){
      this.formCreate.first_totalKoli = e.target.value;
    }

    if(f == "second"){
      this.formCreate.second_totalKoli = e.target.value;
    }

    if(f == "third"){
      this.formCreate.third_totalKoli = e.target.value;
    }

    if(f == "fourth"){
      this.formCreate.fourth_totalKoli = e.target.value;
    }

    if(f == "fifth"){
      this.formCreate.fifth_totalKoli = e.target.value;
    }
  }

  cekVolume(e, f){
    var length    = 0;
    var width     = 0;
    var height    = 0;
    var totalKoli = 0;
    var aw        = 0;

    
    if(f == 'first_length'){
      this.formCreate.first_length  = e.target.value;
      
      totalKoli = this.formCreate.first_totalKoli;
      aw        = this.formCreate.first_aw
      length    = this.formCreate.first_length;
      width     = this.formCreate.first_width;
      height    = this.formCreate.first_height;
    }

    if(f == 'first_width'){
      this.formCreate.first_width   = e.target.value;
      
      totalKoli = this.formCreate.first_totalKoli;
      aw        = this.formCreate.first_aw
      length    = this.formCreate.first_length;
      width     = this.formCreate.first_width;
      height    = this.formCreate.first_height;         
    }
    
    if(f == 'first_height'){
      this.formCreate.first_height = e.target.value;

      totalKoli = this.formCreate.first_totalKoli;
      aw        = this.formCreate.first_aw
      length    = this.formCreate.first_length;
      width     = this.formCreate.first_width;
      height    = this.formCreate.first_height;
    }

    //---------

    if(f == 'second_length'){
      this.formCreate.second_length = e.target.value;

      totalKoli = this.formCreate.second_totalKoli;
      aw        = this.formCreate.second_aw
      length    = this.formCreate.second_length;
      width     = this.formCreate.second_width;
      height    = this.formCreate.second_height;
    }

    if(f == 'second_width'){
      this.formCreate.second_width = e.target.value;

      totalKoli = this.formCreate.second_totalKoli;
      aw        = this.formCreate.second_aw
      length    = this.formCreate.second_length;
      width     = this.formCreate.second_width;
      height    = this.formCreate.second_height;
    }
    
    if(f == 'second_height'){
      this.formCreate.second_height = e.target.value;

      totalKoli = this.formCreate.second_totalKoli;
      aw        = this.formCreate.second_aw
      length    = this.formCreate.second_length;
      width     = this.formCreate.second_width;
      height    = this.formCreate.second_height;
    }

    //---------

    if(f == 'third_length'){
      this.formCreate.third_length = e.target.value;

      totalKoli = this.formCreate.third_totalKoli;
      aw        = this.formCreate.third_aw
      length    = this.formCreate.third_length;
      width     = this.formCreate.third_width;
      height    = this.formCreate.third_height;
    }

    if(f == 'third_width'){
      this.formCreate.third_width = e.target.value;

      totalKoli = this.formCreate.third_totalKoli;
      aw        = this.formCreate.third_aw
      length    = this.formCreate.third_length;
      width     = this.formCreate.third_width;
      height    = this.formCreate.third_height;
    }
    
    if(f == 'third_height'){
      this.formCreate.third_height = e.target.value;

      totalKoli = this.formCreate.third_totalKoli;
      aw        = this.formCreate.third_aw
      length    = this.formCreate.third_length;
      width     = this.formCreate.third_width;
      height    = this.formCreate.third_height;
    }

    //---------

    if(f == 'fourth_length'){
      this.formCreate.fourth_length = e.target.value;

      totalKoli = this.formCreate.fourth_totalKoli;
      aw        = this.formCreate.fourth_aw
      length    = this.formCreate.fourth_length;
      width     = this.formCreate.fourth_width;
      height    = this.formCreate.fourth_height;
    }

    if(f == 'fourth_width'){
      this.formCreate.fourth_width = e.target.value;

      totalKoli = this.formCreate.fourth_totalKoli;
      aw        = this.formCreate.fourth_aw
      length    = this.formCreate.fourth_length;
      width     = this.formCreate.fourth_width;
      height    = this.formCreate.fourth_height;
    }
    
    if(f == 'fourth_height'){
      this.formCreate.fourth_height = e.target.value;

      totalKoli = this.formCreate.fourth_totalKoli;
      aw        = this.formCreate.fourth_aw
      length    = this.formCreate.fourth_length;
      width     = this.formCreate.fourth_width;
      height    = this.formCreate.fourth_height;
    }

    //---------

    if(f == 'fifth_length'){
      this.formCreate.fifth_length = e.target.value;

      totalKoli = this.formCreate.fifth_totalKoli;
      aw        = this.formCreate.fifth_aw
      length    = this.formCreate.fifth_length;
      width     = this.formCreate.fifth_width;
      height    = this.formCreate.fifth_height;
    }

    if(f == 'fifth_width'){
      this.formCreate.fifth_width = e.target.value;

      totalKoli = this.formCreate.fifth_totalKoli;
      aw        = this.formCreate.fifth_aw
      length    = this.formCreate.fifth_length;
      width     = this.formCreate.fifth_width;
      height    = this.formCreate.fifth_height;
    }
    
    if(f == 'fifth_height'){
      this.formCreate.fifth_height = e.target.value;

      totalKoli = this.formCreate.fifth_totalKoli;
      aw        = this.formCreate.fifth_aw
      length    = this.formCreate.fifth_length;
      width     = this.formCreate.fifth_width;
      height    = this.formCreate.fifth_height;
    }


    // console.log('start dimensi');
    
    // console.log(length);
    // console.log(width);
    // console.log(height);
    // console.log(this.spbId);
    // console.log(totalKoli);
    // console.log(aw);
    // console.log(this.ratesType);
    // console.log(this.carrier);

    const params = {
      spbId     : this.spbId,
      totalKoli : totalKoli.toString(),
      aw        : aw.toString(),
      length    : length.toString(),
      width     : width.toString(),
      height    : height.toString(),
      ratesType : this.ratesType,
      carrier   : this.carrierCode
    }
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'spb-update/get-data-volume', params, config)
      .subscribe((data: any) => {
        
        var dataDetail = data.data;
        dataDetail.forEach((item) => {
          // console.log(f, item.volume);
          // console.log(item.cawFinal);
          if(f == 'first_length' || f == 'first_width' || f == 'first_height'){
            this.formCreate.first_vw = item.volume;
            this.formCreate.first_cawFinal = item.cawFinal;
          }
      
          if(f == 'second_length' || f == 'second_width' || f == 'second_height'){
            this.formCreate.second_vw = item.volume;
            this.formCreate.second_cawFinal = item.cawFinal;
          }
          if(f == 'third_length' || f == 'third_width' || f == 'third_height'){
            this.formCreate.third_vw = item.volume;
            this.formCreate.third_cawFinal = item.cawFinal;
          }
          if(f == 'fourth_length' || f == 'fourth_width' || f == 'fourth_height'){
            this.formCreate.fourth_vw = item.volume;
            this.formCreate.fourth_cawFinal = item.cawFinal;
          }
          if(f == 'fifth_length' || f == 'fifth_width' || f == 'fifth_height'){
            this.formCreate.fifth_vw = item.volume;
            this.formCreate.fifth_cawFinal = item.cawFinal;
          }

          this.cawFinalSpb();
                   
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  cawFinalSpb(){
      
    const params = {
      spbId     : this.spbId,
      first_cawFinal    : this.formCreate.first_cawFinal.toString(),
      second_cawFinal   : this.formCreate.second_cawFinal.toString(),
      third_cawFinal    : this.formCreate.third_cawFinal.toString(),
      fourth_cawFinal   : this.formCreate.fourth_cawFinal.toString(),
      fifth_cawFinal    : this.formCreate.fifth_cawFinal.toString(),
      rates             : this.rates.toString(),
      ppn               : this.ppn.toString(),
      discount          : this.discount.toString(),
      ratesType         : this.ratesType
    }
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'spb-update/get-data-caw-final-spb', params, config)
      .subscribe((data: any) => {
        
        var dataDetail = data.data;
        dataDetail.forEach((item) => {
          // console.log(item.cawFinalSpb);
          // console.log(item.discountValue);
          // console.log(item.ppnValue);
          // console.log(item.totalPrice);
         
          this.spbCawFinal    = item.cawFinalSpb;
          this.discountValue  = item.discountValue;
          this.ppnValue       = item.ppnValue;
          this.totalPrice     = item.totalPrice;    

          this.spbCawFinalTS    = this.thousandSeparator(item.cawFinalSpb);
          this.totalPriceTS     = this.thousandSeparator(item.totalPrice);    
          this.ppnValueTS       = this.thousandSeparator(item.ppnValue);      
          this.discountValueTS  = this.thousandSeparator(item.discountValue);   
          this.spbTotalKoli     = this.sumTotalKoli();             
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
  }

  sumTotalKoli(){
      var first_totalKoli  = parseInt(this.formCreate.first_totalKoli.toString());
      var second_totalKoli = parseInt(this.formCreate.second_totalKoli.toString());
      var third_totalKoli  = parseInt(this.formCreate.third_totalKoli.toString());
      var fourth_totalKoli = parseInt(this.formCreate.fourth_totalKoli.toString());
      var fifth_totalKoli  = parseInt(this.formCreate.fifth_totalKoli.toString());
      
      return first_totalKoli + second_totalKoli + third_totalKoli + fourth_totalKoli + fifth_totalKoli;
  }

  onUpdate(){
    const params = {
      spbId                 : this.spbId,
      totalPrice            : this.totalPrice.toString(),
      spbCawFinal           : this.spbCawFinal.toString(),
      rates                 : this.rates.toString(),
      carrierId             : this.carrierCode.toString(),

      first_spbGoodsId      : this.formCreate.first_spbGoodsId,
      first_totalKoli       : this.formCreate.first_totalKoli.toString(),
      first_aw              : this.formCreate.first_aw.toString(),
      first_length          : this.formCreate.first_length.toString(),
      first_width           : this.formCreate.first_width.toString(),
      first_height          : this.formCreate.first_height.toString(),
      first_vw              : this.formCreate.first_vw.toString(),
      first_cawFinal        : this.formCreate.first_cawFinal.toString(),
      
      second_spbGoodsId     : this.formCreate.second_spbGoodsId,
      second_totalKoli      : this.formCreate.second_totalKoli.toString(),
      second_aw             : this.formCreate.second_aw.toString(),
      second_length         : this.formCreate.second_length.toString(),
      second_width          : this.formCreate.second_width.toString(),
      second_height         : this.formCreate.second_height.toString(),
      second_vw             : this.formCreate.second_vw.toString(),
      second_cawFinal       : this.formCreate.second_cawFinal.toString(),
      
      third_spbGoodsId      : this.formCreate.third_spbGoodsId,
      third_totalKoli       : this.formCreate.third_totalKoli.toString(),
      third_aw              : this.formCreate.third_aw.toString(),
      third_length          : this.formCreate.third_length.toString(),
      third_width           : this.formCreate.third_width.toString(),
      third_height          : this.formCreate.third_height.toString(),
      third_vw              : this.formCreate.third_vw.toString(),
      third_cawFinal        : this.formCreate.third_cawFinal.toString(),

      fourth_spbGoodsId     : this.formCreate.fourth_spbGoodsId,
      fourth_totalKoli      : this.formCreate.fourth_totalKoli.toString(),
      fourth_aw             : this.formCreate.fourth_aw.toString(),
      fourth_length         : this.formCreate.fourth_length.toString(),
      fourth_width          : this.formCreate.fourth_width.toString(),
      fourth_height         : this.formCreate.fourth_height.toString(),
      fourth_vw             : this.formCreate.fourth_vw.toString(),
      fourth_cawFinal       : this.formCreate.fourth_cawFinal.toString(),

      fifth_spbGoodsId      : this.formCreate.fifth_spbGoodsId,
      fifth_totalKoli       : this.formCreate.fifth_totalKoli.toString(),
      fifth_aw              : this.formCreate.fifth_aw.toString(),
      fifth_length          : this.formCreate.fifth_length.toString(),
      fifth_width           : this.formCreate.fifth_width.toString(),
      fifth_height          : this.formCreate.fifth_height.toString(),
      fifth_vw              : this.formCreate.fifth_vw.toString(),
      fifth_cawFinal        : this.formCreate.fifth_cawFinal.toString()
    }

    Swal.fire({
      title: 'Tunggu dulu!',text: '',
      imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
      //imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
        imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
      position: 'center',	showCancelButton: false
      //cancelButtonText: 'No, keep it',
    }).then((result) => {
        if (result.isConfirmed) {} 
    })

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
    this.http
      .post(AppGlobalVar.BASE_API_URL + 'spb-update/update-spb', params, config)
      .subscribe((data: any) => {
        
        var dataDetail = data.data;
        dataDetail.forEach((item) => {

          if(item.result == 'true'){
            notify({
              message: item.message,
              type: 'success',
              displayTime: 5000,
              width: 400,
            });

            this.PrintSpbV2(this.spbId);
            Swal.close();
            this.onBack();

          }else{
            notify({
              message: item.message,
              type: 'error',
              displayTime: 5000,
              width: 400,
            });
          }
          
        });

      },
      (error) => {
        alert(error.statusText + '. ' + error.message);
      }
    );
    
    
  }

  PrintSpbV2(spbId) {
		const dialogConfig = new MatDialogConfig();
		dialogConfig.data = {
			urlReport: this.appGlobalVar.BASE_REPORT_URL + 'spb-sender/',
			manifestId: spbId,
			workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
			workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
			username: this.userGlobalVar.USERNAME,
			teks: '9. Lain-lain' + '#toolbar=0'  
      
		};
		dialogConfig.minWidth = this.innerWidth + 'px';
		dialogConfig.minHeight = this.innerHeight + 'px';
		
		
		let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);
		
		dialogRef.afterClosed().subscribe(value => {
			console.log(`Dialog sent: ${value}`);
		});
	}

  onBack(){
    this.router.navigate(['/spb-scans'], 
        { 
            queryParams: { 
            } 
        }); 
  }

  thousandSeparator(data) {
    if(data === null){
      return null;   
    }
    
		return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	}

}
