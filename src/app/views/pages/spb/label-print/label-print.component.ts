import {Component, HostListener, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'kt-label-print',
  templateUrl: './label-print.component.html',
  styleUrls: ['./label-print.component.scss']
})
export class LabelPrintComponent implements OnInit {
    
    urlReport;

    percentMinus = 0.05;
    public innerWidth: any = window.innerWidth - (window.innerWidth * this.percentMinus);
    public innerHeight: any = window.innerHeight - (window.innerHeight * this.percentMinus);
    @HostListener('window:resize', ['$event'])
    onResize(event) {
      this.innerWidth = window.innerWidth - (window.innerWidth * this.percentMinus);
      this.innerHeight = window.innerHeight - (window.innerHeight * this.percentMinus);
    }

    constructor(public dialogRef: MatDialogRef<LabelPrintComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
          private http: HttpClient, private sanitizer: DomSanitizer) {

            console.log(this.data);
            
      this.urlReport = sanitizer.bypassSecurityTrustResourceUrl(this.data.urlReport + this.data.spbId + '/' + this.data.workTimeZoneHour + '/' + this.data.workTimeZoneMinute + '/' + this.data.username + '/' + this.data.teks);

    }



    ngOnInit() {
    }

    close() {
      this.dialogRef.close('Thanks for using me!');
    }
}
