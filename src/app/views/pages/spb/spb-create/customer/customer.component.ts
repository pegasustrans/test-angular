import {Component, Input, OnDestroy, OnInit, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormGroup} from '@angular/forms';
import {UserGlobalVar} from '../../../../../core/globalvar/userGlobalVar';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import {HighlightProcess} from '../../../../../core/helper';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import {SpbCreateService} from '../spb-create.service';
import {Subscription} from 'rxjs';

import * as AspNetData from 'devextreme-aspnet-data-nojquery';
//import {environment} from '../../../../../../environments/environment';
import Swal from 'sweetalert2';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import notify from 'devextreme/ui/notify';
import dxDataGrid from 'devextreme/ui/data_grid';
import { ConnectedPositionStrategy } from '@angular/cdk/overlay';
import { stubString } from 'lodash';
//import { Console } from 'console';

@Component({
	selector: 'kt-customer',
	templateUrl: './customer.component.html',
	styleUrls: ['./customer.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomerComponent implements OnInit, OnDestroy {

	updateDataPengirimPopupVisible = false;
	updateDataPenerimaPopupVisible = false;
	moveDataPengirimPopupVisible = false;
	moveDataPenerimaPopupVisible = false;
	// abc = 'aaaaaaaaaaaaaaa';

	spbForm: FormGroup;
	spbFormSubs: Subscription;
	userGlobalVar = UserGlobalVar;
	appGlobalVar = AppGlobalVar;
	highlightProcess = HighlightProcess;

	phoneType	: any;
	senderColumns	= [];
	receiverColumns	= [];

	senderCustomerId = 0;
	senderPhoneDs: DataSource;
	senderNew: any;   // Pati 3 sept
	senderIsNew; senderIsNewSub: Subscription;
	senderNameDs: DataSource;
	senderNameNew = false;
	senderStoreDs: DataSource;
	senderStoreNew = false;
	senderPlaceDs: DataSource;
	senderPlaceNew = false;
	senderAddressDs: DataSource;
	senderAddressNew = false;
	senderPopUp = false;
	senderTelpPopUp = false; //Pati 14Jul22
	receiverTelpPopUp = false; //Pati 14Jul22
	senderListDs: any;
	senderTelpListDs: any;  //Pati 14Jul22
	senderListData = null;

	senderNamePopUp = false;	//Pati 09Jun22
	senderNameListDs: any;		//Pati 09Jun22
	senderListNameData = null;	//Pati 09Jun22
	
	receiverNamePopUp = false;	//Pati 09Jun22
	receiverNameListDs: any;		//Pati 09Jun22
	receiverNameListData = null;	//Pati 09Jun22

	porterPopUp = false;	//Pati 14Jun22
	porterListDs: any;		//Pati 14Jun22
	porterListData = null;	//Pati 14Jun22
	kodePorter = null;  //Pati 5Jul22
	namaPorter = null;  //Pati 7Jul22
	phonePorter = null; //Pati 7Jul22
	
	receiverIDPelDs: any;  //Pati 18Mei22

	senderPelangganIDPopUp = false; //Pati 19Mei22

	senderIDPelDs: any;  //Pati 18Mei22
	receiverPelangganIDPopUp = false; //Pati 19Mei22

	receiverCustomerId = 0;

	receiverPhoneDs: DataSource;
	receiverNew: any;  receiverIsNewSub: Subscription;
	receiverNameDs: DataSource;
	// receiverNameNew = false;
	receiverStoreDs: DataSource;
	// receiverStoreNew = false;
	receiverPlaceDs: DataSource; 
	receiverAddressDs: DataSource;
	// receiverAddressNew = false;
	receiverUrbanDs: DataSource;
	receiverSubDistrictDs: DataSource;
	receiverCityDs: DataSource;
	receiverPostalCOdeDs: DataSource;
	receiverViaDs: DataSource;
	receiverPopUp = false;
	receiverListDs: any;
	receiverListData = null;

	IsReceiverDisabled = true;
	IsSenderDisabled = true;

	
	IsReceiverTelpDisabled = false;
	IsSenderTelpDisabled = false;

	EditSenderDisabled = true;
	EditReceiverDisabled = true;

	SenderTelpDisable 			= true;
	SenderPhoneDisable 			= true;
	SenderNameDisable 			= true;
	SenderStoreDisable 			= true;
	SenderPlaceDisable 			= true;
	SenderAddressDisable 		= true;

	ReceiverTelpDisable 		= true;
	ReceiverPhoneDisable 		= true;
	ReceiverNameDisable 		= true;
	ReceiverStoreDisable 		= true;
	ReceiverPlaceDisable 		= true;
	ReceiverAddressDisable 		= true;

	approvalWindowVisible = false;   // Pati 1 Sept
	approvalPenerimaVisible = false;
	approvalCariNamaPengirim = false; // Pati 8 jun 22
	approvalCariNamaPenerima = false;
	fromCreate = {
		approveUser : null,
		approvePassword : null
	};    // Pati 1 Sept

	formSpbUpdate = {
		paramSender1 		: null,
		paramSender2 		: null,
		paramSender3 		: null,
		paramSender4 		: null,
		paramSender5 		: null,
		paramSender6 		: null,
		
		paramReceiver1 		: null,
		paramReceiver2 		: null,
		paramReceiver3 		: null,
		paramReceiver4 		: null,
		paramReceiver5 		: null,
		paramReceiver6 		: null,

		mode 				: null,

		senderId 			: null,
		senderNameId 		: null,
		senderStoreId 		: null,
		senderPlaceId 		: null,
		senderAddressId 	: null,

		senderTelp 			: null, //Pati 15Jul22
		senderPhone 		: null,
		senderName 			: null,
		senderStore 		: null,
		senderPlace 		: null,
		senderAddress 		: null,
		
		receiverTelp 		: null, //Pati 15Jul22
		receiverId 			: null,
		receiverNameId 		: null,
		receiverStoreId 	: null,
		receiverPlaceId 	: null,
		receiverAddressId 	: null,
		
		receiverPhone 		: null,
		receiverName 		: null,
		receiverStore 		: null,
		receiverPlace 		: null,
		receiverAddress 	: null
	}

	hasilnya: any;

	IDPelEntry = 0;
	TeleponEntry = 0;
	HPEntry = 0;


	constructor(private http: HttpClient, private spbCreateService: SpbCreateService, private cd: ChangeDetectorRef) {
		this.senderListChoose = this.senderListChoose.bind(this);
	}

	httpOptions = {
		headers: new HttpHeaders({
		  'Content-Type': 'application/json'
			})
	  };
	

	ngOnInit() {
		this.senderColumns = [
			{ Name: 'Hp', ID: 0 },
			{ Name: 'Nama', ID: 1 },
			{ Name: 'Toko', ID: 2 },
			{ Name: 'Tempat', ID: 3 },
			{ Name: 'Alamat', ID: 4 },
			{ Name: 'Telepon', ID: 5 },
		]

		this.receiverColumns = [
			{ Name: 'Hp', ID: 0 },
			{ Name: 'Nama', ID: 1 },
			{ Name: 'Toko', ID: 2 },
			{ Name: 'Tempat', ID: 3 },
			{ Name: 'Alamat', ID: 4 },
			{ Name: 'Telepon', ID: 5 },
		]

		this.formSpbUpdate.paramSender1 = this.senderColumns[0].ID;
		this.formSpbUpdate.paramSender2 = this.senderColumns[1].ID;
		this.formSpbUpdate.paramSender3 = this.senderColumns[2].ID;
		this.formSpbUpdate.paramSender4 = this.senderColumns[3].ID;
		this.formSpbUpdate.paramSender5 = this.senderColumns[4].ID;
		this.formSpbUpdate.paramSender6 = this.senderColumns[5].ID;

		this.formSpbUpdate.paramReceiver1 = this.receiverColumns[0].ID;
		this.formSpbUpdate.paramReceiver2 = this.receiverColumns[1].ID;
		this.formSpbUpdate.paramReceiver3 = this.receiverColumns[2].ID;
		this.formSpbUpdate.paramReceiver4 = this.receiverColumns[3].ID;
		this.formSpbUpdate.paramReceiver5 = this.receiverColumns[4].ID;
		this.formSpbUpdate.paramReceiver6 = this.receiverColumns[5].ID;

		this.IsReceiverDisabled = true;
		this.IsSenderDisabled = true;
		this.SenderPhoneToNew2();   //Pati 1 Sept
		this.ReceiverPhoneToNew2(); //Pati 1 Sept

		this.spbFormSubs = this.spbCreateService.spbForm
			.subscribe(data => {
				this.spbForm = data;
			} );

		this.senderIsNewSub = this.spbCreateService.senderIsNew
			.subscribe((data) => this.senderIsNew = data);

		this.receiverIsNewSub = this.spbCreateService.receiverIsNew
			.subscribe((data) => this.receiverNew = data);

		this.phoneType = [
			{value: 'hp', text: 'Hp'},
			{value: 'home', text: 'Rumah/Tempat'},
		];

		this.getCustomerSenderPhone();
		this.readSenderList();

		this.ReceiverPhone_GetData();
		this.readReceiverList();

		this.getDataCustomer();
	}


	getDataCustomer(){
		this.getSenderName();
		this.SenderStore_GetData();
		this.SenderPlace_GetData();
		this.SenderAddress_GetData();
		
		this.ReceiverName_GetData();
		this.ReceiverStore_GetData();
		this.ReceiverPlace_GetData();
		this.ReceiverAddress_GetData();
		this.ReceiverUrban_GetData();
		this.ReceiverSubDistrict_GetData();
		this.ReceiverCity_GetData();
		this.ReceiverPostalCode_GetData();
		this.ReceiverVia_GetData();
	}


	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  RECEIVER
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */

	// ReceiverPhoneToNew(e) {
	// 	if (e.key === '+') {
	// 		this.spbCreateService.SetReceiverIsNew('receiver', true);
	// 		this.spbCreateService.SetReceiverIsNew('receiverName', true);
	// 		this.spbCreateService.SetReceiverIsNew('receiverStore', true);
	// 		this.spbCreateService.SetReceiverIsNew('receiverPlace', true);
	// 		this.spbCreateService.SetReceiverIsNew('receiverAddress', true);
	// 	}
	// }

	//Pati 18Mei22
	KetikEnterPengirimIdPelanggan(e,input) {
		if ((AppGlobalVar.ENTRY_NOTELP_SENDER != 1) && (AppGlobalVar.ENTRY_NOHP_SENDER != 1) && (AppGlobalVar.ENTRY_NAMA_SENDER != 1)){
			AppGlobalVar.ENTRY_IDPEL_SENDER = 1;
		if (this.spbForm.get('destCity').value === null) {   // cek kota kosong 3 sept
		   Swal.fire({
			   title: 'Tunggu dulu!',text: 'Silahkan pilih Kota ID Pelanggan dan area tujuan terlebih dahulu.',
			   //imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
			   imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				 imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			   position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			   //cancelButtonText: 'No, keep it',
			 }).then((result) => {
			   if (result.isConfirmed) {} 
			 })
		   return;
		}
		Swal.fire({
		   position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 500
		 })
	   this.senderPelangganIDPopUpShow(); // popup ID Pel yg sdh ada
	   
	   
	}
   	}

   KetikEnterPenerimaIdPelanggan(e,input) {
	if ((AppGlobalVar.ENTRY_NOTELP_RECEIVER != 1) && (AppGlobalVar.ENTRY_NOHP_RECEIVER != 1) && (AppGlobalVar.ENTRY_NAMA_RECEIVER != 1)){
		AppGlobalVar.ENTRY_IDPEL_RECEIVER = 1;
	   if (this.spbForm.get('destCity').value === null) {   // cek kota kosong 3 sept
		  Swal.fire({
			  title: 'Tunggu dulu!',text: 'Silahkan pilih Kota ID Pelanggan PENERIMA dan area tujuan terlebih dahulu.',
			  //imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
			  imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			  position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			  //cancelButtonText: 'No, keep it',
			}).then((result) => {
			  if (result.isConfirmed) {} 
			})
		  return;
	   }
	   Swal.fire({
		  position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 500
		})
		console.log('masuk ini ');
		
	  this.receiverPelangganIDPopUpShow(); // popup receiver ID Pel yg sdh ada
	  
	  
	}  
  	}

	  KetikEnterKodePorter(e,input) {   //

		if (this.spbForm.get('destCity').value === null) {   // cek kota kosong 3 sept
		   Swal.fire({
			   title: 'Tunggu dulu!',text: 'Silahkan pilih Kota ID Pelanggan dan area tujuan terlebih dahulu.',
			   //imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
			   imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				 imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			   position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			   //cancelButtonText: 'No, keep it',
			 }).then((result) => {
			   if (result.isConfirmed) {} 
			 })
		   return;
		}
		
		 console.log('trap 1 disini');

		 var kodePorter = this.spbForm.get('idporter').value;
		 this.spbForm.get('namaporter').setValue('');
		 this.spbForm.get('hpporter').setValue('');

		 this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/readporter?kodeporter='+ kodePorter)
					.toPromise()
					.then((result: any) => {
						this.namaPorter = result.data;
					}); 
					
					this.spbForm.get('idporter').setValue(kodePorter);

					if (this.namaPorter === null){
						this.spbForm.get('namaporter').setValue('');
						this.spbForm.get('hpporter').setValue('');
					} else{
						this.spbForm.get('namaporter').setValue(this.namaPorter.name);
						this.spbForm.get('hpporter').setValue(this.namaPorter.phone);
					}

		}

		readKodePorter() {
			const params = {
				kodePorter			: this.spbForm.get('idporter').value
			};
			console.log('trap 2 disini' + this.kodePorter);
			this.porterListDs = new DataSource({
				store: new CustomStore({
					key: '',
					loadMode: 'raw',
					load: (loadOptions) => {
						return this.http.get(AppGlobalVar.BASE_API_URL + 'manifest-carrier/read-first-flight', { headers: this.httpOptions.headers, params  })
						//return this.http.get(AppGlobalVar.BASE_API_URL + 'spbcreate/read-idporter', { headers: this.httpOptions.headers, params  })
						.toPromise()
							.then((result: any) => {
								return result.data;
							});
							console.log('trap 3 disini');
					}
				})
			});
		
		}

		PorterPopUpShow() {
			this.senderPelangganIDPopUp = true;
			Swal.fire({
				position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 700
			  })
			this.senderPelangganIDPopUp = false;
			this.readPorterList();
			
			this.PorterListSetToInput();
		}

		PorterListSetToInput() {
			this.senderIDPelDs.reload().done((res) => {
				console.log('Hasil Root : ' + res[0].toString());
				// if (res.length === 1) {
					
				// 	this.spbForm.patchValue({
				// 		senderId: res[0].customerId,
				// 	});
	
				// 	this.formSpbUpdate.senderId = res[0].customerId;
				// }
			});
		}

		readPorterList() {  

			Swal.fire({
				position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 500
			  })
			// 	// 'https://js.devexpress.com/Demos/Mvc/api/DataGridWebApi' + '/Orders',
			this.senderIDPelDs = AspNetData.createStore({
				key: 'customerId',
				// key: 'OrderID',
				loadUrl: AppGlobalVar.BASE_API_URL + 'spbcreate/read-idporter',
				onBeforeSend: (method, ajaxOptions) => {
					// ajaxOptions.xhrFields = { withCredentials: true };
					const userToken = localStorage.getItem('accessToken');
					ajaxOptions.headers = {
						Authorization:  'Bearer ' + userToken,
						workCityId:  this.userGlobalVar.WORK_CITY_ID,
						senderPhone: this.punctuationPhone((this.spbForm.get('sender').value)), // Pati 1 sept
						tipeCust: 'sender',  // Pati 22nov21
						senderIdPel : this.spbForm.get('idcustpengirim').value
					};
				}
			});
			console.log('cek dotnet porter');
		}

		KetikEnterTelp(e,input) {
			
	
			if (this.spbForm.get('destCity').value === null) {   // cek kota kosong 3 sept
				Swal.fire({
					title: 'Tunggu dulu!',text: 'Silahkan pilih kota dan area tujuan terlebih dahulu.',
					//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
					imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
					  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
					position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
					//cancelButtonText: 'No, keep it',
				  }).then((result) => {
					if (result.isConfirmed) {} 
				  })
				return;
			}
			console.log("Lewat sini Setelah Ketik Enter");
			if (input === 'senderPhone')    // Pengirim
			{
			if ((AppGlobalVar.ENTRY_IDPEL_SENDER != 1) && (AppGlobalVar.ENTRY_NOHP_SENDER != 1) && (AppGlobalVar.ENTRY_NAMA_SENDER != 1)){
					AppGlobalVar.ENTRY_NOTELP_SENDER = 1;
				// Cek terhadap isian 2 digit di depan harus selain 08
				if (this.spbForm.get('sendertelp').value.substring(0,2) === '08' ) {   // cek kota kosong 3 sept
					Swal.fire({
						title: 'Tunggu dulu!',text: 'Dua digit No Telp harus diawali dengan SELAIN 08',
						//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
						imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
						  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
						position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
						//cancelButtonText: 'No, keep it',
					  }).then((result) => {
						if (result.isConfirmed) {} 
					  })
					this.spbForm.get('sendertelp').setValue("");
					return;
					}
	
	
				var tempCheckNumber1 = null;
					tempCheckNumber1 = this.punctuationPhone((this.spbForm.get('sendertelp').value));
			   // Bila ada huruf di bypass pake punctuationPhone
			   this.spbForm.patchValue({
				sender: tempCheckNumber1,});
			// Cek ke database
			
			var adaNoTelp = '';
				const formData: any = new FormData();
					formData.append('telp', tempCheckNumber1);
					formData.append('companycityid', UserGlobalVar.WORK_CITY_ID);
				return this.http
				.post(AppGlobalVar.BASE_API_URL + 'spbcreate/cek-telp', formData)
				.subscribe((data: any) => {
											if (data != null) {
												adaNoTelp = data.data.toString(); }
	
								if (adaNoTelp.toString() === '0' || adaNoTelp.toString() === ''  || adaNoTelp.toString() === 'tidak'){    //Jika Notelp tidak ada di database
	
									Swal.fire({
										position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 300
									  })
									this.IsSenderDisabled = true;
									this.spbCreateService.SetSenderIsNew('sender', true);
									this.spbCreateService.SetSenderIsNew('senderName', true);
									this.spbCreateService.SetSenderIsNew('senderStore', true);
									this.spbCreateService.SetSenderIsNew('senderPlace', true);
									this.spbCreateService.SetSenderIsNew('senderAddress', true);
									this.spbForm.get('sendertelp').disable();
									this.spbForm.get('senderArea').disable();
									this.spbForm.get('sender').setValue("");
									//this.spbForm.get('sender').enable();
									
								}
								else {
									Swal.fire({
										position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 500
									  })
									this.senderTelpPopUpShow(); // popup no telp yg sdh ada
								}
					},
					(error) => {
						// alert(error.statusText + '. ' + error.message);
					}
				);
				}
			}
	
			if (input === 'receiverPhone')   // Penerima
			{
				console.log("Lewat sini receiver Phone");
				if ((AppGlobalVar.ENTRY_IDPEL_RECEIVER != 1) && (AppGlobalVar.ENTRY_NOHP_RECEIVER != 1) && (AppGlobalVar.ENTRY_NAMA_RECEIVER != 1)){
					AppGlobalVar.ENTRY_NOTELP_RECEIVER = 1;
					console.log("Status Telp Penerima : " + AppGlobalVar.ENTRY_NOTELP_RECEIVER);
				// Cek terhadap isian 2 digit di depan harus selain 08
				if (this.spbForm.get('receivertelp').value.substring(0,2) === '08' ) {   // cek kota kosong 3 sept
					Swal.fire({
						title: 'Tunggu dulu!',text: 'Dua digit No Telp harus diawali dengan SELAIN 08',
						//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
						imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
						  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
						position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
						//cancelButtonText: 'No, keep it',
					  }).then((result) => {
						if (result.isConfirmed) {} 
					  })
					this.spbForm.get('receivertelp').setValue("");
					return;
					}
	
				var tempCheckNumber2 = null;
				// var tempCheckNumber3 = this.punctuationPhone((this.spbForm.get('receiver').value));
				// if (this.punctuationPhone((this.spbForm.get('receiverArea').value)) === null)
				// {
					tempCheckNumber2 = this.punctuationPhone((this.spbForm.get('receivertelp').value));
				// }
				// else {
				// 	tempCheckNumber2 = this.punctuationPhone((this.spbForm.get('receiverArea').value)) + this.punctuationPhone((this.spbForm.get('receiver').value));
				// }
	
				// Bila ada huruf di bypass pake punctuationPhone
				this.spbForm.patchValue({ receiver: tempCheckNumber2,});
			//Cek ke Database
			var adaNoTelp = '';
			const formData: any = new FormData();
				formData.append('telp', tempCheckNumber2);
				formData.append('companycityid', this.spbForm.get('destCity').value);
			return this.http
			.post(AppGlobalVar.BASE_API_URL + 'spbcreate/cek-telp', formData)
			.subscribe((data: any) => {
										if (data != null) {
											adaNoTelp = data.data.toString(); }
	
							if (adaNoTelp.toString() === '0' || adaNoTelp.toString() === ''  || adaNoTelp.toString() === 'tidak'){    //Jika Notelp tidak ada di database
	
								Swal.fire({
									position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 300
								  })
								  this.IsReceiverDisabled = false;
								  				this.spbCreateService.SetReceiverIsNew('receiver', true);
												  this.spbCreateService.SetReceiverIsNew('receiverName', true);
												  this.spbCreateService.SetReceiverIsNew('receiverStore', true);
												  this.spbCreateService.SetReceiverIsNew('receiverPlace', true);
												  this.spbCreateService.SetReceiverIsNew('receiverAddress', true);
												  this.spbForm.get('receivertelp').disable();
												  this.spbForm.get('receiverArea').disable();
												  this.spbForm.get('receiver').setValue("");
								
							}
							else {
								Swal.fire({
									position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 500
								  })
								this.receiverTelpPopUpShow(); // popup no telp yg sdh ada
							}
				},
				(error) => {
					// alert(error.statusText + '. ' + error.message);
				}
			);
			}
			

			}
			}
			

	KetikEnter(e,input) {
	
		if (this.spbForm.get('destCity').value === null) {   // cek kota kosong 3 sept
			Swal.fire({
				title: 'Tunggu dulu!',text: 'Silahkan pilih kota dan area tujuan terlebih dahulu.',
				//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
				imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
				//cancelButtonText: 'No, keep it',
			  }).then((result) => {
				if (result.isConfirmed) {} 
			  })
			return;
		}



		if (input === 'senderPhone')    // Pengirim
		{
			if ((AppGlobalVar.ENTRY_IDPEL_SENDER != 1) && (AppGlobalVar.ENTRY_NOTELP_SENDER != 1) && (AppGlobalVar.ENTRY_NAMA_SENDER != 1)){
				AppGlobalVar.ENTRY_NOHP_SENDER = 1;	
			// Cek terhadap isian 2 digit di depan harus 08
			// di non aktifkan 01Agust22 if (this.spbForm.get('sender').value.substring(0,2) != '08' ) {   // cek kota kosong 3 sept
			// 	Swal.fire({
			// 		title: 'Tunggu dulu!',text: 'Dua digit No. HP harus diawali dengan 08',
			// 		//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
			// 		imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
			// 		  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			// 		position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			// 		//cancelButtonText: 'No, keep it',
			// 	  }).then((result) => {
			// 		if (result.isConfirmed) {} 
			// 	  })
			// 	this.spbForm.get('sender').setValue("");
			// 	return; 
			// }

			// if (this.spbForm.get('sender').value === null || this.spbForm.get('sender').value === "") {   // cek kota kosong 3 sept
			// 	Swal.fire({
			// 		title: 'Tunggu dulu!',text: 'Isi No. Telp Pengirim terlebih dahulu.',
			// 		//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
			// 		imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
			// 		  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			// 		position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			// 		//cancelButtonText: 'No, keep it',
			// 	  }).then((result) => {
			// 		if (result.isConfirmed) {} 
			// 	  })
			// 	return;
			// 	}


			var tempCheckNumber1 = null;
			// var tempCheckNumber0 = this.punctuationPhone((this.spbForm.get('sender').value));
			// if (this.punctuationPhone((this.spbForm.get('senderArea').value)) === null)
			// {
				tempCheckNumber1 = this.punctuationPhone((this.spbForm.get('sender').value));
			// }
			// else {
			// 	tempCheckNumber1 = this.punctuationPhone((this.spbForm.get('senderArea').value)) + this.punctuationPhone((this.spbForm.get('sender').value));
			// }
		   // Bila ada huruf di bypass pake punctuationPhone
		   this.spbForm.patchValue({
			sender: tempCheckNumber1,});
		// Cek ke database
		
		var adaNoTelp = '';
			const formData: any = new FormData();
				formData.append('phone', tempCheckNumber1);
				formData.append('companycityid', UserGlobalVar.WORK_CITY_ID);
			return this.http
			.post(AppGlobalVar.BASE_API_URL + 'spbcreate/cek-phone', formData)
			.subscribe((data: any) => {
										if (data != null) {
											adaNoTelp = data.data.toString(); }

							if (adaNoTelp.toString() === '0' || adaNoTelp.toString() === ''  || adaNoTelp.toString() === 'tidak'){    //Jika Notelp tidak ada di database

								Swal.fire({
									position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 300
								  })
								this.IsSenderDisabled = false;

								//this.spbCreateService.SetSenderIsNew('sender', false);
								this.spbCreateService.SetSenderIsNew('senderName', true);
								this.spbCreateService.SetSenderIsNew('senderStore', true);
								this.spbCreateService.SetSenderIsNew('senderPlace', true);
								this.spbCreateService.SetSenderIsNew('senderAddress', true);
								this.spbForm.get('sender').disable();
								this.spbForm.get('senderArea').disable();
								
							}
							else {
								Swal.fire({
									position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 500
								  })
								this.senderPopUpShow(); // popup no telp yg sdh ada
							}
				},
				(error) => {
					// alert(error.statusText + '. ' + error.message);
				}
			);
		
			}
		}

		if (input === 'receiverPhone')   // Penerima
		{
			if ((AppGlobalVar.ENTRY_IDPEL_RECEIVER != 1) && (AppGlobalVar.ENTRY_NOTELP_RECEIVER != 1) && (AppGlobalVar.ENTRY_NAMA_RECEIVER != 1)){
				AppGlobalVar.ENTRY_NOHP_RECEIVER = 1;
			// Cek terhadap isian 2 digit di depan harus 08
			//  di non aktifkan 01Agust22 if (this.spbForm.get('receiver').value.substring(0,2) != '08' ) {   // cek kota kosong 3 sept
			// 	Swal.fire({
			// 		title: 'Tunggu dulu!',text: 'Dua digit No. HP harus diawali dengan 08',
			// 		//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
			// 		imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
			// 		  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			// 		position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			// 		//cancelButtonText: 'No, keep it',
			// 	  }).then((result) => {
			// 		if (result.isConfirmed) {} 
			// 	  })
			// 	  this.spbForm.get('receiver').setValue("");
			// 	return;
			// 	}

			// if (this.spbForm.get('receiver').value === null || this.spbForm.get('receiver').value === "") {   // cek kota kosong 3 sept
			// 	Swal.fire({
			// 		title: 'Tunggu dulu!',text: 'Isi No. Telp Penerima terlebih dahulu.',
			// 		//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
			// 		imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
			// 		  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			// 		position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			// 		//cancelButtonText: 'No, keep it',
			// 	  }).then((result) => {
			// 		if (result.isConfirmed) {} 
			// 	  })
			// 	return;
			// }

			var tempCheckNumber2 = null;
			// var tempCheckNumber3 = this.punctuationPhone((this.spbForm.get('receiver').value));
			// if (this.punctuationPhone((this.spbForm.get('receiverArea').value)) === null)
			// {
				tempCheckNumber2 = this.punctuationPhone((this.spbForm.get('receiver').value));
			// }
			// else {
			// 	tempCheckNumber2 = this.punctuationPhone((this.spbForm.get('receiverArea').value)) + this.punctuationPhone((this.spbForm.get('receiver').value));
			// }

			// Bila ada huruf di bypass pake punctuationPhone
			this.spbForm.patchValue({ receiver: tempCheckNumber2,});
		//Cek ke Database
		var adaNoTelp = '';
		const formData: any = new FormData();
			formData.append('phone', tempCheckNumber2);
			formData.append('companycityid', this.spbForm.get('destCity').value);
		return this.http
		.post(AppGlobalVar.BASE_API_URL + 'spbcreate/cek-phone', formData)
		.subscribe((data: any) => {
									if (data != null) {
										adaNoTelp = data.data.toString(); }

						if (adaNoTelp.toString() === '0' || adaNoTelp.toString() === ''  || adaNoTelp.toString() === 'tidak'){    //Jika Notelp tidak ada di database

							Swal.fire({
								position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 300
							  })
							  this.IsReceiverDisabled = false;
							  				this.spbCreateService.SetReceiverIsNew('receiverName', true);
							  				this.spbCreateService.SetReceiverIsNew('receiverStore', true);
							  				this.spbCreateService.SetReceiverIsNew('receiverPlace', true);
							  				this.spbCreateService.SetReceiverIsNew('receiverAddress', true);
							  				this.spbForm.get('receiver').disable();
							  				this.spbForm.get('receiverArea').disable();
							
						}
						else {
							Swal.fire({
								position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 500
							  })
							this.receiverPopUpShow(); // popup no telp yg sdh ada
						}
			},
			(error) => {
				// alert(error.statusText + '. ' + error.message);
			}
		
		);

		// this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/cekphone?phone='+ tempCheckNumber2)
		// 		.toPromise()
		// 		.then((result: any) => {
		// 			var adaNoTelp = result.data;
		// 			Swal.fire({
		// 				position: 'top',title: 'Tunggu sebentar! ' + adaNoTelp, showConfirmButton: true
		// 			  })
		// 			if (adaNoTelp === 0 || adaNoTelp.toString() === '0' || adaNoTelp.toString() === ''){    //Jika Notelp tidak ada di database
		// 				Swal.fire({
		// 					position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 300
		// 				  })
		// 				this.IsReceiverDisabled = false;
		// 				this.spbCreateService.SetReceiverIsNew('receiverName', true);
		// 				this.spbCreateService.SetReceiverIsNew('receiverStore', true);
		// 				this.spbCreateService.SetReceiverIsNew('receiverPlace', true);
		// 				this.spbCreateService.SetReceiverIsNew('receiverAddress', true);
		// 				this.spbForm.get('receiver').disable();
		// 				this.spbForm.get('receiverArea').disable();
		// 			}
		// 			else {
		// 				Swal.fire({
		// 					position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 300
		// 				  })
		// 				this.receiverPopUpShow(); // popup no telp yg sdh ada
		// 			}
		// 		});
			//return;
		}
		}
	
	}

	KetikEnterName(e,input) {

		if (this.spbForm.get('destCity').value === null) {   // cek kota kosong 3 sept
			Swal.fire({
				title: 'Tunggu dulu!',text: 'Silahkan pilih kota dan area tujuan terlebih dahulu.',
				//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
				imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
				//cancelButtonText: 'No, keep it',
			  }).then((result) => {
				if (result.isConfirmed) {} 
			  })
			return;
		}

		if (input === 'senderName')    // Jika Nama Pengirim
		{
			if ((AppGlobalVar.ENTRY_NOTELP_SENDER != 1) && (AppGlobalVar.ENTRY_NOHP_SENDER != 1) && (AppGlobalVar.ENTRY_IDPEL_SENDER != 1)){
				AppGlobalVar.ENTRY_NAMA_SENDER = 1;
			if (this.spbForm.get('senderName').value === null || this.spbForm.get('senderName').value === "") {   // cek kota kosong 3 sept
				Swal.fire({
					title: 'Tunggu dulu!',text: 'Isi Nama Pengirim terlebih dahulu.',
					//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
					imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
					  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
					position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
					//cancelButtonText: 'No, keep it',
				  }).then((result) => {
					if (result.isConfirmed) {} 
				  })
				return;
				}
				this.senderNamePopUpShow(); // popup Name
			}
		}

		if (input === 'receiverName')   // Penerima
		{
			if ((AppGlobalVar.ENTRY_NOTELP_RECEIVER != 1) && (AppGlobalVar.ENTRY_NOHP_RECEIVER != 1) && (AppGlobalVar.ENTRY_IDPEL_RECEIVER != 1)){
				AppGlobalVar.ENTRY_NAMA_RECEIVER = 1;
			if (this.spbForm.get('receiverName').value === null || this.spbForm.get('receiverName').value === "") {   // cek kota kosong 3 sept
				Swal.fire({
					title: 'Tunggu dulu!',text: 'Isi Nama Penerima terlebih dahulu.',
					//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
					imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
					  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
					position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
					//cancelButtonText: 'No, keep it',
				  }).then((result) => {
					if (result.isConfirmed) {} 
				  })
				return;
				}
				this.receiverNamePopUpShow(); // popup Name
			}
		}
	
	}

	setApproval() {
	    
		var user 	= this.fromCreate.approveUser;
		var pass 	= this.fromCreate.approvePassword;

		
		this.approvalWindowVisible = false;
		this.senderPopUp = false;
		
	// this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/approvalcust?username='+ user)
	this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/approvalcust?username='+ user)
					.toPromise()
					.then((result: any) => {
						var passhasilnya = result.data;
					
					if (passhasilnya === pass && pass != '' && user != '')
					{
						Swal.fire({
							position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 300
						  })
						this.IsSenderDisabled = false;
						this.spbCreateService.SetSenderIsNew('senderName', true);
						this.spbCreateService.SetSenderIsNew('senderStore', true);
						this.spbCreateService.SetSenderIsNew('senderPlace', true);
						this.spbCreateService.SetSenderIsNew('senderAddress', true);	
						
						//this.senderEnableAfterApprove();
						//this.approvalWindowVisible = false;
					}
					else {
						if (passhasilnya === 0 || passhasilnya === '')
						{
							//Swal.fire('Username Salah!', 'Isi Dengan Username yang sudah terdaftar', 'error')
							Swal.fire({
								title: 'Username Salah!',text: 'Isi Username dengan yang sudah terdaftar.',
								//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
								imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
  								imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
								position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
								//cancelButtonText: 'No, keep it',
							  }).then((result) => {
								if (result.isConfirmed) {} 
							  })
							
						}
						else {
							Swal.fire({
								title: 'Password Salah!',text: 'Isi Password sesuai dengan username.',
								imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
  								imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
								position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
								//cancelButtonText: 'No, keep it',
							  }).then((result) => {
								if (result.isConfirmed) {} 
							  })
						}
					}
						
						//return this.adaNoTelp;
					});
		
	}

	setApprovalPenerima() {
	    
		var user 	= this.fromCreate.approveUser;
		var pass 	= this.fromCreate.approvePassword;

		
		this.approvalPenerimaVisible = false;
		this.receiverPopUp = false;
		
	// this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/approvalcust?username='+ user)
	this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/approvalcust?username='+ user)
					.toPromise()
					.then((result: any) => {
						var passhasilnya = result.data;
					
					if (passhasilnya === pass && pass != '' && user != '')
					{
						//this.receiverEnable();
						this.receiverEnableAfterApprove();
					}
					else {
						if (passhasilnya === 0 || passhasilnya === '')
						{
							//Swal.fire('Username Salah!', 'Isi Dengan Username yang sudah terdaftar', 'error')
							Swal.fire({
								title: 'Username Salah!',text: 'Isi Username dengan yang sudah terdaftar.',
								//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
								imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
  								imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
								position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
								//cancelButtonText: 'No, keep it',
							  }).then((result) => {
								if (result.isConfirmed) {} 
							  })
							
						}
						else {
							Swal.fire({
								title: 'Password Salah!',text: 'Isi Password sesuai dengan username.',
								imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
  								imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
								position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
								//cancelButtonText: 'No, keep it',
							  }).then((result) => {
								if (result.isConfirmed) {} 
							  })
						}
					}
						
						//return this.adaNoTelp;
					});
		
	}


	setApprovalCariNamaPengirim() {
	    
		var user 	= this.fromCreate.approveUser;
		var pass 	= this.fromCreate.approvePassword;

		
		this.approvalCariNamaPengirim = false;
		this.senderPopUp = false;
		
	// this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/approvalcust?username='+ user)
	this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/approvalcust?username='+ user)
					.toPromise()
					.then((result: any) => {
						var passhasilnya = result.data;
					
					if (passhasilnya === pass && pass != '' && user != '')
					{
						// Swal.fire({
						// 	position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 600
						//   })
						// this.IsSenderDisabled = false;
						this.spbCreateService.SetSenderIsNew('senderName', true);
						
						

						//this.approvalCariNamaPengirim = false;
					}
					else {
						if (passhasilnya === 0 || passhasilnya === '')
						{
							//Swal.fire('Username Salah!', 'Isi Dengan Username yang sudah terdaftar', 'error')
							Swal.fire({
								title: 'Username Salah!',text: 'Isi Username dengan yang sudah terdaftar.',
								//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
								imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
  								imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
								position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
								//cancelButtonText: 'No, keep it',
							  }).then((result) => {
								if (result.isConfirmed) {} 
							  })
							
						}
						else {
							Swal.fire({
								title: 'Password Salah!',text: 'Isi Password sesuai dengan username.',
								imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
  								imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
								position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
								//cancelButtonText: 'No, keep it',
							  }).then((result) => {
								if (result.isConfirmed) {} 
							  })
						}
					}
						
						//return this.adaNoTelp;
					});
		
	}

	setApprovalCariNamaPenerima() {
	    
		var user 	= this.fromCreate.approveUser;
		var pass 	= this.fromCreate.approvePassword;

		
		this.approvalCariNamaPenerima = false;
		this.senderPopUp = false;
		
	// this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/approvalcust?username='+ user)
	this.http.get(AppGlobalVar.BASE_API_URL + 'Customer/approvalcust?username='+ user)
					.toPromise()
					.then((result: any) => {
						var passhasilnya = result.data;
					
					if (passhasilnya === pass && pass != '' && user != '')
					{
						// Swal.fire({
						// 	position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 600
						//   })
						// this.IsSenderDisabled = false;
						this.spbCreateService.SetReceiverIsNew('receiverName', true);
						
					}
					else {
						if (passhasilnya === 0 || passhasilnya === '')
						{
							//Swal.fire('Username Salah!', 'Isi Dengan Username yang sudah terdaftar', 'error')
							Swal.fire({
								title: 'Username Salah!',text: 'Isi Username dengan yang sudah terdaftar.',
								//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
								imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
  								imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
								position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
								//cancelButtonText: 'No, keep it',
							  }).then((result) => {
								if (result.isConfirmed) {} 
							  })
							
						}
						else {
							Swal.fire({
								title: 'Password Salah!',text: 'Isi Password sesuai dengan username.',
								imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
  								imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
								position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
								//cancelButtonText: 'No, keep it',
							  }).then((result) => {
								if (result.isConfirmed) {} 
							  })
						}
					}
						
						//return this.adaNoTelp;
					});
		
	}


	ReceiverPhoneToNew() {

			this.spbCreateService.SetReceiverIsNew('receiver', true);
			this.spbCreateService.SetReceiverIsNew('receiverName', true);
			this.spbCreateService.SetReceiverIsNew('receiverStore', true);
			this.spbCreateService.SetReceiverIsNew('receiverPlace', true);
			this.spbCreateService.SetReceiverIsNew('receiverAddress', true);
	}

	ReceiverPhoneToOld(e) {
		if (e.key === '=') {
			this.IsReceiverDisabled = true;
			this.spbCreateService.SetReceiverIsNew('receiver', false);
			this.spbCreateService.SetReceiverIsNew('receiverName', false);
			this.spbCreateService.SetReceiverIsNew('receiverStore', false);
			this.spbCreateService.SetReceiverIsNew('receiverPlace', false);
			this.spbCreateService.SetReceiverIsNew('receiverAddress', false);
		}
		this.PhoneRestriction(e);
	}

	ReceiverPhoneToOld2() {
			this.IsReceiverDisabled = true;
			this.spbCreateService.SetReceiverIsNew('receiver', false);
			this.spbCreateService.SetReceiverIsNew('receiverName', false);
			this.spbCreateService.SetReceiverIsNew('receiverStore', false);
			this.spbCreateService.SetReceiverIsNew('receiverPlace', false);
			this.spbCreateService.SetReceiverIsNew('receiverAddress', false);
	}

	ReceiverNameToNew(e) { (e.key === '+') ? this.spbCreateService.SetReceiverIsNew('receiverName', true) : '' ; }
	ReceiverNameToOld(e) { ((e.key === '=')  && this.receiverNew.receiver === false ) ? this.spbCreateService.SetReceiverIsNew('receiverName', false) : '' ; }

	ReceiverStoreToNew(e) { (e.key === '+') ? this.spbCreateService.SetReceiverIsNew('receiverStore', true) : '' ; }
	ReceiverStoreToOld(e) { ((e.key === '=')  && this.receiverNew.receiver === false ) ? this.spbCreateService.SetReceiverIsNew('receiverStore', false) : '' ; }

	ReceiverPlaceToNew(e) { (e.key === '+') ? this.spbCreateService.SetReceiverIsNew('receiverPlace', true) : '' ; }
	ReceiverPlaceToOld(e) { ((e.key === '=')  && this.receiverNew.receiver === false ) ? this.spbCreateService.SetReceiverIsNew('receiverPlace', false) : '' ; }

	ReceiverAddressToNew(e) { (e.key === '+') ? this.spbCreateService.SetReceiverIsNew('receiverAddress', true) : '' ; }
	ReceiverAddressToOld(e) { ((e.key === '=')  && this.receiverNew.receiver === false ) ? this.spbCreateService.SetReceiverIsNew('receiverAddress', false) : '' ; }

	ReceiverViaToNew(e) { (e.key === '+') ? this.spbCreateService.SetReceiverIsNew('receiverVia', true) : '' ; }
	ReceiverViaToOld(e) { ((e.key === '=')  ) ? this.spbCreateService.SetReceiverIsNew('receiverVia', false) : '' ; }

	ReceiverPhone_OnItemClick(e) {
		/**
		 * set receivercustomerId so API customer receiver can consume it
		 * then reset and reload all receiver
		 */
		this.receiverCustomerId = e.itemData.customerId;

		this.Receiver_ResetAll();
		this.Receiver_ReloadAll();
	}

	ReceiverPhone_GetData() {
		/**
		 * get phone data from API with filter :
		 * - destination city
		 * - search keyword
		 */
		this.receiverPhoneDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {

					if (loadOptions.searchValue !== null) {
						this.receiverListData = null;
					}
					const receiverPhoneUrl = (this.receiverListData === null || this.receiverListData === undefined) ?  AppGlobalVar.BASE_API_URL + 'customer/phone?cityId=' + this.spbForm.get('destCity').value
						+ '&searchValue='  + loadOptions.searchValue + '&type=&customerId=' :
						AppGlobalVar.BASE_API_URL + 'customer/phone?cityId=1&searchValue=1&type=&customerId=' + this.receiverListData.customerId;

					return this.http.get(receiverPhoneUrl )
						.toPromise()
						.then((result: any) => {
							// Here, you can perform operations unsupported by the server
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?id=' + key)
						.toPromise();
				}
			})
		});
	}

	ReceiverName_GetData() {
		this.receiverNameDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					const receiverNameUrl = (this.receiverListData === null) ? AppGlobalVar.BASE_API_URL + 'customer/name?customerId=' + this.receiverCustomerId :
						AppGlobalVar.BASE_API_URL + 'customer/name?customerNameId=' + this.receiverListData.customerNameId;
					return this.http.get(receiverNameUrl)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?id=' + key)
						.toPromise();
				}
			})
		});
	}

	ReceiverStore_GetData() {
		this.receiverStoreDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					const receiverStoreUrl = (this.receiverListData === null) ? AppGlobalVar.BASE_API_URL + 'customer/store?customerId=' + this.receiverCustomerId :
						AppGlobalVar.BASE_API_URL + 'customer/store?customerStoreId=' + this.receiverListData.customerStoreId;
					return this.http.get(receiverStoreUrl)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/store?id=' + key)
						.toPromise();
				}
			})
		});
	}

	ReceiverPlace_GetData() {
		this.receiverPlaceDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					const receiverPlaceUrl = (this.receiverListData === null) ? AppGlobalVar.BASE_API_URL + 'customer/place?customerId=' + this.receiverCustomerId :
						AppGlobalVar.BASE_API_URL + 'customer/place?customerPlaceId=' + this.receiverListData.customerPlaceId;
					return this.http.get(receiverPlaceUrl)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/place?id=' + key)
						.toPromise();
				}
			})
		});
	}

	ReceiverAddress_GetData() {
		this.receiverAddressDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					const receiverAddressUrl = (this.receiverListData === null) ? AppGlobalVar.BASE_API_URL + 'customer/address?customerId=' + this.receiverCustomerId :
						AppGlobalVar.BASE_API_URL + 'customer/address?customerAddressId=' + this.receiverListData.customerAddressId;
					return this.http.get(receiverAddressUrl)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/address?id=' + key)
						.toPromise();
				}
			})
		});
	}

	ReceiverUrban_GetData() {
		this.receiverUrbanDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'masterUrbanId',
				loadMode: 'raw',
				load: (loadOptions) => {
					const formData: any = new FormData();
					formData.append('isVia',  false);
					formData.append('receiverCity',  this.spbForm.get('receiverCity').value);
					formData.append('receiverSubDistrict',  this.spbForm.get('receiverSubDistrict').value);
					formData.append('receiverUrban',  loadOptions.searchValue);

					if (this.spbForm.get('destArea').value !== null) {
						formData.append('destArea',  this.spbForm.get('destArea').value);
						formData.append('isVia',  this.spbForm.get('isVia').value);
						formData.append('receiverSubDistrict',  this.spbForm.get('receiverSubDistrict').value);
					}

					return this.http.post(AppGlobalVar.BASE_API_URL + 'spbcreate/receiver-urban', formData)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'spbcreate/receiver-urban?id=' + key)
						.toPromise();
				}
			})
		});
	}

	ReceiverUrban_Reload() {
		this.receiverUrbanDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverUrban: res[0].masterUrbanId,
				});
			} else {
				this.spbForm.patchValue({
					receiverUrban: null,
				});
			}
		});
	}

	ReceiverUrban_OnChange(e) {
		if (this.receiverUrbanDs.items().length > 1) {
			this.spbForm.patchValue({
				receiverPostalCode: e.value,
			});

			if (this.spbForm.get('receiverSubDistrict').value === null) {
			}
		}

	}

	ReceiverPostalCode_GetData() {
		this.receiverPostalCOdeDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'masterUrbanId',
				loadMode: 'raw',
				load: (loadOptions) => {
					const formData: any = new FormData();
					formData.append('isVia',  false);
					formData.append('receiverCity',  this.spbForm.get('receiverCity').value);
					formData.append('receiverSubDistrict',  this.spbForm.get('receiverSubDistrict').value);
					formData.append('receiverPostalCode',  loadOptions.searchValue);

					if (this.spbForm.get('destArea').value !== null) {
						formData.append('destArea',  this.spbForm.get('destArea').value);
						formData.append('isVia',  this.spbForm.get('isVia').value);
						formData.append('receiverSubDistrict',  this.spbForm.get('receiverSubDistrict').value);
					}

					return this.http.post(AppGlobalVar.BASE_API_URL + 'spbcreate/receiver-postalcode', formData)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'spbcreate/receiver-urban?id=' + key)
						.toPromise();
				}
			})
		});
	}

	ReceiverPostalCode_Reload() {
		this.receiverPostalCOdeDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverPostalCode: res[0].masterUrbanId,
				});
			} else {
				this.spbForm.patchValue({
					receiverPostalCode: null,
				});
			}
		});
	}

	ReceiverPostalCode_OnChange(e) {
		if (this.receiverPostalCOdeDs.items().length > 1) {
			this.spbForm.patchValue({
				receiverUrban: e.value,
			});

			if (this.spbForm.get('receiverSubDistrict').value === null) {
				this.ReceiverSubDistrict_Reload();
			}

			//kalau postalcode dihapus, reset destArea yang otomatis mereset kecamatan dan urban
			if (this.spbForm.get('receiverPostalCode').value === null) {
				this.spbForm.patchValue({
					destArea: null,
					isVia: false
			});
			}
		}

	}

	ReceiverSubDistrict_GetData() {
		this.receiverSubDistrictDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'masterSubDistrictCode',
				loadMode: 'raw',
				load: (loadOptions) => {
					const formData: any = new FormData();
					formData.append('isVia',  false);
					formData.append('receiverCity',  this.spbForm.get('receiverCity').value);
					formData.append('receiverSubDistrict',  loadOptions.searchValue);

					// unutk hanya mengetahui kelurahan atau kode pos saja
					formData.append('receiverPostalCode',  this.spbForm.get('receiverPostalCode').value);
					formData.append('receiverUrban',  this.spbForm.get('receiverUrban').value);

					// if destination city is selected, then find by companycityId
					if (this.spbForm.get('destArea').value !== null) {
						formData.append('destArea',  this.spbForm.get('destArea').value);
						formData.append('isVia',  this.spbForm.get('isVia').value);
					}


					return this.http.post(AppGlobalVar.BASE_API_URL + 'spbcreate/receiver-subdistrict', formData)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
						
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'spbcreate/receiver-urban?id=' + key)
						.toPromise();
				}
			})
		});
	}

	ReceiverSubDistrict_Reload() {
		this.receiverSubDistrictDs.reload().done((res) => {
			if (res.length === 1 && (this.spbForm.get('isVia').value === false || this.spbForm.get('isVia').value === null) ) {
				this.spbForm.patchValue({
					receiverSubDistrict: res[0].masterSubDistrictId
				});
				if (this.spbForm.get('receiverPostalCode').value != null){
					this.spbForm.patchValue({
						destArea: res[0].destArea
					});
				}
			} else {
				this.spbForm.patchValue({
					receiverSubDistrict: null,
				});
			}

			// get data urban and postal code
			if(this.spbForm.get('receiverPostalCode').value === null && this.spbForm.get('receiverUrban').value === null ) {
				this.ReceiverUrban_Reload();
				this.ReceiverPostalCode_Reload();
			}

		});
	}

	ReceiverSubDistrict_SelectionChanged(e) {

		if(this.spbForm.get('receiverPostalCode').value === null && this.spbForm.get('receiverUrban').value === null ) {
			this.ReceiverUrban_Reload();
			this.ReceiverPostalCode_Reload();
		}
	}

	ReceiverCity_GetData() {
		this.receiverCityDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'masterCityId',
				loadMode: 'raw',
				load: (loadOptions) => {
					const formData: any = new FormData();

					// if destination city is selected, then find by companycityId
					if (this.spbForm.get('destCity').value !== null) {
						formData.append('destCity',  this.spbForm.get('destCity').value);
						formData.append('isVia',  this.spbForm.get('isVia').value);
					}

					formData.append('ReceiverCity',  loadOptions.searchValue);
					return this.http.post(AppGlobalVar.BASE_API_URL + 'spbcreate/receiver-city', formData)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'spbcreate/receiver-urban?id=' + key)
						.toPromise();
				}
			})
		});
	}

	ReceiverCity_Reload() {
		this.receiverCityDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverCity: res[0].masterCityId,
				});
				this.ReceiverSubDistrict_Reload();
			}
		});
	}

	ReceiverCity_SelectionChanged(e) {
		//this.ReceiverSubDistrict_Reload();
	}

	private Receiver_ResetAll() {
		/**
		 * reset all receiver to null when phone is selected
		 */
		this.spbForm.patchValue({
			receiverNameId: null, receiverName: null,
			receiverStoreId: null, receiverStore: null,
			receiverPlaceId: null, receiverPlace: null,
			receiverAddressId: null, receiverAddress: null,
			receiverRt: null,
			receiverRw: null,
			receiverUrban: null,
			receiverSubDistrict: null,
			receiverCity: null,
			receiverPostalCode: null,
			receiverVia: null, receiverViaId: null,
		});
	}

	private Receiver_ReloadAll() {
		/**
		 * reload all receiver when phone is selected
		 * then set the value if only have one value from API
		 */
		// sender name
		this.receiverNameDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverNameId: res[0].customerNameId,
				});
			}
		});

		// sender store
		this.receiverStoreDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverStoreId: res[0].customerStoreId,
				});
			}
		});

		// sender place
		this.receiverPlaceDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverPlaceId: res[0].customerPlaceId,
				});
			}
		});

		// sender address
		this.receiverAddressDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverAddressId: res[0].customerAddressId,
					receiverAddressText: res[0].customerAddress
				});
			}
		});
	}

	private ReceiverVia_GetData() {
		/**
		 * get data TOG from API
		 *
		 * call by :
		 * - onLoadPage()
		 */
		this.receiverViaDs = new DataSource({
			store: new CustomStore({
				key: 'viaId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'spbcreate/read-via?destCity=' + ((this.spbForm.get('destCity').value === null) ? 0 : this.spbForm.get('destCity').value) )
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	ReceiverVia_Reload() {
		this.receiverViaDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverVia: res[0].viaId,
				});
			}
		});
	}

	receiverPopUpShow() {
		if (this.spbForm.get('destCity').value === null) {  // cek kota kosng 3 sept
			Swal.fire({
				title: 'Tunggu dulu!',text: 'Silahkan pilih kota dan area tujuan terlebih dahulu.',
				imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
				//imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
				//cancelButtonText: 'No, keep it',
			  }).then((result) => {
				if (result.isConfirmed) {} 
			  })
			return;
		}
		this.readReceiverList();
		this.receiverPopUp = true;
	}


	readReceiverList() {

		this.receiverListDs = AspNetData.createStore({
			key: 'customerId',
			loadUrl: AppGlobalVar.BASE_API_URL + 'spbcreate/read-sender-list',
			onBeforeSend: (method, ajaxOptions) => {
				const userToken = localStorage.getItem('accessToken');
				ajaxOptions.headers = {
					Authorization:  'Bearer ' + userToken,
					workCityId:  this.spbForm.get('destCity').value,
					senderPhone: this.punctuationPhone((this.spbForm.get('receiver').value)), // Pati 1 sept
					tipeCust: 'receiver'  // Pati 22nov21
				};
			}
		});
	}

	receiverListChoose = (e) => {
		this.ReceiverPhoneToOld2();
		this.ReceiverListSetToInput(e);
		// this.setFormDataPenerima();
	}

	ReceiverListSetToInput(e) {
		this.receiverListData = e.row.data;
		this.receiverPopUp = false;
		this.cd.detectChanges();
		this.Receiver_ResetAll();
// receiver phone
		this.receiverPhoneDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverId: res[0].customerId,
				});
			}
		});

		this.formSpbUpdate.receiverId 			= e.row.data.customerId;
		this.formSpbUpdate.receiverNameId 		= e.row.data.customerNameId;
		this.formSpbUpdate.receiverStoreId 		= e.row.data.customerStoreId;
		this.formSpbUpdate.receiverPlaceId 		= e.row.data.customerPlaceId;
		this.formSpbUpdate.receiverAddressId 	= e.row.data.customerAddressId;

		this.formSpbUpdate.receiverTelp 		= e.row.data.telp;
		this.formSpbUpdate.receiverPhone 		= e.row.data.phone;
		this.formSpbUpdate.receiverName 		= e.row.data.customerName;
		this.formSpbUpdate.receiverStore 		= e.row.data.customerStore;
		this.formSpbUpdate.receiverPlace 		= e.row.data.customerPlace;
		this.formSpbUpdate.receiverAddress 		= e.row.data.customerAddress;

		this.EditReceiverDisabled = false;

		this.Receiver_ReloadAll();
		if (e.row.data.telp.toString() === 'null')
			this.spbForm.get('receivertelp').setValue('');
		else
			this.spbForm.get('receivertelp').setValue(e.row.data.telp.toString());
		this.cd.detectChanges();
		//
		// variable

		//
		// set variable

		//
		// logic

		// hide popup


	}

	receiverTelpListChoose = (e) => {
		this.ReceiverPhoneToOld2();
		this.ReceiverTelpListSetToInput(e);
		// this.setFormDataPenerima();
	}

	ReceiverTelpListSetToInput(e) {
		this.receiverListData = e.row.data;
		this.receiverTelpPopUp = false;
		this.cd.detectChanges();
		this.Receiver_ResetAll();
// receiver phone
		this.receiverPhoneDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverId: res[0].customerId,
				});
			}
		});

		this.formSpbUpdate.receiverId 			= e.row.data.customerId;
		this.formSpbUpdate.receiverNameId 		= e.row.data.customerNameId;
		this.formSpbUpdate.receiverStoreId 		= e.row.data.customerStoreId;
		this.formSpbUpdate.receiverPlaceId 		= e.row.data.customerPlaceId;
		this.formSpbUpdate.receiverAddressId 	= e.row.data.customerAddressId;

		this.formSpbUpdate.receiverTelp 		= e.row.data.telp;
		this.formSpbUpdate.receiverPhone 		= e.row.data.phone;
		this.formSpbUpdate.receiverName 		= e.row.data.customerName;
		this.formSpbUpdate.receiverStore 		= e.row.data.customerStore;
		this.formSpbUpdate.receiverPlace 		= e.row.data.customerPlace;
		this.formSpbUpdate.receiverAddress 		= e.row.data.customerAddress;

		this.EditReceiverDisabled = false;

		this.Receiver_ReloadAll();
		this.cd.detectChanges();
	}



	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  SENDER
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */

	// SenderPhoneToNew(e) {
	// 	if (e.key === '+') {
	// 		this.spbCreateService.SetSenderIsNew('sender', true);
	// 		this.spbCreateService.SetSenderIsNew('senderName', true);
	// 		this.spbCreateService.SetSenderIsNew('senderStore', true);
	// 		this.spbCreateService.SetSenderIsNew('senderPlace', true);
	// 		this.spbCreateService.SetSenderIsNew('senderAddress', true);
	// 		// this.senderNameNew = true;
	// 		// this.senderStoreNew = true;
	// 		// this.senderPlaceNew = true;
	// 		// this.senderAddressNew = true;

	// 	}
	// }

	SenderPhoneToNew() {
			this.spbCreateService.SetSenderIsNew('sender', true);
			this.spbCreateService.SetSenderIsNew('senderName', true);
			this.spbCreateService.SetSenderIsNew('senderStore', true);
			this.spbCreateService.SetSenderIsNew('senderPlace', true);
			this.spbCreateService.SetSenderIsNew('senderAddress', true);
	}

	SenderPhoneToNew2() {					// Pati 1 sept
		this.spbCreateService.SetSenderIsNew('sender', true);
		this.spbCreateService.SetSenderIsNew('senderName', false);
		this.spbCreateService.SetSenderIsNew('senderStore', false);
		this.spbCreateService.SetSenderIsNew('senderPlace', false);
		this.spbCreateService.SetSenderIsNew('senderAddress', false);
}

	ReceiverPhoneToNew2() {					// Pati 1 sept
		this.spbCreateService.SetReceiverIsNew('receiver', true);
		this.spbCreateService.SetReceiverIsNew('receiverName', false);
		this.spbCreateService.SetReceiverIsNew('receiverStore', false);
		this.spbCreateService.SetReceiverIsNew('receiverPlace', false);
		this.spbCreateService.SetReceiverIsNew('receiverAddress', false);
}

SenderPhoneToNewAfterApprove() {    //Pati 1 Sept
						Swal.fire({
							position: 'top',title: 'Tunggu sebentar!', showConfirmButton: true
						  })
						this.IsSenderDisabled = false;
						this.spbCreateService.SetSenderIsNew('senderName', true);
						this.spbCreateService.SetSenderIsNew('senderStore', true);
						this.spbCreateService.SetSenderIsNew('senderPlace', true);
						this.spbCreateService.SetSenderIsNew('senderAddress', true);
}

// ReceiverPhoneToNewAfterApprove() {    //Pati 1 Sept
// 	this.IsReceiverDisabled = false;
// 	// this.spbCreateService.SetReceiverIsNew('receiver', false);
// 	this.spbCreateService.SetReceiverIsNew('receiverName', true);
// 	this.spbCreateService.SetReceiverIsNew('receiverStore', true);
// 	this.spbCreateService.SetReceiverIsNew('receiverPlace', true);
// 	this.spbCreateService.SetReceiverIsNew('receiverAddress', true);
// }

	
	SenderPhoneToOld(e) {
		if (e.key === '=') {
			this.IsSenderDisabled = true;
			this.spbCreateService.SetSenderIsNew('sender', false);
			this.spbCreateService.SetSenderIsNew('senderName', false);
			this.spbCreateService.SetSenderIsNew('senderStore', false);
			this.spbCreateService.SetSenderIsNew('senderPlace', false);
			this.spbCreateService.SetSenderIsNew('senderAddress', false);
			// this.senderNameNew = false;
			// this.senderStoreNew = false;
			// this.senderPlaceNew = false;
			// this.senderAddressNew = false;
		}
		this.PhoneRestriction(e)
	}

	SenderPhoneToOld2() {
		this.IsSenderDisabled = true;
		this.spbCreateService.SetSenderIsNew('sender', false);
		this.spbCreateService.SetSenderIsNew('senderName', false);
		this.spbCreateService.SetSenderIsNew('senderStore', false);
		this.spbCreateService.SetSenderIsNew('senderPlace', false);
		this.spbCreateService.SetSenderIsNew('senderAddress', false);
	}

	PhoneRestriction(e){
		if (e.key != '1' && e.key != '2' && e.key != '3' && e.key != '4' && e.key != '5' && e.key != '6' && e.key != '7' && e.key != '8' && e.key != '9' && e.key != '0' && e.key != '=' && e.key !='Backspace' && e.key !='Tab' && e.key !='Capslock' && e.key !='ArrowRight' && e.key !='ArrowLeft' && e.key !='ArrowUp' && e.key !='ArrowDown' && e.key !='Shift') {
			alert("INPUT HANYA BISA BERUPA ANGKA!");
		}
	}

	SenderNameToNew(e) {
		if (e.key === '+') {
			this.spbCreateService.SetSenderIsNew('senderName', true);
		}
	}

	SenderNameToOld(e) {
		if (e.key === '=' && this.senderIsNew.sender === false ) {
			this.spbCreateService.SetSenderIsNew('senderName', false);
		}
	}

	// SenderStoreToNew(e) { (e.key === '+') ? this.spbCreateService.SetSenderIsNew('senderStore', true) : '' ; }
	// SenderStoreToOld(e) { ((e.key === '=')  && this.senderIsNew.sender === false ) ? this.spbCreateService.SetSenderIsNew('senderStore', false) : '' ; }

	// SenderPlaceToNew(e) { (e.key === '+') ? this.spbCreateService.SetSenderIsNew('senderPlace', true) : '' ; }
	// SenderPlaceToOld(e) { ((e.key === '=')  && this.senderIsNew.sender === false ) ? this.spbCreateService.SetSenderIsNew('senderPlace', false) : '' ; }

	// SenderAddressToNew(e) { (e.key === '+') ? this.spbCreateService.SetSenderIsNew('senderAddress', true) : '' ; }
	// SenderAddressToOld(e) { ((e.key === '=')  && this.senderIsNew.sender === false ) ? this.spbCreateService.SetSenderIsNew('senderAddress', false) : '' ; }

	SenderStoreToNew() { (this.IsSenderDisabled === false) ? this.spbCreateService.SetSenderIsNew('senderStore', true) : '' ; }
	SenderStoreToOld() { (this.IsSenderDisabled === true) ? this.spbCreateService.SetSenderIsNew('senderStore', false) : '' ; }

	SenderPlaceToNew() { (this.IsSenderDisabled === false) ? this.spbCreateService.SetSenderIsNew('senderPlace', true) : '' ; }
	SenderPlaceToOld() { (this.IsSenderDisabled === true)? this.spbCreateService.SetSenderIsNew('senderPlace', false) : '' ; }

	SenderAddressToNew() { (this.IsSenderDisabled === false) ? this.spbCreateService.SetSenderIsNew('senderAddress', true) : '' ; }
	SenderAddressToOld() { (this.IsSenderDisabled === true)? this.spbCreateService.SetSenderIsNew('senderAddress', false) : '' ; }


	SenderPhone_OnItemClick(e) {
		/**
		 * set customerId so API customer sender can consume it
		 * then reset and reload all sender
		 */
		this.senderCustomerId = e.itemData.customerId;

		this.Sender_ResetAll();
		this.Sender_ReloadAll();
	}

	private Sender_ResetAll() {
		/**
		 * reset all sender to null when phone is selected
		 */
		this.spbForm.patchValue({
			senderNameId: null, senderName: null,
			senderStoreId: null, senderStore: null,
			senderPlaceId: null, senderPlace: null,
			senderAddressId: null, senderAddress: null,
			senderRt: null,
			senderRw: null,
			senderUrbanId: null,
			senderSubDistrictId: null,
			senderCityId: null,
			senderPostalCodeId: null,
		});

		this.formSpbUpdate.senderId 		= null;
		this.formSpbUpdate.senderNameId 	= null;
		this.formSpbUpdate.senderStoreId 	= null;
		this.formSpbUpdate.senderPlaceId 	= null;
		this.formSpbUpdate.senderAddressId 	= null;

		this.EditSenderDisabled = false;
	}

	public Sender_ReloadAll() {
		/**
		 * reload all sender when phone is selected
		 * then set the value  if only have one value from API
		 */
		 
		// sender name
		this.senderNameDs.reload().done((res) => {
			
			if (res.length === 1) {
				this.spbForm.patchValue({
					senderNameId: res[0].customerNameId,
				});

				this.formSpbUpdate.senderNameId = res[0].customerNameId;
			}
		});

		// sender store
		this.senderStoreDs.reload().done((res) => {

			if (res.length === 1) {
				this.spbForm.patchValue({
					senderStoreId: res[0].customerStoreId,
				});

				this.formSpbUpdate.senderStoreId = res[0].customerStoreId;
			}
		});

		// sender place
		this.senderPlaceDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					senderPlaceId: res[0].customerPlaceId,
				});

				this.formSpbUpdate.senderPlaceId = res[0].customerPlaceId;
			}
		});

		// sender place
		this.senderAddressDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					senderAddressId: res[0].customerAddressId,
				});

				this.formSpbUpdate.senderAddressId = res[0].customerAddressId;
			}
		});
	}


	readSenderList() {

		// 'https://js.devexpress.com/Demos/Mvc/api/DataGridWebApi' + '/Orders',
		this.senderListDs = AspNetData.createStore({
			key: 'customerId',
			// key: 'OrderID',
			loadUrl: AppGlobalVar.BASE_API_URL + 'spbcreate/read-sender-list',
			// loadUrl: 'https://js.devexpress.com/Demos/Mvc/api/DataGridWebApi' + '/Orders',
			onBeforeSend: (method, ajaxOptions) => {
				// ajaxOptions.xhrFields = { withCredentials: true };
				const userToken = localStorage.getItem('accessToken');
				ajaxOptions.headers = {
					Authorization:  'Bearer ' + userToken,
					workCityId:  this.userGlobalVar.WORK_CITY_ID,
					senderPhone: this.punctuationPhone((this.spbForm.get('sender').value)), // Pati 1 sept
					tipeCust: 'sender'  // Pati 22nov21
				};
			}
		});
	}

	readSenderTelpList() {
		console.log('kena disini');
		// 'https://js.devexpress.com/Demos/Mvc/api/DataGridWebApi' + '/Orders',
		this.senderTelpListDs = AspNetData.createStore({
			key: 'customerId',
			// key: 'OrderID',
			loadUrl: AppGlobalVar.BASE_API_URL + 'spbcreate/read-sender-telp-list',
			// loadUrl: 'https://js.devexpress.com/Demos/Mvc/api/DataGridWebApi' + '/Orders',
			onBeforeSend: (method, ajaxOptions) => {
				// ajaxOptions.xhrFields = { withCredentials: true };
				const userToken = localStorage.getItem('accessToken');
				ajaxOptions.headers = {
					Authorization:  'Bearer ' + userToken,
					workCityId:  this.userGlobalVar.WORK_CITY_ID,
					senderTelp: this.punctuationPhone((this.spbForm.get('sendertelp').value)), // Pati 1 sept
					tipeCust: 'sender'  // Pati 22nov21
				};
			}
		});
	}

	readReceiverTelpList() {
		console.log('Ini telp penerima :' + this.punctuationPhone((this.spbForm.get('receivertelp').value)));
		this.senderTelpListDs = AspNetData.createStore({
			key: 'customerId',
			// key: 'OrderID',
			loadUrl: AppGlobalVar.BASE_API_URL + 'spbcreate/read-sender-telp-list',
			// loadUrl: 'https://js.devexpress.com/Demos/Mvc/api/DataGridWebApi' + '/Orders',
			onBeforeSend: (method, ajaxOptions) => {
				// ajaxOptions.xhrFields = { withCredentials: true };
				const userToken = localStorage.getItem('accessToken');
				ajaxOptions.headers = {
					Authorization:  'Bearer ' + userToken,
					workCityId:  this.spbForm.get('destCity').value,
					senderTelp: this.punctuationPhone((this.spbForm.get('receivertelp').value)), // Pati 1 sept
					tipeCust: 'receiver'  // Pati 22nov21
				};
			}
		});
	}

	// Pati 18Mei22
	readSenderListIdPelanggan() {  

		Swal.fire({
			position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 500
		  })
		// 	// 'https://js.devexpress.com/Demos/Mvc/api/DataGridWebApi' + '/Orders',
		this.senderIDPelDs = AspNetData.createStore({
			key: 'customerId',
			// key: 'OrderID',
			loadUrl: AppGlobalVar.BASE_API_URL + 'spbcreate/read-sender-idpel',
			onBeforeSend: (method, ajaxOptions) => {
				// ajaxOptions.xhrFields = { withCredentials: true };
				const userToken = localStorage.getItem('accessToken');
				ajaxOptions.headers = {
					Authorization:  'Bearer ' + userToken,
					workCityId:  this.userGlobalVar.WORK_CITY_ID,
					senderPhone: this.punctuationPhone((this.spbForm.get('sender').value)), // Pati 1 sept
					tipeCust: 'sender',  // Pati 22nov21
					senderIdPel : this.spbForm.get('idcustpengirim').value
				};
			}
		});

	}

	// Pati 25Mei22
	readReceiverListIdPelanggan() {  
		

		Swal.fire({
			position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 500
		  })
		this.receiverIDPelDs = AspNetData.createStore({
			key: 'customerId',

			loadUrl: AppGlobalVar.BASE_API_URL + 'spbcreate/read-receiver-idpel',

			onBeforeSend: (method, ajaxOptions) => {
				
				const userToken = localStorage.getItem('accessToken');
				
				ajaxOptions.headers = {
					Authorization:  'Bearer ' + userToken,
					workCityId: this.spbForm.get('destCity').value, //this.userGlobalVar.WORK_CITY_ID,
					receiverPhone: this.punctuationPhone((this.spbForm.get('receiver').value)), 
					tipeCust: 'receiver',  // Pati 22nov21
					receiverIdPel : this.spbForm.get('idcustpenerima').value
				};
			}
		});

	}

	getCustomerSenderPhone() {
		/**
		 * get data phone from API
		 *
		 * call by :
		 * - onLoadPage()
		 */
		this.senderPhoneDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					if (loadOptions.searchValue !== null) {
						this.senderListData = null;
					}
					const senderPhoneUrl = (this.senderListData === null || this.senderListData === undefined) ? AppGlobalVar.BASE_API_URL + 'customer/phone?cityId=' + this.userGlobalVar.WORK_CITY_ID
						+ '&searchValue='  + loadOptions.searchValue + '&type=&customerId=' :
						AppGlobalVar.BASE_API_URL + 'customer/phone?cityId=1&searchValue=1&type=&customerId=' + this.senderListData.customerId;
					return this.http.get(senderPhoneUrl )
						.toPromise()
						.then((result: any) => {
							// Here, you can perform operations unsupported by the server
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?id=' + key)
						.toPromise();
				}
			})
		});
	}

	getSenderName() {
		
		this.senderNameDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					const senerNameUrl = (this.senderListData === null) ? AppGlobalVar.BASE_API_URL + 'customer/name?customerId=' + this.senderCustomerId :
						AppGlobalVar.BASE_API_URL + 'customer/name?customerNameId=' + this.senderListData.customerNameId;
					return this.http.get(senerNameUrl)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?id=' + key)
						.toPromise();
				}
			})
		});
	}

	SenderStore_GetData() {

		this.senderStoreDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					const senderStoreUrl = (this.senderListData === null) ? AppGlobalVar.BASE_API_URL + 'customer/store?customerId=' + this.senderCustomerId :
						AppGlobalVar.BASE_API_URL + 'customer/store?customerStoreId=' + this.senderListData.customerStoreId;
					return this.http.get(senderStoreUrl)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/store?id=' + key)
						.toPromise();
				}
			})
		});
	}

	SenderPlace_GetData() {
		this.senderPlaceDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					const senderPlaceUrl = (this.senderListData === null) ? AppGlobalVar.BASE_API_URL + 'customer/place?customerId=' + this.senderCustomerId :
						AppGlobalVar.BASE_API_URL + 'customer/place?customerPlaceId=' + this.senderListData.customerPlaceId;
					return this.http.get(senderPlaceUrl)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/place?id=' + key)
						.toPromise();
				}
			})
		});
	}

	SenderAddress_GetData() {
		this.senderAddressDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					const senderAddressUrl = (this.senderListData === null) ? AppGlobalVar.BASE_API_URL + 'customer/address?customerId=' + this.senderCustomerId :
						AppGlobalVar.BASE_API_URL + 'customer/address?customerAddressId=' + this.senderListData.customerAddressId;
					return this.http.get(senderAddressUrl)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/address?id=' + key)
						.toPromise();
				}
			})
		});
	}

	senderPopUpShow() {
		if (this.spbForm.get('destCity').value === null) {
			Swal.fire({
				title: 'Tunggu dulu!',text: 'Silahkan pilih kota dan area tujuan terlebih dahulu.',
				imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
				//imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
				//cancelButtonText: 'No, keep it',
			  }).then((result) => {
				if (result.isConfirmed) {} 
			  })
			return;
		}
		this.senderPopUp = true;
		this.readSenderList();
	}

	senderTelpPopUpShow() {
		if (this.spbForm.get('destCity').value === null) {
			Swal.fire({
				title: 'Tunggu dulu!',text: 'Silahkan pilih kota dan area tujuan terlebih dahulu.',
				imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
				//imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
				//cancelButtonText: 'No, keep it',
			  }).then((result) => {
				if (result.isConfirmed) {} 
			  })
			return;
		}
		this.senderTelpPopUp = true;
		this.readSenderTelpList();
	}

	receiverTelpPopUpShow() {
		if (this.spbForm.get('destCity').value === null) {
			Swal.fire({
				title: 'Tunggu dulu!',text: 'Silahkan pilih kota dan area tujuan terlebih dahulu.',
				imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
				//imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
				//cancelButtonText: 'No, keep it',
			  }).then((result) => {
				if (result.isConfirmed) {} 
			  })
			return;
		}
		this.receiverTelpPopUp = true;
		this.readReceiverTelpList();
	}

	senderNamePopUpShow() {
		if (this.spbForm.get('destCity').value === null) {
			Swal.fire({
				title: 'Tunggu dulu!',text: 'Silahkan pilih kota dan area tujuan terlebih dahulu.',
				imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
				//imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
				//cancelButtonText: 'No, keep it',
			  }).then((result) => {
				if (result.isConfirmed) {} 
			  })
			return;
		}
		console.log("popup true");
		this.senderNamePopUp = true;
		this.readSenderNameList();
	}

	readSenderNameList() {
		//console.log("ke readsenderList");
		this.senderNameListDs = AspNetData.createStore({
			key: 'customerId',
			loadUrl: AppGlobalVar.BASE_API_URL + 'spbcreate/read-sender-name-list',
			onBeforeSend: (method, ajaxOptions) => {
				const userToken = localStorage.getItem('accessToken');
				ajaxOptions.headers = {
					Authorization:  'Bearer ' + userToken,
					workCityId:  this.userGlobalVar.WORK_CITY_ID,
					senderName: this.spbForm.get('senderName').value, // Pati 09Jun22
					tipeCust: 'sender'  // Pati 22nov21
				};
			}
		});
	}

	receiverNamePopUpShow() {
		if (this.spbForm.get('destCity').value === null) {
			Swal.fire({
				title: 'Tunggu dulu!',text: 'Silahkan pilih kota dan area tujuan terlebih dahulu.',
				imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
				//imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
				  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
				//cancelButtonText: 'No, keep it',
			  }).then((result) => {
				if (result.isConfirmed) {} 
			  })
			return;
		}
		this.receiverNamePopUp = true;
		this.readReceiverNameList();
	}

	readReceiverNameList() {
		console.log("ke readreceiverList");
		this.receiverNameListDs = AspNetData.createStore({
			key: 'customerId',
			loadUrl: AppGlobalVar.BASE_API_URL + 'spbcreate/read-receiver-name-list',
			onBeforeSend: (method, ajaxOptions) => {
				const userToken = localStorage.getItem('accessToken');
				ajaxOptions.headers = {
					Authorization:  'Bearer ' + userToken,
					workCityId:  this.spbForm.get('destCity').value,
					receiverName: this.spbForm.get('receiverName').value, // Pati 09Jun22
					tipeCust: 'receiver'  // Pati 22nov21
				};
			}
		});
	}

	senderNameListChoose = (e) => {
		this.SenderPhoneToOld2();
		this.SenderNameListSetToInput(e);
		// this.setFormDataPengirim();
	}

	SenderNameListSetToInput(e) {
		this.senderListData = e.row.data;
		this.senderNamePopUp = false;
		this.cd.detectChanges();
		this.Sender_ResetAll();
// sender phone
		// console.log("lihat e");
        // console.log(e.row.data.phone);
		this.formSpbUpdate.senderId 			= e.row.data.customerId;
		this.formSpbUpdate.senderNameId 		= e.row.data.customerNameId;
		this.formSpbUpdate.senderStoreId 		= e.row.data.customerStoreId;
		this.formSpbUpdate.senderPlaceId 		= e.row.data.customerPlaceId;
		this.formSpbUpdate.senderAddressId 		= e.row.data.customerAddressId;
		
		this.formSpbUpdate.senderPhone 			= e.row.data.phone;
		this.formSpbUpdate.senderName 			= e.row.data.customerName;
		this.formSpbUpdate.senderStore 			= e.row.data.customerStore;
		this.formSpbUpdate.senderPlace 			= e.row.data.customerPlace;
		this.formSpbUpdate.senderAddress 		= e.row.data.customerAddress;
		
		this.senderPhoneDs.reload().done((res) => {
			if (res.length === 1) {
				
				this.spbForm.patchValue({
					senderId: res[0].customerId,
				});

				this.formSpbUpdate.senderId = res[0].customerId;
			}
		});

		this.Sender_ReloadAll();
		this.cd.detectChanges();
		//

	}

	receiverNameListChoose = (e) => {
		this.ReceiverPhoneToOld2();
		this.ReceiverNameListSetToInput(e);
		// this.setFormDataPengirim();
	}

	ReceiverNameListSetToInput(e) {
			this.receiverListData = e.row.data;
			this.receiverNamePopUp = false;
			this.cd.detectChanges();
			this.Receiver_ResetAll();
	// receiver phone
			this.receiverPhoneDs.reload().done((res) => {
				if (res.length === 1) {
					this.spbForm.patchValue({
						receiverId: res[0].customerId,
					});
				}
			});
	
			this.formSpbUpdate.receiverId 			= e.row.data.customerId;
			this.formSpbUpdate.receiverNameId 		= e.row.data.customerNameId;
			this.formSpbUpdate.receiverStoreId 		= e.row.data.customerStoreId;
			this.formSpbUpdate.receiverPlaceId 		= e.row.data.customerPlaceId;
			this.formSpbUpdate.receiverAddressId 	= e.row.data.customerAddressId;
	
			this.formSpbUpdate.receiverPhone 		= e.row.data.phone;
			this.formSpbUpdate.receiverName 		= e.row.data.customerName;
			this.formSpbUpdate.receiverStore 		= e.row.data.customerStore;
			this.formSpbUpdate.receiverPlace 		= e.row.data.customerPlace;
			this.formSpbUpdate.receiverAddress 		= e.row.data.customerAddress;
	
			// this.receiverPhoneDs.reload().done((res) => {
			// 	if (res.length === 1) {
					
			// 		this.spbForm.patchValue({
			// 			receiverId: res[0].customerId,
			// 		});
	
			// 		this.formSpbUpdate.receiverId = res[0].customerId;
			// 	}
			// });

			//this.EditReceiverDisabled = false;
	
			this.Receiver_ReloadAll();
			this.cd.detectChanges();
		}

	// Pati 19Mei22
	senderPelangganIDPopUpShow() {
		
		this.senderPelangganIDPopUp = true;
		this.readSenderListIdPelanggan();
		
	}

	// Pati 25Mei22
	receiverPelangganIDPopUpShow() {
		console.log('receiverPelangganIDPopUpShow');
		this.receiverPelangganIDPopUp = true;
		this.readReceiverListIdPelanggan();
		
	}

	CariNamaPengirim(){
		this.approvalCariNamaPengirim = true;   // Pati  8 jun 22
		this.fromCreate.approveUser = "Isi User Name SM";
		this.fromCreate.approvePassword = ""
		}

	CariNamaPenerima(){
		this.approvalCariNamaPenerima = true;   // Pati  8 jun 22
		this.fromCreate.approveUser = "Isi User Name SM";
		this.fromCreate.approvePassword = ""
		}

	ResetDataPengirim(){
		this.IsSenderDisabled = true;
		this.spbForm.get('idcustpengirim').enable(); this.spbForm.get('idcustpengirim').setValue("");
		this.spbForm.get('sendertelp').enable(); this.spbForm.get('sendertelp').setValue("");
		this.spbForm.get('sender').setValue(""); this.spbForm.get('sender').enable();
		this.SenderPhoneToNew2(); 
		this.spbForm.get('senderNameId').setValue(0); this.spbForm.get('senderStoreId').setValue(0);
		this.spbForm.get('senderPlaceId').setValue(0); this.spbForm.get('senderAddressId').setValue(0);
		this.appGlobalVar.ENTRY_IDPEL_SENDER = 0; this.appGlobalVar.ENTRY_IDPEL_RECEIVER = 0;
		this.appGlobalVar.ENTRY_NOTELP_SENDER = 0; this.appGlobalVar.ENTRY_NOTELP_RECEIVER = 0;
		this.appGlobalVar.ENTRY_NOHP_SENDER = 0; this.appGlobalVar.ENTRY_NOHP_RECEIVER = 0;
		this.appGlobalVar.ENTRY_NAMA_SENDER = 0; this.appGlobalVar.ENTRY_NAMA_RECEIVER = 0;
	}

	ResetDataPenerima(){
		this.IsReceiverDisabled = true;
		this.spbForm.get('idcustpenerima').enable(); this.spbForm.get('idcustpenerima').setValue("");
		this.spbForm.get('receivertelp').enable(); this.spbForm.get('receivertelp').setValue("");
		this.spbForm.get('receiver').setValue(""); this.spbForm.get('receiver').enable();
		this.ReceiverPhoneToNew2(); 
		this.spbForm.get('receiverNameId').setValue(0); this.spbForm.get('receiverStoreId').setValue(0);
		this.spbForm.get('receiverPlaceId').setValue(0); this.spbForm.get('receiverAddressId').setValue(0);
		this.appGlobalVar.ENTRY_IDPEL_SENDER = 0; this.appGlobalVar.ENTRY_IDPEL_RECEIVER = 0;
		this.appGlobalVar.ENTRY_NOTELP_SENDER = 0; this.appGlobalVar.ENTRY_NOTELP_RECEIVER = 0;
		this.appGlobalVar.ENTRY_NOHP_SENDER = 0; this.appGlobalVar.ENTRY_NOHP_RECEIVER = 0;
		this.appGlobalVar.ENTRY_NAMA_SENDER = 0; this.appGlobalVar.ENTRY_NAMA_RECEIVER = 0;
	}

	senderEnable(){
		this.IsSenderDisabled = false;
		this.senderPopUp = false;
		this.Sender_ResetAll();
		this.spbForm.patchValue({
			senderId: null,
			sender: null
		});
		this.SenderPhoneToNew();
	}

	senderEnableAfterApprove(){    // Pati 1 Sept
		this.IsSenderDisabled = false;
		this.approvalWindowVisible = false;
		this.senderPopUp = false;
		this.Sender_ResetAll();
		// this.spbForm.patchValue({
		// 	senderId: null,
		// 	sender: null
		//});
		this.SenderPhoneToNewAfterApprove();
	}

	receiverEnableAfterApprove(){    // Pati 1 Sept
		Swal.fire({
			position: 'top',title: 'Tunggu sebentar!', showConfirmButton: false, timer: 300
		  })
		this.IsReceiverDisabled = false;
		this.approvalPenerimaVisible = false;
		this.receiverPopUp = false;
		// this.Receiver_ResetAll();
		// // this.spbForm.patchValue({
		// // 	senderId: null,
		// // 	sender: null
		// //});
		// this.ReceiverPhoneToNewAfterApprove();
		this.Receiver_ResetAll();
		// this.spbForm.patchValue({
		// 	receiverId: null,
		// 	receiver: null
		// });
				
		if (this.spbForm.get('destCity').value != null){
			this.ReceiverCity_Reload();
			if (this.spbForm.get('destArea').value != null){
				this.ReceiverSubDistrict_Reload();
				this.ReceiverUrban_Reload();
			}
		}
		this.ReceiverPhoneToNew();
	}

	senderEnableForApprove(){
		this.approvalWindowVisible = true;   // Pati  1 Sept
		this.fromCreate.approveUser = "";
		this.fromCreate.approvePassword = ""
	}

	receiverEnableForApprove(){
		this.approvalPenerimaVisible = true;   // Pati  1 Sept
		this.fromCreate.approveUser = "";
		this.fromCreate.approvePassword = ""
	}

	receiverEnableForApprove2(){
		
		var hasilnya = Swal.fire({
			title: 'Login Approval', 
			html: 
			   'Username: <input id="swal-input1" class="swal2-input" width="1000">' + 
			  'Password: <input id="swal-input2" class="swal2-input" type="password">',
			focusConfirm: false,
			preConfirm: () => {
			  return [
				// document.getElementById('swal-input1').value,
				// document.getElementById('swal-input2').value
			  ]
			}
		  })
		  
		  if (hasilnya) {
			Swal.fire(JSON.stringify('Ada benarnya'))
		  }
	}

	receiverEnable(){
		this.IsReceiverDisabled = false;
		this.receiverPopUp = false;
		this.Receiver_ResetAll();
		this.spbForm.patchValue({
			receiverId: null,
			receiver: null
		});
		this.ReceiverPhoneToNew();
		if (this.spbForm.get('destCity').value != null){
			this.ReceiverCity_Reload();
			if (this.spbForm.get('destArea').value != null){
				this.ReceiverSubDistrict_Reload();
				this.ReceiverUrban_Reload();
			}
		}
	}

	Punctuation_OnFocusOut(e,input) {
		var temp = '';
		var tempNumber = 0;


		if (input === 'senderPhone') {
			tempNumber = this.punctuationPhone((this.spbForm.get('sender').value))
			this.spbForm.patchValue({
				sender: tempNumber
			});
		}
 
		if (input === 'receiverPhone') {
			tempNumber = this.punctuationPhone((this.spbForm.get('receiver').value))
			this.spbForm.patchValue({
				receiver: tempNumber
			});
		}     

		if (input === 'senderName') {
			temp = this.punctuationNewName((this.spbForm.get('senderName').value))
			this.spbForm.patchValue({
				senderName: temp
			});
		}
		if (input === 'senderPlace') {
			temp = this.punctuationPlace((this.spbForm.get('senderPlace').value))
			this.spbForm.patchValue({
				senderPlace: temp
			});
		}
		if (input === 'senderStore') {
			temp = this.punctuationStore((this.spbForm.get('senderStore').value))
			this.spbForm.patchValue({
				senderStore: temp
			});
		}
		if (input === 'senderAddress') {
			// temp = this.punctuation((this.spbForm.get('senderAddress').value))
			// this.spbForm.patchValue({
			// 	senderAddress: temp
			// });
		}
		if (input === 'receiverName') {
			temp = this.punctuationNewName((this.spbForm.get('receiverName').value))
			this.spbForm.patchValue({
				receiverName: temp
			});
		}
		if (input === 'receiverPlace') {
			temp = this.punctuationPlace((this.spbForm.get('receiverPlace').value))
			this.spbForm.patchValue({
				receiverPlace: temp
			});
		}
		if (input === 'receiverStore') {
			temp = this.punctuationStore((this.spbForm.get('receiverStore').value))
			this.spbForm.patchValue({
				receiverStore: temp
			});
		}
		if (input === 'receiverAddress') {
			// temp = this.punctuation((this.spbForm.get('receiverAddress').value))
			// this.spbForm.patchValue({
			// 	receiverAddress: temp
			// });
		}

	}

	punctuationPhone(int){
		var finalNumber = 0;
		if (int!=null){
			finalNumber = int.replace(/\D/g,'');;
		}
		else {finalNumber = int}

		return finalNumber;
	}

	punctuationName(str){
		// TES DATA: BP. KH. KHOERUDIN MA'RUF M.PD S.PD, M.AG. DRS
		var finalString = ''
		if (str!=null){
			str = " "+str+" ";
			var s;
			if(str.length > 0){
				s = str;
			}else{
				s = str.toUpperCase();
			}
			// var s = str.toUpperCase();
			var punctuationless = s.replace(/[.,\/#!$%\^&\*;:{}=\-_`'"~()@1234567890]/g,"");
			for (var i=0; i<=5; i++){
				//var punctuationless = punctuationless.replace(/ SH | MPD | SSI | SPDI | SKOM | SE | ST | MAG | SPD | SSOS | SKED | MTI | MKOM | KH | KIAI | KIYAI | KYIAYI | NYAI | USTAD | USTADZ | USTAZ | PAK | BAPAK | JL | HAJI | STORE | IBU | BU | BP | BPK | MAS | MBAK | MBA | KK | KAKAK | TUAN | NYONYA | NY | TOKO | H | HJ | GANG | GG | JLN | JALAN | PROF | DR | DRA | DRH | DRS | DOKTER | BRIPKA | BRIPDA | BRIPTU | SERSAN | MAYOR | LETKOL | LETNAN | KOLONEL | JENDRAL | BRIGADIR | OM | LETJEND | KAPTEN /g," ");
				var punctuationless = punctuationless.replace(/ PA | AIPDA | IR | TN | BOS | MBAH | KOMPOL | SH | MPD | SSI | SPDI | SKOM | SE | ST | MAG | SPD | SSOS | SKED | MTI | MKOM | KH | KIAI | KIYAI | KYIAYI | NYAI | USTAD | USTADZ | USTAZ | PAK | BAPAK | JL | HAJI | IBU | BU | BP | BPK | MAS | MBAK | MBA | KK | KAKAK | TUAN | NYONYA | NY | H | HJ | PROF | DR | DRA | DRH | DRS | DOKTER | BRIPKA | IPDA | BRIPDA | BRIPTU | SERSAN | MAYOR | LETKOL | LETNAN | KOLONEL | JENDRAL | BRIGADIR | OM | LETJEND | KAPTEN | COL | COLL | COLL. | COLLECTION | BOUTIQUE | BUTIK | BOUTIQ | COLECTION | KOLEKSION | TOKO | TK /g," ");  // PATI 7 SEPT 21
			}
			punctuationless = punctuationless.replace(/^./, '');
			punctuationless = punctuationless.replace(/.$/,'')
			finalString = punctuationless.replace(/\s{2,}/g," ");
		}
		else {finalString = str}

		return finalString;
	}

	punctuationNewName(str){
		var finalString = ''
		if (str!=null){
			str = " "+str+" ";
			var s = str.toUpperCase();
			//var punctuationless = s.replace(/[.,\/#!$%\^&\*;:{}=\-_`'"~()@]/g,"");
			var punctuationless = s.replace(/[.,\/#!$%\^&\*;:{}=\-_`'"~()@1234567890]/g,"");
			for (var i=0; i<=5; i++){
				var punctuationless = punctuationless.replace(/ PA | AIPDA | IR | TN | BOS | MBAH | KOMPOL | SH | MPD | SSI | SPDI | SKOM | SE | ST | MAG | SPD | SSOS | SKED | MTI | MKOM | KH | KIAI | KIYAI | KYIAYI | NYAI | USTAD | USTADZ | USTAZ | PAK | BAPAK | JL | HAJI | IBU | BU | BP | BPK | MAS | MBAK | MBA | KK | KAKAK | TUAN | NYONYA | NY | H | HJ | PROF | DR | DRA | DRH | DRS | DOKTER | BRIPKA | IPDA | BRIPDA | BRIPTU | SERSAN | MAYOR | LETKOL | LETNAN | KOLONEL | JENDRAL | BRIGADIR | OM | LETJEND | KAPTEN | COL | COLL | COLL. | COLLECTION | BOUTIQUE | BUTIK | BOUTIQ | COLECTION | KOLEKSION | TOKO | TK /g," ");
			}
			punctuationless = punctuationless.replace(/^./, '');
			punctuationless = punctuationless.replace(/.$/,'')
			finalString = punctuationless.replace(/\s{2,}/g," ");
		}
		else {finalString = str}

		return finalString;
	}

	punctuationStore(str){
		var finalString = ''
		if (str!=null){
			str = " "+str+" ";
			var s = str.toUpperCase();
			var punctuationless = s.replace(/[.,\/#!$%\^&\*;:{}=\-_`'"~()@]/g,"");
			for (var i=0; i<=5; i++){
				var punctuationless = punctuationless.replace(/ STORE | PT | CV | FSH | FASHION | SUPPLIER | UD | PO | COLLECTION | BOUTIQUE | BUTIK | BOUTIQ | COLECTION | KOLEKSION | COLL. | COLL | TK | TOKO | STORE  /g," ");
			}
			punctuationless = punctuationless.replace(/^./, '');
			punctuationless = punctuationless.replace(/.$/,'')
			finalString = punctuationless.replace(/\s{2,}/g," ");
		}
		else {finalString = str}

		return finalString;
	}

	punctuationPlace(str){
		var finalString = ''
		if (str!=null){
			str = " "+str+" ";
			var s = str.toUpperCase();
			var punctuationless = s.replace(/[.,\/#!$%\^&\*;:{}=\-_`'"~()@]/g,"");
			for (var i=0; i<=5; i++){
				var punctuationless = punctuationless.replace(/ GANG | GG | JL | JL. | JALAN | JLN  /g," ");
			}
			punctuationless = punctuationless.replace(/^./, '');
			punctuationless = punctuationless.replace(/.$/,'')
			finalString = punctuationless.replace(/\s{2,}/g," ");
		}
		else {finalString = str}

		return finalString;
	}
	
	punctuation(str){
		var finalString = ''
		if (str!=null){
			str = " "+str+" ";
			var s = str.toUpperCase();
			var punctuationless = s.replace(/[.,\/#!$%\^&\*;:{}=\-_`'"~()@]/g,"");
			for (var i=0; i<=5; i++){
				var punctuationless = punctuationless.replace(/ SH | MPD | SSI | SPDI | SKOM | SE | ST | MAG | SPD | SSOS | SKED | MTI | MKOM | KH | KIAI | KIYAI | KYIAYI | NYAI | USTAD | USTADZ | USTAZ | PAK | BAPAK | JL | HAJI | STORE | IBU | BU | BP | BPK | MAS | MBAK | MBA | KK | KAKAK | TUAN | NYONYA | NY | TOKO | H | HJ | GANG | GG | JLN | JALAN | PROF | DR | DRA | DRH | DRS | DOKTER | BRIPKA | BRIPDA | BRIPTU | SERSAN | MAYOR | LETKOL | LETNAN | KOLONEL | JENDRAL | BRIGADIR | OM | LETJEND | KAPTEN | TK | HAJI | COLLECTION | BOUTIQUE | BUTIK | BOUTIQ | COLECTION | KOLEKSION | COLL | IPDA /g," ");
			}
			punctuationless = punctuationless.replace(/^./, '');
			punctuationless = punctuationless.replace(/.$/,'')
			finalString = punctuationless.replace(/\s{2,}/g," ");
		}
		else {finalString = str}

		return finalString;
	}

	senderListChoose = (e) => {
		this.SenderPhoneToOld2();
		this.SenderListSetToInput(e);
		// this.setFormDataPengirim();
	}

	SenderListSetToInput(e) {
		console.log('setting update telepon : ', e.row.data);
		
		this.senderListData = e.row.data;
		this.senderPopUp = false;
		this.cd.detectChanges();
		this.Sender_ResetAll();
// sender phone

		this.formSpbUpdate.senderId 			= e.row.data.customerId;
		this.formSpbUpdate.senderNameId 		= e.row.data.customerNameId;
		this.formSpbUpdate.senderStoreId 		= e.row.data.customerStoreId;
		this.formSpbUpdate.senderPlaceId 		= e.row.data.customerPlaceId;
		this.formSpbUpdate.senderAddressId 		= e.row.data.customerAddressId;
		
		this.formSpbUpdate.senderTelp 			= e.row.data.telp;
	    this.formSpbUpdate.senderPhone 			= e.row.data.phone;
	    this.formSpbUpdate.senderTelp 			= e.row.data.telp;
		this.formSpbUpdate.senderName 			= e.row.data.customerName;
		this.formSpbUpdate.senderStore 			= e.row.data.customerStore;
		this.formSpbUpdate.senderPlace 			= e.row.data.customerPlace;
		this.formSpbUpdate.senderAddress 		= e.row.data.customerAddress;
		
		this.senderPhoneDs.reload().done((res) => {
			if (res.length === 1) {
				
				this.spbForm.patchValue({
					senderId: res[0].customerId,
				});

				this.formSpbUpdate.senderId = res[0].customerId;
			}
		});

		this.Sender_ReloadAll();
		if (e.row.data.telp.toString() === 'null')
			this.spbForm.get('sendertelp').setValue('');
		else
			this.spbForm.get('sendertelp').setValue(e.row.data.telp.toString());
		this.cd.detectChanges();
		//
		// variable

		//
		// set variable

		//
		// logic

		// hide popup


	}

	senderTelpListChoose = (e) => {
		this.SenderPhoneToOld2();
		this.SenderTelpListSetToInput(e);
		// this.setFormDataPengirim();
	}

	SenderTelpListSetToInput(e) {
		this.senderListData = e.row.data;
		this.senderTelpPopUp = false;
		this.cd.detectChanges();
		this.Sender_ResetAll();
// sender phone

		this.formSpbUpdate.senderId 			= e.row.data.customerId;
		this.formSpbUpdate.senderNameId 		= e.row.data.customerNameId;
		this.formSpbUpdate.senderStoreId 		= e.row.data.customerStoreId;
		this.formSpbUpdate.senderPlaceId 		= e.row.data.customerPlaceId;
		this.formSpbUpdate.senderAddressId 		= e.row.data.customerAddressId;
		
		this.formSpbUpdate.senderTelp 			= e.row.data.telp;
	    this.formSpbUpdate.senderPhone 			= e.row.data.phone;
		this.formSpbUpdate.senderName 			= e.row.data.customerName;
		this.formSpbUpdate.senderStore 			= e.row.data.customerStore;
		this.formSpbUpdate.senderPlace 			= e.row.data.customerPlace;
		this.formSpbUpdate.senderAddress 		= e.row.data.customerAddress;
		
		this.senderPhoneDs.reload().done((res) => {
			if (res.length === 1) {
				
				this.spbForm.patchValue({
					senderId: res[0].customerId,
				});

				this.formSpbUpdate.senderId = res[0].customerId;
			}
		});
		this.Sender_ReloadAll();
		this.cd.detectChanges();
		//
		// variable

		//
		// set variable

		//
		// logic

		// hide popup


	}

	senderIdPelListChoose = (e) => {
		this.SenderPhoneToOld2();
		if (this.spbForm.get('idcustpengirim').value.substring(0,3) === UserGlobalVar.WORK_CITY_NAME.substring(0,3)){
			this.spbForm.get('idcustpengirim').setValue(this.spbForm.get('idcustpengirim').value);
		}
		else{
			this.spbForm.get('idcustpengirim').setValue(UserGlobalVar.WORK_CITY_NAME.substring(0,3) + this.spbForm.get('idcustpengirim').value);
		}
		
		this.SenderIdPelListSetToInput(e);
		// this.setFormDataPengirim();

	}

	SenderIdPelListSetToInput(e) {
		this.senderListData = e.row.data;
		this.senderPelangganIDPopUp = false;
		this.cd.detectChanges();
		this.Sender_ResetAll();
// sender phone
		// console.log("lihat e");
        // console.log(e.row.data.phone);
		this.formSpbUpdate.senderId 			= e.row.data.customerId;
		this.formSpbUpdate.senderNameId 		= e.row.data.customerNameId;
		this.formSpbUpdate.senderStoreId 		= e.row.data.customerStoreId;
		this.formSpbUpdate.senderPlaceId 		= e.row.data.customerPlaceId;
		this.formSpbUpdate.senderAddressId 		= e.row.data.customerAddressId;
		
		this.formSpbUpdate.senderPhone 			= e.row.data.phone;
		this.formSpbUpdate.senderName 			= e.row.data.customerName;
		this.formSpbUpdate.senderStore 			= e.row.data.customerStore;
		this.formSpbUpdate.senderPlace 			= e.row.data.customerPlace;
		this.formSpbUpdate.senderAddress 		= e.row.data.customerAddress;

		this.senderPhoneDs.reload().done((res) => {
			if (res.length === 1) {
				
				this.spbForm.patchValue({
					senderId: res[0].customerId,
				});

				this.formSpbUpdate.senderId = res[0].customerId;
				console.log(res[0].customerId);
			}
		});

		this.Sender_ReloadAll();
		this.cd.detectChanges();
		//

	}

	receiverIdPelListChoose = (e) => {
		
		this.ReceiverPhoneToOld2();
		this.SenderPhoneToOld2();
		this.receiverListData = e.row.data;
		if (this.spbForm.get('idcustpenerima').value.substring(0,3) === e.row.data.masterCityCode.substring(0,3)){
			console.log("satu");
			this.spbForm.get('idcustpenerima').setValue(this.spbForm.get('idcustpenerima').value);
		}
		else{
			console.log(e.row.data);
			this.spbForm.get('idcustpenerima').setValue(e.row.data.masterCityCode + this.spbForm.get('idcustpenerima').value);
		}
		
		this.ReceiverIdPelListSetToInput(e);
		// this.setFormDataPengirim();
	}

	ReceiverIdPelListSetToInput(e) {
		
		this.receiverListData = e.row.data;
		this.receiverPelangganIDPopUp = false;
		this.cd.detectChanges();
		this.Receiver_ResetAll();

			this.receiverPhoneDs.reload().done((res) => {
				if (res.length === 1) {
					this.spbForm.patchValue({
						receiverId: res[0].customerId,
					});
				}
			});
	
			this.formSpbUpdate.receiverId			= e.row.data.customerId;
			this.formSpbUpdate.receiverNameId 		= e.row.data.customerNameId;
			this.formSpbUpdate.receiverStoreId 		= e.row.data.customerStoreId;
			this.formSpbUpdate.receiverPlaceId 		= e.row.data.customerPlaceId;
			this.formSpbUpdate.receiverAddressId 	= e.row.data.customerAddressId;
	
			this.formSpbUpdate.receiverPhone 		= e.row.data.phone;
			this.formSpbUpdate.receiverName 		= e.row.data.customerName;
			this.formSpbUpdate.receiverStore 		= e.row.data.customerStore;
			this.formSpbUpdate.receiverPlace 		= e.row.data.customerPlace;
			this.formSpbUpdate.receiverAddress 		= e.row.data.customerAddress;
	
			this.EditReceiverDisabled = false;
			this.Receiver_ReloadAll();
			
			this.cd.detectChanges();
			// //
			// // variable
	
			// //
			// // set variable
	
			// //
			// // logic
	
			// // hide popup
	
	
		}


	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.spbFormSubs.unsubscribe();
		this.senderIsNewSub.unsubscribe();
		this.receiverIsNewSub.unsubscribe();
	}

	enableNOTELP(){
		this.IsSenderDisabled = true;
		this.spbCreateService.SetSenderIsNew('senderName', false);
		this.spbCreateService.SetSenderIsNew('senderStore', false);
		this.spbCreateService.SetSenderIsNew('senderPlace', false);
		this.spbCreateService.SetSenderIsNew('senderAddress', false);
		this.spbForm.get('sender').enable();
		this.spbForm.get('senderArea').disable();
		
	}

	enableNOTELPReceiver(){
		this.IsReceiverDisabled = true;
		this.spbCreateService.SetReceiverIsNew('receiverName', false);
		this.spbCreateService.SetReceiverIsNew('receiverStore', false);
		this.spbCreateService.SetReceiverIsNew('receiverPlace', false);
		this.spbCreateService.SetReceiverIsNew('receiverAddress', false);
		this.spbForm.get('receiver').enable();
		this.spbForm.get('receiverArea').disable();
		
	}

	editPengirim(){
		if(this.formSpbUpdate.senderTelp == null || this.formSpbUpdate.senderTelp == 'null' || this.formSpbUpdate.senderTelp == '' || this.formSpbUpdate.senderTelp == '-'){
			this.SenderTelpDisable = false;

			this.formSpbUpdate.senderTelp = '';
		}

		if(this.formSpbUpdate.senderPhone == null || this.formSpbUpdate.senderPhone == '' || this.formSpbUpdate.senderPhone == '-'){
			this.SenderPhoneDisable = false;
		}
		
		if(this.formSpbUpdate.senderName == null || this.formSpbUpdate.senderName == '' || this.formSpbUpdate.senderName == '-'){
			this.SenderNameDisable = false;
		}

		if(this.formSpbUpdate.senderPlace == null || this.formSpbUpdate.senderPlace == '' || this.formSpbUpdate.senderPlace == '-'){
			this.SenderPlaceDisable = false;
		}

		if(this.formSpbUpdate.senderStore == null || this.formSpbUpdate.senderStore == '' || this.formSpbUpdate.senderStore == '-'){
			this.SenderStoreDisable = false;
		}
		
		if(this.formSpbUpdate.senderAddress == null || this.formSpbUpdate.senderAddress == '' || this.formSpbUpdate.senderAddress == '-'){
			this.SenderAddressDisable = false;
		}

		this.updateDataPengirimPopupVisible = true;
	}

	editPenerima(){
		if(this.formSpbUpdate.receiverTelp == null || this.formSpbUpdate.receiverTelp == 'null' || this.formSpbUpdate.receiverTelp == '' || this.formSpbUpdate.receiverTelp == '-'){
			this.ReceiverTelpDisable = false;

			this.formSpbUpdate.receiverTelp = '';
		}

		if(this.formSpbUpdate.receiverPhone == null || this.formSpbUpdate.receiverPhone == '' || this.formSpbUpdate.receiverPhone == '-'){
			this.ReceiverPhoneDisable = false;
		}
		
		if(this.formSpbUpdate.receiverName == null || this.formSpbUpdate.receiverName == '' || this.formSpbUpdate.receiverName == '-'){
			this.ReceiverNameDisable = false;
		}

		if(this.formSpbUpdate.receiverPlace == null || this.formSpbUpdate.receiverPlace == '' || this.formSpbUpdate.receiverPlace == '-'){
			this.ReceiverPlaceDisable = false;
		}

		if(this.formSpbUpdate.receiverStore == null || this.formSpbUpdate.receiverStore == '' || this.formSpbUpdate.receiverStore == '-'){
			this.ReceiverStoreDisable = false;
		}
		
		if(this.formSpbUpdate.receiverAddress == null || this.formSpbUpdate.receiverAddress == '' || this.formSpbUpdate.receiverAddress == '-'){
			this.ReceiverAddressDisable = false;
		}

		this.updateDataPenerimaPopupVisible = true;
	}

	saveDataUpdatePengirim(){
		this.updateDataPengirimPopupVisible = false;

		this.formSpbUpdate.mode = 'sender';

		var res = this.checkDataSender(this.formSpbUpdate);

		if(res == 'nill'){
			notify({
				message: 'Update Data Pelanggan Gagal, Data Ada yang Kosong',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(res == 'noll'){
			notify({
				message: 'Update Data Pelanggan Gagal, Data Ada yang Sama',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else{
			
			const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			.post(AppGlobalVar.BASE_API_URL + 'spb-create/update-data', JSON.stringify(this.formSpbUpdate), config)
			.subscribe((data: any) => {		
					
					notify({
						message: 'Data Pelanggan Telah Berhasil di Update',
						type: 'success',
						displayTime: 5000,
						width: 400,
					});

					this.senderCustomerId 					= data.data.senderId;
					this.senderListData.customerId			= data.data.senderId;
					this.senderListData.customerNameId 		= data.data.senderNameId;
					this.senderListData.customerStoreId 	= data.data.senderStoreId;
					this.senderListData.customerPlaceId 	= data.data.senderPlaceId;
					this.senderListData.customerAddressId 	= data.data.senderAddressId;

					this.Sender_ResetAll();
					this.Sender_ReloadAll();

					console.log('senderPhone', this.formSpbUpdate.senderPhone);
					console.log('senderTelp',this.formSpbUpdate.senderTelp);
					console.log('sender',this.formSpbUpdate);
					
					this.spbForm.get('sendertelp').setValue(this.formSpbUpdate.senderTelp);
					this.spbForm.get('sender').setValue(this.formSpbUpdate.senderPhone);

				},
				(error) => {
				alert(error.statusText + '. ' + error.message);
				}
			);
			
			
		}
			
	}

	checkDataSender(e){
		var r = '';

		if(e.paramSender2 == null){
			r = 'nill';
		}

		if(e.paramSender3 == null){
			r = 'nill';
		}

		if(e.paramSender4 == null){
			r = 'nill';
		}

		if(e.paramSender5 == null){
			r = 'nill';
		}
		
		if(e.paramSender6 == null){
			r = 'nill';
		}

		if(e.paramSender1 == e.paramSender2){
			r = 'noll'
		}

		if(e.paramSender1 == e.paramSender3){
			r = 'noll'
		}

		if(e.paramSender1 == e.paramSender4){
			r = 'noll'
		}

		if(e.paramSender1 == e.paramSender5){
			r = 'noll'
		}

		if(e.paramSender1 == e.paramSender6){
			r = 'noll'
		}

		//----------
		if(e.paramSender2 == e.paramSender1){
			r = 'noll'
		}

		if(e.paramSender2 == e.paramSender3){
			r = 'noll'
		}

		if(e.paramSender2 == e.paramSender4){
			r = 'noll'
		}

		if(e.paramSender2 == e.paramSender5){
			r = 'noll'
		}

		if(e.paramSender2 == e.paramSender6){
			r = 'noll'
		}

		//----------
		if(e.paramSender3 == e.paramSender1){
			r = 'noll'
		}

		if(e.paramSender3 == e.paramSender2){
			r = 'noll'
		}

		if(e.paramSender3 == e.paramSender4){
			r = 'noll'
		}

		if(e.paramSender3 == e.paramSender5){
			r = 'noll'
		}

		if(e.paramSender3 == e.paramSender6){
			r = 'noll'
		}

		//------------
		if(e.paramSender4 == e.paramSender1){
			r = 'noll'
		}

		if(e.paramSender4 == e.paramSender2){
			r = 'noll'
		}

		if(e.paramSender4 == e.paramSender3){
			r = 'noll'
		}

		if(e.paramSender4 == e.paramSender5){
			r = 'noll'
		}

		if(e.paramSender4 == e.paramSender6){
			r = 'noll'
		}

		//------------------------------
		if(e.paramSender5 == e.paramSender1){
			r = 'noll'
		}

		if(e.paramSender5 == e.paramSender2){
			r = 'noll'
		}

		if(e.paramSender5 == e.paramSender3){
			r = 'noll'
		}

		if(e.paramSender5 == e.paramSender4){
			r = 'noll'
		}

		if(e.paramSender5 == e.paramSender6){
			r = 'noll'
		}

		//------------------------------
		if(e.paramSender6 == e.paramSender1){
			r = 'noll'
		}

		if(e.paramSender6 == e.paramSender2){
			r = 'noll'
		}

		if(e.paramSender6 == e.paramSender3){
			r = 'noll'
		}

		if(e.paramSender6 == e.paramSender4){
			r = 'noll'
		}

		if(e.paramSender6 == e.paramSender5){
			r = 'noll'
		}

		return r;
	}

	checkDataReceiver(e){
		var r = '';

		if(e.paramReceiver2 == null){
			r = 'nill';
		}

		if(e.paramReceiver3 == null){
			r = 'nill';
		}

		if(e.paramReceiver4 == null){
			r = 'nill';
		}

		if(e.paramReceiver5 == null){
			r = 'nill';
		}

		if(e.paramReceiver6 == null){
			r = 'nill';
		}

		if(e.paramReceiver1 == e.paramReceiver2){
			r = 'noll'
		}

		if(e.paramReceiver1 == e.paramReceiver3){
			r = 'noll'
		}

		if(e.paramReceiver1 == e.paramReceiver4){
			r = 'noll'
		}

		if(e.paramReceiver1 == e.paramReceiver5){
			r = 'noll'
		}

		if(e.paramReceiver1 == e.paramReceiver6){
			r = 'noll'
		}

		//----------
		if(e.paramReceiver2 == e.paramReceiver1){
			r = 'noll'
		}

		if(e.paramReceiver2 == e.paramReceiver3){
			r = 'noll'
		}

		if(e.paramReceiver2 == e.paramReceiver4){
			r = 'noll'
		}

		if(e.paramReceiver2 == e.paramReceiver5){
			r = 'noll'
		}
		
		if(e.paramReceiver2 == e.paramReceiver6){
			r = 'noll'
		}

		//----------
		if(e.paramReceiver3 == e.paramReceiver1){
			r = 'noll'
		}

		if(e.paramReceiver3 == e.paramReceiver2){
			r = 'noll'
		}

		if(e.paramReceiver3 == e.paramReceiver4){
			r = 'noll'
		}

		if(e.paramReceiver3 == e.paramReceiver5){
			r = 'noll'
		}

		if(e.paramReceiver3 == e.paramReceiver6){
			r = 'noll'
		}

		//------------
		if(e.paramReceiver4 == e.paramReceiver1){
			r = 'noll'
		}

		if(e.paramReceiver4 == e.paramReceiver2){
			r = 'noll'
		}

		if(e.paramReceiver4 == e.paramReceiver3){
			r = 'noll'
		}

		if(e.paramReceiver4 == e.paramReceiver5){
			r = 'noll'
		}

		if(e.paramReceiver4 == e.paramReceiver6){
			r = 'noll'
		}

		//------------------------------
		if(e.paramReceiver5 == e.paramReceiver1){
			r = 'noll'
		}

		if(e.paramReceiver5 == e.paramReceiver2){
			r = 'noll'
		}

		if(e.paramReceiver5 == e.paramReceiver3){
			r = 'noll'
		}

		if(e.paramReceiver5 == e.paramReceiver4){
			r = 'noll'
		}

		if(e.paramReceiver5 == e.paramReceiver6){
			r = 'noll'
		}

		//------------------------------
		if(e.paramReceiver6 == e.paramReceiver1){
			r = 'noll'
		}

		if(e.paramReceiver6 == e.paramReceiver2){
			r = 'noll'
		}

		if(e.paramReceiver6 == e.paramReceiver3){
			r = 'noll'
		}

		if(e.paramReceiver6 == e.paramReceiver4){
			r = 'noll'
		}

		if(e.paramReceiver6 == e.paramReceiver5){
			r = 'noll'
		}

		return r;
	}
	
	saveDataUpdatePenerima(){
		this.updateDataPenerimaPopupVisible = false;

		this.formSpbUpdate.mode = 'receiver';

		
		var res = this.checkDataReceiver(this.formSpbUpdate);

		if(res == 'nill'){
			notify({
				message: 'Update Data Pelanggan Gagal, Data Ada yang Kosong',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(res == 'noll'){
			notify({
				message: 'Update Data Pelanggan Gagal, Data Ada yang Sama',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else{
			
			const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			.post(AppGlobalVar.BASE_API_URL + 'spb-create/update-data', JSON.stringify(this.formSpbUpdate), config)
			.subscribe((data: any) => {		
				
				notify({
					message: 'Data Pelanggan Telah Berhasil di Update',
					type: 'success',
					displayTime: 5000,
					width: 400,
				});

				this.receiverCustomerId 				= data.data.receiverId;
				this.receiverListData.customerId		= data.data.receiverId;
				this.receiverListData.customerNameId 	= data.data.receiverNameId;
				this.receiverListData.customerStoreId 	= data.data.receiverStoreId;
				this.receiverListData.customerPlaceId 	= data.data.receiverPlaceId;
				this.receiverListData.customerAddressId = data.data.receiverAddressId;

				this.Receiver_ResetAll();
				this.Receiver_ReloadAll();
				
				this.spbForm.get('receivertelp').setValue(this.formSpbUpdate.receiverTelp);
				this.spbForm.get('receiver').setValue(this.formSpbUpdate.receiverPhone);

				
				},
				(error) => {
				alert(error.statusText + '. ' + error.message);
				}
			);
			
			
		}

		// const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        // this.http
        //   .post(AppGlobalVar.BASE_API_URL + 'spb-create/update-data', JSON.stringify(this.formSpbUpdate), config)
        //   .subscribe((data: any) => {

        //     },
        //     (error) => {
        //       alert(error.statusText + '. ' + error.message);
        //     }
		// );

		// this.getDataCustomer();
		// this.ReceiverPhoneToOld2();
		// this.ReceiverListSetToInput(this.formSpbUpdate);
				
	}
}
