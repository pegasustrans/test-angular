import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewContainerRef} from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserGlobalVar} from '../../../../../core/globalvar/userGlobalVar';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import {HighlightProcess} from '../../../../../core/helper';
import * as Enumerable from 'linq';
import {SpbCreateService} from '../spb-create.service';
import {Subscription} from 'rxjs';
import Swal from 'sweetalert2'; // Pati 9 nov 21
import notify from 'devextreme/ui/notify';

@Component({
	selector: 'kt-goods',
	templateUrl: './goods.component.html',
	styleUrls: ['./goods.component.scss']
})
export class GoodsComponent implements OnInit, OnDestroy {

	@Output() calculateTotalPrice = new EventEmitter();
	@Input() spbId;
	@Input() spbNumberManual;

	spbForm: FormGroup = null;
	spbFormSubs: Subscription;

	goodsVisible = false;
	goodsVisibleSubs: Subscription;

	userGlobalVar = UserGlobalVar;
	appGlobalVar = AppGlobalVar;
	highlightProcess = HighlightProcess;

	goodsRow: FormArray;

	modaUdara = 6000;
	modaDarat = 4000;
	modaLaut  = 4000;
	spbNumber = '';

	senderIsNew: any; senderIsNewSub: Subscription;
	receiverNew: any; receiverIsNewSub: Subscription;
	
	constructor(private http: HttpClient, private spbCreateService: SpbCreateService, private viewContainerRef: ViewContainerRef) { }

	ngOnInit() {
		this.goodsVisible = false;
		this.spbForm = null;
		this.goodsRow = null;
		
		// servis
		this.spbCreateService.currentSpbNo.subscribe(data => this.spbNumber = data);
		
		// subs spb form
		this.spbFormSubs = this.spbCreateService.spbForm
		.subscribe(data => {
			this.spbForm = data;
			this.goodsRow =  this.spbForm.get('goods') as FormArray;

		this.senderIsNewSub = this.spbCreateService.senderIsNew
			.subscribe((data) => {
				this.senderIsNew = data;
			});

		this.receiverIsNewSub = this.spbCreateService.receiverIsNew
			.subscribe((data) => {
				this.receiverNew = data;
			});

		} );
		
		// subs visible goods form
		this.goodsVisibleSubs = this.spbCreateService.goodsVisible
		.subscribe(data => {
			this.goodsVisible = data;
		} );

		this.spbCreateService.AddGoodsRow(5);
	}
	
	
	GetGoodsForm() {
		/**
		 * return the goods form
		 */
		return this.spbForm.get('goods') as FormArray;
	}
	
	SetToZero_OnFocusOut(e, index, input) {
		/**
		 * when every goods length, width, height lost the focus then :
		 * IF null or empty set to 0
		 * for goodsWeight, biarkan apa adanya
		 */

		const kti = this.GetGoodsForm().at(index).get('koliTotalItem').value;
		const ge = this.GetGoodsForm().at(index).get('goodsWeight').value;
		let le = this.GetGoodsForm().at(index).get('length').value;
		let wi = this.GetGoodsForm().at(index).get('width').value;
		let he = this.GetGoodsForm().at(index).get('height').value;

		if ( input === 'koliTotalItem' && kti === null ) {
			// set to 0 if null or empty
			this.GetGoodsForm().at(index).patchValue({
				koliTotalItem: 1
			});

			le = 0;
		}

		if ( input === 'length' && le === null ) {
			// set to 0 if null or empty
			this.GetGoodsForm().at(index).patchValue({
				length: 0
			});

			le = 0;
		}

		if ( input === 'width' && wi === null ) {
			// set to 0 if null or empty
			this.GetGoodsForm().at(index).patchValue({
				width: 0
			});

			wi = 0;
		}

		if ( input === 'height' && he === null ) {
			// set to 0 if null or empty
			this.GetGoodsForm().at(index).patchValue({
				height: 0
			});

			he = 0;
		}


		// calc
		this.CalculateFinalVolume(e, index, ge, le, wi, he, kti);

		// total price
		this.calculateTotalPrice.emit();
		
		// get koli no when type tab at height input
		// if (input === 'height' && e.component._lastKeyName === 'tab') {
		if (input === 'goodsWeightVolume' && e.component._lastKeyName === 'tab') {
			this.CalculateFinalVolume(e, index, ge, le, wi, he, kti);
			
			// this.GetKoliNo(e, index, ge, le, wi, he);


			//Cek notelp pengirim dan penerima harus terisi
			// if (this.senderIsNew.senderPhone) {
			// 		if (this.spbForm.get('sender').value === null || this.spbForm.get('sender').value === "") {
			// 			Swal.fire({
			// 				title: 'Kesalahan Input!',text: 'No. Telp Pengirim harus di isi.',
			// 				imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
			// 				imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			// 				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			// 			}).then((result) => {
			// 				if (result.isConfirmed) {} 
			// 			})
			// 			return;
			// 		}
			// }
			// else {
			// 	if (this.spbForm.get('senderId').value === null) {
			// 		Swal.fire({
			// 			title: 'Kesalahan Input!',text: 'No. Telp Pengirim harus di isi.',
			// 			imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
			// 			imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			// 			position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			// 		}).then((result) => {
			// 			if (result.isConfirmed) {} 
			// 		})
			// 		return;
			// 	}
			// }

			// if (this.receiverNew.receiverPhone) {
			// 		if (this.spbForm.get('receiver').value === null || this.spbForm.get('receiver').value === "") {
			// 			Swal.fire({
			// 				title: 'Kesalahan Input!',text: 'No. Telp Penerima harus di isi.',
			// 				imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
			// 				imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			// 				position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			// 			}).then((result) => {
			// 				if (result.isConfirmed) {} 
			// 			})
			// 			return;
			// 		}
			// }
			// else {
			// 	if (this.spbForm.get('receiverId').value === null) {
			// 		Swal.fire({
			// 			title: 'Kesalahan Input!',text: 'No. Telp Penerima Baru juga harus di isi.',
			// 			imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
			// 			imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
			// 			position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
			// 		}).then((result) => {
			// 			if (result.isConfirmed) {} 
			// 		})
			// 		return;
			// 	}
			// }


			if (this.senderIsNew.senderAddress) {
				if (this.spbForm.get('senderAddress').value === null || this.spbForm.get('senderAddress').value === ''){
				
					Swal.fire({
						title: 'Kesalahan Input!',text: 'Alamat tidak boleh kosong.',
						imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
						  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
						position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
						//cancelButtonText: 'No, keep it',
					  }).then((result) => {
						if (result.isConfirmed) {} 
					  })
					  return;
				}
			}
			else {
			if (this.spbForm.get('senderAddressId').value === null){
				
				Swal.fire({
					title: 'Kesalahan Input!',text: 'Alamat tidak boleh kosong.',
					imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
					  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
					position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
					//cancelButtonText: 'No, keep it',
				  }).then((result) => {
					if (result.isConfirmed) {} 
				  })
				  return;
			}
			}

			
			if (this.senderIsNew.senderName) {    //Jika CustomerName Pengirim inputan baru  
				if ((this.spbForm.get('senderName').value === null || this.spbForm.get('senderName').value === '') && (this.spbForm.get('senderStore').value === null || this.spbForm.get('senderStore').value === '')){
				
					Swal.fire({
						title: 'Kesalahan Input!',text: 'Nama dan Toko Pengirim tidak boleh keduanya kosong.',
						imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
						  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
						position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
						//cancelButtonText: 'No, keep it',
					  }).then((result) => {
						if (result.isConfirmed) {} 
					  })
					  return;
				}
			}    
			else {    //Jika CustomerName Pengirim hasil popup
			if (this.spbForm.get('senderNameId').value === null && this.spbForm.get('senderStoreId').value === null){
				
				Swal.fire({
					title: 'Kesalahan Input!',text: 'Nama dan Toko Pengirim tidak boleh keduanya kosong.',
					imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
					  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
					position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
					//cancelButtonText: 'No, keep it',
				  }).then((result) => {
					if (result.isConfirmed) {} 
				  })
				  return;
			}
			}
			
			if (this.receiverNew.receiverAddress) {
				if (this.spbForm.get('receiverAddress').value === null  || this.spbForm.get('receiverAddress').value === ''){
					
					Swal.fire({
						title: 'Kesalahan Input!',text: 'Alamat Penerima tidak boleh kosong.',
						imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
						  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
						position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
						//cancelButtonText: 'No, keep it',
					  }).then((result) => {
						if (result.isConfirmed) {} 
					  })
					  return;
				}
			}
			else {
				if (this.spbForm.get('receiverAddressId').value === null){
					
					Swal.fire({
						title: 'Kesalahan Input!',text: 'Alamat Penerima tidak boleh kosong.',
						imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
						  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
						position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
						//cancelButtonText: 'No, keep it',
					  }).then((result) => {
						if (result.isConfirmed) {} 
					  })
					  return;
				}
			}

			
			if (this.receiverNew.receiverName) {   //Jika CustomerName Penerima inputan baru

				if ((this.spbForm.get('receiverName').value === null || this.spbForm.get('receiverName').value === '') && (this.spbForm.get('receiverStore').value === null || this.spbForm.get('receiverStore').value === '' )){
					
					Swal.fire({
						title: 'Kesalahan Input!',text: 'Nama dan Toko Penerima tidak boleh keduanya kosong.',
						imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
						  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
						position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
						//cancelButtonText: 'No, keep it',
					  }).then((result) => {
						if (result.isConfirmed) {} 
					  })
					  return;
				}

			}    
			else {      //Jika CustomerName Penerima hasil popup
				if (this.spbForm.get('receiverNameId').value === null && this.spbForm.get('receiverStoreId').value === null){
					
					Swal.fire({
						title: 'Kesalahan Input!',text: 'Nama dan Toko Penerima tidak boleh keduanya kosong.',
						imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
						  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
						position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
						//cancelButtonText: 'No, keep it',
					  }).then((result) => {
						if (result.isConfirmed) {} 
					  })
					  return;
				}
			}

			// Cek NoTelp dan NoHP Pengirim tidak boleh keduanya kosong
			if (this.spbForm.get('sender').value === null && this.spbForm.get('sendertelp').value === null) {   // cek kota kosong 3 sept
				Swal.fire({
					title: 'Tunggu dulu!',text: 'No. Telp dan No. HP Pengirim tidak boleh kedua-duanya kosong',
					//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
					imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
					  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
					position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
					//cancelButtonText: 'No, keep it',
				  }).then((result) => {
					if (result.isConfirmed) {} 
				  })
				return;
				}

			// Cek NoTelp dan NoHP Penerima tidak boleh keduanya kosong
			if (this.spbForm.get('receiver').value === null && this.spbForm.get('receivertelp').value === null) {   
				Swal.fire({
					title: 'Tunggu dulu!',text: 'No. Telp dan No. HP Penerima tidak boleh kedua-duanya kosong',
					//imageUrl: 'https://s9.gifyu.com/images/pegasusflying1.gif',
					imageUrl: 'http://pegasustrans.co.id/wp-content/uploads/2021/01/cropped-cropped-pegasus-official-small-1.jpg',
					  imageWidth: 150, imageHeight: 150, imageAlt: 'Pegasus Cargo',
					position: 'center',	showCancelButton: false, confirmButtonText: 'Ok', 
					//cancelButtonText: 'No, keep it',
				  }).then((result) => {
					if (result.isConfirmed) {} 
				  })
				return;
				}

			//Dissable all cust tex box
			this.spbForm.get('senderName').disable();
			this.spbForm.get('senderStore').disable();
			this.spbForm.get('senderPlace').disable();
			this.spbForm.get('senderAddress').disable();

			this.spbForm.get('receiverName').disable();
			this.spbForm.get('receiverStore').disable();
			this.spbForm.get('receiverPlace').disable();
			this.spbForm.get('receiverAddress').disable();


				this.Goods_GetKoliNo(index);


			// this.Goods_GetKoliNo(index);
		}

	}


	CalculateFinalVolume(e, index, goodsWeight, le, wi, he, kti) {
		/**
		 * if goodsWeight L W H is not null and not empty then calculate the final volume
		 * final weight = if AW > CAW then AW
		 * final weight = IF AW < CAW THEN CAW
		 */


		let caw = 0;
		let finalWeight = 0;
		let finalWeight_display = 0;

		// calc for current row
		if ( (goodsWeight !== null )
			&& (le !== null )
			&& ( wi !== null)
			&& (he !== null)
			&& (kti !== null)
		) {
 
			// calc jumlah berat
			const goodsWeightTotal = kti * goodsWeight;

			if (this.spbForm.get('carrier').value === 'air') {
				caw = (le * wi * he) / this.modaUdara;
			} else if (this.spbForm.get('carrier').value === 'sea') {
				caw = (le * wi * he) / this.modaLaut;
			} else {
				caw = (le * wi * he) / this.modaDarat;
			}

			//caw = Number(caw.toFixed(0));  // Pati 02Feb22
			caw = Number(caw.toFixed(1));
			
			//Pembulatan Volume
			// PembulatanGoodsWeightVolume = 0;
			// PembulatanGoodsWeightVolume = Math.round(caw);

			if (goodsWeight > caw) { finalWeight = goodsWeightTotal; }
			if (goodsWeight <= caw) { finalWeight = caw * kti; }

			this.GetGoodsForm().at(index).patchValue({
				goodsWeightTotal,
				//goodsWeightVolume: Math.round(caw),  // Pati 02 Feb 22
				goodsWeightVolume: caw,
				goodsFinalWeight: finalWeight,
			});

			
			const final = this.GetGoodsForm().at(index).get('goodsFinalWeight').value;
			
// 			.forEach(myFunction);

// 			function myFunction(item, index) {
// 			document.getElementById("demo").innerHTML += index + ":" + item + "<br>";
// }
		}

		// udara 6000
		// darat 4000
	}

	// CalculateTotalPrice() {
	//
	// 	let totalgoodsFinalWeight = 0;
	// 	const object = this.GetGoodsForm().value;
	// 	Enumerable.from(object).forEach(
	// 		(obj: any) => {
	//
	// 			if (obj.goodsFinalWeight !== null && obj.koliNo !== null) {
	// 				totalgoodsFinalWeight = totalgoodsFinalWeight + obj.goodsFinalWeight;
	// 			}
	//
	// 		}
	// 	);
	//
	// 	// set total price input
	// 	this.spbForm.patchValue({
	// 		totalPrice: totalgoodsFinalWeight * this.spbForm.get('rates').value,
	// 	});
	//
	// }



	Goods_GetKoliNo(index) {
		/**
		 * disable input box sp user cannot edit after get koli
		 * get koli no. from API
		 *
		 */



			// get all value
		const ktl = this.GetGoodsForm().at(index).get('koliTotalItem').value;
		const gw = this.GetGoodsForm().at(index).get('goodsWeight').value;
		const le = this.GetGoodsForm().at(index).get('length').value;
		const wi = this.GetGoodsForm().at(index).get('width').value;
		const he = this.GetGoodsForm().at(index).get('height').value;
		const destCity = this.spbForm.get('destCity').value;
		const destArea = this.spbForm.get('destArea').value;
		const rates = this.spbForm.get('rates').value;

		const tog = this.spbForm.get('tog').value;
		const carrier = this.spbForm.get('carrier').value;
		const qos = this.spbForm.get('qos').value;
		const tos = this.spbForm.get('tos').value;

		console.log('mabil koli', carrier, tog, gw, destCity);

		var params = { 
			tog 				: tog, 
			moda 				: carrier, 
			goodWeight 			: gw, 
			destinationCityId 	: parseInt(destCity), 
		}

			const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
				.post(AppGlobalVar.BASE_API_URL + 'spb-create/check-fragile-perishable-city', params, config)
				.subscribe((data: any) => {
					console.log(data.data[0].res);
					if(data.data[0].res == 50){
						notify({
							message: 'Untuk Barang Perishable & Pecah Belah di atas 50 Kg, Menggunakan Moda Khusus Perishable & Pecah Belah',
							type: 'error',
							displayTime: 5000,
							width: 800
						});	
					}else if(data.data[0].res == 49){
						notify({
							message: 'Untuk Barang Perishable & Pecah Belah di bawah 50 Kg, tidak perlu menggunakan Moda Khusus Perishable & Pecah Belah',
							type: 'error',
							displayTime: 5000,
							width: 800
						});
					}else{
							// send to API if all value is not null
						if ( (gw !== null )
							&& (le !== null )
							&& ( wi !== null)
							&& (he !== null)
							&& destCity !== null
							&& destArea !== null
							&& ktl !== null
							&& rates !== null
						) {

							// disable input by index
							this.GetGoodsForm().at(index).get('koliTotalItem').disable();
							this.GetGoodsForm().at(index).get('goodsWeight').disable();
							this.GetGoodsForm().at(index).get('length').disable();
							this.GetGoodsForm().at(index).get('width').disable();
							this.GetGoodsForm().at(index).get('height').disable();
							this.GetGoodsForm().at(index).get('goodsWeightVolume').disable();  //Pati22nov21
							this.spbCreateService.SetCarrierRO(true);

							const formData: any = new FormData();
							formData.append('totalKoli', this.GetGoodsForm().at(index).get('koliTotalItem').value);
							formData.append('aw', gw);
							formData.append('length', le);
							formData.append('width', wi);
							formData.append('height', he);
							formData.append('spbNo', this.spbNumber);
							formData.append('spbId', this.spbId);
							formData.append('spbNoManual', this.spbNumberManual);
							formData.append('carrier', this.spbForm.get('carrier').value);

							formData.append('tog', tog);
							formData.append('carrier', carrier);
							formData.append('qos', qos);
							formData.append('tos', tos);
							formData.append('destCity', destCity);
							formData.append('destArea', destArea);

							// if (this.desDisable === false) {
							if (this.spbForm.get('destCity').disabled === false) {
								formData.append('destCity', destCity);
								formData.append('destArea', destArea);
							}

							this.http.post(AppGlobalVar.BASE_API_URL + 'spbcreate/get-koli-number', formData).subscribe(
								(response: any) => {
									this.GetGoodsForm().at(index).patchValue({
										koliNo: response.data
									});

									// for first time, disable destination area
									// if (this.desDisable === false) {
									if (this.spbForm.get('destCity').disabled === false) {
										this.SetDestinationDisable(true);
										// this.desDisable = true;
									}
								},
								(error) => {
									alert(error.error.data);
									this.GetGoodsForm().at(index).get('koliTotalItem').enable();
									this.GetGoodsForm().at(index).get('goodsWeight').enable();
									this.GetGoodsForm().at(index).get('length').enable();
									this.GetGoodsForm().at(index).get('width').enable();
									this.GetGoodsForm().at(index).get('height').enable();
									this.spbCreateService.SetCarrierRO(false);
								}
							);
						}
					}
					
									
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				}
			);
	}


	SetDestinationDisable(status) {
		if (status === true) {
			this.spbForm.get('destCity').disable();
			this.spbForm.get('destArea').disable();
		}

		if (status === false) {
			this.spbForm.get('destCity').enable();
			this.spbForm.get('destArea').enable();
		}
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.spbCreateService.SetGoodsVisible(false);
		this.goodsVisibleSubs.unsubscribe();
		this.spbFormSubs.unsubscribe();
	}
}
