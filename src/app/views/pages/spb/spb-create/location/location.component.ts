import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

// app

import { AppGlobalVar } from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../core/globalvar/userGlobalVar';
import DataSource from 'devextreme/data/data_source';
import { HighlightProcess } from '../../../../../core/helper';
import CustomStore from 'devextreme/data/custom_store';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subject, Subscription} from 'rxjs';
import {SpbCreateService} from '../spb-create.service';
import {takeUntil} from 'rxjs/operators';

@Component({
	selector: 'kt-location',
	templateUrl: './location.component.html',
	styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit, OnDestroy {

	@Output() receiverCityReload = new EventEmitter<any>();
	@Output() receiverSubDistrictReload = new EventEmitter<any>();
	@Output() receiverUrbanReload = new EventEmitter<any>();
	@Output() receiverPostalCodeReload = new EventEmitter<any>();
	@Output() receiverViaReload = new EventEmitter<any>();
	@Output() getRates = new EventEmitter<any>();

	spbFormLocation: FormGroup;
	spbFormLocationSubs: Subject<any> = new Subject<any>();
	userGlobalVar = UserGlobalVar;
	appGlobalVar = AppGlobalVar;
	highlightProcess = HighlightProcess;

	destCity: DataSource;
	destCityData: any;
	destAreaDs: DataSource;
	desDisable = false;

	constructor(private http: HttpClient, private spbCreateService: SpbCreateService) { }
	
	httpOptions = {
		headers: new HttpHeaders({
		  'Content-Type': 'application/json'
		})
	  };
	

	ngOnInit() {
		this.spbCreateService.spbForm.pipe(
			takeUntil(this.spbFormLocationSubs)
		).subscribe(data => {
				this.spbFormLocation = data;
			} );

		this.getDestCity();
		this.getDestArea('');
	}


	onChangeDestCity(e) {
		/**
		 * after choose the city
		 * then reset input area
		 * then get data from area API
		 *
		 * * active when :
		 * - user change or choose city.
		 *
		 * Call by :
		 * - templateUrl
		 *
		 */
			// this.destCityElement =  e.element;
		this.resetDestArea();
		// this.destAreaDs.reload();

		// reset customer receiver
		this.spbFormLocation.patchValue({
			receiverType: 'hp',
			receiverId: null, receiver: null, receiverArea: null,
			receiverNameId: null, receiverName: null,
			receiverStoreId: null, receiverStore: null,
			receiverPlaceId: null, receiverPlace: null,
			receiverAddressId: null, receiverAddress: null,
			receiverRt: null,
			receiverRw: null,
			receiverUrban: null,
			receiverSubDistrict: null,
			receiverCity: null,
			receiverPostalCode: null,
			receiverVia: null,
		});

		const data = this.destCityData.filter(f => f.companyCityId === e.value);
		if (data) {
			if (data.length > 0) {
				this.getDestArea(data[0].cityId);
			}
		}
		
		

		this.receiverCityReload.emit();
		this.receiverViaReload.emit();
		this.getRates.emit();
	}

	onChangeDestArea(e) {
		/**
		 * set receiver city, sub distric, orban
		 *
		 */

		// ketika memilih via maka harus pilih manual untuk kota, kec, kel, dan kode pos
		// if (e.selectedItem.isVia !== true) {
			if (this.spbFormLocation.get('destArea').value !== null) {
				this.spbFormLocation.patchValue({
					isVia: ((e.selectedItem.isVia === true) ? true : false)
				});

				// jika receiverPostalCode dan receiverUrban kosong
				if (this.spbFormLocation.get('receiverUrban').value === null || this.spbFormLocation.get('receiverPostalCode').value === null ) {
					this.spbFormLocation.patchValue({
						receiverUrban: null,
						receiverPostalCode: null,
						receiverSubDistrict: null,
						receiverCity: null
					});
				}

				setTimeout(() => {
					this.receiverCityReload.emit();
					// this.receiverSubDistrictReload.emit();
					// this.receiverUrbanReload.emit();
					// this.receiverPostalCodeReload.emit();

				}, 1000);
			}
		// }

		this.getRates.emit();

	}

	onValueChangedDestArea(e) {

		if (this.spbFormLocation.get('destArea').value === null) {
			this.spbFormLocation.patchValue({
				receiverUrban: null,
				receiverPostalCode: null,
				receiverSubDistrict: null,
				receiverCity: null,
				isVia: false
			});


			setTimeout(() => {
				this.receiverCityReload.emit();
				// this.receiverSubDistrictReload.emit();
				// this.receiverUrbanReload.emit();
				// this.receiverPostalCodeReload.emit();

			}, 1000);
		}
		// }



	}


	private resetDestArea() {
		/**
		 * will reset the input area to null
		 */
		this.spbFormLocation.patchValue({
			destArea: null
		});
	}

	/**
	 * get data from city API
	 * then fill to destination city select box
	 */
	private getDestCity() {
		this.destCity = new DataSource({
			store: new CustomStore({
				key: 'cityId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'company/city')
						.toPromise()
						.then((result: any) => {
							this.destCityData = result.data;
							return result.data;
						});
				}
			})
		});
	}


	// private getDestArea() {
	// 	/**
	// 	 * get data area from API filter by city code and city type
	// 	 */
	// 	this.destAreaDs = new DataSource({
	// 		store: new CustomStore({
	// 			key: 'subDistrictId',
	// 			loadMode: 'raw',
	// 			load: (loadOptions) => {
	// 				return this.http.get(AppGlobalVar.BASE_API_URL + 'company/area?cityId=' + this.destCityValue)
	// 					.toPromise()
	// 					.then((result: any) => {
	// 						return result.data;
	// 					});
	// 			}
	// 		})
	// 	});
	// }

	private getDestArea(destCityValue) {
		// console.log('kota-tujuan : ', destCityValue);
		
		/**
		 * get data area from API filter by city code and city type
		 */
		const params = {
			destinationCityId : destCityValue
		};

		this.destAreaDs = new DataSource({
			store: new CustomStore({
				key: 'subDistrictId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'spb-create/list-sub-district', { headers: this.httpOptions.headers, params  })
					// return this.http.get(AppGlobalVar.BASE_API_URL + 'company/area?cityId=' + this.destCityValue)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event

		this.spbFormLocation = null;

		this.spbFormLocationSubs.next();
		this.spbFormLocationSubs.complete();
	}
}
