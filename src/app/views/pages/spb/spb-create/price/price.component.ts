import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormGroup} from '@angular/forms';
import {UserGlobalVar} from '../../../../../core/globalvar/userGlobalVar';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import {HighlightProcess} from '../../../../../core/helper';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import {SpbCreateService} from '../spb-create.service';
import {Subscription} from 'rxjs';

@Component({
	selector: 'kt-price',
	templateUrl: './price.component.html',
	styleUrls: ['./price.component.scss']
})
export class PriceComponent implements OnInit, OnDestroy {
	@Output()
	calculateTotalPrice = new EventEmitter();

	spbForm: FormGroup;
	spbFormSubs: Subscription;
	userGlobalVar = UserGlobalVar;
	appGlobalVar = AppGlobalVar;
	highlightProcess = HighlightProcess;

	pmDs: any;

	constructor(private http: HttpClient, private spbCreateService: SpbCreateService) { }


	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};
	
	ngOnInit() {

		this.spbFormSubs = this.spbCreateService.spbForm
			.subscribe(data => {
				this.spbForm = data;
			} );

			// console.log(this.spbFormSubs);
		// this.getPm();
	}
	// private getPm()
	getPm() {
		// console.log(this.spbFormSubs);
		
		/**
		 * get data Payment Method (PM) from API
		 *
		 * call by :
		 * - onLoadPage()
		 */

		// this.pmDs = new DataSource({
		// 	store: new CustomStore({
		// 		key: 'lookupKey',
		// 		loadMode: 'raw',
		// 		load: (loadOptions) => {
		// 			return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey=pm')
		// 				.toPromise()
		// 				.then((result: any) => {
		// 					return result.data;
		// 				});
		// 		}
		// 	})
		// });

		const params = { 
			companyCityId 		: this.spbForm.get('destCity').value
		};
		if (this.spbForm.get("receiverViaId").value != null) {
			this.pmDs = [
				{
					lookupId: 25,
					lookupKeyParent: "pm",
					lookupKey: "ta",
					lookupValue: "Tunai Pengirim",
				},
			];
		}else{
			this.pmDs = new CustomStore({
				key: "lookupKey",
				load: (loadOptions: any) => {
					return this.http
						.get(
							AppGlobalVar.BASE_API_URL +
								"spb-create/list-payment-method",
							{ headers: this.httpOptions.headers, params }
						)
						.toPromise()
						.then((data: any) => {
							return {
								data: data.data,
							};
						})
						.catch((error) => {
							console.log(error);
						});
				},
			});
		}
	}

	CalculateTotalPrice(e) {
		this.calculateTotalPrice.emit();	
	}

	CitySubdistrictValue(e){
		console.log('e', e);

		console.log('spbForm - receiverCity : ', this.spbForm.get('receiverCity').value);
		console.log('spbForm - receiverSubDistrict : ', this.spbForm.get('receiverSubDistrict').value);
		// Update get payment method by habibi move to click event  
		// this.getPm();
		
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.spbFormSubs.unsubscribe();
	}

}
