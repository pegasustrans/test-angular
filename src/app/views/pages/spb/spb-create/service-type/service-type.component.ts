import {
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output,
} from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { FormGroup } from "@angular/forms";
import { UserGlobalVar } from "../../../../../core/globalvar/userGlobalVar";
import { AppGlobalVar } from "../../../../../core/globalvar/appGlobalVar";
import { HighlightProcess } from "../../../../../core/helper";
import DataSource from "devextreme/data/data_source";
import CustomStore from "devextreme/data/custom_store";
import { SpbCreateService } from "../spb-create.service";
import { Subscription, interval, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import notify from "devextreme/ui/notify";

@Component({
	selector: "kt-service-type",
	templateUrl: "./service-type.component.html",
	styleUrls: ["./service-type.component.scss"],
})
export class ServiceTypeComponent implements OnInit, OnDestroy {
	@Output()
	calculateTotalPrice = new EventEmitter();

	spbForm: FormGroup;
	spbFormSubs: Subscription;

	goodsVisible = false;
	goodsVisibleSubs: Subscription;

	carrierRO = false;
	carrierROSubs: Subject<any> = new Subject<any>();

	userGlobalVar = UserGlobalVar;
	appGlobalVar = AppGlobalVar;
	highlightProcess = HighlightProcess;

	togDs: DataSource;
	carrierDs: DataSource;
	qosDs: DataSource;
	tosDs: DataSource;

	constructor(
		private http: HttpClient,
		private spbCreateService: SpbCreateService
	) {}

	ngOnInit() {
		this.getQos();

		this.goodsVisible = false;
		this.carrierRO = false;

		this.spbFormSubs = this.spbCreateService.spbForm.subscribe((data) => {
			this.spbForm = data;
		});

		// subs visible goods form
		this.goodsVisibleSubs = this.spbCreateService.goodsVisible.subscribe(
			(data) => {
				this.goodsVisible = data;
			}
		);

		// subs carrier read only
		this.spbCreateService.carrierRO
			.pipe(takeUntil(this.carrierROSubs))
			.subscribe((data) => {
				this.carrierRO = data;
			});

		this.getTog();
		// this.getCarrier();
		this.getTos();

		this.spbForm.patchValue({
			qos: "reg",
			tos: "otd",
		});
	}

	httpOptions = {
		headers: new HttpHeaders({
			"Content-Type": "application/json",
		}),
	};

	ChangeVisibleSubs(e) {
		if (e.value != null) {
			this.spbCreateService.SetGoodsVisible(true);
		} else {
			this.spbCreateService.SetGoodsVisible(false);
		}

		this.GetRates(e);
	}

	public GetRates(e) {
		this.getCarrier();

		// reset to zero
		this.spbForm.patchValue({
			rates: null,
			ratesType: null,
			ratesDetail: null,
		});
		this.calculateTotalPrice.emit();

		const tog = this.spbForm.get("tog").value;
		const carrier = this.spbForm.get("carrier").value;
		const qos = this.spbForm.get("qos").value;
		const tos = this.spbForm.get("tos").value;
		const destCity = this.spbForm.get("destCity").value;
		const destArea = this.spbForm.get("destArea").value;

		if (
			tog === null ||
			carrier === null ||
			qos === null ||
			tos === null ||
			destCity === null ||
			destArea === null
		) {
			return;
		}

		const formData: any = new FormData();
		formData.append("tog", tog);
		formData.append("carrier", carrier);
		formData.append("qos", qos);
		formData.append("tos", tos);
		formData.append("destCity", destCity);
		formData.append("destArea", destArea);

		this.http
			.post(AppGlobalVar.BASE_API_URL + "spbcreate/get-rates", formData)
			.toPromise()
			.then(
				(result: any) => {
					if (result) {
						this.spbForm.patchValue({
							rates: result.data.rates,
							ratesType: result.data.ratesType,
							ratesDetail: result.data.detail,
						});
						console.log(result);
						this.calculateTotalPrice.emit();
					} else {
						this.spbForm.patchValue({
							rates: null,
							ratesType: null,
							ratesDetail: null,
						});

						//--- tambahan laut & darat
						//--- author Pati
						//--- 2021-09-10

						//warning jika rates tidak ada
						notify({
							message: "Tarif tidak ada dalam database",
							type: "error",
							displayTime: 5000,
							width: 400,
						});

						this.calculateTotalPrice.emit();
					}
				},
				(err) => {
					this.spbForm.patchValue({
						rates: 0,
						ratesType: null,
					});
					this.calculateTotalPrice.emit();
				}
			);
	}

	private getTog() {
		/**
		 * get data TOG from API
		 *
		 * call by :
		 * - onLoadPage()
		 */
		this.togDs = new DataSource({
			store: new CustomStore({
				key: "lookupKey",
				loadMode: "raw",
				load: (loadOptions) => {
					return this.http
						.get(
							AppGlobalVar.BASE_API_URL +
								"lookup/get?parentKey=tog"
						)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
			}),
		});
	}

	private getCarrier() {
		// console.log('this.spbForm : ', this.spbForm);

		/**
		 * get data CARRIER from API
		 *
		 * call by :
		 * - onLoadPage()
		 */
		const params = {
			destinationCityId: this.spbForm.get("destCity").value,
		};
		this.carrierDs = new DataSource({
			store: new CustomStore({
				key: "lookupKey",
				loadMode: "raw",
				load: (loadOptions) => {
					return this.http
						.get(
							AppGlobalVar.BASE_API_URL +
								"spb-create/list-moda",
							{ headers: this.httpOptions.headers, params }
						)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
			}),
		});
		if (this.userGlobalVar.USERNAME.includes("torani")) {
			this.carrierDs.items = () => [
				{
					lookupId: 5,
					lookupKeyParent: "carrier",
					lookupKey: "sea",
					lookupValue: "Laut",
				},
			];
		} 
	}

	getQos() {
		const body = {
			LookUpKeyParent: "qos",
		};

		this.http
			.post(
				AppGlobalVar.BASE_API_URL + "lookup-list/check",
				body,
				this.httpOptions
			)
			.subscribe(
				(data: any) => {
					this.qosDs = data.data;
					return data.data;
				},
				(error) => {
					alert(error.statusText + ". " + error.message);
				}
			);
	}

	// private getQos() {
	// 	/**
	// 	 * get data QOS from API
	// 	 *
	// 	 * call by :
	// 	 * - onLoadPage()
	// 	 */
	// 	this.qosDs = new DataSource({
	// 		store: new CustomStore({
	// 			key: 'lookupKey',
	// 			loadMode: 'raw',
	// 			load: (loadOptions) => {
	// 				return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey=qos')
	// 					.toPromise()
	// 					.then((result: any) => {
	// 						return result.data;
	// 					});
	// 			}
	// 		})
	// 	});
	// }

	private getTos() {
		/**
		 * get data TOS from API
		 *
		 * call by :
		 * - onLoadPage()
		 */
		this.tosDs = new DataSource({
			store: new CustomStore({
				key: "lookupKey",
				loadMode: "raw",
				load: (loadOptions) => {
					return this.http
						.get(
							AppGlobalVar.BASE_API_URL +
								"lookup/get?parentKey=tos"
						)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
			}),
		});
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.spbFormSubs.unsubscribe();
		this.goodsVisibleSubs.unsubscribe();

		this.spbCreateService.SetCarrierRO(false);
		this.carrierROSubs.next();
		this.carrierROSubs.complete();
		this.carrierROSubs.unsubscribe();
	}
}
