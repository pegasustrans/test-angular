import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbCreateComponent } from './spb-create.component';

describe('SpbCreateComponent', () => {
  let component: SpbCreateComponent;
  let fixture: ComponentFixture<SpbCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
