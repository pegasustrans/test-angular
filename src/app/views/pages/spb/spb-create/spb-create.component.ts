import {Component, OnInit, ViewChild, ElementRef, HostListener, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import html2canvas from 'html2canvas';

import * as $ from 'jquery';
import * as Enumerable from 'linq';
import {MatDialog, MatDialogConfig } from '@angular/material/dialog';

// devextreme
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { DxSelectBoxModule, DxSelectBoxComponent } from 'devextreme-angular';

// angular
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

// APP
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';
import array from 'devextreme/ui/file_manager/file_provider/array';
import {DatePipe} from '@angular/common';
import {NULL_EXPR} from '@angular/compiler/src/output/output_ast';
import {SpbCreateService} from './spb-create.service';
import {Subject, Subscription} from 'rxjs';
import {SpbPrintComponent} from '../spb-print/spb-print.component';
import {takeUntil} from 'rxjs/operators';
import {SpbPrintV2Component} from '../spb-print-v2/spb-print-v2.component';

import Swal from 'sweetalert2'; //pati5nov21
import { I } from '@angular/cdk/keycodes';
//import { KtDialogService } from 'src/app/core/_base/layout';

@Component({
	selector: 'kt-spb-create',
	templateUrl: './spb-create.component.html',
	styleUrls: ['./spb-create.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpbCreateComponent implements OnInit, OnDestroy {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	// @ViewChild('destCity', { static: false }) destCitySelectBox: DxSelectBoxComponent;
	// @ViewChild('destCity') destCitySelectBox: ElementRef;

	senderIsNew: any; senderIsNewSub: Subscription;
	receiverNew: any; receiverIsNewSub: Subscription;

	destCityElement: any;

	userGlobalVar = UserGlobalVar;
	appGlobalVar = AppGlobalVar;
	spbNumber: string = null;
	spbId: bigint = null;
	spbNumberManual: string = null;

	test: FormArray;
	
	menuPriv = true;
	menuForb = false;

	checkAgreed: any;

	spbForm: FormGroup = null;
	spbFormSubs: Subject<any> = new Subject<any>();

	public innerWidth: any = window.innerWidth;
	public innerHeight: any = window.innerHeight;
	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.innerWidth = window.innerWidth;
		this.innerHeight = window.innerHeight;
	}



	// ----------------------------------------------------------------------------------------------------------
	// PRODUCTS
	// ----------------------------------------------------------------------------------------------------------

	// product
	// goodsRow = this.spbForm.get('goods').value;

	HistoryForm = {
		spbNo		: null,
		typePrint	: null,
		menu		: "Input SPB",
		menuAction	: "Print SPB",
		printed		: true,
		desc		: "Input Gerai",
		historyloginId : 0
	};

	
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};


	//#region CONSTRUCTOR
	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  CONSTRUCTOR
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */
	constructor(private http: HttpClient, private matDialog: MatDialog, private spbCreateService: SpbCreateService, private cd: ChangeDetectorRef) {


		// set phone type
		this.phoneType = [
			{value: 'hp', text: 'Hp'},
			{value: 'home', text: 'Rumah/Tempat'},
		];



	}




	ngOnInit() {
		// this.changeDetectorRef.detach();
		// setInterval(() => {
		// 	// https://github.com/angular/angular/issues/7311
		// 	this.changeDetectorRef.reattach();
		// 	this.changeDetectorRef.detectChanges();
		// 	this.changeDetectorRef.detach();
		// 	console.log('tik ');
		//
		// }, 3000);

		this.checkuserGlobalVar();
		this.CheckAgreed();

		if (this.menuPriv == true){
			// console.log("input SPB");
			this.spbForm = null;
	
			this.senderIsNewSub = this.spbCreateService.senderIsNew
				.subscribe((data) => {
					this.senderIsNew = data;
				});
	
			this.receiverIsNewSub = this.spbCreateService.receiverIsNew
				.subscribe((data) => {
					this.receiverNew = data;
				});
	
			this.spbCreateService.currentSpbNo.subscribe(data => {
				this.spbNumber = data;
			});
	
			this.spbCreateService.SetForm();
			// get spb form service
			this.spbCreateService.spbForm
				.pipe(
					takeUntil(this.spbFormSubs)
				)
				.subscribe(data => {
					this.spbForm = data;
				} );
	
	
			// get data
			this.onLoadPage();
			this.cd.detectChanges();
			this.historyLoginStart();
			// this.historyLogin();
		}
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}
	//#endregion CONSTRUCTOR

	//#region COMMON
	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  COMMON
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */

	 private async checkuserGlobalVar() {
		if(await UserGlobalVar.USER_ID == '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				// console.log(value);
				if (value == "spb-create"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv != 1){
				this.menuPriv = false;
				this.menuForb = true;
			
			}
		}
	}

	historyPrint(data, typePrint){
		this.HistoryForm.spbNo 		= data.data;
		this.HistoryForm.typePrint 	= typePrint;
		
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-print/create', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	private onLoadPage() {
		/**
		 * Get all data from API after page is load
		 *
		 * call by :
		 * - method ngOnInit()
		 */


		if (this.menuPriv === true){
			// console.log("priv");
			// SPB
			this.GetSpbNo();
		}

		// // location
		// this.getDestCity();
		// this.getDestArea();
		//
		// this.getKotamadya();
		// this.getKecamatan();
		// this.getKelurahan();
		//
		// // TOS QOS OTHERS
		// this.getTog();
		// this.getCarrier();
		// this.getQos();
		// this.getTos();
		// this.getPm();
		//
		// // customer sender
		// this.getCustomerSenderPhone();
		// this.getSenderName();
		// this.SenderStore_GetData();
		// this.SenderPlace_GetData();
		// this.SenderAddress_GetData();
		//
		// // customer receiver
		// this.ReceiverPhone_GetData();
		// this.ReceiverName_GetData();
		// this.ReceiverStore_GetData();
		// this.ReceiverPlace_GetData();
		// this.ReceiverAddress_GetData();

		this.cd.detectChanges();

	}



	highlightProcess(text, objectId) {
		/**
		 * used for highlight search keyword at select box
		 *
		 */
		const search = $('#' + objectId).find('.dx-texteditor-input').val().toString(); // .value

		if (search.length <= 0) { return text; }
		return text.toUpperCase().split(search.toUpperCase())
			.join(['<span class=\'kt-font-boldest kt-font-danger\'>', search.toUpperCase(), '</span>'].join(''));
		this.cd.detectChanges();
	}

	GetGoodsForm() {
		// console.log(this.spbForm.get('goods'));
		/**
		 * return the goods form
		 */
		return this.spbForm.get('goods') as FormArray;
	}

	//#region API
	/** #########################
	 *  API
	 * ##########################
	 */

	GetSpbNo() {
		// TODO : HARD CODE
		return this.http
			.get(AppGlobalVar.BASE_API_URL + 'spb/number-generator?companyId=1&timeZone=7')
			.subscribe((data: any) => {

				if (data != null) {
					this.spbCreateService.setSpbNo(data.data.spbNo);
					this.spbNumberManual = data.data.spbNo;
					this.spbId = data.data.spbId;



					this.cd.detectChanges();
					// this.changeDetectorRef.detectChanges();
					// this.spbForm.patchValue({
					// 	spbNo: data.data.spbNo
					// });
				}
			});
	}

	saveAndPrint(e, index) {


		// this.GoodsLength_OnFocusOut(e, index);
		const formData: any = new FormData();
		// formData.append('CreatedAt', Date.now());
		formData.append('aw', this.GetGoodsForm().at(index).get('goodsWeight').value);
		formData.append('length', this.GetGoodsForm().at(index).get('length').value);
		formData.append('width', this.GetGoodsForm().at(index).get('width').value);
		formData.append('height', this.GetGoodsForm().at(index).get('height').value);
		// formData.append('caw', this.GetGoodsForm().at(index).get('goodsFinalWeight').value);
		formData.append('spbNo', this.spbNumber);

		this.http.post(AppGlobalVar.BASE_API_URL + 'spb/SpbCreate/', formData).subscribe(
			(response) => console.log(response),
			(error) => console.log(error)
		);

		// return this.http
		// 	.post(AppGlobalVar.BASE_API_URL,spbForm, {
		// 	headers: new HttpHeaders({
		// 		'Content-Type':  'application/json',
		// 	})
		// }).map(data=>
		// 	data);
	}

	//#endregion API


	//#endregion COMMON




	//#region LOCATION
	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  LOCATION
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */

	//#region ORIGIN
	/** 222222222222222222222222222222222222222222222222222222222222222222
	 * ORIGIN
	 * 2222222222222222222222222222222222222222222222222222222222222222222
	 */
	//#endregion ORIGIN

	//#region DESTINATION
	/** 222222222222222222222222222222222222222222222222222222222222222222
	 * DESTINATION
	 * 2222222222222222222222222222222222222222222222222222222222222222222
	 */
	destCity: DataSource;
	destCityValue = 0;
	destCityType = '';
	destCityData: any;
	destAreaDs: DataSource;
	desDisable = false;

	kotamadya: DataSource;
	kotamadyaValue = 0;
	kotamadyaType = '';
	kotamadyaData: any;
	kecamatanDs:DataSource;
	kecamatanValue = 0;
	kecamatanType ='';
	kecamatanData:any;
	kelurahanDs:DataSource;

	beratTotal = 0;
	minBerat = 0;
	minHarga = 0;
	pembulatanGoodsWeightTotal=0;
	pembulatanGoodsFinalWeight=0;
	pembulatanGoodsWeightVolume=0;

	SetDestinationDisable(status) {
		if (status === true) {
			this.spbForm.get('destCity').disable();
			this.spbForm.get('destArea').disable();
		}

		if (status === false) {
			this.spbForm.get('destCity').enable();
			this.spbForm.get('destArea').enable();
		}
		this.cd.detectChanges();
	}

	SetKotamadyaDisable(status) {
		if (status === true) {
			this.spbForm.get('kotamadya').disable();
			this.spbForm.get('kecamatan').disable();
			this.spbForm.get('kelurahan').disable();
		}

		if (status === false) {
			this.spbForm.get('kotamadya').enable();
			this.spbForm.get('kecamatan').enable();
			this.spbForm.get('kelurahan').enable();
		}
		this.cd.detectChanges();
	}

	//#region CITY
	/** 333333333333333333333333333333333
	 * CITY
	 * 3333333333333333333333333333333333
	 */

	/** #########################
	 * USER ACTION
	 * ##########################
	 */

	setFocus(e) {
		this.destCityElement = e;
		// console.log('setFocus', e);
		// setTimeout(() => { // this will make the execution after the above boolean has changed
		// 	e.component.focus();
		// }, 0);
		this.cd.detectChanges();
	}

	onChangeDestCity(e) {
		/**
		 * after choose the city
		 * then reset input area
		 * then get data from area API
		 *
		 * * active when :
		 * - user change or choose city.
		 *
		 * Call by :
		 * - templateUrl
		 *
		 */
			// this.destCityElement =  e.element;
		const data = this.destCityData.filter(f => f.companyCityId === e.value);
		this.destCityType = '';
		if (data) {
			// console.log('data: ', data);
			if (data) {
				this.destCityValue = data[0].cityId;
			}
		}

		this.resetDestArea();
		this.destAreaDs.reload();
		this.cd.detectChanges();
	}

	onChangeKotamadya(e) {
		const data = this.kotamadyaData.filter(f => f.cityCode === e.value);
		this.kotamadyaType = '';
		if (data) { this.kotamadyaType = data[0].cityType.charAt(0); }
		this.kotamadyaValue = e.value;
		this.resetKecamatan();
		this.kecamatanDs.reload();
		this.cd.detectChanges();
	}

	onChangeKecamatan(e){
		const data = this.kecamatanData.filter(f=>f.subDistrictCodeBps === e.value);
		this.kecamatanValue = e.value;
		this.resetKelurahan();
		this.kelurahanDs.reload();
		this.cd.detectChanges();
	}

	/** #########################
	 *  API
	 * ##########################
	 */

	/**
	 * get data from city API
	 * then fill to destination city select box
	 *
	 * call by :
	 * - onLoadPage()
	 */
	private getDestCity() {
		this.destCity = new DataSource({
			store: new CustomStore({
				key: 'cityId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'company/city')
						.toPromise()
						.then((result: any) => {
							this.cd.detectChanges();
							this.destCityData = result.data;
							return result.data;
						});
				}
			})
		});
	}

	private getKotamadya() {
		this.kotamadya = new DataSource({
			store: new CustomStore({
				key: 'cityId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'location/city')
						.toPromise()
						.then((result: any) => {
							this.kotamadyaData = result.data;
							this.cd.detectChanges();
							return result.data;
						});
				}
			})
		});
	}


	//#endregion CITY

	//#region AREA
	/** 333333333333333333333333333333333
	 * AREA
	 * 3333333333333333333333333333333333
	 */

	/** #########################
	 * FUNCTION
	 * ##########################
	 */

	private resetDestArea() {
		/**
		 * will reset the input area to ''
		 *
		 * active when : -
		 *
		 * Call By :
		 * - method onChangeDestCity()
		 *
		 */
		this.spbForm.patchValue({
			destArea: ''
		});
	}

	private resetKecamatan(){
		this.spbForm.patchValue({
			kecamatan:''
		})
	}

	private resetKelurahan(){
		this.spbForm.patchValue({
			kelurahan:''
		})
	}
	/** #########################
	 *  API
	 * ##########################
	 */

	private getDestArea() {
		/**
		 * get data area from API filter by city code and city type
		 *
		 * call by :
		 * - onLoadPage()
		 */
		this.destAreaDs = new DataSource({
			store: new CustomStore({
				key: 'subDistrictId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'company/area?cityId=' + this.destCityValue)
						.toPromise()
						.then((result: any) => {
							this.cd.detectChanges();
							return result.data;
						});
				}
			})
		});
	}
	//#endregion AREA

	private getKecamatan() {
		this.kecamatanDs = new DataSource({
			store: new CustomStore({
				key: 'subDistrictId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'location/subDistrict?cityId=' + this.kotamadyaValue )
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}

	private getKelurahan() {
		this.kelurahanDs = new DataSource({
			store: new CustomStore({
				key: 'subDistrictId',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'location/urban?subDistrict=' + this.kotamadyaValue )
						.toPromise()
						.then((result: any) => {
							this.cd.detectChanges();
							return result.data;
						});
				}
			})
		});
	}

	//#endregion DESTINATION

	//#region
	//#endregion

	//#region
	//#endregion


	//#endregion LOCATION

	//#region CUSTOMER

	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 * CUSTOMER
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */

	phoneType: any;

	//#region SENDER
	/** 222222222222222222222222222222222222222222222222222222222222222222
	 * SENDER
	 * 2222222222222222222222222222222222222222222222222222222222222222222
	 */
	senderCustomerId = 0;

	senderPhoneDs: DataSource;
	senderPhoneNew = false;

	senderNameDs: DataSource;
	senderNameNew = false;

	senderStoreDs: DataSource;
	senderStoreNew = false;

	senderPlaceDs: DataSource;
	senderPlaceNew = false;

	senderAddressDs: DataSource;
	senderAddressNew = false;


	private Sender_ResetAll() {
		/**
		 * reset all sender to null when phone is selected
		 */
		this.spbForm.patchValue({
			senderName: null,
			senderStore: null,
			senderPlace: null,
			senderAddress: null,
		});
		this.cd.detectChanges();
	}

	private Sender_ReloadAll() {
		/**
		 * reload all sender when phone is selected
		 * then set the value  if only have one value from API
		 */
		// sender name
		this.senderNameDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					senderName: res[0].customerNameId,
				});
			}
		});

		// sender store
		this.senderStoreDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					senderStore: res[0].customerStoreId,
				});
			}
		});

		// sender place
		this.senderPlaceDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					senderPlace: res[0].customerPlaceId,
				});
			}
		});

		// sender place
		this.senderAddressDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					senderAddress: res[0].customerAddressId,
				});
			}
		});
		this.cd.detectChanges();
	}

	//#region PHONE
	/** 333333333333333333333333333333333
	 * PHONE
	 * 3333333333333333333333333333333333
	 */

	/** #########################
	 * FUNCTION
	 * ##########################
	 */

	/** #########################
	 * USER ACTION
	 * ##########################
	 */
	SenderPhoneToNew(e) {
		if (e.key === '+') {
			this.senderPhoneNew = true;
			this.senderNameNew = true;
			this.senderStoreNew = true;
			this.senderPlaceNew = true;
			this.senderAddressNew = true;
		}
		this.cd.detectChanges();
	}
	SenderPhoneToOld(e) {
		if (e.key === '=') {
			this.senderPhoneNew = false;
			this.senderNameNew = false;
			this.senderStoreNew = false;
			this.senderPlaceNew = false;
			this.senderAddressNew = false;
		}
		this.cd.detectChanges();
	}
	SenderPhone_OnItemClick(e) {
		/**
		 * set customerId so API customer sender can consume it
		 * then reset and reload all sender
		 */
		this.senderCustomerId = e.itemData.customerId;

		this.Sender_ResetAll();
		this.Sender_ReloadAll();
		this.cd.detectChanges();
	}

	/** #########################
	 *  API
	 * ##########################
	 */
	getCustomerSenderPhone() {
		/**
		 * get data phone from API
		 *
		 * call by :
		 * - onLoadPage()
		 */
		this.senderPhoneDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?cityId=' + this.userGlobalVar.WORK_CITY_ID
						+ '&searchValue='  + loadOptions.searchValue + '&type=' )
						.toPromise()
						.then((result: any) => {
							// Here, you can perform operations unsupported by the server
							this.cd.detectChanges();
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?id=' + key)
						.toPromise();
				}
			})
		});
	}


	//#endregion

	//#region NAME
	/** 333333333333333333333333333333333
	 * NAME
	 * 3333333333333333333333333333333333
	 */
	/** #########################
	 *  API
	 * ##########################
	 */
	getSenderName() {
		this.senderNameDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/name?customerId=' + this.senderCustomerId)
						.toPromise()
						.then((result: any) => {
							this.cd.detectChanges();
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?id=' + key)
						.toPromise();
				}
			})
		});
	}

	//#endregion NAME

	//#region STORE
	/** 333333333333333333333333333333333
	 * STORE
	 * 3333333333333333333333333333333333
	 */
	/** #########################
	 *  API
	 * ##########################
	 */
	SenderStore_GetData() {
		this.senderStoreDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/store?customerId=' + this.senderCustomerId)
						.toPromise()
						.then((result: any) => {
							this.cd.detectChanges();
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/store?id=' + key)
						.toPromise();
				}
			})
		});
	}

	//#endregion

	//#region PLACE
	/** 333333333333333333333333333333333
	 * PLACE
	 * 3333333333333333333333333333333333
	 */

	/** #########################
	 *  API
	 * ##########################
	 */
	SenderPlace_GetData() {
		this.senderPlaceDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/place?customerId=' + this.senderCustomerId)
						.toPromise()
						.then((result: any) => {
							this.cd.detectChanges();
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/place?id=' + key)
						.toPromise();
				}
			})
		});
	}
	//#endregion PLACE

	//#region ADDRESS
	/** 333333333333333333333333333333333
	 * ADDRESS
	 * 3333333333333333333333333333333333
	 */
	/** #########################
	 *  API
	 * ##########################
	 */
	SenderAddress_GetData() {
		this.senderAddressDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/address?customerId=' + this.senderCustomerId)
						.toPromise()
						.then((result: any) => {
							this.cd.detectChanges();
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/address?id=' + key)
						.toPromise();
				}
			})
		});
	}
	//#endregion ADDRESS


	//#endregion

	//#region RECEIVER
	/** 222222222222222222222222222222222222222222222222222222222222222222
	 * RECEIVER
	 * 2222222222222222222222222222222222222222222222222222222222222222222
	 */
	receiverCustomerId = 0;

	receiverPhoneDs: DataSource;
	receiverPhoneNew = false;
	receiverNameDs: DataSource;
	receiverNameNew = false;
	receiverStoreDs: DataSource;
	receiverStoreNew = false;
	receiverPlaceDs: DataSource;
	receiverPlaceNew = false;
	receiverAddressDs: DataSource;
	receiverAddressNew = false;

	private Receiver_ResetAll() {
		/**
		 * reset all receiver to null when phone is selected
		 */
		this.spbForm.patchValue({
			receiverName: null,
			receiverStore: null,
			receiverPlace: null,
			receiverAddress: null,
		});
		this.cd.detectChanges();
	}

	private Receiver_ReloadAll() {
		/**
		 * reload all receiver when phone is selected
		 * then set the value if only have one value from API
		 */
		// sender name
		this.receiverNameDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverName: res[0].customerNameId,
				});
			}
		});

		// sender store
		this.receiverStoreDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverStore: res[0].customerStoreId,
				});
			}
		});

		// sender place
		this.receiverPlaceDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverPlace: res[0].customerPlaceId,
				});
			}
		});

		// sender address
		this.receiverAddressDs.reload().done((res) => {
			if (res.length === 1) {
				this.spbForm.patchValue({
					receiverAddress: res[0].customerAddressId,
				});
			}
		});
		this.cd.detectChanges();
	}



	//#region PHONE
	/** 333333333333333333333333333333333
	 * PHONE
	 * 3333333333333333333333333333333333
	 */
	/** #########################
	 * USER ACTION
	 * ##########################
	 */
	ReceiverPhone_OnItemClick(e) {
		/**
		 * set receivercustomerId so API customer receiver can consume it
		 * then reset and reload all receiver
		 */
		this.receiverCustomerId = e.itemData.customerId;

		this.Receiver_ResetAll();
		this.Receiver_ReloadAll();
		this.cd.detectChanges();
	}

	/** #########################
	 *  API
	 * ##########################
	 */
	ReceiverPhone_GetData() {
		/**
		 * get phone data from API with filter :
		 * - destination city
		 * - search keyword
		 */
		this.receiverPhoneDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?cityId=' + this.spbForm.value.destCity
						+ '&searchValue='  + loadOptions.searchValue )
						.toPromise()
						.then((result: any) => {
							// Here, you can perform operations unsupported by the server
							this.cd.detectChanges();
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?id=' + key)
						.toPromise();
				}
			})
		});
	}
	//#endregion PHONE

	//#region NAME
	/** 333333333333333333333333333333333
	 * NAME
	 * 3333333333333333333333333333333333
	 */
	/** #########################
	 *  API
	 * ##########################
	 */
	ReceiverName_GetData() {
		this.receiverNameDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/name?customerId=' + this.receiverCustomerId)
						.toPromise()
						.then((result: any) => {
							this.cd.detectChanges();
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/phone?id=' + key)
						.toPromise();
				}
			})
		});
	}

	//#endregion NAME

	//#region STORE
	/** 333333333333333333333333333333333
	 * STORE
	 * 3333333333333333333333333333333333
	 */
	/** #########################
	 *  API
	 * ##########################
	 */
	ReceiverStore_GetData() {
		this.receiverStoreDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/store?customerId=' + this.receiverCustomerId)
						.toPromise()
						.then((result: any) => {
							this.cd.detectChanges();
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/store?id=' + key)
						.toPromise();
				}
			})
		});
	}

	//#endregion

	//#region PLACE
	/** 333333333333333333333333333333333
	 * PLACE
	 * 3333333333333333333333333333333333
	 */

	/** #########################
	 *  API
	 * ##########################
	 */
	ReceiverPlace_GetData() {
		this.receiverPlaceDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/place?customerId=' + this.receiverCustomerId)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/place?id=' + key)
						.toPromise();
				}
			})
		});
	}
	//#endregion PLACE

	//#region ADDRESS
	/** 333333333333333333333333333333333
	 * ADDRESS
	 * 3333333333333333333333333333333333
	 */
	/** #########################
	 *  API
	 * ##########################
	 */
	ReceiverAddress_GetData() {
		this.receiverAddressDs = new DataSource({
			paginate: false,
			store: new CustomStore({
				key: 'customerId',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/address?customerId=' + this.receiverCustomerId)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				},
				byKey: (key) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'customer/address?id=' + key)
						.toPromise();
				}
			})
		});
	}
	//#endregion ADDRESSReceiverAddress_GetData

	//#endregion RECEIVER

	//#endregion CUSTOMER

	//#region GOODS
	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 * GOODS
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */
	modaUdara = 6000;
	previousRowIndex = null;
	currentRowIndex = null;



	/** #########################
	 * USER ACTION
	 * ##########################
	 */

	Test_Action(e, i) {
		console.log(' Test_Action ', e);
	}

	// SetToZero_OnFocusOut(e, index, input) {
	// 	/**
	// 	 * when every goods length, width, height lost the focus then :
	// 	 * IF null or empty set to 0
	// 	 * for goodsWeight, biarkan apa adanya
	// 	 */
	//
	// 	if (this.previousRowIndex === null) {
	// 		this.previousRowIndex = index;
	// 	}
	//
	// 	const ge = this.GetGoodsForm().at(index).get('goodsWeight').value;
	// 	let le = this.GetGoodsForm().at(index).get('length').value;
	// 	let wi = this.GetGoodsForm().at(index).get('width').value;
	// 	let he = this.GetGoodsForm().at(index).get('height').value;
	//
	//
	// 	if ( input === 'length' && le === null ) {
	// 		// set to 0 if null or empty
	// 		this.GetGoodsForm().at(index).patchValue({
	// 			length: 0
	// 		});
	//
	// 		le = 0;
	// 	}
	//
	// 	if ( input === 'width' && wi === null ) {
	// 		// set to 0 if null or empty
	// 		this.GetGoodsForm().at(index).patchValue({
	// 			width: 0
	// 		});
	//
	// 		wi = 0;
	// 	}
	//
	// 	if ( input === 'height' && he === null ) {
	// 		// set to 0 if null or empty
	// 		this.GetGoodsForm().at(index).patchValue({
	// 			height: 0
	// 		});
	//
	// 		he = 0;
	// 	}
	//
	//
	// 	// calc
	// 	this.CalculateFinalVolume(e, index, ge, le, wi, he);
	//
	// 	// total price
	// 	this.CalculateTotalPrice();
	//
	// 	// get koli no when type tab at height input
	// 	if (input === 'height' && e.component._lastKeyName === 'tab') {
	// 		// this.GetKoliNo(e, index, ge, le, wi, he);
	// 		this.Goods_GetKoliNo(index);
	// 	}
	//
	// }






	/** #########################
	 * FUNCTION
	 * ##########################
	 */

	// CalculateFinalVolume(e, index, goodsWeight, le, wi, he) {
	// 	/**
	// 	 * if goodsWeight L W H is not null and not empty then calculate the final volume
	// 	 * final weight = if AW > CAW then AW
	// 	 * final weight = IF AW < CAW THEN CAW
	// 	 */
	//
	//
	// 	let caw = 0;
	// 	let finalWeight = 0;
	//
	// 	// calc for current row
	// 	if ( (goodsWeight !== null )
	// 		&& (le !== null )
	// 		&& ( wi !== null)
	// 		&& (he !== null)
	// 	) {
	// 		caw = (le * wi * he) / this.modaUdara;
	// 		caw = Number(caw.toFixed(0));
	// 		if (goodsWeight > caw) { finalWeight = goodsWeight; }
	// 		if (goodsWeight < caw) { finalWeight = caw; }
	// 		this.GetGoodsForm().at(index).patchValue({
	// 			goodsWeightVolume: caw,
	// 			goodsFinalWeight: finalWeight,
	// 		});
	// 		const final = this.GetGoodsForm().at(index).get('goodsFinalWeight').value;
	//
	//
	// 	}
	//
	// 	// udara 6000
	// 	// darat 4000
	// }

	cityDestination(){
		return '10.56'
	}

	CalculateTotalPrice() {
		// default value for total price
		this.spbForm.patchValue({
			totalPrice: null,
		});

		const object = this.GetGoodsForm().value;

		// if flat type
		if (this.spbForm.get('ratesType').value === 'flat') {
			let totalgoodsFinalWeight= 0;

			Enumerable.from(object).forEach(
				(obj: any, indexGoods) => {

					if (obj.goodsFinalWeight !== null) {
						// Begin Pati 4Mrt22 , masalah berat dikenakan PSM tdk terjadi pembulatan
						// Pembulatan goodsWeightTotal
						if (Math.round(obj.goodsFinalWeight) < obj.goodsFinalWeight)
						{
									this.pembulatanGoodsWeightTotal = Math.round(obj.goodsFinalWeight) + 0.5
						}
						else{
								// Jika pas di 0,5 maka jadi 0,5   misal 6,5  7,5   
								if (Math.round(obj.goodsFinalWeight) - 0.5 === obj.goodsFinalWeight)
									{
										this.pembulatanGoodsWeightTotal = obj.goodsFinalWeight
									}
									else{
										//this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
										this.pembulatanGoodsWeightTotal = Math.round(obj.goodsFinalWeight)
									}
						}
						//totalgoodsFinalWeight = totalgoodsFinalWeight + obj.goodsFinalWeight;
						totalgoodsFinalWeight = totalgoodsFinalWeight + this.pembulatanGoodsWeightTotal;
						this.GetGoodsForm().at(indexGoods).patchValue({
						goodsFinalWeight_display:  this.pembulatanGoodsWeightTotal 
					});
						// End Pati 4Mrt22
					}

				}

			);

			// set total price input
			let totalPrice = 0;
			

			let ppnValue = 0; let  discountValue = 0;
			if (this.spbForm.get('packing').value !== null || this.spbForm.get('packing').value !== '' ) { totalPrice = totalPrice + Number(this.spbForm.get('packing').value); }
			if (this.spbForm.get('quarantine').value !== null || this.spbForm.get('quarantine').value !== '' ) { totalPrice = totalPrice + Number(this.spbForm.get('quarantine').value); }
			if (this.spbForm.get('etc').value !== null || this.spbForm.get('etc').value !== '' ) { totalPrice = totalPrice + Number(this.spbForm.get('etc').value); }
			if (this.spbForm.get('ppn').value !== null || this.spbForm.get('ppn').value !== '' ) {
				ppnValue = totalPrice * ( Number(this.spbForm.get('ppn').value) / 100 );
				this.spbForm.patchValue({ ppnValue });
			}
			if (this.spbForm.get('discount').value !== null || this.spbForm.get('discount').value !== '' ) {
				discountValue = totalPrice * ( Number(this.spbForm.get('discount').value) / 100 );
				this.spbForm.patchValue({ discountValue });
			}

			let totalFinalCaw = 0;
			let totalAw = 0;
			let totalKoli = 0;
			var stringAw;

			Enumerable.from(this.GetGoodsForm().value).forEach(
				(obj: any, indexGoods) => {
					if (obj.goodsFinalWeight !== null && obj.goodsFinalWeight !== '') {

						// Begin Pati 4mrt22 Pembulatan dulu
						if (Math.round(obj.goodsFinalWeight) < obj.goodsFinalWeight)
						{
									this.pembulatanGoodsWeightTotal = Math.round(obj.goodsFinalWeight) + 0.5
						}
						else{
								// Jika pas di 0,5 maka jadi 0,5   misal 6,5  7,5   
								if (Math.round(obj.goodsFinalWeight) - 0.5 === obj.goodsFinalWeight)
									{
										this.pembulatanGoodsWeightTotal = obj.goodsFinalWeight
									}
									else{
										//this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
										this.pembulatanGoodsWeightTotal = Math.round(obj.goodsFinalWeight)
									}
						}
						//totalFinalCaw = totalFinalCaw + obj.goodsFinalWeight;
						totalFinalCaw = totalFinalCaw + this.pembulatanGoodsWeightTotal;
						// End Pati 4mrt22 Pembulatan dulu

						totalAw = totalAw + obj.goodsWeightTotal;

						if (totalAw % 1 == 0){
							totalAw = Math.round(totalAw);
						}
						if(totalAw % 1 != 0){
							stringAw = totalAw.toFixed(2);
							totalAw = Number(stringAw);
						}

						this.GetGoodsForm().at(indexGoods).patchValue({
							price: this.GetGoodsForm().at(indexGoods).get('goodsFinalWeight').value * Math.round(totalgoodsFinalWeight) * this.spbForm.get('rates').value / totalgoodsFinalWeight
						});
						// console.log(totalAw);

						//masih hanya untuk 1 koli							
						totalKoli+=1;
					}
				}
			);
	
			this.spbForm.patchValue({
				totalFinalCaw: totalFinalCaw,//Math.round(totalFinalCaw),
				totalAw: totalAw,
				totalKoli: totalKoli
			});
			
			// totalPrice = Math.round(totalFinalCaw) * this.spbForm.get('rates').value;
			totalPrice = (totalFinalCaw) * this.spbForm.get('rates').value;

			this.spbForm.patchValue({
				totalPrice : (totalPrice + ppnValue) - discountValue,
			});
		}

		// IF FLATMIN
		if (this.spbForm.get('ratesType').value === 'flatmin') {

			Enumerable.from(object).forEach(
				(obj: any, indexGoods) => {
					if (obj.goodsFinalWeight === null) { return; }

					// rates detail
					Enumerable.from(this.spbForm.get('ratesDetail').value).forEach(
						(row: any, indexRatesDetail) => {

							// Begin Pati 4Mrt22 , masalah berat dikenakan PSM tdk terjadi pembulatan
						// Pembulatan goodsWeightTotal
						if (Math.round(obj.goodsFinalWeight) < obj.goodsFinalWeight)
						{
									this.pembulatanGoodsWeightTotal = Math.round(obj.goodsFinalWeight) + 0.5
						}
						else{
								// Jika pas di 0,5 maka jadi 0,5   misal 6,5  7,5   
								if (Math.round(obj.goodsFinalWeight) - 0.5 === obj.goodsFinalWeight)
									{
										this.pembulatanGoodsWeightTotal = obj.goodsFinalWeight
									}
									else{
										//this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
										this.pembulatanGoodsWeightTotal = Math.round(obj.goodsFinalWeight)
									}
						}
						//totalgoodsFinalWeight = totalgoodsFinalWeight + obj.goodsFinalWeight;
						//totalgoodsFinalWeight = totalgoodsFinalWeight + this.pembulatanGoodsWeightTotal;
						//obj.goodsFinalWeight = this.pembulatanGoodsWeightTotal ;

						if (obj.goodsFinalWeight >= row.min && obj.goodsFinalWeight <= row.max ) {
							obj.goodsFinalWeight = row.max;	
						} else {
							obj.goodsFinalWeight = this.pembulatanGoodsWeightTotal ;;
						}

						this.GetGoodsForm().at(indexGoods).patchValue({
							goodsFinalWeight_display:  obj.goodsFinalWeight
						});
						// End Pati 4Mrt22

							// console.log(this.GetGoodsForm().value);
							if (obj.goodsFinalWeight >= row.min && obj.goodsFinalWeight <= row.max ) {
								this.GetGoodsForm().at(indexGoods).patchValue({
									goodsFinalWeight: row.max
								});

								this.GetGoodsForm().at(indexGoods).patchValue({
									price: row.rates
								});
							}
							// more than max
							if (obj.goodsFinalWeight > row.max) {
								this.GetGoodsForm().at(indexGoods).patchValue({
									//goodsFinalWeight: Math.round(obj.goodsFinalWeight) // Pati 4mrt22
									goodsFinalWeight: obj.goodsFinalWeight
								});

								this.GetGoodsForm().at(indexGoods).patchValue({
									price: obj.goodsFinalWeight * this.spbForm.get('rates').value
								});
							}
						}
					);

				}
			);

			// set final total
			let totalPrice = 0;

			let totalFinalCaw = 0;
			let totalAw = 0;
			let totalKoli = 0;
			
				Enumerable.from(this.GetGoodsForm().value).forEach(
					(obj: any, indexGoods) => {
						if (obj.goodsFinalWeight !== null && obj.goodsFinalWeight !== '') {
						
						// Begin Pati 4mrt22 Pembulatan dulu
						if (Math.round(obj.goodsFinalWeight) < obj.goodsFinalWeight)
						{
									this.pembulatanGoodsWeightTotal = Math.round(obj.goodsFinalWeight) + 0.5
						}
						else{
								// Jika pas di 0,5 maka jadi 0,5   misal 6,5  7,5   
								if (Math.round(obj.goodsFinalWeight) - 0.5 === obj.goodsFinalWeight)
									{
										this.pembulatanGoodsWeightTotal = obj.goodsFinalWeight
									}
									else{
										//this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
										this.pembulatanGoodsWeightTotal = Math.round(obj.goodsFinalWeight)
									}
						}
						//totalFinalCaw = totalFinalCaw + obj.goodsFinalWeight;
						totalFinalCaw = totalFinalCaw + this.pembulatanGoodsWeightTotal;
						// End Pati 4mrt22 Pembulatan dulu

							totalAw = totalAw + obj.goodsWeightTotal;
							totalKoli+=1;
						}
					}
				);

	
			//masih hanya untuk 1 koli
			this.spbForm.patchValue({
				totalFinalCaw: totalFinalCaw,
				totalAw: totalAw,
				totalKoli: totalKoli
			});

			// console.log(this.GetGoodsForm().value);

			Enumerable.from(this.GetGoodsForm().value).forEach(
				(obj: any, indexGoods) => {
					if (obj.price !== null && obj.price !== '') {
						totalPrice = totalPrice + obj.price;
					}
				}
			);


			let ppnValue = 0; let  discountValue = 0;
			if (this.spbForm.get('packing').value !== null || this.spbForm.get('packing').value !== '' ) { totalPrice = totalPrice + Number(this.spbForm.get('packing').value); }
			if (this.spbForm.get('quarantine').value !== null || this.spbForm.get('quarantine').value !== '' ) { totalPrice = totalPrice + Number(this.spbForm.get('quarantine').value); }
			if (this.spbForm.get('etc').value !== null || this.spbForm.get('etc').value !== '' ) { totalPrice = totalPrice + Number(this.spbForm.get('etc').value); }
			if (this.spbForm.get('ppn').value !== null || this.spbForm.get('ppn').value !== '' ) {
				ppnValue = totalPrice * ( Number(this.spbForm.get('ppn').value) / 100 );
				this.spbForm.patchValue({ ppnValue });
			}
			if (this.spbForm.get('discount').value !== null || this.spbForm.get('discount').value !== '' ) {
				discountValue = totalPrice * ( Number(this.spbForm.get('discount').value) / 100 );
				this.spbForm.patchValue({ discountValue });
			}
			this.spbForm.patchValue({
				totalPrice : (totalPrice + ppnValue) - discountValue,
			});

		}
			// IF FLATMINSPB
			if (this.spbForm.get('ratesType').value === 'flatminspb') {
				// this.totalKoli=0;
				// Enumerable.from(object).forEach(
				// 	(obj: any, indexGoods) => {
				// 		if ((this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value) != null){
				// 			this.totalKoli+=1;
				// 			console.log(this.totalKoli);
				// 		}	
				// 	});
	
				Enumerable.from(object).forEach(
					(obj: any, indexGoods) => {					
						if (obj.goodsFinalWeight === null) { return; }
	
						
						// console.log(this.GetGoodsForm().at(indexGoods).value);
						let beratSementara = 0;
	
						Enumerable.from(object).forEach(
							(obj2: any, indexGoods) => {
									// if (this.GetGoodsForm().at(indexGoods).get('koliNo').value)
									// console.log(this.GetGoodsForm().at(indexGoods).get('koliNo').value);
									beratSementara = beratSementara + obj2.goodsWeightTotal;
						});
	
					 
						var totVolume = 0;
				this.GetGoodsForm().value.forEach(function(value){
					// console.log(value);
					
					var tot = 0;
					if(value.goodsWeightVolume > value.goodsWeightTotal){
						tot = value.goodsWeightVolume;
					}else{
						tot = value.goodsWeightTotal;
					}
					totVolume =  totVolume + tot;
	
				});
				 //console.log(totVolume);
	
						// rates detail
						Enumerable.from(this.spbForm.get('ratesDetail').value).forEach(
							(row: any, indexRatesDetail) => {
								
								let kti = this.GetGoodsForm().at(indexGoods).get('koliTotalItem').value;
								if (beratSementara < row.max && beratSementara > row.min ){
									this.minBerat = row.max;
									this.minHarga = row.rates;
								}

								//PEMBULATAN UTK JABAR-JATENG  truck_jabar  truck_jateng  0,3=>0   0,4=>1  Math.round(0,49) = 0   Math.round(0,5)=1

							if (this.spbForm.get('carrier').value === 'truck_jabar' || this.spbForm.get('carrier').value === 'truck_jateng'){
								// Pembulatan goodsWeightTotal Jabar-jateng
								if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeight').value ) < this.GetGoodsForm().at(indexGoods).get('goodsWeight').value)
								{
									//  round(X) + 0.3 >= X
									if ((Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeight').value) + 0.3) >= this.GetGoodsForm().at(indexGoods).get('goodsWeight').value)
										{		
											this.pembulatanGoodsWeightTotal = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeight').value) * kti
										}
										else{
											this.pembulatanGoodsWeightTotal = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeight').value + 0.1) * kti
										}
								}
								else{  // jika X > = 0,5
									this.pembulatanGoodsWeightTotal = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeight').value) * kti
								}

								// Pembulatan goodsWeightVolume Jabar-jateng
								if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value ) < this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value)
								{
									//  round(X) + 0.3 >= X
									if ((Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) + 0.3) >= this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value)
										{		
											this.pembulatanGoodsWeightVolume = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) * kti
										}
										else{
											this.pembulatanGoodsWeightVolume = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value + 0.1) * kti
										}
								} 
								else{  // jika X > = 0,5
									this.pembulatanGoodsWeightVolume = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) * kti
								}

						}
						else {   //  PEMBULATAN SELAIN JABAR-JATENG  
								// Pembulatan goodsWeightTotal selain Jabar-jateng
								if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeight').value)  < this.GetGoodsForm().at(indexGoods).get('goodsWeight').value)
								{
											this.pembulatanGoodsWeightTotal = (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeight').value) + 0.50) * kti
								}
								else{
										// Jika pas di 0,5 maka jadi 0,5   misal 6,5  7,5   
										if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeight').value) - 0.5 === this.GetGoodsForm().at(indexGoods).get('goodsWeight').value)
											{
												this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeight').value * kti
											}
											else{
												//this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
												this.pembulatanGoodsWeightTotal = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeight').value) * kti
											}
								}

								// Pembulatan goodsWeightVolume selain Jabar-Jateng
								if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) < this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value)
								{
											this.pembulatanGoodsWeightVolume = (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) + 0.5) * kti
										}
								else{
										// Jika pas di 0,5 maka jadi 0,5   misal 6,5  7,5   
										if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) - 0.5 === this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value)
											{
												this.pembulatanGoodsWeightVolume = this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value * kti
											}
											else{
												//this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
												this.pembulatanGoodsWeightVolume =  Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) * kti
											}
								}

							}

								// // Pembulatan goodsWeightTotal
								// if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value) < this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value)
								// {
								// 			this.pembulatanGoodsWeightTotal = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value) + 0.5
								// }
								// else{
								// 		// Jika pas di 0,5 maka jadi 0,5   misal 6,5  7,5   
								// 		if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value) - 0.5 === this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value)
								// 			{
								// 				this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
								// 			}
								// 			else{
								// 				//this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
								// 				this.pembulatanGoodsWeightTotal = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value)
								// 			}
								// }
	
								// // Pembulatan goodsWeightVolume
								// if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) < this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value)
								// {
								// 			this.pembulatanGoodsWeightVolume = Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) + 0.5
								// 		}
								// else{
								// 		// Jika pas di 0,5 maka jadi 0,5   misal 6,5  7,5   
								// 		if (Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value) - 0.5 === this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value)
								// 			{
								// 				this.pembulatanGoodsWeightVolume = this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value
								// 			}
								// 			else{
								// 				//this.pembulatanGoodsWeightTotal = this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
								// 				this.pembulatanGoodsWeightVolume =  Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value)
								// 			}
								// }
	
								this.pembulatanGoodsFinalWeight = this.GetGoodsForm().at(indexGoods).get('goodsFinalWeight').value
								
								// if (this.pembulatanGoodsWeightTotal >= this.pembulatanGoodsWeightVolume)
								// {this.pembulatanGoodsFinalWeight = this.pembulatanGoodsWeightTotal}
								// else {this.pembulatanGoodsFinalWeight = this.pembulatanGoodsWeightVolume}
								
	
								//LEBIH BESAR DARI MIN
								if (Math.round(beratSementara) >= this.minBerat){
								//if (this.pembulatanGoodsFinalWeight >= this.minBerat){
									if (this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value * kti >= this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value){
										this.GetGoodsForm().at(indexGoods).patchValue({
											//goodsFinalWeight: this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value *  kti,
											goodsFinalWeight: this.pembulatanGoodsWeightVolume,
											//goodsFinalWeight_display: this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value *  kti
											goodsFinalWeight_display: this.pembulatanGoodsWeightVolume
										});
									}
									else{
										
										this.GetGoodsForm().at(indexGoods).patchValue({
											//goodsFinalWeight: Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value),
											goodsFinalWeight:  this.pembulatanGoodsWeightTotal, // Total Berat Dikenakan
											//goodsFinalWeight_display: Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value)
											goodsFinalWeight_display:  this.pembulatanGoodsWeightTotal // Berat Dikenakan
										});
									}
									
									this.GetGoodsForm().at(indexGoods).patchValue({
										//price: this.GetGoodsForm().at(indexGoods).get('goodsFinalWeight').value * this.spbForm.get('rates').value * Math.round(beratSementara) / beratSementara
										price: this.pembulatanGoodsWeightTotal * this.spbForm.get('rates').value * Math.round(beratSementara) / beratSementara
									});
									
								}
								//LEBIH KECIL DARI MIN 1 Maret 22   this.pembulatanGoodsWeightTotal  this.pembulatanGoodsWeightVolume
								if (Math.round(beratSementara) < this.minBerat){
								//if (this.pembulatanGoodsWeightTotal < this.minBerat || this.pembulatanGoodsWeightVolume < this.minBerat){
									if(totVolume > this.minBerat){  // Total Volume > Min berat
										if(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value * kti >= this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value){ // Volume >=AW
											this.GetGoodsForm().at(indexGoods).patchValue({
												//goodsFinalWeight: Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value),
												goodsFinalWeight: this.pembulatanGoodsWeightVolume,
												goodsFinalWeight_display: Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value)
												//goodsFinalWeight_display: this.pembulatanGoodsWeightVolume
											});
										}else{ // Volume < AW 
											this.GetGoodsForm().at(indexGoods).patchValue({
												//goodsFinalWeight: Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value),
												goodsFinalWeight:  this.pembulatanGoodsWeightVolume,
												//goodsFinalWeight_display: Math.round(this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value)
												goodsFinalWeight_display:  this.pembulatanGoodsWeightVolume
											});
										}
										
									}else{  //Total  Volume <= Min berat
										if(this.GetGoodsForm().at(indexGoods).get('goodsWeightVolume').value * kti <= this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value){ // Volume <=AW
											this.GetGoodsForm().at(indexGoods).patchValue({
											//BERAT FINAL  KESINI 23mrt22
											goodsFinalWeight: this.minBerat * this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value / beratSementara,
											//goodsFinalWeight_display:  this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
											goodsFinalWeight_display:  this.pembulatanGoodsWeightTotal // BERAT DIKENAKAN
											});
										}else{
											this.GetGoodsForm().at(indexGoods).patchValue({
												//BERAT FINAL
												goodsFinalWeight:  this.minBerat * this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value / beratSementara,
												//goodsFinalWeight:  this.pembulatanGoodsWeightTotal,
												//goodsFinalWeight_display:  this.GetGoodsForm().at(indexGoods).get('goodsWeightTotal').value
												goodsFinalWeight_display:  this.pembulatanGoodsWeightVolume // BERAT DIKENAKAN
												});
										}
									}
	
									// console.log(this.GetGoodsForm().at(indexGoods).get('goodsFinalWeight').value,this.minBerat,beratSementara);
	
									this.GetGoodsForm().at(indexGoods).patchValue({
										price: this.spbForm.get('rates').value * this.GetGoodsForm().at(indexGoods).get('goodsFinalWeight').value
									});
	
								}
							}
						);
						this.beratTotal = beratSementara;
					}
					
				);
			
	
	
				// set final total
				let totalPrice = 0;
				
				let totalFinalCaw = 0;
				let totalAw = 0;
				let totalKoli = 0;
				var stringAw;
				
				//--- tambahan laut & darat
				//--- author Pati
				//--- 2021-09-10
				let etc = 0;
				var stringKet;
	
				Enumerable.from(this.GetGoodsForm().value).forEach(
					(obj: any, indexGoods) => {
						if (obj.goodsFinalWeight !== null && obj.goodsFinalWeight !== '') {
	
							totalFinalCaw = totalFinalCaw + obj.goodsFinalWeight; // CawFinal di Header Total Berat Dikenakan(Berat Final)
							totalAw = totalAw + obj.goodsWeightTotal; // Total di kolom Jumlah Berat
	
							if (totalAw % 1 == 0){
								totalAw = totalAw;
							}
							if(totalAw % 1 != 0){
								stringAw = totalAw.toFixed(2);
								totalAw = Number(stringAw);
							}
							// console.log(totalAw);
	
							//masih hanya untuk 1 koli							
							totalKoli+=1;
						}
					}
				);
	
				
				
				//--- tambahan laut & darat
				//--- author Pati
				//--- 2021-09-10
				// console.log("------------------------------ paket darat start");
				var mx = 0;
				var pc = 0;
				var rs = this.spbForm.get('rates').value;
				
				if(this.spbForm.get('carrier').value === 'pack' || this.spbForm.get('carrier').value === 'truck_sub'){
					this.spbForm.get('ratesDetail').value.forEach(function(r){
						// console.log(r);
						mx = r.max;
						pc = r.rates;
					});
					
				}
				
				// console.log('checkAgreed 1 : ', this.checkAgreed);
				
				// if(this.checkAgreed == 0){
				// 	// console.log('checkAgreed 2 : ', this.checkAgreed);
					
				// 	if((this.spbForm.get('carrier').value === 'pack' || this.spbForm.get('carrier').value === 'truck_sub') && totalFinalCaw > mx){
				// 		// etc = pc;
				// 		var pa = (pc).toLocaleString('en');
				// 		var pb = pa.replace(/,/g,'.');
	
				// 		var ra = (rs).toLocaleString('en');
				// 		var rb = ra.replace(/,/g,'.');
	
				// 		stringKet =  "Minimum "+mx+" Kg pertama Rp."+pb+",00. Selanjutnya Rp."+rb+"/Kg." + "\n" + "----------------------------------------------------------------------------------------" + "\n\n";
				// 	}
				// }
	
				var desc_value = "";
	
				if(this.spbForm.get('description').value === null || this.spbForm.get('description').value === undefined ){
					desc_value = stringKet;
				}else{
					desc_value = stringKet + this.spbForm.get('description').value.replace(stringKet,"");	
					if(desc_value == "undefined" || desc_value == "NaN"){ desc_value = ""; }
					 
					desc_value = desc_value.replace('undefined','');
				}
					
				
				
				this.spbForm.patchValue({
					//totalFinalCaw: Math.round(totalFinalCaw),
					totalFinalCaw: totalFinalCaw,
					totalAw: totalAw,
					totalKoli: totalKoli
					//--- tambahan laut & darat
					//--- author Pati
					//--- 2021-09-10
					, min : mx
					, minRates : pc
					, description : desc_value
				});
				
				// console.log("totalFinalCaw : "+totalFinalCaw);
				// console.log("beratTotal : " + this.beratTotal);
				// console.log("totalFinalCaw : "+totalFinalCaw);
				
	
				// console.log("------------------------------ paket darat end");
	
				//--- tambahan laut & darat
				//--- author Pati
				//--- 2021-09-10
				if((this.spbForm.get('carrier').value === 'pack' || this.spbForm.get('carrier').value === 'truck_sub') && totalFinalCaw > mx){
					//totalPrice = (Math.round(totalFinalCaw-mx)*this.spbForm.get('rates').value)+pc;
					totalPrice = ((totalFinalCaw-mx)*this.spbForm.get('rates').value)+pc;
				}
	
				if((this.spbForm.get('carrier').value === 'pack' || this.spbForm.get('carrier').value === 'truck_sub') && totalFinalCaw <= mx){
					totalPrice = pc;
				}
	
				if (totalFinalCaw > this.minBerat && this.spbForm.get('carrier').value != 'pack' && this.spbForm.get('carrier').value != 'truck_sub'){
					//totalPrice = Math.round(totalFinalCaw) * this.spbForm.get('rates').value;
					totalPrice = (totalFinalCaw) * this.spbForm.get('rates').value;
				}
	
				if (totalFinalCaw <= this.minBerat && this.spbForm.get('carrier').value != 'pack' && this.spbForm.get('carrier').value != 'truck_sub'){
					totalPrice = this.minHarga;
				}
				
				// console.log(this.spbForm.get())
				// disini penambahan baru pada tarif surabaya,lamongan,gresik dan sidoarjo
				// if (this.spbForm.get('carrier').value == 'truck_jatim' && 
				// 	(this.userGlobalVar.WORK_CITY_NAME == 'TNA - [ Kota Jakarta Pusat ] - [ DKI Jakarta ]' ||
				// 	this.userGlobalVar.WORK_CITY_NAME == 'THM - [ Kota Thamrin City ] - [ DKI Jakarta ]'	
				// 	)
				// ){
				// 	//totalPrice = Math.round(totalFinalCaw) * this.spbForm.get('rates').value;
				// 		 let specialFees= 0;
				// 		if(this.spbForm.get('destCity').value == 264){ 
				// 			specialFees = 6000;
				// 		}
				// 		else if(this.spbForm.get('destCity').value == 242){
				// 			specialFees = 5900;
				// 		}
				// 		else if(this.spbForm.get('destCity').value == 252){
				// 			specialFees = 6300;
				// 		}
				// 		else if(this.spbForm.get('destCity').value == 251){
				// 			specialFees = 6700;
				// 		}else{
				// 			specialFees = 0;
				// 		}
				// 		totalPrice = ((totalFinalCaw) * this.spbForm.get('rates').value) + specialFees;
				// }

				// console.log("totalPrice : " + totalPrice);
				
	
				let ppnValue = 0; let  discountValue = 0;
				if (this.spbForm.get('packing').value !== null || this.spbForm.get('packing').value !== '' ) { totalPrice = totalPrice + Number(this.spbForm.get('packing').value); }
				if (this.spbForm.get('quarantine').value !== null || this.spbForm.get('quarantine').value !== '' ) { totalPrice = totalPrice + Number(this.spbForm.get('quarantine').value); }
				if (this.spbForm.get('etc').value !== null || this.spbForm.get('etc').value !== '' ) { totalPrice = totalPrice + Number(this.spbForm.get('etc').value); }
				if (this.spbForm.get('ppn').value !== null || this.spbForm.get('ppn').value !== '' ) {
					ppnValue = totalPrice * ( Number(this.spbForm.get('ppn').value) / 100 );
					this.spbForm.patchValue({ ppnValue });
				}
				if (this.spbForm.get('discount').value !== null || this.spbForm.get('discount').value !== '' ) {
					discountValue = totalPrice * ( Number(this.spbForm.get('discount').value) / 100 );
					this.spbForm.patchValue({ discountValue });
				}
				
				this.spbForm.patchValue({
					totalPrice : (totalPrice + ppnValue) - discountValue,
				});
			}


		this.cd.detectChanges();
	}

	CheckAgreed(){
		const params = {};
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
			.post(AppGlobalVar.BASE_API_URL + 'spb-create/check-agreed', params, config)
			.subscribe((data: any) => {
				// console.log('AS AGREED',data.data[0].checkAgree);
				this.checkAgreed = data.data[0].checkAgree;
				
			},
			(error) => {
				alert(error.statusText + '. ' + error.message);
			}
		);
	}

	

	// GetKoliNo(e, index, ge, le, wi, he) {
	// 	/**
	// 	 * get koli no when row index changed
	// 	 */
	// 	console.log(ge, le, wi, he);
	// }

	/** #########################
	 * API
	 * ##########################
	 */

	Goods_GetKoliNo(index) {
		/**
		 * disable input box sp user cannot edit after get koli
		 * get koli no. from API
		 *
		 */



			// get all value
		const gw = this.GetGoodsForm().at(index).get('goodsWeight').value;
		const le = this.GetGoodsForm().at(index).get('length').value;
		const wi = this.GetGoodsForm().at(index).get('width').value;
		const he = this.GetGoodsForm().at(index).get('height').value;
		const destCity = this.spbForm.get('destCity').value;
		const destArea = this.spbForm.get('destArea').value;


		// send to API if all value is not null
		if ( (gw !== null )
			&& (le !== null )
			&& ( wi !== null)
			&& (he !== null)
			&& destCity !== null
			&& destArea !== null
		) {

			// disable input by index
			this.GetGoodsForm().at(index).get('goodsWeight').disable();
			this.GetGoodsForm().at(index).get('length').disable();
			this.GetGoodsForm().at(index).get('width').disable();
			this.GetGoodsForm().at(index).get('height').disable();

			const formData: any = new FormData();
			// formData.append('CreatedAt', Date.now());
			formData.append('aw', gw);
			formData.append('length', le);
			formData.append('width', wi);
			formData.append('height', he);
			formData.append('spbNo', this.spbNumber);

			if (this.desDisable === false) {
				formData.append('destCity', destCity);
				formData.append('destArea', destArea);
			}
			// console.log('spbcreate');

			this.http.post(AppGlobalVar.BASE_API_URL + 'spb/get-koli-number', formData).subscribe(
				(response: any) => {

					this.GetGoodsForm().at(index).patchValue({
						
						koliNo: response.data.koliNo
					});

					// for first time, disable destination area
					if (this.desDisable === false) {
						this.SetDestinationDisable(true);
						this.desDisable = true;
					}
				},
				(error) => {
					this.GetGoodsForm().at(index).get('goodsWeight').enable();
					this.GetGoodsForm().at(index).get('length').enable();
					this.GetGoodsForm().at(index).get('width').enable();
					this.GetGoodsForm().at(index).get('height').enable();
				}
			);
		}


		this.cd.detectChanges();

	}

	//#endregion GOODS

	//#region MODA AND OTHER
	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 * MODA AND OTHER
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */

	// //#region TOG - TYPE OF GOODS
	// /** 222222222222222222222222222222222222222222222222222222222222222222
	//  * TYPE OF GOODS
	//  * 2222222222222222222222222222222222222222222222222222222222222222222
	//  */
	// togDs: DataSource;


	// /** #########################
	//  *  API
	//  * ##########################
	//  */

	// private getTog() {
	// 	/**
	// 	 * get data TOG from API
	// 	 *
	// 	 * call by :
	// 	 * - onLoadPage()
	// 	 */
	// 	this.togDs = new DataSource({
	// 		store: new CustomStore({
	// 			key: 'lookupKey',
	// 			loadMode: 'raw',
	// 			load: (loadOptions) => {
	// 				return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey=tog')
	// 					.toPromise()
	// 					.then((result: any) => {
	// 						return result.data;
	// 					});
	// 			}
	// 		})
	// 	});
	// }

	// //#endregion TOG - TYPE OF GOODS

	// //#region CARRIER
	// /** 222222222222222222222222222222222222222222222222222222222222222222
	//  * CARRIER
	//  * 2222222222222222222222222222222222222222222222222222222222222222222
	//  */
	// carrierDs: DataSource;

	// /** #########################
	//  *  API
	//  * ##########################
	//  */
	// private getCarrier() {
	// 	/**
	// 	 * get data CARRIER from API
	// 	 *
	// 	 * call by :
	// 	 * - onLoadPage()
	// 	 */
	// 	this.carrierDs = new DataSource({
	// 		store: new CustomStore({
	// 			key: 'lookupKey',
	// 			loadMode: 'raw',
	// 			load: (loadOptions) => {
	// 				return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey=carrier')
	// 					.toPromise()
	// 					.then((result: any) => {
	// 						return result.data;
	// 					});
	// 			}
	// 		})
	// 	});
	// }
	// //#endregion CARRIER

	// //#region QOS
	// /** 222222222222222222222222222222222222222222222222222222222222222222
	//  * QOS
	//  * 2222222222222222222222222222222222222222222222222222222222222222222
	//  */
	// qosDs: DataSource;

	// /** #########################
	//  *  API
	//  * ##########################
	//  */
	// private getQos() {
	// 	/**
	// 	 * get data QOS from API
	// 	 *
	// 	 * call by :
	// 	 * - onLoadPage()
	// 	 */
	// 	this.qosDs = new DataSource({
	// 		store: new CustomStore({
	// 			key: 'lookupKey',
	// 			loadMode: 'raw',
	// 			load: (loadOptions) => {
	// 				return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey=qos')
	// 					.toPromise()
	// 					.then((result: any) => {
	// 						return result.data;
	// 					});
	// 			}
	// 		})
	// 	});
	// }
	// //#endregion QOS

	// //#region TOS
	// /** 222222222222222222222222222222222222222222222222222222222222222222
	//  * TOS
	//  * 2222222222222222222222222222222222222222222222222222222222222222222
	//  */
	// tosDs: DataSource;

	// /** #########################
	//  *  API
	//  * ##########################
	//  */
	// private getTos() {
	// 	/**
	// 	 * get data TOS from API
	// 	 *
	// 	 * call by :
	// 	 * - onLoadPage()
	// 	 */
	// 	this.tosDs = new DataSource({
	// 		store: new CustomStore({
	// 			key: 'lookupKey',
	// 			loadMode: 'raw',
	// 			load: (loadOptions) => {
	// 				return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey=tos')
	// 					.toPromise()
	// 					.then((result: any) => {
	// 						return result.data;
	// 					});
	// 			}
	// 		})
	// 	});
	// }
	//#endregion TOS

	//#endregion MODA AND OTHER





	//#region Payment Method
	/** 222222222222222222222222222222222222222222222222222222222222222222
	 * Payment method
	 * 2222222222222222222222222222222222222222222222222222222222222222222
	 */
	// pmDs: DataSource;


	/** #########################
	 *  API
	 * ##########################
	 */

	// private getPm() {
	// 	/**
	// 	 * get data Payment Method (PM) from API
	// 	 *
	// 	 * call by :
	// 	 * - onLoadPage()
	// 	 */
	// 	this.pmDs = new DataSource({
	// 		store: new CustomStore({
	// 			key: 'lookupKey',
	// 			loadMode: 'raw',
	// 			load: (loadOptions) => {
	// 				return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey=pm')
	// 					.toPromise()
	// 					.then((result: any) => {
	// 						return result.data;
	// 					});
	// 			}
	// 		})
	// 	});
	// }

	//#endregion Payment Method

	DoSubmit(e) {
		
		// turn ON or OFF screen Capture
		// html2canvas(document.body).then((canvas) => {
		// 	// document.body.appendChild(canvas);
		// 	// console.log(canvas.toDataURL());
		// 	this.screenCaptureSave(canvas.toDataURL());
		// });


		/**
		 * Submit All Information in Form
		 *
		 */

		//
		// validation

			// check if goods is emty
		let i = 0;
		let isValid = false;
		const object = this.GetGoodsForm().value;
		Enumerable.from(object).forEach(
			(obj: any) => {
				if (this.GetGoodsForm().at(i).get('koliNo').value !== null) {
					isValid = true;
				}
				i++;
			}

		);
		
		//Pengecekan PENGIRIM
		// if(this.spbForm.get('senderTelp').value == null && this.spbForm.get('sender').value == null) {isValid = false;}
		// if(this.spbForm.get('senderTelp').value == "" && this.spbForm.get('sender').value == "") {isValid = false;}
		// if(this.spbForm.get('senderPlace').value == null && this.spbForm.get('senderAddress').value == null) {isValid = false;}
		// if(this.spbForm.get('senderPlace').value == "" && this.spbForm.get('senderAddress').value == "") {isValid = false;}
		// if(this.spbForm.get('senderPlaceId').value == 0 && this.spbForm.get('senderAddressId').value == 0) {isValid = false;}

		// //Pngecekan PENERIMA
		// if(this.spbForm.get('receiverTelp').value == null && this.spbForm.get('receiver').value == null) {isValid = false;}
		/// if(this.spbForm.get('receiverTelp').value == "" && this.spbForm.get('receiver').value == "") {isValid = false;}
		// if(this.spbForm.get('receiverPlace').value == null && this.spbForm.get('receiverAddress').value == null) {isValid = false;}
		// if(this.spbForm.get('receiverPlace').value == "" && this.spbForm.get('receiverAddress').value == "") {isValid = false;}
		/// if(this.spbForm.get('receiverPlaceId').value == 0 && this.spbForm.get('receiverAddressId').value == 0) {isValid = false;}
		
		// console.log("Receiver : " + this.spbForm.get('receiver').value);
		// console.log("ReceiverTelp : " + this.spbForm.get('receiverTelp').value);
		// console.log("receiverPlace : " + this.spbForm.get('receiverPlace').value);
		// console.log("receiverAddress : " + this.spbForm.get('receiverAddress').value);
		// console.log("receiverPlaceId : " + this.spbForm.get('receiverPlaceId').value);
		// console.log("receiverAddressId : " + this.spbForm.get('receiverAddressId').value);
		//console.log("isValid : " + isValid);

		// @ts-ignore
		if (isValid !== true) {
			alert('Data Pengirim atau Data Penerima atau Data Barang belum lengkap...');
			return;
		}
			// get all value

			// tariff & other
		const typesOfGoods = this.spbForm.get('tog').value;
		const carrier = this.spbForm.get('carrier').value;
		const qos = this.spbForm.get('qos').value;
		const tos = this.spbForm.get('tos').value;
		const rates = this.spbForm.get('rates').value;
		const ratesType = this.spbForm.get('ratesType').value;
		const ratesDetail = this.spbForm.get('ratesDetail').value;
		const totalPrice = this.spbForm.get('totalPrice').value;
		const paymentMethod = this.spbForm.get('paymentMethod').value;

		const formData: any = new FormData();
	
		formData.append('SpbNo', this.spbNumber);
		formData.append('spbId', this.spbId);
		formData.append('spbNoManual', this.spbNumberManual);
		formData.append('destCity', this.spbForm.get('destCity').value);
		formData.append('destArea', this.spbForm.get('destArea').value);

		// sender
		formData.append('senderType', this.spbForm.get('senderType').value);
		
		if (this.senderIsNew.sender) {
			formData.append('senderArea', this.spbForm.get('senderArea').value); 
			formData.append('sender', this.spbForm.get('sender').value); 
			formData.append('sendertelp', this.spbForm.get('sendertelp').value);
		} else { 
			if(this.spbForm.get('sender').value === '' || this.spbForm.get('sender').value === null) {
				formData.append('senderArea', this.spbForm.get('senderArea').value); 
				formData.append('sender', this.spbForm.get('sender').value); 
				formData.append('sendertelp', this.spbForm.get('sendertelp').value);
			} else {
			formData.append('senderId', this.spbForm.get('senderId').value); }
		}
		if (this.senderIsNew.senderName) { formData.append('senderName', this.spbForm.get('senderName').value); } else { formData.append('senderNameId', this.spbForm.get('senderNameId').value); }
		if (this.senderIsNew.senderStore) { formData.append('senderStore', this.spbForm.get('senderStore').value); } else { formData.append('senderStoreId', this.spbForm.get('senderStoreId').value); }
		if (this.senderIsNew.senderPlace) { formData.append('senderPlace', this.spbForm.get('senderPlace').value); } else { formData.append('senderPlaceId', this.spbForm.get('senderPlaceId').value); }
		if (this.senderIsNew.senderAddress) { formData.append('senderAddress', this.spbForm.get('senderAddress').value); } else { formData.append('senderAddressId', this.spbForm.get('senderAddressId').value); }


		if (this.receiverNew.receiver) {
			formData.append('receiverArea', this.spbForm.get('receiverArea').value); 
			formData.append('receiver', this.spbForm.get('receiver').value); 
			formData.append('receivertelp', this.spbForm.get('receivertelp').value);
		} else { 
			if(this.spbForm.get('receiver').value === '' || this.spbForm.get('receiver').value === null) {
				formData.append('receiverArea', this.spbForm.get('receiverArea').value); 
				formData.append('receiver', this.spbForm.get('receiver').value); 
				formData.append('receivertelp', this.spbForm.get('receivertelp').value);
			} else {
			formData.append('receiverId', this.spbForm.get('receiverId').value); }

		}
		if (this.receiverNew.receiverName) { formData.append('receiverName', this.spbForm.get('receiverName').value); } else { formData.append('receiverNameId', this.spbForm.get('receiverNameId').value); }
		if (this.receiverNew.receiverStore) { formData.append('receiverStore', this.spbForm.get('receiverStore').value); } else { formData.append('receiverStoreId', this.spbForm.get('receiverStoreId').value); }
		if (this.receiverNew.receiverPlace) { formData.append('receiverPlace', this.spbForm.get('receiverPlace').value); } else { formData.append('receiverPlaceId', this.spbForm.get('receiverPlaceId').value); }
		if (this.receiverNew.receiverAddress) {
			formData.append('receiverAddress', this.spbForm.get('receiverAddress').value);
			formData.append('receiverRt', this.spbForm.get('receiverRt').value);
			formData.append('receiverRw', this.spbForm.get('receiverRw').value);
			formData.append('receiverUrban', this.spbForm.get('receiverUrban').value);
			formData.append('receiverSubDistrict', this.spbForm.get('receiverSubDistrict').value);
			formData.append('receiverCity', this.spbForm.get('receiverCity').value);
			formData.append('receiverPostalCode', this.spbForm.get('receiverPostalCode').value);

		} else { formData.append('receiverAddressId', this.spbForm.get('receiverAddressId').value); }
		if (this.receiverNew.receiverVia) { formData.append('receiverVia', this.spbForm.get('receiverVia').value); } else { formData.append('receiverViaId', this.spbForm.get('receiverViaId').value); }

		// TOS & ETC
		formData.append('typesOfGoods', typesOfGoods);
		formData.append('carrier', carrier);
		formData.append('qualityOfService', qos);
		formData.append('typeOfService', tos);

		formData.append('rates', rates);
		formData.append('ratesType', ratesType);

		ratesDetail.forEach(function(value){
			formData.append('minWeight', value.max);
		});
		
		formData.append('packing', this.spbForm.get('packing').value);
		formData.append('quarantine', this.spbForm.get('quarantine').value);
		formData.append('etc', this.spbForm.get('etc').value);
		formData.append('ppn', this.spbForm.get('ppn').value);
		formData.append('discount', this.spbForm.get('discount').value);
		formData.append('totalPrice', totalPrice);
		formData.append('cawFinal', this.spbForm.get('totalFinalCaw').value);

		formData.append('min', this.spbForm.get('min').value);
		formData.append('minRates', this.spbForm.get('minRates').value);


		
		formData.append('PaymentMethod', paymentMethod);
		formData.append('description', this.spbForm.get('description').value);

		return this.http
			.post(AppGlobalVar.BASE_API_URL + 'spbcreate/do-submit', formData)
			.subscribe((data: any) => {
					if (data != null) {
						this.PrintSpbV2();
						this.historyPrint(data,'spb');

					}
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				},
				() => {

					this.cd.detectChanges();
				}
			);
	}


	PrintSpb() {
		const dialogConfig = new MatDialogConfig();
		// dialogConfig.data = this.spbNumber;
		// dialogConfig.data = '1912.4.1.1.5.1.1.001451';
		dialogConfig.data = {
			spbNo: this.spbNumber,
			urlSpb: 'spbcreate/read-spb-print',
			urlSpbDetail: 'spbcreate/read-spb-goods-print',
		};
		dialogConfig.minWidth = this.innerWidth + 'px';
		dialogConfig.minHeight = this.innerHeight + 'px';
		let dialogRef = this.matDialog.open(SpbPrintComponent, dialogConfig);

		dialogRef.afterClosed().subscribe(value => {
			this.ResetFormAfterSave();
		});
		this.cd.detectChanges();
	}

	PrintSpbV2() {
		const dialogConfig = new MatDialogConfig();
		dialogConfig.data = {
			urlReport: this.appGlobalVar.BASE_REPORT_URL + 'spb-create/',
			manifestId: this.spbNumber,
			workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
			workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
			username: this.userGlobalVar.USERNAME,
			teks: 'undefined#toolbar=0' // Pati 15 Agust

		};
		dialogConfig.minWidth = this.innerWidth + 'px';
		dialogConfig.minHeight = this.innerHeight + 'px';
		let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);

		dialogRef.afterClosed().subscribe(value => {
			// this.ResetFormAfterSave();
			window.location.reload()
			this.spbCreateService.SetCarrierRO(false);
			this.cd.detectChanges();
		});
	}

	TestPrint() {

		this.ResetFormAfterSave();
		// location.reload();

		// html2canvas(document.body).then((canvas) => {
		// 	// document.body.appendChild(canvas);
		// 	// console.log(canvas.toDataURL());
		// 	this.screenCaptureSave(canvas.toDataURL());
		// });

		// const dialogConfig = new MatDialogConfig();
		// // dialogConfig.data = data.spbNo;
		// dialogConfig.data = {
		// 	urlReport: this.appGlobalVar.BASE_REPORT_URL + 'spb-create/',
		// 	manifestId: '1912.4.1.1.5.1.1.001451',
		// 	workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
		// 	workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
		// 	username: this.userGlobalVar.USERNAME
		// };
		// dialogConfig.minWidth = this.innerWidth + 'px';
		// dialogConfig.minHeight = this.innerHeight + 'px';
		// let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);
		//
		// dialogRef.afterClosed().subscribe(value => {
		// 	console.log(`Dialog sent: ${value}`);
		// });
	}

	screenCaptureSave(image) {
		const formData: any = new FormData();
		formData.append('SpbNo', this.spbNumber);
		formData.append('image', image);

		return this.http
			.post(AppGlobalVar.BASE_API_URL + 'spbcreate/screen-capture', formData)
			.subscribe((data: any) => {

					this.cd.detectChanges();
				},
				(error) => {
					// alert(error.statusText + '. ' + error.message);
				}
			);
	}


	ResetFormAfterSave() {
		// get new SPB NO
		this.GetSpbNo();
		this.cd.detectChanges();

		//
		// -- enable input

		// goods
		let i = 0;
		const object = this.GetGoodsForm().value;
		Enumerable.from(object).forEach(
			(obj: any) => {
				this.GetGoodsForm().at(i).get('goodsWeight').enable();
				this.GetGoodsForm().at(i).get('length').enable();
				this.GetGoodsForm().at(i).get('width').enable();
				this.GetGoodsForm().at(i).get('height').enable();
				this.GetGoodsForm().at(i).get('koliTotalItem').enable();
				i++;
			}
		);
		this.CleanForm('goods');



		// destination
		this.spbForm.get('destCity').enable();
		this.spbForm.get('destArea').enable();
		this.CleanForm('destination');


		//
		// sender
		// set to old
		this.spbCreateService.SetSenderIsNew('sender', false);
		this.spbCreateService.SetSenderIsNew('senderName', false);
		this.spbCreateService.SetSenderIsNew('senderStore', false);
		this.spbCreateService.SetSenderIsNew('senderPlace', false);
		this.spbCreateService.SetSenderIsNew('senderAddress', false);
		this.spbForm.patchValue({senderType: 'hp'});
		this.CleanForm('sender');

		//
		// receiver
		// set to old
		this.spbCreateService.SetReceiverIsNew('receiver', false);
		this.spbCreateService.SetReceiverIsNew('receiverName', false);
		this.spbCreateService.SetReceiverIsNew('receiverStore', false);
		this.spbCreateService.SetReceiverIsNew('receiverPlace', false);
		this.spbCreateService.SetReceiverIsNew('receiverAddress', false);
		this.spbCreateService.SetReceiverIsNew('receiverVia', false);
		this.spbForm.patchValue({receiverType: 'hp'});
		this.CleanForm('receiver');

		// set ppn & disc
		this.CleanForm('price');


		this.CleanForm('moda');

		// set focuse to top

		(function smoothscroll() {
			var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
			if (currentScroll > 0) {
				window.requestAnimationFrame(smoothscroll);
				window.scrollTo(0, currentScroll - (currentScroll / 8));
			}
		})();


		// document.getElementById('toTop').focus();


		this.cd.detectChanges();

	}

	CleanForm(groupForm: string) {
		if (groupForm.toLowerCase() === 'destination') {
			this.spbForm.patchValue({
				destCity : null,
				destArea: null
			});
		}


		if (groupForm.toLowerCase() === 'sender') {
			this.spbForm.patchValue({
				senderId: null, sender: null, senderArea: null,
				senderNameId: null, senderName: null,
				senderStoreId: null, senderStore: null,
				senderPlaceId: null, senderPlace: null,
				senderAddressId: null, senderAddress: null,
				senderRt: null,
				senderRw: null,
				senderUrbanId: null,
				senderSubDistrictId: null,
				senderCityId: null,
				senderPostalCodeId: null,
			});
		}

		if (groupForm.toLowerCase() === 'receiver') {
			this.spbForm.patchValue({
				receiverId: null, receiver: null, receiverArea: null,
				receiverNameId: null, receiverName: null,
				receiverStoreId: null, receiverStore: null,
				receiverPlaceId: null, receiverPlace: null,
				receiverAddressId: null, receiverAddress: null,
				receiverRt: null,
				receiverRw: null,
				receiverUrban: null,
				receiverSubDistrict: null,
				receiverCity: null,
				receiverPostalCode: null,
				receiverVia: null, receiverViaId: null,
			});
		}

		if (groupForm.toLowerCase() === 'goods') {
			let i = 0;
			const object = this.GetGoodsForm().value;
			Enumerable.from(object).forEach(
				(obj: any) => {
					this.GetGoodsForm().at(i).patchValue({
						koliNo : null,
						koliTotalItem : null,
						goodsWeight : null,
						goodsWeightTotal : null,
						length : null,
						width : null,
						height : null,
						goodsWeightVolume : null,
						goodsFinalWeight : null,
						goodsFinalWeight_display:null,
						price : null,
					});
					i++;
				}
			);
		}

		if (groupForm.toLowerCase() === 'price') {
			this.spbForm.patchValue({
				packing : null,
				quarantine : null,
				etc : null,
				ppn : 1,
				ppnValue : null,
				discount : 1,
				discountValue : null,
				totalPrice : null,
				paymentMethod : null,
				description : null,
			});

		}

		if (groupForm.toLowerCase() === 'moda') {
			this.spbForm.patchValue({
				// tog : null,
				carrier : null,
				// qos : null,
				// tos : null,
				rates : null,
				ratesType : null,
				ratesDetail : null,
			});

		}
		this.cd.detectChanges();
	}




	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.historyLoginEnd();

		// this.senderIsNewSub.unsubscribe();

		this.spbForm = null;
		this.spbFormSubs.next();
		this.spbFormSubs.complete();
		this.senderIsNewSub.unsubscribe();
		this.receiverIsNewSub.unsubscribe();
	}
}
