import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

@Injectable({
	providedIn: 'root',
})
export class SpbCreateService {

	//
	// no SPB
	private spbNoSource = new BehaviorSubject('Loading..');
	currentSpbNo = this.spbNoSource.asObservable();

	constructor() { }

	setSpbNo(message: string) {
		this.spbNoSource.next(message);
	}

	//
	// form group
	private spbFormService = new BehaviorSubject(null);

	spbForm = this.spbFormService.asObservable();


	SetForm() {
		const aa =  new FormGroup({
			// location
			spbNo : new FormControl(),

			destCity : new FormControl(),
			destArea : new FormControl(),

			// customer sender
			senderType: new FormControl('hp'),
			senderId: new FormControl(), sender: new FormControl(), senderArea: new FormControl(),
			senderNameId: new FormControl(), senderName: new FormControl(),
			senderStoreId: new FormControl(), senderStore: new FormControl(),
			senderPlaceId: new FormControl(), senderPlace: new FormControl(),

			senderAddressId: new FormControl(), senderAddress: new FormControl(),
			senderRt: new FormControl(),
			senderRw: new FormControl(),
			senderUrbanId: new FormControl(),
			senderSubDistrictId: new FormControl(),
			senderCityId: new FormControl(),
			senderPostalCodeId: new FormControl(),
			idcustpengirim :  new FormControl(),
			idcustpenerima :  new FormControl(),
			idporter :  new FormControl(),
			namaporter : new FormControl(),
			hpporter : new FormControl(),
			sendertelp : new FormControl(), // Pati 14Jul22

			// customer receiver
			receiverType: new FormControl('hp'),
			receiverId: new FormControl(), receiver: new FormControl(), receiverArea: new FormControl(),
			receiverNameId: new FormControl(), receiverName: new FormControl(),
			receiverStoreId: new FormControl(), receiverStore: new FormControl(),
			receiverPlaceId: new FormControl(), receiverPlace: new FormControl(),
			receiverAddressId: new FormControl(), receiverAddress: new FormControl(), receiverAddressText: new FormControl(),
			receiverRt: new FormControl(),
			receiverRw: new FormControl(),
			receiverUrban: new FormControl(),
			receiverSubDistrict: new FormControl(),
			receiverCity: new FormControl(),
			receiverPostalCode: new FormControl(),
			receiverViaId: new FormControl(), receiverVia: new FormControl(), isVia: new FormControl(),
			receivertelp: new FormControl(), //Pati 14Jul22

			// goods
			goods: new FormArray([]),

			totalAw: new FormControl(),
			totalKoli: new FormControl(),
			totalFinalCaw: new FormControl(),


			// tos & others
			tog : new FormControl(),
			carrier : new FormControl(),
			qos : new FormControl(),
			tos : new FormControl(),
			rates : new FormControl(),
			ratesType : new FormControl(),
			ratesDetail : new FormControl(),

			packing : new FormControl(),
			quarantine : new FormControl(),
			etc : new FormControl(),
			ppn : new FormControl(1),
			ppnValue : new FormControl(),
			discount : new FormControl(1),
			discountValue : new FormControl(),
			totalPrice : new FormControl(),
			paymentMethod : new FormControl(),
			description : new FormControl(),
			min : new FormControl(),
			minRates : new FormControl(),

			// just helper
			// senderListPopUp : new FormControl(),
		});
		this.spbFormService.next(aa);
	}

	AddGoodsRow(totalrow = 1) {
		const currentSpb = this.spbFormService.getValue();
		const tempGoods = currentSpb.get('goods') as FormArray;
		for (let i = 1; i <= totalrow; i++) {
			tempGoods.push(
				new FormGroup({
					koliNo : new FormControl(),
					koliTotalItem : new FormControl(),
					goodsWeight : new FormControl(),
					goodsWeightTotal : new FormControl(),
					length : new FormControl(),
					width : new FormControl(),
					height : new FormControl(),
					goodsWeightVolume : new FormControl(),
					goodsFinalWeight : new FormControl(),
					goodsFinalWeight_display : new FormControl(),
					price : new FormControl(),
				})
			);
		}

		this.spbFormService.next(currentSpb);
	}

	//
	// sender
	// untuk menentukan data apa yang dikirm saat menyimpan SPB
	// private senderIsNewSource = new BehaviorSubject(false);
	// senderIsNew = this.senderIsNewSource.asObservable();
	// SetSenderIsNew(message) { this.senderIsNewSource.next(message); }

	private senderIsNewSubs = new BehaviorSubject({
		sender: false,
		senderName: false,
		senderStore: false,
		senderPlace: false,
		senderAddress: false,
	});
	senderIsNew = this.senderIsNewSubs.asObservable();
	SetSenderIsNew(key, value) {
		const current = this.senderIsNewSubs.getValue();
		current[key] = value;
		this.senderIsNewSubs.next(current);
	}

	//
	// receiver
	// untuk menentukan data apa yang dikirm saat menyimpan SPB
	private receiverIsNewSubs =  new BehaviorSubject({
		receiver: false,
		receiverName: false,
		receiverStore: false,
		receiverPlace: false,
		receiverAddress: false,
		receiverVia: false,
	});
	receiverIsNew = this.receiverIsNewSubs.asObservable();
	SetReceiverIsNew(key, value) {
		const current = this.receiverIsNewSubs.getValue();
		current[key] = value;
		this.receiverIsNewSubs.next(current); }

	//
	// show hide goods
	private goodsVisibleSource = new BehaviorSubject(false);
	goodsVisible = this.goodsVisibleSource.asObservable();
	SetGoodsVisible(message) { this.goodsVisibleSource.next(message); }

	//
	// carrier read onyl
	private carrierROSub = new BehaviorSubject(false);
	carrierRO = this.carrierROSub.asObservable();
	SetCarrierRO(message) { this.carrierROSub.next(message); }

	//
	// receiver
}
