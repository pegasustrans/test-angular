import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbListReceiverComponent } from './spb-list-receiver.component';

describe('SpbListReceiverComponent', () => {
  let component: SpbListReceiverComponent;
  let fixture: ComponentFixture<SpbListReceiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbListReceiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbListReceiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
