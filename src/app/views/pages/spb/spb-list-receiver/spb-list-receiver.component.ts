import {Component, HostListener, OnInit} from '@angular/core';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {SpbPrintComponent} from '../spb-print/spb-print.component';
import {SpbPrintV2Component} from '../spb-print-v2/spb-print-v2.component';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-spb-list-receiver',
  templateUrl: './spb-list-receiver.component.html',
  styleUrls: ['./spb-list-receiver.component.scss']
})
export class SpbListReceiverComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }
	 
	appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;
	spbListDs: CustomStore;

	menuPriv = false;
	menuForb = true;

	printPopupVisible = false;
	printPrivResult: any;
	isiPrint : any;

	// formSearch = {
	// 	date: new Date(), dateString: null
	// };

	filterSpb = {
		dateStart: new Date(),
		minDateStart: new Date(),
		dateString: null,
		spbNo: null
	};

	choosePrint = {
		isiPrint: null
	};

	public innerWidth: any = window.innerWidth;
	public innerHeight: any = window.innerHeight;
	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.innerWidth = window.innerWidth;
		this.innerHeight = window.innerHeight;
	}

	HistoryForm = {
		spbNo		: null,
		typePrint	: null,
		menu: "SPB List Receiver",
		menuAction: "Print SPB",
		printed: true,
		desc: "",historyloginId:0
	};

  constructor(private http: HttpClient, private matDialog: MatDialog) { }

  ngOnInit() {
	this.checkuserGlobalVar();

	//   if (this.formSearch.date !== null) {
	// 	  this.formSearch.dateString = this.formSearch.date.getFullYear() + '-' + (this.formSearch.date.getMonth() + 1) + '-' + this.formSearch.date.getDate();
	//   }
	if (this.filterSpb.dateStart !== null) {
		this.filterSpb.dateString = this.filterSpb.dateStart.getFullYear() + '-' + (this.filterSpb.dateStart.getMonth() + 1) + '-' + this.filterSpb.dateStart.getDate();
	}

	this.setDays();
	this.historyLoginStart();
  }

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	private async setDays(){
		const params = {};
		const result = await this.http.get(AppGlobalVar.BASE_API_URL + 'spblistsender/days', { headers: this.httpOptions.headers, params }).toPromise();
		var dsearch = 0; 
		result['data'].forEach(function(value){
			dsearch = value.days;
		});
		this.filterSpb.minDateStart.setDate(this.filterSpb.minDateStart.getDate() - dsearch);
	  }

	  ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyPrint(data, typePrint){
		this.HistoryForm.spbNo 		= data.spbNo;
		this.HistoryForm.typePrint 	= typePrint;

		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-print/create', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	private async checkuserGlobalVar() {
		if(this.userGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}

		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "spb-list-receiver"){
					checkPriv = 1;
				}
			});

			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;
			
			}
		}
		this.printPrivCheck();
	}

	GetSpbList() {
		// const params = {
		// 	createdAt: this.formSearch.dateString
		// };

		// this.spbListDs = new CustomStore({
		// 	key: 'spbNo',
		// 	load:  (loadOptions: any) => {
		// 		return this.http.get(AppGlobalVar.BASE_API_URL + 'spb-receiver/list', { headers: this.httpOptions.headers, params  })
		// 			.toPromise()
		// 			.then((data: any) => {
		// 				return {
		// 					data: data.data
		// 				};
		// 			})
		// 			.catch(error => { console.log(error); });
		// 	}
		// });

		const params = {
			createdAt: this.filterSpb.dateString
			, type : 'receiver'
		}; 
		this.spbListDs = new CustomStore({
			key: 'spbNo',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring/list-spb', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						console.log(data.data);
						
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	printPrivCheck(){
		let printPrivResult: string[] = [];

		const params = {};
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'distribusi-print/list', params, config)
		.subscribe((data: any) => {

			// console.log(data);
			data.data.forEach(function(value){
				printPrivResult.push(value.printValue);
			});

			this.printPrivResult = printPrivResult;
		},
		(error) => {
			alert(error.statusText + '. ' + error.message);
		}
		);
		
	}

	// 			if (value == "btn-print-spb-label")
	// 			{
	// 				printPrivResult.push("10. Label");
	// 			}
	// 		});
	// 	}
	// 	this.printPrivResult = printPrivResult;
	// 	// console.log(this.printPrivResult);
	// }


	Print = (e) => {
		if (e.itemData.key === 'spb') {
			this.isiPrint = e.itemData.data;
			if (this.printPrivResult != null){
				if (this.printPrivResult.length < 1){
					notify({
						message: 'Anda tidak punya akses untuk print SPB',
						type: 'warning',
						displayTime: 5000,
						width: 800
					});
				}

				else{
					this.printPopupVisible = true;
				}	
			}
		}
	}

	PrintResult(e){
		
		if (this.choosePrint.isiPrint == null || this.choosePrint.isiPrint == ''){
			alert("Silahkan pilih jenis print terlebih dahulu");
		}
		else{
			this.printPopupVisible = false;
			this.PrintSpbV2(this.isiPrint, this.choosePrint.isiPrint);
			// console.log(this.isiPrint, this.choosePrint.isiPrint);
			
		}
	}


	PrintSpbV2(data, teks) {
		// const params = { 
		// 	Type   	: 'spb'
		// 	, DocId	: parseInt(data.spbId) 
		//   };
	  
		//   const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		//   var res = this.http
		// 	.post(AppGlobalVar.BASE_API_URL + 'print-status/check-status', params, config)
        //   	.subscribe((dx: any) => {
		// 		console.log(dx);
				
		// 		if(dx.data[0].result > 0){
		// 			notify({
		// 				message: 'Divisi Anda Sudah Melakukan Print, silahkan Melakukan Pengajuan Re-Print untuk dapat melakukan Print Ulang',
		// 				type: 'warning',
		// 				displayTime: 5000,
		// 				width: 800
		// 			});
		// 		}else{
					this.HistoryForm.desc = teks;
					this.historyPrint(data, 'spb');		
					
					const dialogConfig = new MatDialogConfig();
					// dialogConfig.data = data.spbNo;
					dialogConfig.data = {
						urlReport: this.appGlobalVar.BASE_REPORT_URL + 'spb-receiver/',
						manifestId: data.spbId,
						workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
						workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
						username: this.userGlobalVar.USERNAME,
						teks: teks + '#toolbar=0'  // Pati 15 Agust 21
						//teks: teks
					};
					dialogConfig.minWidth = this.innerWidth + 'px';
					dialogConfig.minHeight = this.innerHeight + 'px';
					let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);

					dialogRef.afterClosed().subscribe(value => {
						console.log(`Dialog sent: ${value}`);
					});
			// 	}
				
			// 	},
			// 	(error) => {
			// 	alert(error.statusText + '. ' + error.message);
			// 	}
			// );
	}



	// onFormSubmitSpbFilter(e) {
	// 	if (this.formSearch.date !== null) {
	// 		this.formSearch.dateString = this.formSearch.date.getFullYear() + '-' + (this.formSearch.date.getMonth() + 1) + '-' + this.formSearch.date.getDate();
	// 	}
	// 	this.GetSpbList();
	// }

	onFormSubmitSpbFilter(e) {
		if (this.filterSpb.dateStart !== null) {
			this.filterSpb.dateString = this.filterSpb.dateStart.getFullYear() + '-' + (this.filterSpb.dateStart.getMonth() + 1) + '-' + this.filterSpb.dateStart.getDate();
		}

		if (this.filterSpb.dateStart === null) {
			this.filterSpb.dateString = null;
		}

		this.GetSpbList();
	}
}
