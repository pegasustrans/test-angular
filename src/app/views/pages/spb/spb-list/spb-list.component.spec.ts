import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbListComponent } from './spb-list.component';

describe('SpbListComponent', () => {
  let component: SpbListComponent;
  let fixture: ComponentFixture<SpbListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
