import {Component, enableProdMode, HostListener, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import CustomStore from 'devextreme/data/custom_store';
import {SpbPrintComponent} from '../spb-print/spb-print.component';
import DataSource from 'devextreme/data/data_source';
import {SpbPrintV2Component} from '../spb-print-v2/spb-print-v2.component';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';
import {FormControl, FormGroup} from '@angular/forms';
import notify from 'devextreme/ui/notify';
import { CONTEXT_MENU, RIGHT_ARROW } from '@angular/cdk/keycodes';
import { event } from 'jquery';
import { eventsHandler } from 'devextreme/events';
import { LabelPrintComponent } from '../label-print/label-print.component';


@Component({
	selector: 'kt-spb-list',
	templateUrl: './spb-list.component.html',
	styleUrls: ['./spb-list.component.scss']
})
export class SpbListComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

	spbListDs: CustomStore;
	paymentMethodDs: DataSource;
	printStatus : any;

	currentSpb: any;
	filterSpb = {
		dateStart: new Date(),
		minDateStart: new Date(),
		dateString: null,
		spbNo: null
	};

	paymentMethodPopupVisible = false;
	voidPopupVisible = false;
	updateDescriptionPopupVisible = false;
	printPopupVisible = false;

	permit = false;
	forbid = true;
	menuPriv = false;
	menuForb = true;

	isiPrint : any;

	choosePrint = {
		isiPrint: null
	};

	public innerWidth: any = window.innerWidth;
	public innerHeight: any = window.innerHeight;
	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.innerWidth = window.innerWidth;
		this.innerHeight = window.innerHeight;
	}

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	spbNoList = '';
	// printPrivResult: [];
	printPrivResult: string[] = [];


	private async checkuserGlobalVar() {
		if(await this.userGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}

		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "spb-list-sender"){
					checkPriv = 1;
				}
			});

			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;
			
			}

			var privVoidResult = this.voidCheck();

			if (privVoidResult===true) {
				this.permit = true;
				this.forbid = false;
			}

			this.printPrivCheck();
		}
	}

	HistoryForm = {
		historyloginId 	: 0,
		spbNo			: null,
		typePrint		: null,
		menu			: "SPB List Sender",
		menuAction		: "Print SPB",
		printed			: true,
		desc			: ""
	};

	

	constructor(private http: HttpClient, private matDialog: MatDialog) { }

	ngOnInit() {
		if (this.filterSpb.dateStart !== null) {
			this.filterSpb.dateString = this.filterSpb.dateStart.getFullYear() + '-' + (this.filterSpb.dateStart.getMonth() + 1) + '-' + this.filterSpb.dateStart.getDate();
		}
		this.checkuserGlobalVar();

		// this.GetSpbList();
		this.getPm();
		
		this.setDays();

		this.historyLoginStart();
		// console.log(this.userGlobalVar.MENU_ACTION_LOGIN_NAME);
	}

	private async setDays(){
		const params = {};
		const result = await this.http.get(AppGlobalVar.BASE_API_URL + 'spblistsender/days', { headers: this.httpOptions.headers, params }).toPromise();
		var dsearch = 0; 
		result['data'].forEach(function(value){
			dsearch = value.days;
		});
		this.filterSpb.minDateStart.setDate(this.filterSpb.minDateStart.getDate() - dsearch);
	  }

	ngOnDestroy() { 
		this.historyLoginEnd();
	}

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyPrint(data, typePrint){
		this.HistoryForm.spbNo 		= data.spbNo;
		this.HistoryForm.typePrint 	= typePrint;
		
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-print/create', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	GetSpbList() {
		
		// const params = {
		// 	createdAt: this.filterSpb.dateString
		// };

		// this.spbListDs = new CustomStore({
		// 	key: 'spbNo',
		// 	load:  (loadOptions: any) => {
		// 		return this.http.get(AppGlobalVar.BASE_API_URL + 'spblistsender/list', { headers: this.httpOptions.headers, params  })
		// 			.toPromise()
		// 			.then((data: any) => {
		// 				return {
		// 					data: data.data
		// 				};
		// 			})
		// 			.catch(error => { console.log(error); });
		// 	}
		// });

		const params = {
			createdAt: this.filterSpb.dateString
			, type : 'sender'
		}; 
		this.spbListDs = new CustomStore({
			key: 'spbNo',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'monitoring/list-spb', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						console.log(data.data);
						
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	getPrintDistribution(){
		const params = 'spb-print'

		return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey='+params)
			.toPromise()
			.then((result: any) => {
				// console.log(result.data);
				return result.data;
			});
	}

	private getPm() {
		const params = 'pm'
		this.paymentMethodDs = new DataSource({
			store: new CustomStore({
				key: 'lookupKey',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey='+params)
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
	}


	Print = (e) => {
		if (e.itemData.key === 'spb') {
			this.isiPrint = e.itemData.data;
			
			if (this.printPrivResult != null){
				if (this.printPrivResult.length < 1){
					notify({
						message: 'Anda tidak punya akses untuk print SPB',
						type: 'warning',
						displayTime: 5000,
						width: 800
					});
				}

				else{
					this.printPopupVisible = true;
				}	
			}
		}

		if (e.itemData.key === 'label') {
			this.LabelPrint(e.itemData.data, 'label');
		}
	}

	PrintResult(e){
		
		if (this.choosePrint.isiPrint == null || this.choosePrint.isiPrint == ''){
			alert("Silahkan pilih jenis print terlebih dahulu");
		}
		else{
			this.printPopupVisible = false;
			this.PrintSpbV2(this.isiPrint, this.choosePrint.isiPrint);
		}
	}

	Update = (e) => {
		if (e.itemData.key === 'pm') {
			this.paymentMethodChange(e.itemData.data);
		}

		if (e.itemData.key === 'void') {
			this.void(e.itemData.data);
		}

		if (e.itemData.key === 'updateDescription') {
			this.updateDescription(e.itemData.data);
		}

		// if (e.itemData.key === 'editPengirim') {
		// 	this.editPengirim(e.itemData.data);
		// }

		// if (e.itemData.key === 'editPenerima') {
		// 	this.editPenerima(e.itemData.data);
		// }
	}



	PrintSpbV2(data, teks) {
		// const params = { 
		// 	Type   	: 'spb'
		// 	, DocId	: parseInt(data.spbId) 
		//   };
	  
		//   const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		//   var res = this.http
		// 	.post(AppGlobalVar.BASE_API_URL + 'print-status/check-status', params, config)
        //   	.subscribe((dx: any) => {
		// 		console.log(dx);
				
				// if(dx.data[0].result > 0){
				// 	notify({
				// 		message: 'Divisi Anda Sudah Melakukan Print, silahkan Melakukan Pengajuan Re-Print untuk dapat melakukan Print Ulang',
				// 		type: 'warning',
				// 		displayTime: 5000,
				// 		width: 800
				// 	});
				// }else{
					this.HistoryForm.desc = teks;
					this.historyPrint(data, 'spb');					
					
					const dialogConfig = new MatDialogConfig();
					// dialogConfig.data = data.spbNo;
					
					dialogConfig.data = {
						urlReport: this.appGlobalVar.BASE_REPORT_URL + 'spb-sender/',
						manifestId: data.spbId,
						workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
						workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
						username: this.userGlobalVar.USERNAME,
						teks: teks + '#toolbar=0'  // Pati 15 Agust 21
						//teks: teks
					};
					// console.log(dialogConfig.data);
					dialogConfig.minWidth = this.innerWidth + 'px';
					dialogConfig.minHeight = this.innerHeight + 'px';
					
					
					let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);
					
					dialogRef.afterClosed().subscribe(value => {
						console.log(`Dialog sent: ${value}`);
					});
				// }
				
			// 	},
			// 	(error) => {
			// 	alert(error.statusText + '. ' + error.message);
			// 	}
			// );
			
	}



	onFormSubmitSpbFilter(e) {
		if (this.filterSpb.dateStart !== null) {
			this.filterSpb.dateString = this.filterSpb.dateStart.getFullYear() + '-' + (this.filterSpb.dateStart.getMonth() + 1) + '-' + this.filterSpb.dateStart.getDate();
		}

		if (this.filterSpb.dateStart === null) {
			this.filterSpb.dateString = null;
		}

		this.GetSpbList();
	}

	void(e) {
		this.currentSpb = e.row.data;

		if (this.permit === true){
			this.voidPopupVisible = true;
		}

		if (this.permit === false){
			notify({
				message: 'Anda tidak dapat melakukan void',
				type: 'warning',
				displayTime: 5000,
				width: 400
			});
		}
	}

	voidCheck(){
		var count= 0;
 
		if  (UserGlobalVar.MENU_ACTION_LOGIN_NAME!=null){
			UserGlobalVar.MENU_ACTION_LOGIN_NAME.forEach(function(value){
				// console.log(value);
				if (value == "btn-void")
				{
					count++;
				}
			});
		}

		if (count>0){return true;}

		else {return false;}
	}

	// printPrivCheck(){

	// 	let printPrivResult: string[] = [];
	// 	// let tesData: number[] = [];
	// 	// console.log(UserGlobalVar.MENU_ACTION_LOGIN_NAME);

	// 	if  (UserGlobalVar.MENU_ACTION_LOGIN_NAME!=null){
	// 		UserGlobalVar.MENU_ACTION_LOGIN_NAME.forEach(function(value){
	// 			// if (
	// 			// value == "btn-print-spb-copy-sender" ||	
	// 			// value == "btn-print-spb-copy-receiver" ||	
	// 			// value == "btn-print-spb-subbranch" ||
	// 			// value == "btn-print-spb-others" ||
	// 			// value == "btn-print-spb-ops" ||
	// 			// value == "btn-print-spb-branch" ||
	// 			// value == "btn-print-spb-finance" ||
	// 			// value == "btn-print-spb-transhipment" ||
	// 			// value == "btn-print-spb-marking"
	// 			// )
	// 			// {
	// 			// 	tesData.push(value);
	// 			// }

	// 			if (value == "btn-print-spb-copy-sender")
	// 			{
	// 				printPrivResult.push("1. Salinan Pengirim");
	// 			}

	// 			if (value == "btn-print-spb-copy-receiver")
	// 			{
	// 				printPrivResult.push("2. Salinan Penerima");
	// 			}
				
	// 			if (value == "btn-print-spb-subbranch")
	// 			{
	// 				printPrivResult.push("3. Gerai");
	// 			}

	// 			if (value == "btn-print-spb-ops")
	// 			{
	// 				printPrivResult.push("4. Ops");
	// 			}

	// 			if (value == "btn-print-spb-branch")
	// 			{
	// 				printPrivResult.push("5. Cabang Perwakilan");
	// 			}

	// 			if (value == "btn-print-spb-finance")
	// 			{
	// 				printPrivResult.push("6. Finance");
	// 			}

	// 			if (value == "btn-print-spb-transhipment")
	// 			{
	// 				printPrivResult.push("7. Transhipment");
	// 			}

	// 			if (value == "btn-print-spb-marking")
	// 			{
	// 				printPrivResult.push("8. Marking");
	// 			}

	// 			if (value == "btn-print-spb-others")
	// 			{
	// 				printPrivResult.push("10. Lain-lain");
	// 			}
	// 		});
	// 	}
	// 	this.printPrivResult = printPrivResult;
	// }

	printPrivCheck(){
		let printPrivResult: string[] = [];

		const params = {};
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
		this.http
		.post(AppGlobalVar.BASE_API_URL + 'distribusi-print/list', params, config)
		.subscribe((data: any) => {

			// console.log(data);
			data.data.forEach(function(value){
				printPrivResult.push(value.printValue);
			});

			this.printPrivResult = printPrivResult;
		},
		(error) => {
			alert(error.statusText + '. ' + error.message);
		}
		);
		
	}

	updateDescription(e){
		var editPriv = 0;
		UserGlobalVar.MENU_ACTION_LOGIN_NAME.forEach(function(value){
			if (value == "btn-cara-bayar"){
				editPriv = 1;
			}
		});
		this.currentSpb = e.row.data;

		if (editPriv == 1){
			this.updateDescriptionPopupVisible = true;
		}

		else if (editPriv == 0){
			notify({
				message: 'Anda tidak dapat melakukan edit deskripsi',
				type: 'warning',
				displayTime: 5000,
				width: 400
			});
		}	
	}

	voidSubmit(e) {

		// const formData: any = new FormData();

		// formData.append('SpbId', this.currentSpb.spbId);
		// formData.append('voidReason', this.currentSpb.voidReason);

		const body = { 
			SpbId: 			this.currentSpb.spbId
			, voidReason: 	this.currentSpb.voidReason
		 };

		this.http
			.post(AppGlobalVar.BASE_API_URL + 'spblistsender/void',  body, this.httpOptions)
			.subscribe((data: any) => {
					if (data != null) {
						notify({
							message: 'SPB ' + this.currentSpb.spbNo + ' VOID berhasil.  ',
							type: 'success',
							displayTime: 5000,
							width: 400
						});
					}
					if (data === null){
						notify({
							message: 'Void Gagal',
							type: 'warning',
							displayTime: 5000,
							width: 800
						});
					}
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				}
			);

		this.voidPopupVisible = false;
	}

	updateDescriptionSubmit(e){
		const body = { 
			SpbId: 			this.currentSpb.spbId
			,Description: 	this.currentSpb.Description
		 };

		this.http
			.post(AppGlobalVar.BASE_API_URL + 'spblistsender/update-description',  body, this.httpOptions)
			.subscribe((data: any) => {
					if (data != null) {
						notify({
							message: 'SPB ' + this.currentSpb.spbNo + ' Edit Keterangan Berhasil  ',
							type: 'success',
							displayTime: 5000,
							width: 400
						});
					}
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				}
			);

		this.updateDescriptionPopupVisible = false;
	}


	paymentMethodChange(e) {
		this.currentSpb = e.row.data;

		var pmPriv = 0;
		UserGlobalVar.MENU_ACTION_LOGIN_NAME.forEach(function(value){
			if (value == "btn-cara-bayar"){
				pmPriv = 1;
			}
		});

		
		if (this.currentSpb.paymentMethod!= null){
			if (pmPriv == 1){
			this.currentSpb.paymentMethod = this.currentSpb.paymentMethod.toLowerCase();
			this.paymentMethodPopupVisible = true;
			}

			else if (pmPriv == 0){
				notify({
					message: 'Anda tidak dapat melakukan perubahan cara bayar',
					type: 'warning',
					displayTime: 5000,
					width: 400
				});
			}
		}
	}

	paymentMethodSubmit(e) {

		// const formData: any = new FormData();

		// formData.append('SpbNo', this.currentSpb.spbNo);
		// formData.append('paymentMethod', this.currentSpb.paymentMethod);

		const body = { 
			SpbNo: 			this.currentSpb.spbNo
			, paymentMethod: 	this.currentSpb.paymentMethod
		 };

		this.http
			.post(AppGlobalVar.BASE_API_URL + 'spblistsender/paymentmethod-update', body, this.httpOptions)
			.subscribe((data: any) => {
					if (data != null) {
						notify({
							message: 'SPB ' + data.data.spbNo + ' berhasil diubah menjadi '  + data.data.paymentMethod,
							type: 'success',
							displayTime: 5000,
							width: 400,
							// position: {
							// 	my: 'center',
							// 	at: 'top',
							// 	of: '#targetElement'
							// }
						});
					}
				},
				(error) => {
					alert(error.statusText + '. ' + error.message);
				}
			);

		this.paymentMethodPopupVisible = false;
	}


	onRowPrepared(e) {
		if (e.rowType === 'data' && e.data.isVoid === true) {
			e.rowElement.style.backgroundColor = 'orange';
			e.rowElement.className = e.rowElement.className.replace('dx-row-alt', '');
		}
	}

	LabelPrint(data, teks) {	  
		  const dialogConfig = new MatDialogConfig();
		  dialogConfig.data = {
			  urlReport: this.appGlobalVar.BASE_REPORT_URL + 'label-print/',
			  spbId: data.spbId,
			  workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
			  workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
			  username: this.userGlobalVar.USERNAME,
			  teks: teks + '#toolbar=0'  // Pati 15 Agust 21
		  };
		  // console.log(dialogConfig.data);
		  dialogConfig.minWidth = this.innerWidth + 'px';
		  dialogConfig.minHeight = this.innerHeight + 'px';
		  
		  console.log(dialogConfig);
		  

		  let dialogRef = this.matDialog.open(LabelPrintComponent, dialogConfig);
		  
		  dialogRef.afterClosed().subscribe(value => {
			  console.log(`Dialog sent: ${value}`);
		  });
			
	}


}
