import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbPrintV2Component } from './spb-print-v2.component';

describe('SpbPrintV2Component', () => {
  let component: SpbPrintV2Component;
  let fixture: ComponentFixture<SpbPrintV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbPrintV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbPrintV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
