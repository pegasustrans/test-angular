import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbPrintComponent } from './spb-print.component';

describe('SpbPrintComponent', () => {
  let component: SpbPrintComponent;
  let fixture: ComponentFixture<SpbPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
