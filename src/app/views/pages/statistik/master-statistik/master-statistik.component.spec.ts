import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterStatistikComponent } from './master-statistik.component';

describe('MasterStatistikComponent', () => {
  let component: MasterStatistikComponent;
  let fixture: ComponentFixture<MasterStatistikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterStatistikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterStatistikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
