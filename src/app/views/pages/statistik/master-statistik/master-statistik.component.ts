import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { UserGlobalVar } from '../../../../core/globalvar/userGlobalVar';
import { AppGlobalVar } from '../../../../core/globalvar/appGlobalVar';

@Component({
  selector		: 'kt-master-statistik',
  templateUrl	: './master-statistik.component.html',
  styleUrls		: ['./master-statistik.component.scss']
})
export class MasterStatistikComponent implements OnInit {
  	ds: any 	= {};
	cityDs      : any;
	// dataSource: any;

  	menuPriv 	= false;
	menuForb 	= true;

	formSearch = {
		cityId 	  : null,
		dateStart : new Date(),
		dateTo    : new Date()
	};

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	HistoryForm = {
		menu			: "Statistik Omset",
		historyloginId 	: 0
	};

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
	// this.dataSource =  [{
	// 	day: 'Monday',
	// 	oranges: 3,
	//   }, {
	// 	day: 'Tuesday',
	// 	oranges: 2,
	//   }, {
	// 	day: 'Wednesday',
	// 	oranges: 3,
	//   }, {
	// 	day: 'Thursday',
	// 	oranges: 4,
	//   }, {
	// 	day: 'Friday',
	// 	oranges: 6,
	//   }, {
	// 	day: 'Saturday',
	// 	oranges: 11,
	//   }, {
	// 	day: 'Sunday',
	// 	oranges: 4,
	//   }];
  }

  ngOnInit() {
	this.historyLoginStart();
	this.checkuserGlobalVar();
	this.getCity();
  }

  onSearch(){
	this.GetList();
  }

  getCity(){
	const body = {};

		this.http
				.post(AppGlobalVar.BASE_API_URL + 'master-moda-city/get-city',  body, this.httpOptions)
				.subscribe((data: any) => {
						this.cityDs = data.data;
						return data.data;
					},
					(error) => {
						alert(error.statusText + '. ' + error.message);
					}
				);
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

  private async checkuserGlobalVar() {
    if(UserGlobalVar.USER_ID === '') {
      setTimeout(() => {
        this.checkuserGlobalVar();
      }, 1000);
    }
  
    else{		
      var checkPriv = 0;
      await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
        if (value == "statistik-omset"){
          checkPriv = 1;
        }
      });
  
      if (checkPriv == 1){
        this.menuPriv = true;
        this.menuForb = false;
        
      	this.GetList();
      }
    }
  }

	GetList() {
		var fromYear 	= this.formSearch.dateStart.getFullYear();
		var fromMonth 	= this.formSearch.dateStart.getMonth()+1;

		var toYear 		= this.formSearch.dateTo.getFullYear();
		var toMonth 	= this.formSearch.dateTo.getMonth()+1;

		var lastDate = new Date(toYear,toMonth, 0,23,59,59,999);
		const firstDay = 1;
		const lastDay = lastDate.getDate(); 

		var dateStart 	= fromYear+"-"+fromMonth+"-"+firstDay;
		var dateTo 		= toYear+"-"+toMonth+"-"+lastDay;

		const params = {
			cityId		: this.formSearch.cityId,
			dateStart	: dateStart,
			dateTo		: dateTo,
		}; 
		this.ds = new CustomStore({
			key: 'month_year',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'master-statistik/list', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
              
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}


	thousandSeparator(data) {
		var roundUp = Math.round((data.value + Number.EPSILON) * 100) / 100;
		//roundUp = Math.round(roundUp);
		return roundUp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	}

}