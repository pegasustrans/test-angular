import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbStatisticInputComponent } from './spb-statistic-input.component';

describe('SpbStatisticInputComponent', () => {
  let component: SpbStatisticInputComponent;
  let fixture: ComponentFixture<SpbStatisticInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpbStatisticInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbStatisticInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
