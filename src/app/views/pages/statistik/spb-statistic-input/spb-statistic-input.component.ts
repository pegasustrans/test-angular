import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-spb-statistic-input',
  templateUrl: './spb-statistic-input.component.html',
  styleUrls: ['./spb-statistic-input.component.scss']
})
export class SpbStatisticInputComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	readDs: CustomStore;
	readCustomDs: CustomStore;
	userGlobalVar = UserGlobalVar;


	menuPriv = false;
	menuForb = true;

	formSearch = {
		date: new Date(), dateString: null
	};

	formSearchCustom = {
		dateFrom: new Date(), dateFromString: null,
		dateTo: new Date(), dateToString: null,
	};

	HistoryForm = {
		menu: "Statistik Input",historyloginId:0
	};

	constructor(private http: HttpClient) { }

	ngOnInit() {
		this.checkuserGlobalVar(); 
		if (this.formSearch.date !== null) {
			this.formSearch.dateString = this.formSearch.date.getFullYear() + '-' + (this.formSearch.date.getMonth() + 1) + '-' + this.formSearch.date.getDate();
		}

		if (this.formSearchCustom.dateFrom !== null) { this.formSearchCustom.dateFromString = this.formSearchCustom.dateFrom.getFullYear() + '-' + (this.formSearchCustom.dateFrom.getMonth() + 1) + '-' + this.formSearchCustom.dateFrom.getDate(); }
		if (this.formSearchCustom.dateTo !== null) { this.formSearchCustom.dateToString = this.formSearchCustom.dateTo.getFullYear() + '-' + (this.formSearchCustom.dateTo.getMonth() + 1) + '-' + this.formSearchCustom.dateTo.getDate(); }

		this.read();
		this.historyLoginStart();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	checkuserGlobalVar() {
		if(this.userGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}

		else{		
			var checkPriv = 0;
			UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "spb-statistik"){
					checkPriv = 1;
				}
			});

			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;
			
			}
		}
	}

	read() {

		this.readDs = new CustomStore({
			key: 'username',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'spbstatistic/read-input-time?dateFrom=' + this.formSearch.dateString + '&dateTo=' + this.formSearch.dateString)
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}



	onFormSearchSubmit(e) {
		if (this.formSearch.date !== null) {
			this.formSearch.dateString = this.formSearch.date.getFullYear() + '-' + (this.formSearch.date.getMonth() + 1) + '-' + this.formSearch.date.getDate();
		}

		if (this.formSearch.date === null) {
			this.formSearch.dateString = null;
		}

		this.read();
	}

	// custom
	// -----------------

	readCustom() {

		this.readCustomDs = new CustomStore({
			key: 'username',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'spbstatistic/read-input-time?dateFrom=' + this.formSearchCustom.dateFromString + '&dateTo=' + this.formSearchCustom.dateToString)
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	onFormSearchCustomSubmit(e) {
		if (this.formSearchCustom.dateFrom !== null) { this.formSearchCustom.dateFromString = this.formSearchCustom.dateFrom.getFullYear() + '-' + (this.formSearchCustom.dateFrom.getMonth() + 1) + '-' + this.formSearchCustom.dateFrom.getDate(); }
		if (this.formSearchCustom.dateTo !== null) { this.formSearchCustom.dateToString = this.formSearchCustom.dateTo.getFullYear() + '-' + (this.formSearchCustom.dateTo.getMonth() + 1) + '-' + this.formSearchCustom.dateTo.getDate(); }

		if (this.formSearchCustom.dateFrom === null) { this.formSearchCustom.dateFromString = null; }
		if (this.formSearchCustom.dateTo === null) { this.formSearchCustom.dateToString = null; }

		this.readCustom();
	}

}
