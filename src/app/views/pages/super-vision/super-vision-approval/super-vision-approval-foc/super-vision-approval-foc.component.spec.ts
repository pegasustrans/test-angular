import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionApprovalFocComponent } from './super-vision-approval-foc.component';

describe('SuperVisionApprovalFocComponent', () => {
  let component: SuperVisionApprovalFocComponent;
  let fixture: ComponentFixture<SuperVisionApprovalFocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionApprovalFocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionApprovalFocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
