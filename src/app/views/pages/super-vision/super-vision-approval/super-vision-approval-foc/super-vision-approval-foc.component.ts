import { Component, OnInit, Renderer2, ElementRef, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import { UserGlobalVar } from '../../../../../../app/core/globalvar/userGlobalVar';
import notify from 'devextreme/ui/notify';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { SpbPrintV2Component } from '../../../spb/spb-print-v2/spb-print-v2.component';


@Component({
  selector: 'kt-super-vision-approval-foc',
  templateUrl: './super-vision-approval-foc.component.html',
  styleUrls: ['./super-vision-approval-foc.component.scss']
})
export class SuperVisionApprovalFocComponent implements OnInit {

  appGlobalVar = AppGlobalVar;
	userGlobalVar = UserGlobalVar;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router, private matDialog: MatDialog) { }

  formCreate = {
    spbNo         : null,
    reason        : null,
    reasonReject  : null
  }

  superVisionId:any;
  spbId:any;
  
  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  public innerWidth: any = window.innerWidth;
	public innerHeight: any = window.innerHeight;
	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.innerWidth = window.innerWidth;
		this.innerHeight = window.innerHeight;
	}
  
  
  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {

        this.superVisionId              = params.p1;
        this.spbId                      = params.p2;
				this.formCreate.spbNo           = params.p3;
				this.formCreate.reason          = params.p4;
                
      }
    );
  }

  onApprove(){
    const params = {
      superVisionId   : parseInt(this.superVisionId),
      spbId           : parseInt(this.spbId),
      spbNo           : this.formCreate.spbNo,
      reason          : this.formCreate.reason,
      reasonReject    : this.formCreate.reasonReject
    };

    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'super-vision/approve-foc', params, config)
			  .subscribe((data: any) => {

            if(data.data[0].result == 'true'){
              notify({
                message: data.data[0].message,
                type: 'success',
                displayTime: 5000,
                width: 400,
              });

            }else{
              notify({
                message: data.data[0].message,
                type: 'error',
                displayTime: 5000,
                width: 400,
              });

            }

            setTimeout(() => {
              this.router.navigate(['/super-vision-list']);
            }, 1000);
            
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
  }

  onReject(){
    if(this.formCreate.reasonReject == null){
			notify({
				message: 'Alasan Tolak harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
      const params = {
        superVisionId   : parseInt(this.superVisionId),
        spbId           : parseInt(this.spbId),
        spbNo           : this.formCreate.spbNo,
        reason          : this.formCreate.reason,
        reasonReject    : this.formCreate.reasonReject
      };

      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'super-vision/reject-foc', params, config)
			  .subscribe((data: any) => {

          console.log(data);
          
            if(data.status == 200){
              notify({
                message: data.data,
                type: 'success',
                displayTime: 5000,
                width: 400,
              });
            }else{
              notify({
                message: "Data Gagal di Reject",
                type: 'error',
                displayTime: 5000,
                width: 400,
              });             
            }

            setTimeout(() => {
              this.router.navigate(['/super-vision-list']);
            }, 1000);
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
  }

  onBack(){
    this.router.navigate(['/super-vision-list']);
  }

  onPrint(){
    this.PrintSpbV2(this.spbId);
   }
 
   PrintSpbV2(spbId) {
     const dialogConfig = new MatDialogConfig();
     dialogConfig.data = {
       urlReport: this.appGlobalVar.BASE_REPORT_URL + 'spb-sender/',
       manifestId: spbId,
       workTimeZoneHour: this.userGlobalVar.WORK_TIME_ZONE_HOUR,
       workTimeZoneMinute: this.userGlobalVar.WORK_TIME_ZONE_MINUTE,
       username: this.userGlobalVar.USERNAME,
       teks: '9. Lain-lain' + '#toolbar=0'  
       
     };
     dialogConfig.minWidth = this.innerWidth + 'px';
     dialogConfig.minHeight = this.innerHeight + 'px';
     
     
     let dialogRef = this.matDialog.open(SpbPrintV2Component, dialogConfig);
     
     dialogRef.afterClosed().subscribe(value => {
       console.log(`Dialog sent: ${value}`);
     });
   }

}
