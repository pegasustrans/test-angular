import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionApprovalKiloComponent } from './super-vision-approval-kilo.component';

describe('SuperVisionApprovalKiloComponent', () => {
  let component: SuperVisionApprovalKiloComponent;
  let fixture: ComponentFixture<SuperVisionApprovalKiloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionApprovalKiloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionApprovalKiloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
