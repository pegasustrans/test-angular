import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionApprovalReprintComponent } from './super-vision-approval-reprint.component';

describe('SuperVisionApprovalReprintComponent', () => {
  let component: SuperVisionApprovalReprintComponent;
  let fixture: ComponentFixture<SuperVisionApprovalReprintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionApprovalReprintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionApprovalReprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
