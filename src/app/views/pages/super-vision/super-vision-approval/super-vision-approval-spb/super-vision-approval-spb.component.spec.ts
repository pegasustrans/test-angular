import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionApprovalSpbComponent } from './super-vision-approval-spb.component';

describe('SuperVisionApprovalSpbComponent', () => {
  let component: SuperVisionApprovalSpbComponent;
  let fixture: ComponentFixture<SuperVisionApprovalSpbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionApprovalSpbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionApprovalSpbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
