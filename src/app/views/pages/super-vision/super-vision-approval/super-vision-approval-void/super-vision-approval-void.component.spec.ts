import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionApprovalVoidComponent } from './super-vision-approval-void.component';

describe('SuperVisionApprovalVoidComponent', () => {
  let component: SuperVisionApprovalVoidComponent;
  let fixture: ComponentFixture<SuperVisionApprovalVoidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionApprovalVoidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionApprovalVoidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
