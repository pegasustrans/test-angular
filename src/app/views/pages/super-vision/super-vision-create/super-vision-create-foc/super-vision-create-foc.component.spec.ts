import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionCreateFocComponent } from './super-vision-create-foc.component';

describe('SuperVisionCreateFocComponent', () => {
  let component: SuperVisionCreateFocComponent;
  let fixture: ComponentFixture<SuperVisionCreateFocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionCreateFocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionCreateFocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
