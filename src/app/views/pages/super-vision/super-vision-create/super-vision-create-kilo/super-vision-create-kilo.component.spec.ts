import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionCreateKiloComponent } from './super-vision-create-kilo.component';

describe('SuperVisionCreateKiloComponent', () => {
  let component: SuperVisionCreateKiloComponent;
  let fixture: ComponentFixture<SuperVisionCreateKiloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionCreateKiloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionCreateKiloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
