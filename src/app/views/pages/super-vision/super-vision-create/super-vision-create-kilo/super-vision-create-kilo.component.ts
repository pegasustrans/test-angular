import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kt-super-vision-create-kilo',
  templateUrl: './super-vision-create-kilo.component.html',
  styleUrls: ['./super-vision-create-kilo.component.scss']
})
export class SuperVisionCreateKiloComponent implements OnInit {

  constructor() { }
  
  formCreate = {
    spbNo   : null,
    reason  : null
  }

  ngOnInit() {
  }

  onSave(){
    console.log('save kilo');
  }

}
