import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionCreateReprintComponent } from './super-vision-create-reprint.component';

describe('SuperVisionCreateReprintComponent', () => {
  let component: SuperVisionCreateReprintComponent;
  let fixture: ComponentFixture<SuperVisionCreateReprintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionCreateReprintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionCreateReprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
