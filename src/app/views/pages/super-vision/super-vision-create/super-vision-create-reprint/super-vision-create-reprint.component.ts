import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import {UserGlobalVar} from '../../../../../core/globalvar/userGlobalVar';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-super-vision-create-reprint',
  templateUrl: './super-vision-create-reprint.component.html',
  styleUrls: ['./super-vision-create-reprint.component.scss']
})
export class SuperVisionCreateReprintComponent implements OnInit {
  typePrintDs: any;

  constructor(private http: HttpClient) { }

  formCreate = {
    docNo     : null,
    typePrint : null,
    reason    : null
  }

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  ngOnInit() {
    this.typePrint();
  }

  typePrint(){
    const params = { };
    this.typePrintDs = new CustomStore({
			key: 'code',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'super-vision/type-print', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
  }

  onSave(){
    if(this.formCreate.docNo == null){
			notify({
				message: 'Dokumen No harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.typePrint == null){
			notify({
				message: 'Jenis Print harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.reason == null){
			notify({
				message: 'Alasan harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'super-vision/create-reprint', JSON.stringify(this.formCreate), config)
			  .subscribe((data: any) => {
          
            if(data.status == 203){
              notify({
                message: data.data[0].message,
                type: 'error',
                displayTime: 5000,
                width: 400,
              });
            }else{
              notify({
                message: data.data,
                type: 'success',
                displayTime: 5000,
                width: 400,
              });

              setTimeout(() => {
                window.location.reload();
              }, 1000);
              
            }
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }

}
