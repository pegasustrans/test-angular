import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionCreateSpbComponent } from './super-vision-create-spb.component';

describe('SuperVisionCreateSpbComponent', () => {
  let component: SuperVisionCreateSpbComponent;
  let fixture: ComponentFixture<SuperVisionCreateSpbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionCreateSpbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionCreateSpbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
