import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import {UserGlobalVar} from '../../../../../core/globalvar/userGlobalVar';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-super-vision-create-spb',
  templateUrl: './super-vision-create-spb.component.html',
  styleUrls: ['./super-vision-create-spb.component.scss']
})
export class SuperVisionCreateSpbComponent implements OnInit {

  constructor(private http: HttpClient) { }

  pmDs: any;

  formCreate = {
    spbNo           : null,
    paymentMethod   : null,
    reason          : null
  }

  ngOnInit() {
    this.onPaymentMethod();
  }

  onPaymentMethod(){
    this.pmDs = new DataSource({
			store: new CustomStore({
				key: 'lookupKey',
				loadMode: 'raw',
				load: (loadOptions) => {
					return this.http.get(AppGlobalVar.BASE_API_URL + 'lookup/get?parentKey=pm')
						.toPromise()
						.then((result: any) => {
							return result.data;
						});
				}
			})
		});
  }

  onSave(){
    if(this.formCreate.spbNo == null){
			notify({
				message: 'Spb No harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.reason == null){
			notify({
				message: 'Alasan harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
    }else if(this.formCreate.paymentMethod == null){
			notify({
				message: 'Metode Pembayaran harus di pilih terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'super-vision/create-payment', JSON.stringify(this.formCreate), config)
			  .subscribe((data: any) => {
          
            if(data.status == 203){
              notify({
                message: data.data[0].message,
                type: 'error',
                displayTime: 5000,
                width: 400,
              });
            }else{
              notify({
                message: data.data,
                type: 'success',
                displayTime: 5000,
                width: 400,
              });

              setTimeout(() => {
                window.location.reload();
              }, 1000);
              
            }
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }

}
