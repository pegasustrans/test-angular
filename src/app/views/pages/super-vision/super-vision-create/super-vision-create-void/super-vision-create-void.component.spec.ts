import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionCreateVoidComponent } from './super-vision-create-void.component';

describe('SuperVisionCreateVoidComponent', () => {
  let component: SuperVisionCreateVoidComponent;
  let fixture: ComponentFixture<SuperVisionCreateVoidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionCreateVoidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionCreateVoidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
