import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../../core/globalvar/appGlobalVar';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'kt-super-vision-create-void',
  templateUrl: './super-vision-create-void.component.html',
  styleUrls: ['./super-vision-create-void.component.scss']
})
export class SuperVisionCreateVoidComponent implements OnInit {

  constructor(private http: HttpClient) { }

  formCreate = {
    spbNo   : null,
    reason  : null
  }

  ngOnInit() {
  }

  onSave(){
    if(this.formCreate.spbNo == null){
			notify({
				message: 'Spb No harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else if(this.formCreate.reason == null){
			notify({
				message: 'Alasan harus di isi terlebih dahulu',
				type: 'error',
				displayTime: 5000,
				width: 400,
			});
		}else {
      const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
			this.http
			  .post(AppGlobalVar.BASE_API_URL + 'super-vision/create-void', JSON.stringify(this.formCreate), config)
			  .subscribe((data: any) => {
          
            if(data.status == 203){
              notify({
                message: data.data[0].message,
                type: 'error',
                displayTime: 5000,
                width: 400,
              });
            }else{
              notify({
                message: data.data,
                type: 'success',
                displayTime: 5000,
                width: 400,
              });

              setTimeout(() => {
                window.location.reload();
              }, 1000);
              
            }
					
			    },
			    (error) => {
			      alert(error.statusText + '. ' + error.message);
			    }
			);
    }
    
  }

}
