import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionCreateComponent } from './super-vision-create.component';

describe('SuperVisionCreateComponent', () => {
  let component: SuperVisionCreateComponent;
  let fixture: ComponentFixture<SuperVisionCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
