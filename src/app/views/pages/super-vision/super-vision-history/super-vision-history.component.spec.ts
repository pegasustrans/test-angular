import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionHistoryComponent } from './super-vision-history.component';

describe('SuperVisionHistoryComponent', () => {
  let component: SuperVisionHistoryComponent;
  let fixture: ComponentFixture<SuperVisionHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
