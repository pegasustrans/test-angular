import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-super-vision-history',
  templateUrl: './super-vision-history.component.html',
  styleUrls: ['./super-vision-history.component.scss']
})
export class SuperVisionHistoryComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
	  this.historyLoginEnd();
   }

	constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

	HistoryForm = {
		menu			: "Supervisi History",
		historyloginId 	: 0
	};

  ds:any;

  appGlobalVar = AppGlobalVar;
  userGlobalVar = UserGlobalVar;

  menuPriv = false;
  menuForb = true;


  ngOnInit() {
    this.checkuserGlobalVar();
	this.GetList();
	this.historyLoginStart();
  }

  ngOnDestroy() { 
	this.historyLoginEnd();
 }

historyLoginStart(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	  .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
	  .subscribe((data: any) => {
			this.HistoryForm.historyloginId = data.data;
		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	);
}

historyLoginEnd(){
	const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
	this.http
	  .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
	  .subscribe((data: any) => {

		},
		(error) => {
		  alert(error.statusText + '. ' + error.message);
		}
	);
}

  private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "supervision-history"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;			
			}
		}
	}

  httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

  GetList() {
		
		const params = { };

		this.ds = new CustomStore({
			key: 'spbNo',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'super-vision/list-history', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

}
