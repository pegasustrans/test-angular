import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionListComponent } from './super-vision-list.component';

describe('SuperVisionListComponent', () => {
  let component: SuperVisionListComponent;
  let fixture: ComponentFixture<SuperVisionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
