import { Component, HostListener, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobalVar} from '../../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../../core/globalvar/userGlobalVar';

@Component({
  selector: 'kt-super-vision-list',
  templateUrl: './super-vision-list.component.html',
  styleUrls: ['./super-vision-list.component.scss']
})
export class SuperVisionListComponent implements OnInit {
	@HostListener('window:beforeunload', ["$event"]) unload(event) {
        this.historyLoginEnd();
     }

	constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

   	ds:any;

   	menuPriv = false;
   	menuForb = true;

  	appGlobalVar = AppGlobalVar;
   	userGlobalVar = UserGlobalVar;

   	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	HistoryForm = {
		menu			: "Supervisi List",
		historyloginId 	: 0
	};


	ngOnInit() {
		this.checkuserGlobalVar();
		this.GetList();
		this.historyLoginStart();
	}

	ngOnDestroy() { 
		this.historyLoginEnd();
	 }

	historyLoginStart(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/start-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {
				this.HistoryForm.historyloginId = data.data;
            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}

	historyLoginEnd(){
		const config = { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };
        this.http
          .post(AppGlobalVar.BASE_API_URL + 'history-login/end-time', JSON.stringify(this.HistoryForm), config)
          .subscribe((data: any) => {

            },
            (error) => {
              alert(error.statusText + '. ' + error.message);
            }
		);
	}
	
	private async checkuserGlobalVar() {
		if(UserGlobalVar.USER_ID === '') {
			setTimeout(() => {
				this.checkuserGlobalVar();
			}, 1000);
		}
	
		else{		
			var checkPriv = 0;
			await UserGlobalVar.MENU_KEY_LOGIN_NAME.forEach(function(value){
				if (value == "supervision-approve"){
					checkPriv = 1;
				}
			});
	
			if (checkPriv == 1){
				this.menuPriv = true;
				this.menuForb = false;			
			}
		}
	}

	GetList() {
		
		const params = { };

		this.ds = new CustomStore({
			key: 'spbNo',
			load:  (loadOptions: any) => {
				return this.http.get(AppGlobalVar.BASE_API_URL + 'super-vision/list-approve', { headers: this.httpOptions.headers, params  })
					.toPromise()
					.then((data: any) => {
						return {
							data: data.data
						};
					})
					.catch(error => { console.log(error); });
			}
		});
	}

	detailApproval = (e) => {
		console.log(e);
		
		if(e.row.data.typeCode == 'void'){
			this.router.navigate(['/super-vision-approval-void'], 
			{ 
				queryParams: { 
					p1: e.row.data.superVisionId,
					p2: e.row.data.spbId,
					p3: e.row.data.spbNo,
					p4: e.row.data.reason
				} 
			});
		}else if(e.row.data.typeCode == 'updatePayment'){
			this.router.navigate(['/super-vision-approval-spb'], 
			{ 
				queryParams: { 
					p1: e.row.data.superVisionId,
					p2: e.row.data.spbId,
					p3: e.row.data.spbNo,
					p4: e.row.data.reason,
					p5: e.row.data.param1
				} 
			});
		}else if(e.row.data.typeCode == 'foc'){
			this.router.navigate(['/super-vision-approval-foc'], 
			{ 
				queryParams: { 
					p1: e.row.data.superVisionId,
					p2: e.row.data.spbId,
					p3: e.row.data.spbNo,
					p4: e.row.data.reason
				} 
			});
		}else if(e.row.data.typeCode == 'reprint'){
			this.router.navigate(['/super-vision-approval-reprint'], 
			{ 
				queryParams: { 
					p1: e.row.data.superVisionId,
					p2: e.row.data.spbId,
					p3: e.row.data.spbNo,
					p4: e.row.data.reason,
					p5: e.row.data.param2,
					p6: e.row.data.param3
				} 
			});
		}
	}

}
