import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperVisionComponent } from './super-vision.component';

describe('SuperVisionComponent', () => {
  let component: SuperVisionComponent;
  let fixture: ComponentFixture<SuperVisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperVisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperVisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
