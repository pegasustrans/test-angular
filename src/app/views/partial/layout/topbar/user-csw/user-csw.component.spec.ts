import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCswComponent } from './user-csw.component';

describe('UserCswComponent', () => {
  let component: UserCswComponent;
  let fixture: ComponentFixture<UserCswComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCswComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCswComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
