import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCsw2Component } from './user-csw2.component';

describe('UserCsw2Component', () => {
  let component: UserCsw2Component;
  let fixture: ComponentFixture<UserCsw2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCsw2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCsw2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
