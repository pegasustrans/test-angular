// Angular
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// RxJS
import { Observable, Subscription } from 'rxjs';
// Object-Path
import * as objectPath from 'object-path';
// Layout
import { LayoutConfigService, MenuConfigService, PageConfigService } from '../../../core/_base/layout';
import { HtmlClassService } from '../html-class.service';
import { LayoutConfig } from '../../../core/_config/layout.config';
import { MenuConfig } from '../../../core/_config/menu.config';
import { PageConfig } from '../../../core/_config/page.config';
// User permissions
import { NgxPermissionsService } from 'ngx-permissions';
import {currentUserPermissions, Login, Permission} from '../../../core/auth';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../core/reducers';
import {environment} from '../../../../environments/environment';
import {AppGlobalVar} from '../../../core/globalvar/appGlobalVar';
import {UserGlobalVar} from '../../../core/globalvar/userGlobalVar';
import {HttpClient} from '@angular/common/http';

@Component({
	selector: 'kt-base',
	templateUrl: './base.component.html',
	styleUrls: ['./base.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class BaseComponent implements OnInit, OnDestroy {
	// Public variables
	selfLayout: string;
	asideDisplay: boolean;
	asideSecondary: boolean;
	subheaderDisplay: boolean;
	fluid: boolean;

	// Private properties
	private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
	private currentUserPermissions$: Observable<Permission[]>;


	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 * @param menuConfigService: MenuConfifService
	 * @param pageConfigService: PageConfigService
	 * @param htmlClassService: HtmlClassService
	 * @param store
	 * @param permissionsService
	 */
	constructor(
		private layoutConfigService: LayoutConfigService,
		private menuConfigService: MenuConfigService,
		private pageConfigService: PageConfigService,
		private htmlClassService: HtmlClassService,
		private store: Store<AppState>,
		private router: Router,
		private http: HttpClient,
		private permissionsService: NgxPermissionsService) {
		this.loadRolesWithPermissions();

		// register configs by demos
		this.layoutConfigService.loadConfigs(new LayoutConfig().configs);
		this.menuConfigService.loadConfigs(new MenuConfig().configs);
		this.pageConfigService.loadConfigs(new PageConfig().configs);

		// setup element classes
		this.htmlClassService.setConfig(this.layoutConfigService.getConfig());

		const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(layoutConfig => {
			// reset body class based on global and page level layout config, refer to html-class.service.ts
			document.body.className = '';
			this.htmlClassService.setConfig(layoutConfig);
		});
		this.unsubscribe.push(subscr);

		// csw
		this.RefreshToken();

	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {

		const config = this.layoutConfigService.getConfig();
		this.selfLayout = objectPath.get(config, 'self.layout');
		this.asideDisplay = objectPath.get(config, 'aside.self.display');
		this.asideSecondary = objectPath.get(config, 'aside-secondary.self.display');
		this.subheaderDisplay = objectPath.get(config, 'subheader.display');
		this.fluid = objectPath.get(config, 'content.width') === 'fluid';

		// let the layout type change
		const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(cfg => {
			setTimeout(() => {
				this.selfLayout = objectPath.get(cfg, 'self.layout');
			});
		});
		this.unsubscribe.push(subscr);
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.unsubscribe.forEach(sb => sb.unsubscribe());
	}

	/**
	 * NGX Permissions, init roles
	 */
	loadRolesWithPermissions() {
		this.currentUserPermissions$ = this.store.pipe(select(currentUserPermissions));
		const subscr = this.currentUserPermissions$.subscribe(res => {
			if (!res || res.length === 0) {
				return;
			}

			this.permissionsService.flushPermissions();
			res.forEach((pm: Permission) => this.permissionsService.addPermission(pm.name));
		});
		this.unsubscribe.push(subscr);
	}

	// csw
	RefreshToken() {
		// get refresh token
		const refreshToken = localStorage.getItem('refreshToken');

		// return if refreshToken is null
		if (refreshToken === null) { console.log('RefreshToken kosong'); return; }

		// get new token
		// @ts-ignore
		this.http
			.post(AppGlobalVar.BASE_API_URL + 'auth/refresh-token/' + refreshToken)
			.subscribe((user: any) => {
					if (user) {

						UserGlobalVar.USER_ID = user.userProfile.userId;
						UserGlobalVar.USERNAME = user.userProfile.username;
						UserGlobalVar.EMAIL = user.userProfile.email;
						UserGlobalVar.COMPANY_ID = user.userProfile.companyId;
						UserGlobalVar.COMPANY_NAME = user.userProfile.companyName;
						UserGlobalVar.COMPANY_ADDRESS = user.userProfile.companyAddress;
						UserGlobalVar.WORK_TYPE = user.userProfile.workType;
						UserGlobalVar.WORK_ID = user.userProfile.workId;
						UserGlobalVar.WORK_NAME = user.userProfile.workName;
						UserGlobalVar.WORK_ADDRESS = user.userProfile.workAddress;
						UserGlobalVar.WORK_TIMEZONE = user.userProfile.workTimeZone;
						UserGlobalVar.WORK_CITY_ID = user.userProfile.workCityId;
						UserGlobalVar.WORK_AREA_ID = user.userProfile.workAreaId;
						UserGlobalVar.WORK_CITY_NAME = user.userProfile.workCityName;
						UserGlobalVar.WORK_AREA_NAME = user.userProfile.workAreaName;

						UserGlobalVar.MENU_ACTION_ID_LOGIN = user.userProfile.menuActionIdLogin;
						
						if(user.userProfile.menuKeyLogin != null){
							UserGlobalVar.MENU_KEY_LOGIN = user.userProfile.menuKeyLogin;
							UserGlobalVar.MENU_KEY_LOGIN_NAME = [];

							var temp = [];
							UserGlobalVar.MENU_KEY_LOGIN.forEach(function(value){
								temp.push(value.split("~"));
							});

							temp.forEach(function(value){
								UserGlobalVar.MENU_KEY_LOGIN_NAME.push(value[1]);
							});
						}

						if(user.userProfile.menuActionLogin != null){
							
							UserGlobalVar.MENU_ACTION_LOGIN = user.userProfile.menuActionLogin;
							UserGlobalVar.MENU_ACTION_LOGIN_NAME = [];

							var temp2 = [];
							UserGlobalVar.MENU_ACTION_LOGIN.forEach(function(value){
								temp2.push(value.split("~"));
							})
							temp2.forEach(function(value){
								UserGlobalVar.MENU_ACTION_LOGIN_NAME.push(value[1]);
							});
						}

						UserGlobalVar.WORK_TIME_ZONE_HOUR = user.userProfile.workTimeZoneHour;
						UserGlobalVar.WORK_TIME_ZONE_MINUTE = user.userProfile.workTimeZoneMinute;


						AppGlobalVar.ACCESS_TOKEN = user.accessToken;
						localStorage.setItem('refreshToken', user.refreshToken);
						localStorage.setItem('accessToken', user.accessToken);

						this.store.dispatch(new Login({authToken: user.accessToken}));
						// this.router.navigateByUrl('/'); // Main page

					} else {
						localStorage.removeItem('accessToken');
						localStorage.removeItem('refreshToken');
						localStorage.removeItem('authce9d77b308c149d5992a80073637e4d5');
						this.router.navigateByUrl('/auth/login'); // Main page
					}
				}, (e) => {   }
				, () => {	  }
			);


		// if refresh token is invalid then remove refresh token from local strage
	}
}
